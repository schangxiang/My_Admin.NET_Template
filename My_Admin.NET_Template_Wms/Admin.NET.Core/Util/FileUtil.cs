﻿using Furion;
using RestSharp;

namespace Admin.NET.Core
{
    public static class FileUtil
    {
        /// <summary>
        /// 往文件里追加内容
        /// </summary>
        /// <param name="folder">文件</param>
        /// <param name="folder">目录</param>
        /// <param name="fileName">文件</param>
        /// <param name="args">内容参数</param>
        public static void WriteLine(string folder, string fileName,  string content)
        {
              
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
                  
            File.AppendAllText(Path.Combine(folder, fileName), content + "\r\n");
        }

        /// <summary>
        /// 下载文件
        /// </summary>
        /// <param name="path"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static Stream Download(string path, string fileName)
        {
            var options = new RestClientOptions(App.Configuration["FileUrl"])
            {
                ThrowOnAnyError = true,
                MaxTimeout = 2000
            };
            var client = new RestClient(options);
            var request = new RestRequest($"/file/download", Method.Get);
            request?.AddParameter(Parameter.CreateParameter("path", path, ParameterType.QueryString))
                ?.AddParameter(Parameter.CreateParameter("fileName", fileName, ParameterType.QueryString));
            var response = client.DownloadData(request ?? null!);
            MemoryStream ms = new(response ?? null!);
            return ms;
        }





    }
}