﻿using Admin.NET.Core;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Admin.NET.Application
{
    public interface ITestStudentService
    {
        Task<TestStudentOutput> Get([FromQuery] QueryeTestStudentInput input);
        Task<List<TestStudentOutput>> List([FromQuery] TestStudentInput input);
        Task<PageResult<TestStudentOutput>> Page([FromQuery] TestStudentSearch input);
        Task<List<TestStudentOutput>> ListNonPageAsync([FromQuery] TestStudentSearchNonPage input);
        
        Task Add(AddTestStudentInput input);
        Task Update(UpdateTestStudentInput input);
        Task Delete(DeleteTestStudentInput input);
        Task<int> ImportExcelAsync(IFormFile file);
        IActionResult DownloadExcelTemplate(string version);
    }
}