﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    /// <summary>
    /// 库口表
    /// </summary>
    [Table("wms_warehouse_entrance")]
    [Comment("库口表")]
    public class WmsWarehouseEntrance : DEntityBase
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Comment("名称")]
        [Required, MaxLength(100)]
        public string Name { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        [Comment("编码")]
        [Required, MaxLength(50)]
        public string Code { get; set; }

        /// <summary>
        /// 类型-1.入库_2.出库
        /// </summary>
        [Comment("类型-1.入库口_2.出库口")]
        [Required]
        public WarehouseEntranceEnum Type { get; set; }

        /// <summary>
        /// 所属车间
        /// </summary>
        [Comment("所属车间")]
        public LesWorkShopType LesWorkShopType { get; set; }

        /// <summary>
        /// 所属巷道
        /// </summary>
        [Comment("所属巷道")]
        public int AffiliatedRoadway { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Comment("备注")]
        [MaxLength(100)]
        public string Remark { get; set; }

        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        [Comment("状态")]
        public CommonStatus Status { get; set; } = CommonStatus.ENABLE;
    }
}
