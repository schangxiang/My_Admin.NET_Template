import request from '@/api/api.js'
import qs from 'qs'


// 根据单据号获取明细 /api/pdareceiptorder/GetOrderDetail
export function GetOrderDetail (data) {
  return request({
    url: `/api/pdareceiptorder/GetOrderDetail?${qs.stringify(data)}`,
    method: 'get'
  })
}


// 入库 /api/pdareceiptorder/ConfirmReceipt
export function confirmReceipt (data) {
  return request({
    url: `/api/pdareceiptorder/ConfirmReceipt`,
    method: 'post',
	data
  })
}


