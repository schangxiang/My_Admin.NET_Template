﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 报检状态
    /// </summary>
    public enum InspectionStatus
    {
        /// <summary>
        /// 未报检
        /// </summary>
        WEIBAOJIAN = 1,
        /// <summary>
        /// 已报检
        /// </summary>
        YIBAOJIAN = 2
    }
}
