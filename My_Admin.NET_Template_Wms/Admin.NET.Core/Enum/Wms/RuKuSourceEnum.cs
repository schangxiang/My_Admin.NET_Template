﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Core
{
    /// <summary>
    /// 入库来源
    /// </summary>
    public enum RuKuSourceEnum
    {
        /// <summary>
        /// 空托
        /// </summary>
        [Description("空托")]
        KONGTUO =1,
        /// <summary>
        /// 物料
        /// </summary>
        [Description("物料")]
        WULIAO =2
    }
}
