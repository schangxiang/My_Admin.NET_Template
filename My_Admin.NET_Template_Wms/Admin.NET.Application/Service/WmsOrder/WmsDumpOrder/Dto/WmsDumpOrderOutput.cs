﻿using Admin.NET.Core.Util.LowCode.Dto;
using System;
using Admin.NET.Core;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace Admin.NET.Application
{
    /// <summary>
    /// 转储单输出参数
    /// </summary>
    public class WmsDumpOrderOutput
    {
        /// <summary>
        /// 单据大类
        /// </summary>
        public long OrderLargeCategory { get; set; }
        
        /// <summary>
        /// 单据小类
        /// </summary>
        public long OrderSubclass { get; set; }
        
        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        public long SOID { get; set; }
        
        /// <summary>
        /// 单据编号
        /// </summary>
        public string NO { get; set; }
        
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTimeOffset Billdate { get; set; }
        
        /// <summary>
        /// 创建人
        /// </summary>
        public string Creator { get; set; }
        
        /// <summary>
        /// 公司
        /// </summary>
        public string Companyname { get; set; }
        
        /// <summary>
        /// 单据状态
        /// </summary>
        public Admin.NET.Core.OrderStatusEnum OrderStatus { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public Admin.NET.Core.IssueState IssueState { get; set; }

        /// <summary>
        /// 对应YiGo系统
        /// </summary>
        public List<DumpPageDetailOutput> Dtls { get; set; }
    }
    /// <summary>
    /// 转储单详情输出参数
    /// </summary>
    public class DumpPageDetailOutput
    {
        /// <summary>
        /// Id  
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 上位系统单据明细唯一识别码
        /// </summary>
        public long OID { get; set; }

        /// <summary>
        /// 单据Id  
        /// </summary>
        public long OrderId { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string Materialname { get; set; }
        /// <summary>
        /// 物料编号
        /// </summary>
        public string Materialcode { get; set; }
        /// <summary>
        /// 基本单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Qty { get; set; }

        /// <summary>
        /// 长度 
        /// </summary>
        public decimal Length { get; set; }

        /// <summary>
        /// SCM批次号    
        /// </summary>
        public string Batchno_SCM { get; set; }

        /// <summary>
        /// WMS批次号    
        /// </summary>
        public string Batchno_WMS { get; set; }

        /// <summary>
        /// 库存项目编码    
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 新库存项目编码    
        /// </summary>
        public string Projectcode_To { get; set; }

        /// <summary>
        /// 单据状态
        /// </summary>
        public OrderDetailsStatusEnum OrderStatus { get; set; }
    }

    /// <summary>
    /// yigo转储单获取参数
    /// </summary>
    public class WmsDumpOrderOutputByYiGO
    {
        public bool Status { get; set; }

        public List<WmsDumpOrderOutput> Data { get; set; }

        public string Result { get; set; }
    }
}
