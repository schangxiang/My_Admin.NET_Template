﻿using Admin.NET.Core.Util.LowCode.Dto;
using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 单据表输出参数
    /// </summary>
    public class WmsOrderOutput
    {
        /// <summary>
        /// 单据大类
        /// </summary>
        public long OrderLargeCategory { get; set; }
        
        /// <summary>
        /// 单据小类
        /// </summary>
        public long OrderSubclass { get; set; }
        
        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        public long SOID { get; set; }
        
        /// <summary>
        /// 单据编号
        /// </summary>
        public string NO { get; set; }
        
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTimeOffset Billdate { get; set; }
        
        /// <summary>
        /// 送货通知单号
        /// </summary>
        public string SourceBillNo { get; set; }
        
        /// <summary>
        /// 供应商编码
        /// </summary>
        public string Vendorcode { get; set; }
        
        /// <summary>
        /// 供应商名称
        /// </summary>
        public string Vendorname { get; set; }
        
        /// <summary>
        /// 公司名称
        /// </summary>
        public string Companyname { get; set; }
        
        /// <summary>
        /// 仓管员
        /// </summary>
        public string WarehouseKeepername { get; set; }
        
        /// <summary>
        /// 贸易方式
        /// </summary>
        public Admin.NET.Core.TradeMode TradeMode { get; set; }
        
        /// <summary>
        /// 仓储中心
        /// </summary>
        public string WarehouseCentername { get; set; }
        
        /// <summary>
        /// 仓间
        /// </summary>
        public string StoreRoomname { get; set; }
        
        /// <summary>
        /// 报关单号
        /// </summary>
        public string DeclarationCode { get; set; }
        
        /// <summary>
        /// 专业
        /// </summary>
        public string Majorname { get; set; }
        
        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks { get; set; }
        
        /// <summary>
        /// 单据状态
        /// </summary>
        public Admin.NET.Core.OrderStatusEnum OrderStatus { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public Admin.NET.Core.IssueState IssueState { get; set; }
        
    }

    /// <summary>
    /// 单据表详情输出参数
    /// </summary>
    public class WmsOrderDetailOutput
    {
        /// <summary>
        /// Id  
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 上位系统单据明细唯一识别码
        /// </summary>
        public long OID { get; set; }

        /// <summary>
        /// 单据Id  
        /// </summary>
        public long OrderId { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string Materialname { get; set; }
        /// <summary>
        /// 物料编号
        /// </summary>
        public string Materialcode { get; set; }
        /// <summary>
        /// 基本单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Qty { get; set; }

        /// <summary>
        /// 单根长度 
        /// </summary>
        public decimal SingleLength { get; set; }

        /// <summary>
        /// 理论重量    
        /// </summary>
        public decimal TheoreticalWeight { get; set; }

        /// <summary>
        /// 不含税单价    
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// 成本金额    
        /// </summary>
        public decimal SumPrice { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 合同编号
        /// </summary>
        public string ContractCode { get; set; }

        /// <summary>
        /// TA号
        /// </summary>
        public string TACode { get; set; }

        /// <summary>
        /// 报关品名
        /// </summary>
        public string TradeName { get; set; }

        /// <summary>
        /// 报关项号
        /// </summary>
        public string ItemNo { get; set; }

        /// <summary>
        /// 报关单位
        /// </summary>
        public string CustomsUnitName { get; set; }

        /// <summary>
        /// 报关数量   
        /// </summary>
        public decimal CustomsNum { get; set; }

        /// <summary>
        /// 报关金额 
        /// </summary>
        public decimal CustomsPrices { get; set; }

        /// <summary>
        /// 特殊要求
        /// </summary>
        public string SpecialNeeds { get; set; }

        /// <summary>
        /// 单据状态
        /// </summary>
        public OrderDetailsStatusEnum OrderStatus { get; set; }

        /// <summary>
        /// 检验结果
        /// </summary>
        public InspectionResultsEnum InspectionResults { get; set; }
    }
}
