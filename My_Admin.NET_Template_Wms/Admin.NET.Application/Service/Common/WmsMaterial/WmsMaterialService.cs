﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 物料信息服务
    /// </summary>
    [ApiDescriptionSettings("自己的业务", Name = "WmsMaterial", Order = 100)]
    [Route("api/[Controller]")]
    public class WmsMaterialService : IDynamicApiController, ITransient
    {
        private readonly IRepository<WmsMaterial, MasterDbContextLocator> _wmsMaterialRep;
        private readonly IRepository<WmsArea, MasterDbContextLocator> _wmsAreaRep;
        private readonly IRepository<LesStation, MasterDbContextLocator> _lesStationRep;

        /// <summary>
        /// 构造函数
        /// </summary>
        public WmsMaterialService(
            IRepository<WmsMaterial, MasterDbContextLocator> wmsMaterialRep,
            IRepository<WmsArea, MasterDbContextLocator> wmsAreaRep,
            IRepository<LesStation, MasterDbContextLocator> lesStationRep
        )
        {
            _wmsMaterialRep = wmsMaterialRep;
            _wmsAreaRep = wmsAreaRep;
            _lesStationRep = lesStationRep;
        }

        /// <summary>
        /// 分页查询物料信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("page")]
        public async Task<PageResult<WmsMaterialOutput>> Page([FromQuery] WmsMaterialSearch input)
        {
            var wmsMaterials = await _wmsMaterialRep.DetachedEntities
                .Where(!string.IsNullOrEmpty(input.MaterialName), u => EF.Functions.Like(u.MaterialName, $"%{input.MaterialName.Trim()}%"))
                .Where(input.MaterialType != null, u => u.MaterialType == input.MaterialType)
                .Where(!string.IsNullOrEmpty(input.MaterialSpec), u => EF.Functions.Like(u.MaterialSpec, $"%{input.MaterialSpec.Trim()}%"))
                .Where(!string.IsNullOrEmpty(input.MaterialDensity), u => EF.Functions.Like(u.MaterialDensity, $"%{input.MaterialDensity.Trim()}%"))
                //.Where(input.IsMaturation != null, u => u.IsMaturation == input.IsMaturation)
                .OrderBy(PageInputOrder.OrderBuilder<WmsMaterialSearch>(input))
                .ProjectToType<WmsMaterialOutput>()
                .ToADPagedListAsync(input.PageNo, input.PageSize);
            return wmsMaterials;
        }

        /// <summary>
        /// 增加物料信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("add")]
        public async Task Add(AddWmsMaterialInput input)
        {
            var wmsMaterial = input.Adapt<WmsMaterial>();
            await _wmsMaterialRep.InsertAsync(wmsMaterial);
        }

        /// <summary>
        /// 删除物料信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("delete")]
        public async Task Delete(DeleteWmsMaterialInput input)
        {
            var wmsMaterial = await _wmsMaterialRep.FirstOrDefaultAsync(u => u.Id == input.Id);
            await _wmsMaterialRep.DeleteAsync(wmsMaterial);
        }

        /// <summary>
        /// 更新物料信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("edit")]
        public async Task Update(UpdateWmsMaterialInput input)
        {
            var isExist = await _wmsMaterialRep.AnyAsync(u => u.Id == input.Id, false);
            if (!isExist) throw Oops.Oh(ErrorCode.D3000);

            var wmsMaterial = input.Adapt<WmsMaterial>();
            await _wmsMaterialRep.UpdateAsync(wmsMaterial, ignoreNullValues: true);
        }

        /// <summary>
        /// 获取物料信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("detail")]
        public async Task<WmsMaterialOutput> Get([FromQuery] QueryeWmsMaterialInput input)
        {
            return (await _wmsMaterialRep.DetachedEntities.FirstOrDefaultAsync(u => u.Id == input.Id)).Adapt<WmsMaterialOutput>();
        }

        /// <summary>
        /// 获取物料信息列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("list")]
        public async Task<List<WmsMaterialOutput>> List([FromQuery] WmsMaterialInput input)
        {
            return await _wmsMaterialRep.DetachedEntities.ProjectToType<WmsMaterialOutput>().ToListAsync();
        }

        /// <summary>
        /// 库区下拉框
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetAreaList")]
        public async Task<List<AreaListOutput>> GetAreaList()
        {
            var data = await _wmsAreaRep.DetachedEntities.ProjectToType<WmsAreaOutput>().ToListAsync();
            List<AreaListOutput> areaListOutputLists = new List<AreaListOutput>();
            foreach (var item in data)
            {
                AreaListOutput areaListOutputList = new AreaListOutput();
                areaListOutputList.AreaId = item.Id.ToString();
                areaListOutputList.AreaName = item.AreaName;
                areaListOutputLists.Add(areaListOutputList);
            }
            return areaListOutputLists;
        }

        /// <summary>
        /// 工作区（工位）下拉框
        /// </summary>
        /// <returns></returns>
        //[HttpGet("GetStationList")]
        //public async Task<List<StationListOutput>> GetStationList()
        //{
        //    var data = await _lesStationRep.DetachedEntities.ProjectToType<LesStationOutput>().ToListAsync();
        //    List<StationListOutput> stationListOutputLists = new List<StationListOutput>();
        //    foreach (var item in data)
        //    {
        //        StationListOutput stationListOutputList = new StationListOutput();
        //        stationListOutputList.StationId = item.Id.ToString();
        //        stationListOutputList.StationName = item.Name;
        //        stationListOutputLists.Add(stationListOutputList);
        //    }
        //    return stationListOutputLists;
        //}
    }
}
