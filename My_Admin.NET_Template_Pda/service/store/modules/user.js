import { getObjectType } from '@/static/js/utils/index.js'
export default {
	namespaced: true,
	state: () => ({
		token:'',
		account:'',
		visited:[],
		userInfo:{},
		refreshToken:''
	}),
	mutations: {
		setToken(state,val){
			uni.setStorageSync('user_token',val);
			state.token = val;
		},
		setRefreshToken(state,val){
			uni.setStorageSync('user_refresh_token',val);
			state.refreshToken = val;
		},
		setAccount(state,val){
			uni.setStorageSync('user_account',val);
			state.account = val;
		},
		setUserInfo(state,obj){
			uni.setStorageSync('user_info',JSON.stringify(obj));
			state.userInfo = obj;
		},
		addVisited(state,obj){
			if (obj.account){
				let oldArr = state.visited;
				if (oldArr.length===0) {
					let _local = uni.getStorageSync('user_visited');
					if (_local) {
						try{
							oldArr=JSON.parse(_local)
						}catch(e){
							console.log(e)
						}
					}
				}
				let errFlag = false;
				if (getObjectType(oldArr)!=='array') {
					oldArr = []
					errFlag = true;
				}
				let inFlag = false;
				for (let i=0;i<oldArr.length;i++) {
					if (oldArr[i].account===obj.account) {
						inFlag = true;
						break;
					}
				}
				if (!inFlag) {
					oldArr.push(obj)
					state.visited = oldArr;
					uni.setStorageSync('user_visited',JSON.stringify(oldArr));
				} else if (errFlag) {
					state.visited = oldArr;
					uni.setStorageSync('user_visited',JSON.stringify(oldArr));
				}
			}
		},
		clear(state){
			state.token = '';
			state.refreshToken = '';
			state.userInfo = {};
			state.account = '';
			uni.removeStorageSync('user_token');
			uni.removeStorageSync('user_info');
			uni.removeStorageSync('user_account');
			uni.removeStorageSync('user_refresh_token');
		}
	},
	actions: {
		
	},
	getters: {
		getToken(state){
			let res = state.token;
			if (!res) {
				let temp = uni.getStorageSync('user_token');
				if (temp) {
					res = temp;
					state.token = res;
				}
			}
			return res;
		},
		getRefreshToken(state){
			let res = state.refreshToken;
			if (!res) {
				let temp = uni.getStorageSync('user_refresh_token');
				if (temp) {
					res = temp;
					state.refreshToken = res;
				}
			}
			return res;
		},
		getUserInfo(state){
			let res = state.userInfo;
			if (!res || !res.id) {
				let _str = uni.getStorageSync('user_info');
				if (_str) {
					try{
						res = JSON.parse(_str)
						state.userInfo = res
					}catch(e){
						console.log('storage user_info err parse to json')
					}
				}
			}
			return res
		},
		getAccount(state){
			let res = state.account;
			if (!res) {
				let temp = uni.getStorageSync('user_account');
				if (temp) {
					res = temp;
					state.account = res;
				}
			}
			return res;
		},
		getVisited(state){
			let res = state.visited;
			if (!res.length) {
				let _local = uni.getStorageSync('user_visited');
				if (_local) {
					try{
						let temp = JSON.parse(_local)
						if (getObjectType(temp)==='array') {
							res = temp;
							state.visited = res;
						}
					}catch(e){
						console.log(e)
					}
				}
			}
			return res;
		}
	}
}