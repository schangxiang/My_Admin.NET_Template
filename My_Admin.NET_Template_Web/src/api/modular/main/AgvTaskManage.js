import { axios } from '@/utils/request'

/**
 * 查询泡沫车间Agv任务表
 *
 * @author yrj
 */
export function AgvTaskPage (parameter) {
  return axios({
    url: '/assembleagvtask/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 强制完成
 *
 * @author yrj 
 */
export function AgvTaskForceComplete (id) {
  return axios({
    url: '/assembleagvtask/finish',
    method: 'post',
    data: {id}
  })
}




