﻿using Microsoft.AspNetCore.Components.Web.Virtualization;
using System.ComponentModel.DataAnnotations;
using Admin.NET.Core;
using Admin.NET.Core.Service;

namespace Admin.NET.Application
{
    /// <summary>
    /// 库存表查询参数
    /// </summary>
    public class WmsMaterialStockSearch : PageInputBase
    {
        /// <summary>
        /// 物料编号
        /// </summary>
        public virtual string MaterialNo { get; set; }

        /// <summary>
        /// 物料类型
        /// </summary>
        public virtual MaterialType? MaterialType { get; set; }

        /// <summary>
        /// 物料批次
        /// </summary>
        public virtual string MaterialBatch { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public virtual string MaterialName { get; set; }

        /// <summary>
        /// 物料规格
        /// </summary>
        public virtual string MaterialSpec { get; set; }

        /// <summary>
        /// 物料密度
        /// </summary>
        public virtual string MaterialDensity { get; set; }

        /// <summary>
        /// 检验方式
        /// </summary>
        public virtual MaterialInspection InspectionMethod { get; set; }

        /// <summary>
        /// 单位类别
        /// </summary>
        public virtual UnitType UnitType { get; set; }

        /// <summary>
        /// 单位编号
        /// </summary>
        public virtual UnitNoType UnitNo { get; set; }

        /// <summary>
        /// 库存数
        /// </summary>
        public virtual decimal? StockNumber { get; set; }

        /// <summary>
        /// 库位编码
        /// </summary>
        public virtual string PlaceCode { get; set; }

        /// <summary>
        /// 托盘编码
        /// </summary>
        public virtual string ContainerCode { get; set; }

        /// <summary>
        /// 库区Id
        /// </summary>
        public virtual long? AreaId { get; set; }

        /// <summary>
        /// 图号
        /// </summary>
        public virtual string DrawingNo { get; set; }

        /// <summary>
        /// 船号
        /// </summary>
        public virtual string ShipNo { get; set; }

    }
    /// <summary>
    /// 库存信息（熟化库）查询参数
    /// </summary>
    public class MaterialStockSearch : PageInputBase
    {
        /// <summary>
        /// 物料编号
        /// </summary>
        public virtual string MaterialNo { get; set; }
        
        /// <summary>
        /// 物料类型
        /// </summary>
        public virtual MaterialType? MaterialType { get; set; }
        
        /// <summary>
        /// 物料批次
        /// </summary>
        public virtual string MaterialBatch { get; set; }
        
        /// <summary>
        /// 物料名称
        /// </summary>
        public virtual string MaterialName { get; set; }
        
        /// <summary>
        /// 物料规格
        /// </summary>
        public virtual string MaterialSpec { get; set; }

        /// <summary>
        /// 长    
        /// </summary>
        public virtual int? Long { get; set; }

        /// <summary>
        /// 宽    
        /// </summary>
        public virtual int? Wide { get; set; }

        /// <summary>
        /// 高    
        /// </summary>
        public virtual int? High { get; set; }

        /// <summary>
        /// 物料密度
        /// </summary>
        public virtual string MaterialDensity { get; set; }
        
        /// <summary>
        /// 检验方式
        /// </summary>
        public virtual MaterialInspection InspectionMethod { get; set; }
        
        /// <summary>
        /// 单位类别
        /// </summary>
        public virtual UnitType UnitType { get; set; }
        
        /// <summary>
        /// 单位编号
        /// </summary>
        public virtual UnitNoType UnitNo { get; set; }
        
        /// <summary>
        /// 库存数
        /// </summary>
        public virtual decimal? StockNumber { get; set; }
        
        /// <summary>
        /// 库位编码
        /// </summary>
        public virtual string PlaceCode { get; set; }
        
        /// <summary>
        /// 托盘编码
        /// </summary>
        public virtual string ContainerCode { get; set; }
        
        /// <summary>
        /// 库区Id
        /// </summary>
        public virtual long? AreaId { get; set; }
    }

    /// <summary>
    /// 库存信息（熟化库）输入参数
    /// </summary>
    public class MaterialStockInput
    {
        /// <summary>
        /// 物料编号
        /// </summary>
        public virtual string MaterialNo { get; set; }
        
        /// <summary>
        /// 物料类型
        /// </summary>
        public virtual MaterialType MaterialType { get; set; }
        
        /// <summary>
        /// 物料批次
        /// </summary>
        public virtual string MaterialBatch { get; set; }
        
        /// <summary>
        /// 物料名称
        /// </summary>
        public virtual string MaterialName { get; set; }
        
        /// <summary>
        /// 物料规格
        /// </summary>
        public virtual string MaterialSpec { get; set; }

        /// <summary>
        /// 长    
        /// </summary>
        public virtual int? Long { get; set; }

        /// <summary>
        /// 宽    
        /// </summary>
        public virtual int? Wide { get; set; }

        /// <summary>
        /// 高    
        /// </summary>
        public virtual int? High { get; set; }

        /// <summary>
        /// 物料密度
        /// </summary>
        public virtual string MaterialDensity { get; set; }
        
        /// <summary>
        /// 检验方式
        /// </summary>
        public virtual MaterialInspection InspectionMethod { get; set; }
        
        /// <summary>
        /// 单位类别
        /// </summary>
        public virtual UnitType UnitType { get; set; }
        
        /// <summary>
        /// 单位编号
        /// </summary>
        public virtual UnitNoType UnitNo { get; set; }
        
        /// <summary>
        /// 库存数
        /// </summary>
        public virtual decimal StockNumber { get; set; }
        
        /// <summary>
        /// 库位编码
        /// </summary>
        public virtual string PlaceCode { get; set; }
        
        /// <summary>
        /// 托盘编码
        /// </summary>
        public virtual string ContainerCode { get; set; }
        
        /// <summary>
        /// 库区Id
        /// </summary>
        public virtual long AreaId { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class AddMaterialStockInput : MaterialStockInput
    {
    }

    /// <summary>
    /// 
    /// </summary>
    public class DeleteMaterialStockInput : BaseId
    {
    }

    /// <summary>
    /// 
    /// </summary>
    public class UpdateMaterialStockInput : MaterialStockInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class QueryeMaterialStockInput : BaseId
    {

    }
}
