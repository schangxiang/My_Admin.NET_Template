﻿import { axios } from '@/utils/request'

/**
 * 查询设备报警
 *
 * @author XJF
 */
export function LesDeviceWaringPage (parameter) {
  return axios({
    url: '/LesDeviceWaring/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 设备报警列表
 *
 * @author XJF
 */
export function LesDeviceWaringList (parameter) {
  return axios({
    url: '/LesDeviceWaring/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加设备报警
 *
 * @author XJF
 */
export function LesDeviceWaringAdd (parameter) {
  return axios({
    url: '/LesDeviceWaring/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑设备报警
 *
 * @author XJF
 */
export function LesDeviceWaringEdit (parameter) {
  return axios({
    url: '/LesDeviceWaring/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除设备报警
 *
 * @author XJF
 */
export function LesDeviceWaringDelete (parameter) {
  return axios({
    url: '/LesDeviceWaring/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出设备报警的Excel文件
 *
 * @author XJF
 */
export function LesDeviceWaringToExcel (parameter) {
  return axios({
    url: '/LesDeviceWaring/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入设备报警的Excel文件
 *
 * @author XJF
 */
export function LesDeviceWaringFromExcel (data, parameter) {
  return axios({
    url: '/LesDeviceWaring/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载设备报警的Excel导入模板
 *
 * @author XJF
 */
export function LesDeviceWaringDownloadExcelTemplate(parameter) {
  return axios({
    url: '/LesDeviceWaring/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}



