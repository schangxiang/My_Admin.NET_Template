﻿import { axios } from '@/utils/request'
 
/**
 * 根据父id查询单据类型列表
 * @author yuhuanhuan
 */
export function WareOrderTypePage (pid) {
  return axios({
    url: '/WareOrderType/listNonPage',
    method: 'get',
    params: {Pid:pid}
  })
}
 
 
/**
 * 添加单据类型维护表
 *
 * @author yrj
 */
export function WareOrderTypeAdd (parameter) {
  return axios({
    url: '/WareOrderType/add',
    method: 'post',
    data: parameter
  })
}
 
/**
 * 编辑单据类型维护表
 *
 * @author yrj
 */
export function WareOrderTypeEdit (parameter) {
  return axios({
    url: '/WareOrderType/edit',
    method: 'post',
    data: parameter
  })
}
 
/**
 * 删除单据类型维护表
 *
 * @author yrj
 */
export function WareOrderTypeDelete (parameter) {
  return axios({
    url: '/WareOrderType/delete',
    method: 'post',
    data: parameter
  })
}