﻿using Mapster;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    public class LesDeviceWaringMapper : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<AddLesDeviceWaringInput, LesDeviceWaring>()
            ;
            config.ForType<UpdateLesDeviceWaringInput, LesDeviceWaring>()
            ;
            config.ForType<LesDeviceWaring, LesDeviceWaringOutput>()
            ;
        }
    }
}
