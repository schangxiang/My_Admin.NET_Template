﻿using Mapster;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    public class WmsOrderMapper : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<AddWmsOrderInput, WmsOrder>()
            ;
            config.ForType<UpdateWmsOrderInput, WmsOrder>()
            ;
            config.ForType<WmsOrder, WmsOrderOutput>()
            ;
        }
    }
}
