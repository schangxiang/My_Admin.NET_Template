﻿using Admin.NET.Core;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Admin.NET.Application
{
    public interface ITestStudent5Service
    {
        Task Add(AddTestStudent5Input input);
        Task Delete(DeleteTestStudent5Input input);
        Task<TestStudent5Output> Get([FromQuery] QueryeTestStudent5Input input);
        Task<List<TestStudent5Output>> List([FromQuery] TestStudent5Input input);
        Task<PageResult<TestStudent5Output>> Page([FromQuery] TestStudent5Search input);
        Task Update(UpdateTestStudent5Input input);

        Task<List<TestStudent5Output>> ListNonPageAsync([FromQuery] TestStudent5SearchNonPage input);

        Task<int> FromExcelAsync(IFormFile file, [FromQuery] ImportExcelType importExcelType);

        IActionResult DownloadExcelTemplate(string version);

        Task<IActionResult> ToExcelAsync([FromQuery] TestStudent5SearchNonPage input);
    }
}