﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 组装车间提升机枚举
    /// </summary>
    public enum LesAisle
    {
        /// <summary>
        /// 一号提升机
        /// </summary>
        [Description("一号")]
        YI = 1,

        /// <summary>
        /// 二号提升机
        /// </summary>
        [Description("二号")]
        ER = 2,

        /// <summary>
        /// 三号提升机
        /// </summary>
        [Description("三号")]
        SAN = 3,

        /// <summary>
        /// 四号提升机
        /// </summary>
        [Description("四号")]
        SI = 4,

        /// <summary>
        /// 五号提升机
        /// </summary>
        [Description("五号")]
        WU = 5,

        /// <summary>
        /// 六号提升机
        /// </summary>
        [Description("六号")]
        LIU = 6,
    }
}
