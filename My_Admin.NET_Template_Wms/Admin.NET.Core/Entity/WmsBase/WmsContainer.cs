﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    /// <summary>
    /// 托盘信息表
    /// </summary>
    [Table("wms_container")]
    [Comment("托盘信息表")]
    public class WmsContainer : DEntityBase //, IEntityTypeBuilder<WmsContainer>
    {
        /// <summary>
        /// 编号
        /// </summary>
        [Comment("编号")]
        [Required]
        [MaxLength(50)]
        public string ContainerCode { get; set; }

        /// <summary>
        /// 类型;数据字典
        /// </summary>
        [Comment("类型")]
        [Required]
        public ContainerType ContainerType { get; set; }

        /// <summary>
        /// 托盘状态;数据字典
        /// </summary>
        [Comment("托盘状态")]
        [Required]
        public ContainerStatus ContainerStatus { get; set; }

        /// <summary>
        /// 长度
        /// </summary>
        [Comment("长度")]
        public decimal SpecLength { get; set; }

        /// <summary>
        /// 宽度
        /// </summary>
        [Comment("宽度")]
        public decimal SpecWidth { get; set; }

        /// <summary>
        /// 高度
        /// </summary>
        [Comment("高度")]
        public decimal SpecHeight { get; set; }

        /// <summary>
        /// 限长
        /// </summary>
        [Comment("限长")]
        public decimal LimitLength { get; set; }

        /// <summary>
        /// 限宽
        /// </summary>
        [Comment("限宽")]
        public decimal LimitWidth { get; set; }

        /// <summary>
        /// 限高
        /// </summary>
        [Comment("限高")]
        public decimal LimitHeight { get; set; }

        /// <summary>
        /// 载重上限
        /// </summary>
        [Comment("载重上限")]
        public decimal MaxWeight { get; set; }

        /// <summary>
        /// 父托盘Id
        /// </summary>
        [Comment("父托盘Id")]
        public long ParentContainerId { get; set; }

        /// <summary>
        /// 资产编号
        /// </summary>
        [Comment("资产编号")]
        [Required]
        [MaxLength(50)]
        public string AssetNo { get; set; }

        /// <summary>
        /// 托盘分类
        /// </summary>
        [Comment("托盘分类")]
        [Required]
        public ContainerCategory ContainerCategory { get; set; } = ContainerCategory.TY;

        /// <summary>
        /// Erp单号
        /// </summary>
        [Comment("Erp单号")]
        [Required]
        [MaxLength(50)]
        public string ErpNo { get; set; }

        /// <summary>
        /// 是否虚拟;数据字典
        /// </summary>
        [Comment("是否虚拟")]
        [Required]
        public YesOrNot IsVirtually { get; set; } = YesOrNot.N;

        /// <summary>
        /// 所属车间
        /// </summary>
        [Comment("所属车间")]
        [Required]
        public LesWorkShopType WorkShopType { get; set; }

        ///// <summary>
        ///// 组装车间物料托盘关系表
        ///// </summary>
        //public ICollection<WmsAssembleMaterialContainer> WmsAssembleMaterialContainer { get; set; }

        ///// <summary>
        ///// 物料托盘历史表
        ///// </summary>
        //public ICollection<WmsAssembleHistoryMaterialContainer> WmsAssembleHistoryMaterialContainer { get; set; }

        ///// <summary>
        ///// 构建一对多的关系
        ///// </summary>
        ///// <param name="entityBuilder"></param>
        ///// <param name="dbContext"></param>
        ///// <param name="dbContextLocator"></param>
        //public void Configure(EntityTypeBuilder<WmsContainer> entityBuilder, DbContext dbContext, Type dbContextLocator)
        //{
        //    entityBuilder.HasMany(x => x.WmsAssembleMaterialContainer)
        //        .WithOne(x => x.WmsContainer)
        //        .HasForeignKey(x => x.ContainerId);

        //    entityBuilder.HasMany(x => x.WmsAssembleHistoryMaterialContainer)
        //        .WithOne(x => x.WmsContainer)
        //        .HasForeignKey(x => x.Id).IsRequired(false);
        //}
    }
}
