﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 物料信息输出参数
    /// </summary>
    public class WmsMaterialOutput
    {
        /// <summary>
        /// 物料名称
        /// </summary>
        public string MaterialName { get; set; }
        
        /// <summary>
        /// 物料编号
        /// </summary>
        public string MaterialNo { get; set; }
        
        /// <summary>
        /// 物料批次
        /// </summary>
        public string MaterialBatch { get; set; }
        
        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }
        
        /// <summary>
        /// 物料类别
        /// </summary>
        public MaterialType MaterialType { get; set; }
        
        /// <summary>
        /// 物料规格
        /// </summary>
        public string MaterialSpec { get; set; }
        
        /// <summary>
        /// 物料密度
        /// </summary>
        public string MaterialDensity { get; set; }
        /// <summary>
        /// 物料单位
        /// </summary>
        /// 
        public string MaterialUnit { get; set; }
        /// <summary>
        /// 库区Ids
        /// </summary>
        public string AreaIds { get; set; }

        /// <summary>
        /// 库区参数
        /// </summary>
        public string AreaNameParameter { get; set; }

        /// <summary>
        /// 工作区Ids
        /// </summary>
        public string StationIds { get; set; }

        /// <summary>
        /// 工作区参数
        /// </summary>
        public string StationNameParameter { get; set; }
        
        /// <summary>
        /// 熟化时间
        /// </summary>
        public int MaturationTime { get; set; }
        
        /// <summary>
        /// 是否需要熟化
        /// </summary>
        public bool IsMaturation { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }

        //1217

        /// <summary>
        /// 检验方式;数据字典
        /// </summary>

        public MaterialInspection InspectionMethod { get; set; }
        /// <summary>
        /// 安全存量
        /// </summary>
        public decimal Safeqty { get; set; }

        /// <summary>
        /// 最大存量
        /// </summary>

        public decimal MaxImumqty { get; set; }

        /// <summary>
        /// 最小库龄
        /// </summary>

        public decimal MinstorageAge { get; set; }

        /// <summary>
        /// 最大库龄
        /// </summary>

        public decimal MaxstorageAge { get; set; }

       


    }

    /// <summary>
    /// 库区下拉框输出参数
    /// </summary>
    public class AreaListOutput
    {
        /// <summary>
        /// 所在库区Id
        /// </summary>
        public string AreaId { get; set; }

        /// <summary>
        /// 库区名称
        /// </summary>
        public string AreaName { get; set; }
    }

    /// <summary>
    /// 工作区（工位）下拉框输出参数
    /// </summary>
    public class StationListOutput
    {
        /// <summary>
        /// 站点Id
        /// </summary>
        public string StationId { get; set; }

        /// <summary>
        /// 站点名称
        /// </summary>
        public string StationName { get; set; }
    }
}
