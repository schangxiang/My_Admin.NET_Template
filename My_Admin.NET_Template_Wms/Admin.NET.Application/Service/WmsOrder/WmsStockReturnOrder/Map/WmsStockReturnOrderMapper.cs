﻿using Mapster;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    public class WmsStockReturnOrderMapper : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<AddWmsStockReturnOrderInput, WmsStockReturnOrder>()
            ;
            config.ForType<UpdateWmsStockReturnOrderInput, WmsStockReturnOrder>()
            ;
            config.ForType<WmsStockReturnOrder, WmsStockReturnOrderOutput>()
            ;
        }
    }
}
