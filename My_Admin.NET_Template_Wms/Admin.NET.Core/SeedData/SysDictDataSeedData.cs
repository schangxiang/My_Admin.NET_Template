﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;

namespace Admin.NET.Core
{
    /// <summary>
    /// 系统字典值种子数据
    /// </summary>
    public class SysDictDataSeedData : IEntitySeedData<SysDictData>
    {
        /// <summary>
        /// 种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<SysDictData> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new[]
            {
                new SysDictData{Id=142307070902375, TypeId=142307070906484, Value="男", Code="1", Sort=100, Remark="男性", Status=0 },
                new SysDictData{Id=142307070902376, TypeId=142307070906484, Value="女", Code="2", Sort=100, Remark="女性", Status=0 },
                new SysDictData{Id=142307070902377, TypeId=142307070906484, Value="未知", Code="3", Sort=100, Remark="未知性别", Status=0 },

                //new SysDictData{Id=142307070902378, TypeId=142307070906485, Value="默认常量", Code="DEFAULT", Sort=100, Remark="默认常量，都以XIAONUO_开头的", Status=0 },
                //new SysDictData{Id=142307070902379, TypeId=142307070906485, Value="阿里云短信", Code="ALIYUN_SMS", Sort=100, Remark="阿里云短信配置", Status=0 },
                //new SysDictData{Id=142307070902380, TypeId=142307070906485, Value="腾讯云短信", Code="TENCENT_SMS", Sort=100, Remark="腾讯云短信", Status=0 },
                //new SysDictData{Id=142307070902381, TypeId=142307070906485, Value="邮件配置", Code="EMAIL", Sort=100, Remark="邮件配置", Status=0 },
                //new SysDictData{Id=142307070902382, TypeId=142307070906485, Value="文件上传路径", Code="FILE_PATH", Sort=100, Remark="文件上传路径", Status=0 },
                //new SysDictData{Id=142307070902383, TypeId=142307070906485, Value="Oauth配置", Code="OAUTH", Sort=100, Remark="Oauth配置", Status=0 },

                new SysDictData{Id=142307070902384, TypeId=142307070906483, Value="正常", Code="0", Sort=100, Remark="正常", Status=0 },
                new SysDictData{Id=142307070902385, TypeId=142307070906483, Value="停用", Code="1", Sort=100, Remark="停用", Status=0 },
                new SysDictData{Id=142307070902386, TypeId=142307070906483, Value="删除", Code="2", Sort=100, Remark="删除", Status=0 },

                new SysDictData{Id=142307070902387, TypeId=142307070906486, Value="否", Code="N", Sort=100, Remark="否", Status=0 },
                new SysDictData{Id=142307070902388, TypeId=142307070906486, Value="是", Code="Y", Sort=100, Remark="是", Status=0 },

                new SysDictData{Id=142307070902389, TypeId=142307070906487, Value="登录", Code="1", Sort=100, Remark="登录", Status=0 },
                new SysDictData{Id=142307070902390, TypeId=142307070906487, Value="登出", Code="2", Sort=100, Remark="登出", Status=0 },

                new SysDictData{Id=142307070902391, TypeId=142307070906488, Value="目录", Code="0", Sort=100, Remark="目录", Status=0 },
                new SysDictData{Id=142307070902392, TypeId=142307070906488, Value="菜单", Code="1", Sort=100, Remark="菜单", Status=0 },
                new SysDictData{Id=142307070902393, TypeId=142307070906488, Value="按钮", Code="2", Sort=100, Remark="按钮", Status=0 },

                new SysDictData{Id=142307070902394, TypeId=142307070906489, Value="未发送", Code="0", Sort=100, Remark="未发送", Status=0 },
                new SysDictData{Id=142307070902395, TypeId=142307070906489, Value="发送成功", Code="1", Sort=100, Remark="发送成功", Status=0 },
                new SysDictData{Id=142307070902396, TypeId=142307070906489, Value="发送失败", Code="2", Sort=100, Remark="发送失败", Status=0 },
                new SysDictData{Id=142307070902397, TypeId=142307070906489, Value="失效", Code="3", Sort=100, Remark="失效", Status=0 },

                new SysDictData{Id=142307070902398, TypeId=142307070906490, Value="无", Code="0", Sort=100, Remark="无", Status=0 },
                new SysDictData{Id=142307070902399, TypeId=142307070906490, Value="组件", Code="1", Sort=100, Remark="组件", Status=0 },
                new SysDictData{Id=142307070906437, TypeId=142307070906490, Value="内链", Code="2", Sort=100, Remark="内链", Status=0 },
                new SysDictData{Id=142307070906438, TypeId=142307070906490, Value="外链", Code="3", Sort=100, Remark="外链", Status=0 },

                new SysDictData{Id=142307070906439, TypeId=142307070906491, Value="系统权重", Code="1", Sort=100, Remark="系统权重", Status=0 },
                new SysDictData{Id=142307070906440, TypeId=142307070906491, Value="业务权重", Code="2", Sort=100, Remark="业务权重", Status=0 },


                new SysDictData{Id=142307070906441, TypeId=142307070906492, Value="全部数据", Code="1", Sort=100, Remark="全部数据", Status=0 },
                new SysDictData{Id=142307070906442, TypeId=142307070906492, Value="本部门及以下数据", Code="2", Sort=100, Remark="本部门及以下数据", Status=0 },
                new SysDictData{Id=142307070906443, TypeId=142307070906492, Value="本部门数据", Code="3", Sort=100, Remark="本部门数据", Status=0 },

                new SysDictData{Id=142307070906444, TypeId=142307070906492, Value="仅本人数据", Code="4", Sort=100, Remark="仅本人数据", Status=0 },
                new SysDictData{Id=142307070906445, TypeId=142307070906492, Value="自定义数据", Code="5", Sort=100, Remark="自定义数据", Status=0 },

                //new SysDictData{Id=142307070906446, TypeId=142307070906493, Value="app", Code="1", Sort=100, Remark="app", Status=0 },
                //new SysDictData{Id=142307070906447, TypeId=142307070906493, Value="pc", Code="2", Sort=100, Remark="pc", Status=0 },
                //new SysDictData{Id=142307070906448, TypeId=142307070906493, Value="其他", Code="3", Sort=100, Remark="其他", Status=0 },

                new SysDictData{Id=142307070906449, TypeId=142307070906494, Value="其它", Code="0", Sort=100, Remark="其它", Status=0 },
                new SysDictData{Id=142307070906450, TypeId=142307070906494, Value="增加", Code="1", Sort=100, Remark="增加", Status=0 },
                new SysDictData{Id=142307070906451, TypeId=142307070906494, Value="删除", Code="2", Sort=100, Remark="删除", Status=0 },
                new SysDictData{Id=142307070906452, TypeId=142307070906494, Value="编辑", Code="3", Sort=100, Remark="编辑", Status=0 },
                new SysDictData{Id=142307070906453, TypeId=142307070906494, Value="更新", Code="4", Sort=100, Remark="更新", Status=0 },
                new SysDictData{Id=142307070906454, TypeId=142307070906494, Value="查询", Code="5", Sort=100, Remark="查询", Status=0 },
                new SysDictData{Id=142307070906455, TypeId=142307070906494, Value="详情", Code="6", Sort=100, Remark="详情", Status=0 },
                new SysDictData{Id=142307070906456, TypeId=142307070906494, Value="树", Code="7", Sort=100, Remark="树", Status=0 },
                new SysDictData{Id=142307070906457, TypeId=142307070906494, Value="导入", Code="8", Sort=100, Remark="导入", Status=0 },
                new SysDictData{Id=142307070906458, TypeId=142307070906494, Value="导出", Code="9", Sort=100, Remark="导出", Status=0 },
                new SysDictData{Id=142307070906459, TypeId=142307070906494, Value="授权", Code="10", Sort=100, Remark="授权", Status=0 },
                new SysDictData{Id=142307070906460, TypeId=142307070906494, Value="强退", Code="11", Sort=100, Remark="强退", Status=0 },
                new SysDictData{Id=142307070906461, TypeId=142307070906494, Value="清空", Code="12", Sort=100, Remark="清空", Status=0 },
                new SysDictData{Id=142307070906462, TypeId=142307070906494, Value="修改状态", Code="13", Sort=100, Remark="修改状态", Status=0 },

                new SysDictData{Id=142307070906463, TypeId=142307070906495, Value="阿里云", Code="1", Sort=100, Remark="阿里云", Status=0 },
                new SysDictData{Id=142307070906464, TypeId=142307070906495, Value="腾讯云", Code="2", Sort=100, Remark="腾讯云", Status=0 },
                new SysDictData{Id=142307070906465, TypeId=142307070906495, Value="minio", Code="3", Sort=100, Remark="minio", Status=0 },
                new SysDictData{Id=142307070906466, TypeId=142307070906495, Value="本地", Code="4", Sort=100, Remark="本地", Status=0 },

                new SysDictData{Id=142307070906467, TypeId=142307070910533, Value="运行", Code="1", Sort=100, Remark="运行", Status=0 },
                new SysDictData{Id=142307070906468, TypeId=142307070910533, Value="停止", Code="2", Sort=100, Remark="停止", Status=0 },

                new SysDictData{Id=142307070906469, TypeId=142307070910534, Value="通知", Code="1", Sort=100, Remark="通知", Status=0 },
                new SysDictData{Id=142307070906470, TypeId=142307070910534, Value="公告", Code="2", Sort=100, Remark="公告", Status=0 },

                new SysDictData{Id=142307070906471, TypeId=142307070910535, Value="草稿", Code="0", Sort=100, Remark="草稿", Status=0 },
                new SysDictData{Id=142307070906472, TypeId=142307070910535, Value="发布", Code="1", Sort=100, Remark="发布", Status=0 },

                new SysDictData{Id=142307070906473, TypeId=142307070910535, Value="撤回", Code="2", Sort=100, Remark="撤回", Status=0 },
                new SysDictData{Id=142307070906474, TypeId=142307070910535, Value="删除", Code="3", Sort=100, Remark="删除", Status=0 },

                new SysDictData{Id=142307070906475, TypeId=142307070910536, Value="是", Code="true", Sort=100, Remark="是", Status=0 },
                new SysDictData{Id=142307070906476, TypeId=142307070910536, Value="否", Code="false", Sort=100, Remark="否", Status=0 },

                new SysDictData{Id=142307070906477, TypeId=142307070910537, Value="下载压缩包", Code="1", Sort=100, Remark="下载压缩包", Status=0 },
                new SysDictData{Id=142307070906478, TypeId=142307070910537, Value="生成到本项目", Code="2", Sort=100, Remark="生成到本项目", Status=0 },

                new SysDictData{Id=142307070906479, TypeId=142307070910538, Value="GET", Code="1", Sort=100, Remark="GET", Status=0 },
                new SysDictData{Id=142307070906480, TypeId=142307070910538, Value="POST", Code="2", Sort=100, Remark="POST", Status=0 },
                new SysDictData{Id=142307070906481, TypeId=142307070910538, Value="PUT", Code="3", Sort=100, Remark="PUT", Status=0 },
                new SysDictData{Id=142307070906482, TypeId=142307070910538, Value="DELETE", Code="4", Sort=100, Remark="DELETE", Status=0 },

                new SysDictData{Id=142307070922829, TypeId=142307070922827, Value="外键", Code="fk", Sort=100, Remark="外键", Status=0 },
                new SysDictData{Id=142307070922830, TypeId=142307070922827, Value="输入框", Code="input", Sort=100, Remark="输入框", Status=0 },
                new SysDictData{Id=142307070922831, TypeId=142307070922827, Value="日期选择", Code="datepicker", Sort=100, Remark="日期选择", Status=0 },
                new SysDictData{Id=386523071475758, TypeId=142307070922827, Value="日期时间选择", Code="datetimepicker", Sort=100, Remark="日期时间选择", Status=0 },

                new SysDictData{Id=142307070922832, TypeId=142307070922827, Value="下拉框", Code="select", Sort=100, Remark="下拉框", Status=0 },
                new SysDictData{Id=142307070922833, TypeId=142307070922827, Value="单选框", Code="radio", Sort=100, Remark="单选框", Status=0 },
                new SysDictData{Id=142307070922834, TypeId=142307070922827, Value="开关", Code="switch", Sort=100, Remark="开关", Status=0 },
                new SysDictData{Id=142307070922835, TypeId=142307070922827, Value="多选框", Code="checkbox", Sort=100, Remark="多选框", Status=0 },
                new SysDictData{Id=142307070922836, TypeId=142307070922827, Value="数字输入框", Code="inputnumber", Sort=100, Remark="数字输入框", Status=0 },
                new SysDictData{Id=142307070922837, TypeId=142307070922827, Value="文本域", Code="textarea", Sort=100, Remark="文本域", Status=0 },

                new SysDictData{Id=142307070922838, TypeId=142307070922828, Value="等于", Code="==", Sort=1, Remark="等于", Status=0 },
                new SysDictData{Id=142307070922839, TypeId=142307070922828, Value="模糊", Code="like", Sort=2, Remark="模糊", Status=0 },
                new SysDictData{Id=142307070922840, TypeId=142307070922828, Value="大于", Code=">", Sort=3, Remark="大于", Status=0 },
                new SysDictData{Id=142307070922841, TypeId=142307070922828, Value="小于", Code="<", Sort=4, Remark="小于", Status=0 },
                new SysDictData{Id=142307070922842, TypeId=142307070922828, Value="不等于", Code="!=", Sort=5, Remark="不等于", Status=0 },
                new SysDictData{Id=142307070922843, TypeId=142307070922828, Value="大于等于", Code=">=", Sort=6, Remark="大于等于", Status=0 },
                new SysDictData{Id=142307070922844, TypeId=142307070922828, Value="小于等于", Code="<=", Sort=7, Remark="小于等于", Status=0 },
                new SysDictData{Id=142307070922845, TypeId=142307070922828, Value="不为空", Code="isNotNull", Sort=8, Remark="不为空", Status=0 },
                new SysDictData{Id=386523071475759, TypeId=142307070922828, Value="介于", Code="between", Sort=9, Remark="介于", Status=0 },

                new SysDictData{Id=142307070922851, TypeId=142307070922829, Value="int", Code="int", Sort=100, Remark="int", Status=0 },
                new SysDictData{Id=142307070922846, TypeId=142307070922829, Value="long", Code="long", Sort=100, Remark="long", Status=0 },
                new SysDictData{Id=142307070922847, TypeId=142307070922829, Value="string", Code="string", Sort=100, Remark="string", Status=0 },
                new SysDictData{Id=142307070922850, TypeId=142307070922829, Value="bool", Code="bool", Sort=100, Remark="bool", Status=0 },
                new SysDictData{Id=142307070922852, TypeId=142307070922829, Value="double", Code="double", Sort=100, Remark="double", Status=0 },
                new SysDictData{Id=142307070922848, TypeId=142307070922829, Value="DateTime", Code="DateTime", Sort=100, Remark="DateTime", Status=0 },
                new SysDictData{Id=142307070922861, TypeId=142307070922829, Value="float", Code="float", Sort=100, Remark="float", Status=0 },
                new SysDictData{Id=142307070922862, TypeId=142307070922829, Value="decimal", Code="decimal", Sort=100, Remark="decimal", Status=0 },
                new SysDictData{Id=142307070922863, TypeId=142307070922829, Value="Guid", Code="Guid", Sort=100, Remark="Guid", Status=0 },
                new SysDictData{Id=142307070922864, TypeId=142307070922829, Value="DateTimeOffset", Code="DateTimeOffset", Sort=100, Remark="DateTimeOffset", Status=0 },

                new SysDictData{Id=142307070926943, TypeId=142307070926941, Value="集团角色", Code="0", Sort=100, Remark="集团角色", Status=0 },
                new SysDictData{Id=142307070926944, TypeId=142307070926941, Value="加盟商角色", Code="1", Sort=100, Remark="加盟商角色", Status=0 },
                new SysDictData{Id=142307070926945, TypeId=142307070926941, Value="门店角色", Code="2", Sort=100, Remark="门店角色", Status=0 },

                new SysDictData{Id=142307070926946, TypeId=142307070926942, Value="一级", Code="1", Sort=100, Remark="一级", Status=0 },
                new SysDictData{Id=142307070926947, TypeId=142307070926942, Value="二级", Code="2", Sort=100, Remark="二级", Status=0 },
                new SysDictData{Id=142307070926948, TypeId=142307070926942, Value="三级", Code="3", Sort=100, Remark="三级", Status=0 },
                new SysDictData{Id=142307070926949, TypeId=142307070926942, Value="四级", Code="4", Sort=100, Remark="四级", Status=0 },

                new SysDictData{Id=285605336563781, TypeId=285605149831237, Value="默认表单分类", Code="DefaultFormType", Sort=100, Remark="默认表单分类", Status=0 },

                new SysDictData{Id=285641495289925, TypeId=285641358889029, Value="默认分类", Code="DefaultGroup", Sort=100, Remark="默认分类", Status=0 },

                new SysDictData{Id=355697634480197, TypeId=355694205325381, Value="其他", Code="0", Sort=100, Remark="", Status=0 },
                new SysDictData{Id=355697676427333, TypeId=355694205325381, Value="木质", Code="1", Sort=100, Remark="", Status=0 },
                new SysDictData{Id=355697726726213, TypeId=355694205325381, Value="金属", Code="2", Sort=100, Remark="", Status=0 },
                new SysDictData{Id=355697755361349, TypeId=355694205325381, Value="塑料", Code="3", Sort=100, Remark="", Status=0 },

                new SysDictData{Id=355697944657989, TypeId=355697881423941, Value="禁用", Code="0", Sort=100, Remark="", Status=0 },
                new SysDictData{Id=355697974415429, TypeId=355697881423941, Value="空闲", Code="1", Sort=100, Remark="", Status=0 },
                new SysDictData{Id=355698005966917, TypeId=355697881423941, Value="组盘", Code="2", Sort=100, Remark="", Status=0 },
                new SysDictData{Id=355698032828485, TypeId=355697881423941, Value="库位", Code="3", Sort=100, Remark="", Status=0 },

                new SysDictData{Id=355697634480198, TypeId=355697881423942, Value="通用", Code="0", Sort=100, Remark="", Status=0 },
                new SysDictData{Id=355697676427399, TypeId=355697881423942, Value="危化品", Code="1", Sort=100, Remark="", Status=0 },
                new SysDictData{Id=355697726726200, TypeId=355697881423942, Value="液体", Code="2", Sort=100, Remark="", Status=0 },
                new SysDictData{Id=355697755361301, TypeId=355697881423942, Value="贵重品", Code="3", Sort=100, Remark="", Status=0 },
                new SysDictData{Id=355697755361302, TypeId=355697881423942, Value="气体", Code="4", Sort=100, Remark="", Status=0 },
                new SysDictData{Id=355697755361303, TypeId=355697881423942, Value="低温", Code="5", Sort=100, Remark="", Status=0 },

                new SysDictData{Id=355698270015557, TypeId=355698224758853, Value="其他", Code="0", Sort=100, Remark="", Status=0 },
                new SysDictData{Id=355698303508549, TypeId=355698224758853, Value="立体库", Code="1", Sort=100, Remark="", Status=0 },
                new SysDictData{Id=355698328531013, TypeId=355698224758853, Value="普通库", Code="2", Sort=100, Remark="", Status=0 },
                new SysDictData{Id=355698362712133, TypeId=355698224758853, Value="公共库", Code="3", Sort=100, Remark="", Status=0 },

                new SysDictData{Id=355698476777541, TypeId=355698441912389, Value="空闲", Code="1", Sort=100, Remark="", Status=0 },
                new SysDictData{Id=355698510786629, TypeId=355698441912389, Value="待入", Code="2", Sort=100, Remark="", Status=0 },
                new SysDictData{Id=355698549424197, TypeId=355698441912389, Value="存货", Code="3", Sort=100, Remark="", Status=0 },
                new SysDictData{Id=355698576941125, TypeId=355698441912389, Value="待出", Code="4", Sort=100, Remark="", Status=0 },

                new SysDictData{Id=355698694561861, TypeId=355698661130309, Value="低", Code="1", Sort=100, Remark="", Status=0 },
                new SysDictData{Id=355698716401733, TypeId=355698661130309, Value="中", Code="2", Sort=100, Remark="", Status=0 },
                new SysDictData{Id=355698740326469, TypeId=355698661130309, Value="高", Code="3", Sort=100, Remark="", Status=0 },

                new SysDictData{Id=355698898735173,TypeId=355698855280709,Value="免检",Code="1",Sort=100,Remark="",Status=0},
                new SysDictData{Id=355698924970053,TypeId=355698855280709,Value="抽检",Code="2",Sort=100,Remark="",Status=0},

                new SysDictData{Id=355699041800261,TypeId=355699003580485,Value="原材料",Code="1",Sort=100,Remark="",Status=0},
                new SysDictData{Id=355699063255109,TypeId=355699003580485,Value="半成品",Code="2",Sort=100,Remark="",Status=0},
                new SysDictData{Id=355699088330821,TypeId=355699003580485,Value="成品",Code="3",Sort=100,Remark="",Status=0},
                new SysDictData{Id=355699113156677,TypeId=355699003580485,Value="空托",Code="4",Sort=100,Remark="",Status=0},

                new SysDictData{Id=355699219353669,TypeId=355699189051461,Value="长度",Code="1",Sort=100,Remark="",Status=0},
                new SysDictData{Id=355699243540549,TypeId=355699189051461,Value="重量",Code="2",Sort=100,Remark="",Status=0},

                new SysDictData{Id=355699345645637,TypeId=355699314061381,Value="cm",Code="1",Sort=100,Remark="",Status=0},
                new SysDictData{Id=355699367575621,TypeId=355699314061381,Value="t",Code="2",Sort=100,Remark="",Status=0},

                new SysDictData{Id=355699479236677,TypeId=355699447029829,Value="手动",Code="1",Sort=100,Remark="",Status=0},
                new SysDictData{Id=355699505229893,TypeId=355699447029829,Value="自动",Code="2",Sort=100,Remark="",Status=0},

                new SysDictData{Id=355699615133765,TypeId=355699576467525,Value="入库",Code="1",Sort=100,Remark="",Status=0},
                new SysDictData{Id=355699638476869,TypeId=355699576467525,Value="出库",Code="2",Sort=100,Remark="",Status=0},
                new SysDictData{Id=355699661889605,TypeId=355699576467525,Value="移库",Code="3",Sort=100,Remark="",Status=0},

                new SysDictData{Id=355699760455749,TypeId=355699731099717,Value="未执行",Code="1",Sort=100,Remark="",Status=0},
                new SysDictData{Id=355699784736837,TypeId=355699731099717,Value="执行中",Code="2",Sort=100,Remark="",Status=0},
                new SysDictData{Id=355699813859397,TypeId=355699731099717,Value="完成",Code="3",Sort=100,Remark="",Status=0},

                new SysDictData{Id=355699760455798,TypeId=355699731099718,Value="未执行",Code="1",Sort=100,Remark="",Status=0},
                new SysDictData{Id=355699784736899,TypeId=355699731099718,Value="执行中-AGV",Code="2",Sort=100,Remark="",Status=0},
                new SysDictData{Id=355699813859300,TypeId=355699731099718,Value="执行中-输送线",Code="3",Sort=100,Remark="",Status=0},
                new SysDictData{Id=355699813859301,TypeId=355699731099718,Value="执行中-堆垛机",Code="4",Sort=100,Remark="",Status=0},
                new SysDictData{Id=355699813859302,TypeId=355699731099718,Value="等待",Code="5",Sort=100,Remark="",Status=0},
                new SysDictData{Id=355699813859303,TypeId=355699731099718,Value="完成",Code="6",Sort=100,Remark="",Status=0},

                new SysDictData{Id=383290784104517,TypeId=383290718294085,Value="泡沫切割",Code="1",Sort=100,Remark="",Status=0},
                new SysDictData{Id=383290816860229,TypeId=383290718294085,Value="装配车间",Code="2",Sort=100,Remark="",Status=0},

                new SysDictData{Id=383317608218693,TypeId=383317542490181,Value="未开始",Code="0",Sort=100,Remark="",Status=0},
                new SysDictData{Id=383317718302789,TypeId=383317542490181,Value="运送中",Code="1",Sort=100,Remark="",Status=0},
                new SysDictData{Id=383317751234629,TypeId=383317542490181,Value="完成",Code="2",Sort=100,Remark="",Status=0},

                new SysDictData{Id=384394737053765,TypeId=384394636988485,Value="AGV",Code="1",Sort=100,Remark="",Status=0},
                new SysDictData{Id=384394851618885,TypeId=384394636988485,Value="堆垛机",Code="2",Sort=100,Remark="",Status=0},
                new SysDictData{Id=384394877870149,TypeId=384394636988485,Value="RGV",Code="3",Sort=100,Remark="",Status=0},

                new SysDictData{Id=385412179521605,TypeId=385412129075269,Value="切割",Code="1",Sort=100,Remark="",Status=0},
                new SysDictData{Id=385412217618501,TypeId=385412129075269,Value="压合",Code="2",Sort=100,Remark="",Status=0},
                new SysDictData{Id=385412280418373,TypeId=385412129075269,Value="CNC",Code="3",Sort=100,Remark="",Status=0},

                new SysDictData{Id=386523049214021,TypeId=386522984120389,Value="入库口",Code="1",Sort=100,Remark="",Status=0},
                new SysDictData{Id=386523071475781,TypeId=386522984120389,Value="出库口",Code="2",Sort=100,Remark="",Status=0},


                new SysDictData{Id=386523049214025,TypeId=386522984120391,Value="未开始",Code="1",Sort=100,Remark="",Status=0},
                new SysDictData{Id=386523071475726,TypeId=386522984120391,Value="进行中",Code="2",Sort=100,Remark="",Status=0},
                new SysDictData{Id=386523071475727,TypeId=386522984120391,Value="完成",Code="3",Sort=100,Remark="",Status=0},
                new SysDictData{Id=386523071475728,TypeId=386522984120391,Value="撤回",Code="4",Sort=100,Remark="",Status=0},

                new SysDictData{Id=386523049214029,TypeId=386522984120392,Value="入库",Code="1",Sort=100,Remark="",Status=0},
                new SysDictData{Id=386523071475730,TypeId=386522984120392,Value="出库",Code="2",Sort=100,Remark="",Status=0},
                new SysDictData{Id=386523071475731,TypeId=386522984120392,Value="调拨",Code="3",Sort=100,Remark="",Status=0},
                new SysDictData{Id=386523071475732,TypeId=386522984120392,Value="越库",Code="4",Sort=100,Remark="",Status=0},

                new SysDictData{Id=386523049214033,TypeId=386522984120393,Value="未下发",Code="1",Sort=100,Remark="",Status=0},
                new SysDictData{Id=386523071475734,TypeId=386522984120393,Value="已下发",Code="2",Sort=100,Remark="",Status=0},
                new SysDictData{Id=386523071475735,TypeId=386522984120393,Value="执行中",Code="3",Sort=100,Remark="",Status=0},
                new SysDictData{Id=386523071475736,TypeId=386522984120393,Value="完成",Code="4",Sort=100,Remark="",Status=0},
                new SysDictData{Id=386523071475737,TypeId=386522984120393,Value="撤回",Code="5",Sort=100,Remark="",Status=0},

            };
        }
    }
}