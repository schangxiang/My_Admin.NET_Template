﻿import { axios } from '@/utils/request'

/**
 * 查询学生视图
 *
 * @author 邵长祥
 */
export function VStudentPage (parameter) {
  return axios({
    url: '/VStudent/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 学生视图列表
 *
 * @author 邵长祥
 */
export function VStudentList (parameter) {
  return axios({
    url: '/VStudent/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加学生视图
 *
 * @author 邵长祥
 */
export function VStudentAdd (parameter) {
  return axios({
    url: '/VStudent/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑学生视图
 *
 * @author 邵长祥
 */
export function VStudentEdit (parameter) {
  return axios({
    url: '/VStudent/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除学生视图
 *
 * @author 邵长祥
 */
export function VStudentDelete (parameter) {
  return axios({
    url: '/VStudent/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出学生视图的Excel文件
 *
 * @author 邵长祥
 */
export function VStudentToExcel (parameter) {
  return axios({
    url: '/VStudent/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入学生视图的Excel文件
 *
 * @author 邵长祥
 */
export function VStudentFromExcel (data, parameter) {
  return axios({
    url: '/VStudent/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载学生视图的Excel导入模板
 *
 * @author 邵长祥
 */
export function VStudentDownloadExcelTemplate(parameter) {
  return axios({
    url: '/VStudent/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}



