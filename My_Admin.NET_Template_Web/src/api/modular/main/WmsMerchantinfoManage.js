﻿import { axios } from '@/utils/request'

/**
 * 查询客商信息表
 *
 * @author XJF
 */
export function WmsMerchantinfoPage (parameter) {
  return axios({
    url: '/WmsMerchantinfo/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 客商信息表列表
 *
 * @author XJF
 */
export function WmsMerchantinfoList (parameter) {
  return axios({
    url: '/WmsMerchantinfo/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加客商信息表
 *
 * @author XJF
 */
export function WmsMerchantinfoAdd (parameter) {
  return axios({
    url: '/WmsMerchantinfo/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑客商信息表
 *
 * @author XJF
 */
export function WmsMerchantinfoEdit (parameter) {
  return axios({
    url: '/WmsMerchantinfo/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除客商信息表
 *
 * @author XJF
 */
export function WmsMerchantinfoDelete (parameter) {
  return axios({
    url: '/WmsMerchantinfo/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出客商信息表的Excel文件
 *
 * @author XJF
 */
export function WmsMerchantinfoToExcel (parameter) {
  return axios({
    url: '/WmsMerchantinfo/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入客商信息表的Excel文件
 *
 * @author XJF
 */
export function WmsMerchantinfoFromExcel (data, parameter) {
  return axios({
    url: '/WmsMerchantinfo/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载客商信息表的Excel导入模板
 *
 * @author XJF
 */
export function WmsMerchantinfoDownloadExcelTemplate(parameter) {
  return axios({
    url: '/WmsMerchantinfo/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}



