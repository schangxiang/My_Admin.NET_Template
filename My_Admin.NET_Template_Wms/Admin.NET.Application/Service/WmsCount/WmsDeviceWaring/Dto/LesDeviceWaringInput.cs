﻿using Admin.NET.Core;
using Admin.NET.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace Admin.NET.Application
{    
    
    /// <summary>
    /// 设备报警查询参数
    /// </summary>
    public class LesDeviceWaringSearch : PageInputBase
    {
        /// <summary>
        /// WcsId
        /// </summary>
        public virtual int? WcsId { get; set; }
        
        /// <summary>
        /// 设备名称
        /// </summary>
        public virtual string DeviceName { get; set; }
        
        /// <summary>
        /// 故障名称
        /// </summary>
        public virtual string FaultName { get; set; }
        
        /// <summary>
        /// 发生时间
        /// </summary>
        public virtual DateTimeOffset? StartTime { get; set; }
        
        /// <summary>
        /// 结束时间
        /// </summary>
        public virtual DateTimeOffset? EndTime { get; set; }
        
        /// <summary>
        /// 持续时间
        /// </summary>
        public virtual int? RunningTime { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState IssueState { get; set; }
        
    }

        /// <summary>
    /// 设备报警不分页查询参数
    /// </summary>
    public class LesDeviceWaringSearchNonPage : PageInputNonPageBase
    {
        /// <summary>
        /// WcsId
        /// </summary>
        public virtual int? WcsId { get; set; }
        
        /// <summary>
        /// 设备名称
        /// </summary>
        public virtual string? DeviceName { get; set; }
        
        /// <summary>
        /// 故障名称
        /// </summary>
        public virtual string? FaultName { get; set; }
        
        /// <summary>
        /// 发生时间
        /// </summary>
        public virtual DateTimeOffset? StartTime { get; set; }
        
        /// <summary>
        /// 结束时间
        /// </summary>
        public virtual DateTimeOffset? EndTime { get; set; }
        
        /// <summary>
        /// 持续时间
        /// </summary>
        public virtual int? RunningTime { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState? IssueState { get; set; }
        
    }

    /// <summary>
    /// 设备报警输入参数
    /// </summary>
    public class LesDeviceWaringInput
    {
        /// <summary>
        /// WcsId
        /// </summary>
        public virtual int WcsId { get; set; }
        
        /// <summary>
        /// 设备名称
        /// </summary>
        public virtual string DeviceName { get; set; }
        
        /// <summary>
        /// 故障名称
        /// </summary>
        public virtual string FaultName { get; set; }
        
        /// <summary>
        /// 发生时间
        /// </summary>
        public virtual DateTimeOffset StartTime { get; set; }
        
        /// <summary>
        /// 结束时间
        /// </summary>
        public virtual DateTimeOffset EndTime { get; set; }
        
        /// <summary>
        /// 持续时间
        /// </summary>
        public virtual int RunningTime { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState IssueState { get; set; }
        
    }

    public class AddLesDeviceWaringInput : LesDeviceWaringInput
    {
    }

    public class DeleteLesDeviceWaringInput : BaseId
    {
    }

    public class UpdateLesDeviceWaringInput : LesDeviceWaringInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
        
    }

    public class QueryeLesDeviceWaringInput : BaseId
    {

    }
}
