﻿using Admin.NET.Core;
using Admin.NET.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace Admin.NET.Application
{    
    
    /// <summary>
    /// 托盘信息查询参数
    /// </summary>
    public class WmsContainerSearch : PageInputBase
    {
        /// <summary>
        /// 编号
        /// </summary>
        public virtual string ContainerCode { get; set; }
        
        /// <summary>
        /// 类型
        /// </summary>
        public virtual Admin.NET.Core.ContainerType? ContainerType { get; set; }
        
        /// <summary>
        /// 托盘状态
        /// </summary>
        public virtual Admin.NET.Core.ContainerStatus? ContainerStatus { get; set; }
        
        /// <summary>
        /// 长度
        /// </summary>
        public virtual decimal? SpecLength { get; set; }
        
        /// <summary>
        /// 宽度
        /// </summary>
        public virtual decimal? SpecWidth { get; set; }
        
        /// <summary>
        /// 高度
        /// </summary>
        public virtual decimal? SpecHeight { get; set; }
        
        /// <summary>
        /// 限长
        /// </summary>
        public virtual decimal? LimitLength { get; set; }
        
        /// <summary>
        /// 限宽
        /// </summary>
        public virtual decimal? LimitWidth { get; set; }
        
        /// <summary>
        /// 限高
        /// </summary>
        public virtual decimal? LimitHeight { get; set; }
        
        /// <summary>
        /// 载重上限
        /// </summary>
        public virtual decimal? MaxWeight { get; set; }
        
        /// <summary>
        /// 父托盘Id
        /// </summary>
        public virtual long? ParentContainerId { get; set; }
        
        /// <summary>
        /// 资产编号
        /// </summary>
        public virtual string AssetNo { get; set; }
        
        /// <summary>
        /// 托盘分类
        /// </summary>
        public virtual Admin.NET.Core.ContainerCategory? ContainerCategory { get; set; }
        
        /// <summary>
        /// Erp单号
        /// </summary>
        public virtual string ErpNo { get; set; }
        
        /// <summary>
        /// 是否虚拟
        /// </summary>
        public virtual Admin.NET.Core.YesOrNot IsVirtually { get; set; }
        
        /// <summary>
        /// 所属车间
        /// </summary>
        public virtual Admin.NET.Core.LesWorkShopType? WorkShopType { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState IssueState { get; set; }
        
    }

        /// <summary>
    /// 托盘信息不分页查询参数
    /// </summary>
    public class WmsContainerSearchNonPage : PageInputNonPageBase
    {
        /// <summary>
        /// 编号
        /// </summary>
        public virtual string ContainerCode { get; set; }
        
        /// <summary>
        /// 类型
        /// </summary>
        public virtual Admin.NET.Core.ContainerType? ContainerType { get; set; }
        
        /// <summary>
        /// 托盘状态
        /// </summary>
        public virtual Admin.NET.Core.ContainerStatus? ContainerStatus { get; set; }
        
        /// <summary>
        /// 长度
        /// </summary>
        public virtual decimal? SpecLength { get; set; }
        
        /// <summary>
        /// 宽度
        /// </summary>
        public virtual decimal? SpecWidth { get; set; }
        
        /// <summary>
        /// 高度
        /// </summary>
        public virtual decimal? SpecHeight { get; set; }
        
        /// <summary>
        /// 限长
        /// </summary>
        public virtual decimal? LimitLength { get; set; }
        
        /// <summary>
        /// 限宽
        /// </summary>
        public virtual decimal? LimitWidth { get; set; }
        
        /// <summary>
        /// 限高
        /// </summary>
        public virtual decimal? LimitHeight { get; set; }
        
        /// <summary>
        /// 载重上限
        /// </summary>
        public virtual decimal? MaxWeight { get; set; }
        
        /// <summary>
        /// 父托盘Id
        /// </summary>
        public virtual long? ParentContainerId { get; set; }
        
        /// <summary>
        /// 资产编号
        /// </summary>
        public virtual string AssetNo { get; set; }
        
        /// <summary>
        /// 托盘分类
        /// </summary>
        public virtual Admin.NET.Core.ContainerCategory? ContainerCategory { get; set; }
        
        /// <summary>
        /// Erp单号
        /// </summary>
        public virtual string ErpNo { get; set; }
        
        /// <summary>
        /// 是否虚拟
        /// </summary>
        public virtual Admin.NET.Core.YesOrNot? IsVirtually { get; set; }
        
        /// <summary>
        /// 所属车间
        /// </summary>
        public virtual Admin.NET.Core.LesWorkShopType? WorkShopType { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState? IssueState { get; set; }
        
    }

    /// <summary>
    /// 托盘信息输入参数
    /// </summary>
    public class WmsContainerInput
    {
        /// <summary>
        /// 编号
        /// </summary>
        public virtual string ContainerCode { get; set; }
        
        /// <summary>
        /// 类型
        /// </summary>
        public virtual Admin.NET.Core.ContainerType ContainerType { get; set; }
        
        /// <summary>
        /// 托盘状态
        /// </summary>
        public virtual Admin.NET.Core.ContainerStatus ContainerStatus { get; set; }
        
        /// <summary>
        /// 长度
        /// </summary>
        public virtual decimal SpecLength { get; set; }
        
        /// <summary>
        /// 宽度
        /// </summary>
        public virtual decimal SpecWidth { get; set; }
        
        /// <summary>
        /// 高度
        /// </summary>
        public virtual decimal SpecHeight { get; set; }
        
        /// <summary>
        /// 限长
        /// </summary>
        public virtual decimal LimitLength { get; set; }
        
        /// <summary>
        /// 限宽
        /// </summary>
        public virtual decimal LimitWidth { get; set; }
        
        /// <summary>
        /// 限高
        /// </summary>
        public virtual decimal LimitHeight { get; set; }
        
        /// <summary>
        /// 载重上限
        /// </summary>
        public virtual decimal MaxWeight { get; set; }
        
        /// <summary>
        /// 父托盘Id
        /// </summary>
        public virtual long ParentContainerId { get; set; }
        
        /// <summary>
        /// 资产编号
        /// </summary>
        public virtual string AssetNo { get; set; }
        
        /// <summary>
        /// 托盘分类
        /// </summary>
        public virtual Admin.NET.Core.ContainerCategory ContainerCategory { get; set; }
        
        /// <summary>
        /// Erp单号
        /// </summary>
        public virtual string ErpNo { get; set; }
        
        /// <summary>
        /// 是否虚拟
        /// </summary>
        public virtual Admin.NET.Core.YesOrNot IsVirtually { get; set; }
        
        /// <summary>
        /// 所属车间
        /// </summary>
        public virtual Admin.NET.Core.LesWorkShopType WorkShopType { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState IssueState { get; set; }
        
    }

    public class AddWmsContainerInput : WmsContainerInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "数量不能为空")]
        public long Quantity { get; set; }
    }

    public class DeleteWmsContainerInput : BaseId
    {
    }

    public class UpdateWmsContainerInput : WmsContainerInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
        
    }

    public class QueryeWmsContainerInput : BaseId
    {

    }
}
