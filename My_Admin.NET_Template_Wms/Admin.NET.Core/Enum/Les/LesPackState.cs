﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 打包状态
    /// </summary>
    public enum LesPackState
    {
        /// <summary>
        /// 停用
        /// </summary>
        [Description("停用")]
        TINGYONG = 0,
        /// <summary>
        /// 未出库
        /// </summary>
        [Description("未出库")]
        WEICHUKU = 1,

        /// <summary>
        /// 转移库中
        /// </summary>
        [Description("转移库中")]
        ZHUANYIKU = 2
    }
}
