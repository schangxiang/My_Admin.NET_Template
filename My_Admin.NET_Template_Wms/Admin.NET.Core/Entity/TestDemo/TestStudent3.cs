﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

/*
 * @author : 您的名字
 * @date : 2024/4/25上午11:08:30
 * @desc : 学生信息表(测试可空字段)
 */
namespace Admin.NET.Core
{
    /// <summary>
    /// 学生信息表(测试可空字段)
    /// </summary>
    [Table("test_students3")]
    [Comment("学生信息表(测试可空字段)")]
    public class TestStudent3 : DEntityBase
            
    {


        /// <summary>
        /// 学生姓名
        /// </summary>
        [Comment("学生姓名")]
        [MaxLength(255)]
        public string Name { get; set; }


        /// <summary>
        /// 学生年龄
        /// </summary>
        [Comment("学生年龄")]

        public int? Age { get; set; }


        /// <summary>
        /// 零花钱
        /// </summary>
        [Comment("零花钱")]

        [Column("HasMoney", TypeName = "decimal(10,3)")]
        public decimal? HasMoney { get; set; }


        /// <summary>
        /// 是否在校
        /// </summary>
        [Comment("是否在校")]

        public bool? StartName { get; set; }


        /// <summary>
        /// 性别
        /// </summary>
        [Comment("性别")]

        public GenderChinese? Gender { get; set; }


        /// <summary>
        /// 出生日期
        /// </summary>
        [Comment("出生日期")]

        public DateTime? BrithDate { get; set; }









    }
}