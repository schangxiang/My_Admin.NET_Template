﻿using Admin.NET.Core;
using Furion.FriendlyException;
using System.Collections.Generic;

namespace Admin.NET.Application
{
    /// <summary>
    /// 代码生成帮助类
    /// </summary>
    public class CodeGenHelper
    {

        /// <summary>
        /// 校验 代码配置
        /// </summary>
        /// <param name="list"></param>
        public static void ValidateCodeGenConfig(List<SysCodeGenConfig> list)
        {
            //如果是下拉框，那么就必须选择字典！【Editby shaocx,2024-04-16】
            foreach (var item in list)
            {
                if (item.EffectType == "select" && string.IsNullOrEmpty(item.DictTypeCode))
                {
                    throw Oops.Oh($"字段{item.ColumnName}的作用类型是'下拉框',字典必须选择！");
                }
                if (item.ColumnName.ToLower() != "id" && item.WhetherRequired == YesOrNot.Y.ToString() && item.WhetherAddUpdate == YesOrNot.N.ToString())
                {
                    throw Oops.Oh($"字段{item.ColumnName}必填字段必须配置增改！");
                }
            }
        }

        /// <summary>
        /// 校验 代码配置
        /// </summary>
        /// <param name="list"></param>
        public static void ValidateCodeGenConfig(List<CodeGenConfig> list)
        {
            //如果是下拉框，那么就必须选择字典！【Editby shaocx,2024-04-16】
            foreach (var item in list)
            {
                if (item.EffectType == "select" && string.IsNullOrEmpty(item.DictTypeCode))
                {
                    throw Oops.Oh($"字段{item.ColumnName}的作用类型是'下拉框',字典必须选择！");
                }
                if (item.ColumnName.ToLower() != "id" && item.WhetherRequired == YesOrNot.Y.ToString() && item.WhetherAddUpdate == YesOrNot.N.ToString())
                {
                    throw Oops.Oh($"字段{item.ColumnName}必填字段必须配置增改！");
                }
            }
        }
    }
}
