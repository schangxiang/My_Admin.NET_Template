﻿using Furion;
using Furion.DatabaseAccessor;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Admin.NET.Core.Service;

namespace Admin.NET.EntityFramework.Core
{
    public class Startup : AppStartup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDatabaseAccessor(options =>
            {
                options.CustomizeMultiTenants(); // 自定义租户

                options.AddDbPool<DefaultDbContext>(providerName: default, optionBuilder: (services, opt) =>
                {
                    opt.UseBatchEF_MSSQL(); // EF批量组件 --- SQlite数据库包
                });
                //options.AddDbPool<ZdDbContext,ZdDbContextLocation>(DbProvider.SqlServer);
                //options.AddDbPool<MultiTenantDbContext, MultiTenantDbContextLocator>();
            }, "Admin.NET.Database.Migrations");
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //// 自动迁移数据库（update-database命令）
            //if (env.IsDevelopment())
            //{
            //    Scoped.Create((_, scope) =>
            //    {
            //        var context = scope.ServiceProvider.GetRequiredService<DefaultDbContext>();
            //        context.Database.Migrate();
            //    });
            //}
        }
    }
}