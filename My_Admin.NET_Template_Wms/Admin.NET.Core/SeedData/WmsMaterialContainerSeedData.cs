﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;

namespace YaKe.Les.Core.SeedData
{
    public class WmsMaterialContainerSeedData : IEntitySeedData<WmsMaterialContainer>
    {
        /// <summary>
        /// 物料托盘关系关系表种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<WmsMaterialContainer> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new[]
            {
                new WmsMaterialContainer{Id=358856845353031,ContainerId=358852767887429,ContainerCode="TM00002",MaterialName="气动拧松枪",MaterialNo="3070400120",MaterialBatch="3070400120",MaterialSpec="16SUP PRO",MaterialId=358845906632773,BindQuantity=(decimal)100.00,BindStatus=CommonStatus.DELETED,SampleQty=(decimal)0.00,OrderNo="358856845353029"},
                new WmsMaterialContainer{Id=358856980582469,ContainerId=358852767887429,ContainerCode="TM00002",MaterialName="气动拧松枪",MaterialNo="3070400120",MaterialBatch="3070400120",MaterialSpec="16SUP PRO",MaterialId=358845906632773,BindQuantity=(decimal)100.00,BindStatus=CommonStatus.DISABLE,SampleQty=(decimal)0.00,OrderNo="358856980553798"},
                new WmsMaterialContainer{Id=358857030975557,ContainerId=358852767887429,ContainerCode="TM00002",MaterialName="气动拧松枪",MaterialNo="3070400120",MaterialBatch="3070400120",MaterialSpec="16SUP PRO",MaterialId=358845906632773,BindQuantity=(decimal)100.00,BindStatus=CommonStatus.ENABLE,SampleQty=(decimal)0.00,OrderNo="358857030971461"},
                new WmsMaterialContainer{Id=358857618075719,ContainerId=358853016055877,ContainerCode="TM00004",MaterialName="气动拧松枪",MaterialNo="3070400120",MaterialBatch="3070400120",MaterialSpec="16SUP PRO",MaterialId=358845906632773,BindQuantity=(decimal)100.00,BindStatus=CommonStatus.ENABLE,SampleQty=(decimal)0.00,OrderNo="358857618075717"},
                new WmsMaterialContainer{Id=358857618153542,ContainerId=358853016055877,ContainerCode="TM00004",MaterialName="定位销",MaterialNo="3110800160",MaterialBatch="3110800160",MaterialSpec="YDL31-D8-P12-L18-B8-X",MaterialId=358847125188677,BindQuantity=(decimal)100.00,BindStatus=CommonStatus.ENABLE,SampleQty=(decimal)0.00,OrderNo="358857618075717"},
                new WmsMaterialContainer{Id=358858261872714,ContainerId=358853070798917,ContainerCode="TM00005",MaterialName="电机",MaterialNo="1222202451",MaterialBatch="1222202451",MaterialSpec="LA21038-020.01-01.10",MaterialId=358851158249541,BindQuantity=(decimal)10.00,BindStatus=CommonStatus.ENABLE,SampleQty=(decimal)0.00,OrderNo="358858261872712"},
                new WmsMaterialContainer{Id=358858262044742,ContainerId=358853070798917,ContainerCode="TM00005",MaterialName="传感器安装支架",MaterialNo="2200074102",MaterialBatch="2200074102",MaterialSpec="40488-020-D086",MaterialId=358848226168901,BindQuantity=(decimal)1.00,BindStatus=CommonStatus.ENABLE,SampleQty=(decimal)0.00,OrderNo="358858261872712"},
                new WmsMaterialContainer{Id=358858262089798,ContainerId=358853070798917,ContainerCode="TM00005",MaterialName="套筒收纳盒安装板",MaterialNo="2200074125",MaterialBatch="2200074125",MaterialSpec="4301015-40-110-04-01",MaterialId=358847917588549,BindQuantity=(decimal)10.00,BindStatus=CommonStatus.ENABLE,SampleQty=(decimal)0.00,OrderNo="358858261872712"}
            };
        }
    }
}
