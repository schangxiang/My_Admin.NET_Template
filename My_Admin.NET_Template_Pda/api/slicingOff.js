import request from '@/api/api.js'
import qs from 'qs'


// 空托出库 /api/callempty/PdaContainerOut
export function PdaContainerOut (data) {
  return request({
    url: `/api/callempty/PdaContainerOut`,
    method: 'post',
	data
  })
}