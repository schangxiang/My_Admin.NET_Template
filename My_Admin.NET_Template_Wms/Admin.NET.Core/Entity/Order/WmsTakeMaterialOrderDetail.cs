﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    /// <summary>
    /// 单据明细表
    /// </summary>
    [Table("wms_takematerial_orderdetail")]
    [Comment("单据明细表")]
    public class WmsTakeMaterialOrderDetail : DEntityBase
    {
        /// <summary>
        /// 单据Id  
        /// </summary>
        [Comment("单据Id")]
        [MaxLength(50)]
        public long OrderId { get; set; }

        /// <summary>
        /// SCM领料申请单明细行唯一标识
        /// </summary>
        [Comment("SCM领料申请单明细行唯一标识")]
        public long OID { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        [Comment("物料编号")]
        [MaxLength(255)]
        public string Materialcode { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        [Comment("物料名称")]
        [MaxLength(255)]
        public string Materialname { get; set; }

        /// <summary>
        /// 基本单位    
        /// </summary>
        [Comment("基本单位")]
        [MaxLength(255)]
        public string Unit { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        [Comment("数量")]
        public decimal Qty { get; set; }

        /// <summary>
        /// 下发数量
        /// </summary>
        [Comment("下发数量")]
        public decimal DistributeQty { get; set; }

        /// <summary>
        /// 单根长度 
        /// </summary>
        [Comment("单根长度")]
        public decimal SingleLength { get; set; }

        /// <summary>
        /// 件数    
        /// </summary>
        [Comment("件数")]
        public decimal Number { get; set; }

        /// <summary>
        /// SCM批次号    
        /// </summary>
        [Comment("SCM批次号")]
        [MaxLength(255)]
        public string Batchno_SCM { get; set; }

        /// <summary>
        /// WMS批次号    
        /// </summary>
        [Comment("WMS批次号")]
        public string Batchno_WMS { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        [Comment("项目编号")]
        [MaxLength(255)]
        public string ProjectCode { get; set; }

        /// <summary>
        /// TA号
        /// </summary>
        [Comment("TA号")]
        [MaxLength(255)]
        public string TACode { get; set; }

        /// <summary>
        /// 分段号
        /// </summary>
        [Comment("分段号")]
        [MaxLength(255)]
        public string PartCode { get; set; }

        /// <summary>
        /// 单据状态
        /// </summary>
        [Comment("单据状态")]
        [Required]
        public OrderDetailsStatusEnum OrderStatus { get; set; } = OrderDetailsStatusEnum.WEIZHIXING;

        /// <summary>
        /// 托盘编码
        /// </summary>
        [Comment("托盘编码")]
        [MaxLength(50)]
        public string ContainerCode { get; set; }

        /// <summary>
        /// 库位编码
        /// </summary>
        [Comment("库位编码")]
        [MaxLength(50)]
        public string PlaceCode { get; set; }

        /// <summary>
        /// 单据主表
        /// </summary>
        public WmsTakeMaterialOrder WmsTakeMaterialOrder { get; set; }
    }
}
