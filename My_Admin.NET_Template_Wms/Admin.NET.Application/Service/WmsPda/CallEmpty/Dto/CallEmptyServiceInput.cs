﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 空托出库入参
    /// </summary>
    public class PdaContainerOutInput
    {
        /// <summary>
        /// 数量
        /// </summary>
        public int Qty { get; set; }
    }
}
