﻿using Microsoft.AspNetCore.Mvc;

namespace Admin.NET.Application
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISysEnumDataService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<dynamic> GetEnumDataList([FromQuery] EnumDataInput input);

        /// <summary>
        /// 通过枚举类型获取枚举值字符串集合
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<string> GetEnumDataListStrAsync([FromQuery] EnumDataInput input);

        /// <summary>
        /// 通过枚举类型获取枚举值字符串集合
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        string GetEnumDataListStr([FromQuery] EnumDataInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<dynamic> GetEnumDataListByField([FromQuery] QueryEnumDataInput input);
    }
}