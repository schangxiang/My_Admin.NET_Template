﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 库区类型
    /// </summary>
    public enum AreaType
    {
        /// <summary>
        /// 立体库
        /// </summary>
        [Description("立体库")]
        LITIKU = 1,

        /// <summary>
        /// 普通库
        /// </summary>
        [Description("普通库")]
        PUTONGK = 2,

        /// <summary>
        /// 公共库
        /// </summary>
        [Description("外库")]
        WAIKU = 3,

        /// <summary>
        /// 缓存库
        /// </summary>
        [Description("缓存库")]
        HUANCUNKU = 4,

        /// <summary>
        /// AGV站点
        /// </summary>
        [Description("AGV站点")]
        AGV = 5,
        /// <summary>
        /// 转移库
        /// </summary>
        [Description("转移库")]
        ZHUANYIKU = 6
    }

    /// <summary>
    /// 库位状态
    /// </summary>
    public enum PlaceStatus
    {
        /// <summary>
        /// 空闲
        /// </summary>
        [Description("空闲")]
        KONGXIAN = 1,

        /// <summary>
        /// 待入
        /// </summary>
        [Description("待入")]
        DAIRU = 2,

        /// <summary>
        /// 存货
        /// </summary>
        [Description("存货")]
        CUNHUO = 3,

        /// <summary>
        /// 待出
        /// </summary>
        [Description("待出")]
        DAICHU = 4,
    }

    /// <summary>
    /// 库位高度属性
    /// </summary>
    public enum Heightlevel
    {
        /// <summary>
        /// 低
        /// </summary>
        [Description("低")]
        DI = 1,

        /// <summary>
        /// 中
        /// </summary>
        [Description("中")]
        ZHONG = 2,

        /// <summary>
        /// 高
        /// </summary>
        [Description("高")]
        GAO = 3
    }
}
