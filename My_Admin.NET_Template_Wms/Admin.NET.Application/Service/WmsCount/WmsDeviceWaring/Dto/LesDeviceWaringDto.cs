﻿using Admin.NET.Core.Util.LowCode.Dto;
using System;

namespace Admin.NET.Application
{
    /// <summary>
    /// 设备报警输出参数
    /// </summary>
    public class LesDeviceWaringDto
    {
        /// <summary>
        /// WcsId
        /// </summary>
        public int WcsId { get; set; }
        
        /// <summary>
        /// 设备名称
        /// </summary>
        public string DeviceName { get; set; }
        
        /// <summary>
        /// 故障名称
        /// </summary>
        public string FaultName { get; set; }
        
        /// <summary>
        /// 发生时间
        /// </summary>
        public DateTimeOffset StartTime { get; set; }
        
        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTimeOffset EndTime { get; set; }
        
        /// <summary>
        /// 持续时间
        /// </summary>
        public int RunningTime { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public Admin.NET.Core.IssueState IssueState { get; set; }
        
    }
}
