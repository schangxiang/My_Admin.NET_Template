﻿using Mapster;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    public class WmsReceiptOrderMapper : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<AddWmsReceiptOrderInput, WmsReceiptOrder>()
            ;
            config.ForType<UpdateWmsReceiptOrderInput, WmsReceiptOrder>()
            ;
            config.ForType<WmsReceiptOrder, WmsReceiptOrderOutput>()
            ;
        }
    }
}
