﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    /// <summary>
    /// 库区表
    /// </summary>
    [Table("wms_area")]
    [Comment("库区表")]
    public class WmsArea : DEntityBase, IEntityTypeBuilder<WmsArea>
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Comment("名称")]
        [Required]
        [MaxLength(50)]
        public string AreaName { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Comment("描述")]
        [MaxLength(250)]
        public string AreaDesc { get; set; }

        /// <summary>
        /// 状态;数据字典
        /// </summary>
        [Comment("状态")]
        [Required]
        public CommonStatus AreaStatus { get; set; }

        /// <summary>
        /// 分类;数据字典
        /// </summary>
        [Comment("分类")]
        [Required]
        public AreaType AreaType { get; set; }

        /// <summary>
        /// 是否为钢平台
        /// </summary>
        [Comment("是否为钢平台")]
        public bool IsSteel { get; set; }

        /// <summary>
        /// 所属车间
        /// </summary>
        [Comment("所属车间")]
        public LesWorkShopType WorkShopType { get; set; }

        /// <summary>
        /// 库位表
        /// </summary>
        public ICollection<WmsPlace> WmsPlaces { get; set; }

        /// <summary>
        /// 库口表
        /// </summary>
        public ICollection<LesEntrance> LesEntrance { get; set; }

        /// <summary>
        /// 构建一对多的关系
        /// </summary>
        /// <param name="entityBuilder"></param>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        public void Configure(EntityTypeBuilder<WmsArea> entityBuilder, DbContext dbContext, Type dbContextLocator)
        {
            entityBuilder.HasMany(x => x.WmsPlaces)
                .WithOne(x => x.WmsArea)
                .HasForeignKey(x => x.AreaId).IsRequired(true);
            entityBuilder.HasMany(x => x.LesEntrance)
                .WithOne(x => x.WmsArea)
                .HasForeignKey(x => x.AreaId).IsRequired(true);
        }
    }
}
