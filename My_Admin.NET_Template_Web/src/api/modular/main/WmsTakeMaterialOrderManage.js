﻿import { axios } from '@/utils/request'

/**
 * 查询领料单
 *
 * @author XJF
 */
export function WmsTakeMaterialOrderPage (parameter) {
  return axios({
    url: '/WmsTakeMaterialOrder/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 领料单列表
 *
 * @author XJF
 */
export function WmsTakeMaterialOrderList (parameter) {
  return axios({
    url: '/WmsTakeMaterialOrder/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加领料单
 *
 * @author XJF
 */
export function WmsTakeMaterialOrderAdd (parameter) {
  return axios({
    url: '/WmsTakeMaterialOrder/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑领料单
 *
 * @author XJF
 */
export function WmsTakeMaterialOrderEdit (parameter) {
  return axios({
    url: '/WmsTakeMaterialOrder/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除领料单
 *
 * @author XJF
 */
export function WmsTakeMaterialOrderDelete (parameter) {
  return axios({
    url: '/WmsTakeMaterialOrder/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出领料单的Excel文件
 *
 * @author XJF
 */
export function WmsTakeMaterialOrderToExcel (parameter) {
  return axios({
    url: '/WmsTakeMaterialOrder/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入领料单的Excel文件
 *
 * @author XJF
 */
export function WmsTakeMaterialOrderFromExcel (data, parameter) {
  return axios({
    url: '/WmsTakeMaterialOrder/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载领料单的Excel导入模板
 *
 * @author XJF
 */
export function WmsTakeMaterialOrderDownloadExcelTemplate(parameter) {
  return axios({
    url: '/WmsTakeMaterialOrder/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}







/**
 * 物料详情
 */
 export function WmsOrderLldDetail (parameter) {
  return axios({
    url: '/wmstakematerialorder/PageDetail',
    method: 'get',
    params: parameter
  })
}

/**
 * 领料单下发
 *
 * @author yhh
 */
 export function Distribute (ids) {
  return axios({
    url: '/wmstakematerialorder/DistributeOrder',
    method: 'post',
    data:{id:ids}
  })
}
