import request from '@/api/api.js'
import qs from 'qs'
// 

// 获取托盘信息 
export function GetContainer (data) {
  return request({
    url: `/api/wmsstockreturnwarehouse/GetContainer?${qs.stringify(data)}`,
    method: 'get'
  })
}

// 获取单据信息
export function GetMaterialPad (data) {
  return request({
    url: `/api/wmsstockreturnwarehouse/PadGetMaterial?${qs.stringify(data)}`,
    method: 'get'
  })
}

// 组盘
export function GroupDisk (data) {
  return request({
    url: `/api/wmsstockreturnwarehouse/GroupDisk`,
    method: 'post',
	data
  })
}

// 入库
export function PdaInWarehouse (data) {
  return request({
    url: `/api/wmsstockreturnwarehouse/PdaInWarehouse`,
    method: 'post',
	data
  })
}


