﻿using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 库口表输出参数
    /// </summary>
    public class WmsWarehouseEntranceOutput
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 类型-1.入库口_2.出库口
        /// </summary>
        public WarehouseEntranceEnum Type { get; set; }

        /// <summary>
        /// 所属车间
        /// </summary>
        public LesWorkShopType LesWorkShopType { get; set; }
        
        /// <summary>
        /// 所属巷道
        /// </summary>
        public int AffiliatedRoadway { get; set; }
        
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        
        /// <summary>
        /// 状态
        /// </summary>
        public CommonStatus Status { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
    }
}
