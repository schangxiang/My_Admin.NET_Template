﻿namespace Admin.NET.Application
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISysUserRoleService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        Task DeleteUserRoleListByRoleId(long roleId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task DeleteUserRoleListByUserId(long userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="orgId"></param>
        /// <returns></returns>
        Task<List<long>> GetUserRoleDataScopeIdList(long userId, long orgId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="checkRoleStatus"></param>
        /// <returns></returns>
        Task<List<long>> GetUserRoleIdList(long userId, bool checkRoleStatus = true);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task GrantRole(UpdateUserRoleDataInput input);
    }
}