﻿import { axios } from '@/utils/request'

/**
 * 查询Excel模板
 *
 * @author liuwq
 */
export function SysExcelTemplatePage (parameter) {
  return axios({
    url: '/SysExcelTemplate/page',
    method: 'get',
    params: parameter
  })
}

/**
 * Excel模板列表
 *
 * @author liuwq
 */
export function SysExcelTemplateList (parameter) {
  return axios({
    url: '/SysExcelTemplate/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加Excel模板
 *
 * @author liuwq
 */
export function SysExcelTemplateAdd (parameter) {
  return axios({
    url: '/SysExcelTemplate/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑Excel模板
 *
 * @author liuwq
 */
export function SysExcelTemplateEdit (parameter) {
  return axios({
    url: '/SysExcelTemplate/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除Excel模板
 *
 * @author liuwq
 */
export function SysExcelTemplateDelete (parameter) {
  return axios({
    url: '/SysExcelTemplate/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出Excel模板的Excel文件
 *
 * @author liuwq
 */
export function SysExcelTemplateToExcel (parameter) {
  return axios({
    url: '/SysExcelTemplate/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入Excel模板的Excel文件
 *
 * @author liuwq
 */
export function SysExcelTemplateFromExcel (data, parameter) {
  return axios({
    url: '/SysExcelTemplate/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载Excel模板的Excel导入模板
 *
 * @author liuwq
 */
export function SysExcelTemplateDownloadExcelTemplate(parameter) {
  return axios({
    url: '/SysExcelTemplate/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}



