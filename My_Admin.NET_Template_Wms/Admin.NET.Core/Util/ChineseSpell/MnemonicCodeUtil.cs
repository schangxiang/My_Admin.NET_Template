using Furion;
using System.Text;

namespace Admin.NET.Core
{
    /// <summary>
    /// 生成助记码
    /// </summary>
    public class MnemonicCodeUtil
    {
        private struct ItemWord
        {
            public int numFlag;     //是否多音字
            public char strWord;    //当前单词
            public string strSpell1;//输入码1
            public string strSpell2;//输入码2
            public string strExt;   //多音字保留串
            public string strName;  //用于定义姓氏码
        }

        private static readonly int INTMAX = 65536;
        private static ItemWord[] stWord = new ItemWord[INTMAX];
        private static Boolean b_LoadData = false;

        //读取所有汉字
        public static Boolean fun_LoadWords()
        {
            int i, numIndex;
            int numCount1, numCount2;
            string strFileName;
            char[] chrWord;

            if (b_LoadData)
                return true;

            //以下初始化所有的数据
            for (i = 0; i < INTMAX; i++)
            {
                stWord[i].numFlag = 0;
                stWord[i].strSpell1 = "";
                stWord[i].strSpell2 = "";
                stWord[i].strName = "";
                stWord[i].strExt = "|";
            }

            var rootPath = System.IO.Path.Combine(App.WebHostEnvironment.WebRootPath, "ChineseSpellFile");

            //读取汉字拼音
            strFileName = rootPath + @"\hzpy1.txt";
            StreamReader srFile = new StreamReader(strFileName, Encoding.ASCII);
            string strInput = null;

            numCount1 = 0;
            while ((strInput = srFile.ReadLine()) != null)
            {
                chrWord = strInput.ToCharArray();
                numIndex = (int)chrWord[0];

                if (numIndex <= 0) continue;
                if (numIndex >= INTMAX) continue;

                stWord[numIndex].strWord = chrWord[0];
                stWord[numIndex].strSpell1 = chrWord[2].ToString();
                numCount1++;
            }
            srFile.Close();

            //读取多音字
            strFileName = rootPath + @"\hzpy2.txt";
            srFile = new StreamReader(strFileName, Encoding.ASCII);

            while ((strInput = srFile.ReadLine()) != null)
            {
                chrWord = strInput.ToCharArray();
                numIndex = (int)chrWord[0];

                if (numIndex <= 0) continue;
                if (numIndex >= INTMAX) continue;

                stWord[numIndex].numFlag = 1;
                strInput = strInput.Substring(2);
                strInput = strInput.Replace("\t", " ");

                while (strInput.IndexOf("  ") >= 0)
                {
                    strInput = strInput.Replace("  ", " ");
                }
                strInput = strInput.Replace(" ", "/");
                stWord[numIndex].strExt = stWord[numIndex].strExt + strInput + "|";
            }
            srFile.Close();

            //读取姓氏
            strFileName = rootPath + @"\hzpy3.txt";
            srFile = new StreamReader(strFileName, Encoding.ASCII);

            while ((strInput = srFile.ReadLine()) != null)
            {
                chrWord = strInput.ToCharArray();
                numIndex = (int)chrWord[0];

                if (numIndex <= 0) continue;
                if (numIndex >= INTMAX) continue;

                stWord[numIndex].numFlag = 1;
                strInput = strInput.Substring(2);
                strInput = strInput.Replace("\t", " ");

                while (strInput.IndexOf("  ") >= 0)
                {
                    strInput = strInput.Replace("  ", " ");
                }
                stWord[numIndex].strName = strInput;
            }
            srFile.Close();

            //以下部分读取五笔码的首字符
            strFileName = rootPath + @"\hzwb.txt";
            numCount2 = 0;
            srFile = new StreamReader(strFileName, Encoding.ASCII);

            while ((strInput = srFile.ReadLine()) != null)
            {
                chrWord = strInput.ToCharArray();
                numIndex = (int)chrWord[0];

                if (numIndex <= 0) continue;
                if (numIndex >= INTMAX) continue;
                stWord[numIndex].strWord = chrWord[0];
                stWord[numIndex].strSpell2 = chrWord[1].ToString();
                numCount2++;
            }
            srFile.Close();

            b_LoadData = true;
            return true;
        }
        private static string funFindMulti(int numIndex, char ch1, char ch2)
        {
            int numPos;
            string strWord;

            strWord = "|" + ch1 + ch2 + "/";
            numPos = stWord[numIndex].strExt.IndexOf(strWord);
            if (numPos >= 0)
                return stWord[numIndex].strExt.Substring(numPos + strWord.Length, 1);
            return "";
        }

        /// <summary>
        /// 助记码(拼音)
        /// </summary>
        /// <param name="strChinese"></param>
        /// <param name="IsName"></param>
        /// <returns></returns>
        public static string funChineseSpell(string strChinese, bool IsName)
        {
            string strSpell, strThis;
            int i, numCount, numIndex;
            char[] chrWord;

            strSpell = "";
            chrWord = strChinese.ToCharArray();
            numCount = strChinese.Length;

            //2010-03-19 依次处理各汉字的拼音码
            for (i = 0; i < numCount; i++)
            {
                numIndex = (int)chrWord[i];

                if (numIndex <= 0) continue;
                if (numIndex >= INTMAX) continue;


                //2010-03-19 是否多音字
                strThis = "";
                if (stWord[numIndex].numFlag > 0 && !IsName)
                {
                    if (i > 0)
                    {
                        strThis = funFindMulti(numIndex, chrWord[i - 1], chrWord[i]);
                    }
                    if (strThis.Length == 0)
                    {
                        if (i < numCount - 1)
                            strThis = funFindMulti(numIndex, chrWord[i], chrWord[i + 1]);
                    }

                }

                //2010-04-02 按姓名进行处理
                if (IsName && i == 0 && stWord[numIndex].strName.Length > 0)
                    strThis = stWord[numIndex].strName;

                //2010-03-20 未找着时是标准码
                if (strThis.Length == 0)
                    strThis = stWord[numIndex].strSpell1.ToString();
                strSpell = strSpell + strThis;
            }
            return strSpell;
        }

        /// <summary>
        /// 助记码(五笔)
        /// </summary>
        /// <param name="strChinese"></param>
        /// <returns></returns>
        public string funWbzxSpell(string strChinese)
        {
            string strSpell = "";
            int i, numCount, numIndex;
            char[] chrWord;

            strSpell = "";
            chrWord = strChinese.ToCharArray();
            numCount = strChinese.Length;

            //2010-03-19 依次处理各汉字的五笔编码
            for (i = 0; i < numCount; i++)
            {
                numIndex = (int)chrWord[i];

                if (numIndex <= 0) continue;
                if (numIndex >= INTMAX) continue;

                strSpell = strSpell + stWord[numIndex].strSpell2.ToString();
            }
            return strSpell;
        }
    }

}