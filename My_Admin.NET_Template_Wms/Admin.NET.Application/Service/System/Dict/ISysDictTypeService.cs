﻿using Admin.NET.Core;
using Microsoft.AspNetCore.Mvc;

namespace Admin.NET.Application
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISysDictTypeService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task AddDictType(AddDictTypeInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task ChangeDictTypeStatus(ChangeStateDictTypeInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task DeleteDictType(DeleteDictTypeInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<List<DictTreeOutput>> GetDictTree();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<SysDictType> GetDictType([FromQuery] QueryDictTypeInfoInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<List<SysDictData>> GetDictTypeDropDown([FromQuery] DropDownDictTypeInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<List<SysDictType>> GetDictTypeList();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PageResult<SysDictType>> QueryDictTypePageList([FromQuery] DictTypePageInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task UpdateDictType(UpdateDictTypeInput input);
    }
}