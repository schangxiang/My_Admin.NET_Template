﻿import { axios } from '@/utils/request'

/**
 * 查询PDA菜单
 *
 * @author ldp
 */
export function WmsPdaPowerPage (parameter) {
  return axios({
    url: '/WmsPdaPower/page',
    method: 'get',
    params: parameter
  })
}

/**
 * PDA菜单列表
 *
 * @author ldp
 */
export function WmsPdaPowerList (parameter) {
  return axios({
    url: '/WmsPdaPower/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加PDA菜单
 *
 * @author ldp
 */
export function WmsPdaPowerAdd (parameter) {
  return axios({
    url: '/WmsPdaPower/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑PDA菜单
 *
 * @author ldp
 */
export function WmsPdaPowerEdit (parameter) {
  return axios({
    url: '/WmsPdaPower/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除PDA菜单
 *
 * @author ldp
 */
export function WmsPdaPowerDelete (parameter) {
  return axios({
    url: '/WmsPdaPower/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出PDA菜单的Excel文件
 *
 * @author ldp
 */
export function WmsPdaPowerToExcel (parameter) {
  return axios({
    url: '/WmsPdaPower/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入PDA菜单的Excel文件
 *
 * @author ldp
 */
export function WmsPdaPowerFromExcel (data, parameter) {
  return axios({
    url: '/WmsPdaPower/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载PDA菜单的Excel导入模板
 *
 * @author ldp
 */
export function WmsPdaPowerDownloadExcelTemplate(parameter) {
  return axios({
    url: '/WmsPdaPower/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}



