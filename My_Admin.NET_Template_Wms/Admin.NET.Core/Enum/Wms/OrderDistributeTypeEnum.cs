﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 单据下发类型-全自动；半自动
    /// </summary>
    public enum OrderDistributeTypeEnum
    {
        /// <summary>
        /// 全自动
        /// </summary>
        [Description("全自动")]
        QUANZIDONG = 1,

        /// <summary>
        /// 半自动
        /// </summary>
        [Description("半自动")]
        BANZIDONG = 2
    }
}
