﻿using Admin.NET.Core;
using Admin.NET.Core.Service;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Admin.NET.Application
{    
    
    /// <summary>
    /// 领料单查询参数
    /// </summary>
    public class WmsTakeMaterialOrderSearch : PageInputBase
    {
        /// <summary>
        /// 单据大类
        /// </summary>
        public virtual long? OrderLargeCategory { get; set; }
        
        /// <summary>
        /// 单据小类
        /// </summary>
        public virtual long? OrderSubclass { get; set; }
        
        /// <summary>
        /// 事务类型
        /// </summary>
        public virtual string MoveType { get; set; }
        
        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        public virtual long? SOID { get; set; }
        
        /// <summary>
        /// 单据编号
        /// </summary>
        public virtual string NO { get; set; }
        
        /// <summary>
        /// 领料单申请日期
        /// </summary>
        public virtual DateTimeOffset? Billdate { get; set; }
        
        /// <summary>
        /// 领用项目号
        /// </summary>
        public virtual string WBSElementcode { get; set; }
        
        /// <summary>
        /// 领料部门
        /// </summary>
        public virtual string BenefitingDepartcode { get; set; }
        
        /// <summary>
        /// 受益部门
        /// </summary>
        public virtual string CostCenterID { get; set; }
        
        /// <summary>
        /// 客户
        /// </summary>
        public virtual string FI_Client_Analysis_H { get; set; }
        
        /// <summary>
        /// 是否公司间交易
        /// </summary>
        public virtual long? IsInnerCompany { get; set; }
        
        /// <summary>
        /// 领料人
        /// </summary>
        public virtual string PickerID { get; set; }
        
        /// <summary>
        /// 仓储中心
        /// </summary>
        public virtual string WarehouseCentername { get; set; }
        
        /// <summary>
        /// 公司
        /// </summary>
        public virtual string Companyname { get; set; }
        
        /// <summary>
        /// 施工队
        /// </summary>
        public virtual string ConstructionTeamID { get; set; }
        
        /// <summary>
        /// 单据状态
        /// </summary>
        public virtual Admin.NET.Core.OrderStatusEnum? OrderStatus { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState IssueState { get; set; }
        
    }

        /// <summary>
    /// 领料单不分页查询参数
    /// </summary>
    public class WmsTakeMaterialOrderSearchNonPage : PageInputNonPageBase
    {
        /// <summary>
        /// 单据大类
        /// </summary>
        public virtual long? OrderLargeCategory { get; set; }
        
        /// <summary>
        /// 单据小类
        /// </summary>
        public virtual long? OrderSubclass { get; set; }
        
        /// <summary>
        /// 事务类型
        /// </summary>
        public virtual string? MoveType { get; set; }
        
        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        public virtual long? SOID { get; set; }
        
        /// <summary>
        /// 单据编号
        /// </summary>
        public virtual string? NO { get; set; }
        
        /// <summary>
        /// 领料单申请日期
        /// </summary>
        public virtual DateTimeOffset? Billdate { get; set; }
        
        /// <summary>
        /// 领用项目号
        /// </summary>
        public virtual string? WBSElementcode { get; set; }
        
        /// <summary>
        /// 领料部门
        /// </summary>
        public virtual string? BenefitingDepartcode { get; set; }
        
        /// <summary>
        /// 受益部门
        /// </summary>
        public virtual string? CostCenterID { get; set; }
        
        /// <summary>
        /// 客户
        /// </summary>
        public virtual string? FI_Client_Analysis_H { get; set; }
        
        /// <summary>
        /// 是否公司间交易
        /// </summary>
        public virtual long? IsInnerCompany { get; set; }
        
        /// <summary>
        /// 领料人
        /// </summary>
        public virtual string? PickerID { get; set; }
        
        /// <summary>
        /// 仓储中心
        /// </summary>
        public virtual string? WarehouseCentername { get; set; }
        
        /// <summary>
        /// 公司
        /// </summary>
        public virtual string? Companyname { get; set; }
        
        /// <summary>
        /// 施工队
        /// </summary>
        public virtual string? ConstructionTeamID { get; set; }
        
        /// <summary>
        /// 单据状态
        /// </summary>
        public virtual Admin.NET.Core.OrderStatusEnum? OrderStatus { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState? IssueState { get; set; }
        
    }

    /// <summary>
    /// 领料单输入参数
    /// </summary>
    public class WmsTakeMaterialOrderInput
    {
        /// <summary>
        /// 单据大类
        /// </summary>
        public virtual long OrderLargeCategory { get; set; }
        
        /// <summary>
        /// 单据小类
        /// </summary>
        public virtual long OrderSubclass { get; set; }
        
        /// <summary>
        /// 事务类型
        /// </summary>
        public virtual string MoveType { get; set; }
        
        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        public virtual long SOID { get; set; }
        
        /// <summary>
        /// 单据编号
        /// </summary>
        public virtual string NO { get; set; }
        
        /// <summary>
        /// 领料单申请日期
        /// </summary>
        public virtual DateTimeOffset Billdate { get; set; }
        
        /// <summary>
        /// 领用项目号
        /// </summary>
        public virtual string WBSElementcode { get; set; }
        
        /// <summary>
        /// 领料部门
        /// </summary>
        public virtual string BenefitingDepartcode { get; set; }
        
        /// <summary>
        /// 受益部门
        /// </summary>
        public virtual string CostCenterID { get; set; }
        
        /// <summary>
        /// 客户
        /// </summary>
        public virtual string FI_Client_Analysis_H { get; set; }
        
        /// <summary>
        /// 是否公司间交易 1.是 2.否
        /// </summary>
        public virtual long IsInnerCompany { get; set; }
        
        /// <summary>
        /// 领料人
        /// </summary>
        public virtual string PickerID { get; set; }
        
        /// <summary>
        /// 仓储中心
        /// </summary>
        public virtual string WarehouseCentername { get; set; }
        
        /// <summary>
        /// 公司
        /// </summary>
        public virtual string Companyname { get; set; }
        
        /// <summary>
        /// 施工队
        /// </summary>
        public virtual string ConstructionTeamID { get; set; }
        
        /// <summary>
        /// 单据状态
        /// </summary>
        public virtual Admin.NET.Core.OrderStatusEnum OrderStatus { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState IssueState { get; set; }
        
    }

    public class AddWmsTakeMaterialOrderInput : WmsTakeMaterialOrderInput
    {
    }

    public class DeleteWmsTakeMaterialOrderInput : BaseId
    {
    }

    public class UpdateWmsTakeMaterialOrderInput : WmsTakeMaterialOrderInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
        
    }

    public class QueryeWmsTakeMaterialOrderInput : BaseId
    {

    }
    /// <summary>
    /// 获取单据明细参数
    /// </summary>
    public class TakePageDetailInput : PageInputBase
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long? Id { get; set; }
    }

    /// <summary>
    /// 查询库存明细
    /// </summary>
    public class TakeMaterialStockSearch : PageInputBase
    {
        /// <summary>
        /// 物料编号
        /// </summary>
        public string MaterialNo { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string MaterialName { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 合同编号
        /// </summary>
        public string ContractCode { get; set; }

        /// <summary>
        /// TA号
        /// </summary>
        public string TACode { get; set; }
    }
    /// <summary>
    /// 下发领料单参数
    /// </summary>
    public class DistributeOrderInput
    {
        /// <summary>
        /// 领料单id
        /// </summary>
        public long Id { get; set; }
    }

    /// <summary>
    /// 领料下架给yigo系统的参数
    /// </summary>
    public class ReturnTakeOrderInput
    {
        /// <summary>
        /// Id
        /// </summary>
        public long? Id { get; set; }

        /// <summary>
        /// YIGO系统id
        /// </summary>
        public long? SOID { get; set; }

        /// <summary>
        /// 入库通知单号
        /// </summary>
        public string NO { get; set; }

        /// <summary>
        /// WMS退货单号
        /// </summary>
        public string WMSNO { get; set; }

        /// <summary>
        ///出库时间
        /// </summary>
        public DateTimeOffset? Billdate { get; set; }

        /// <summary>
        /// 出库详情
        /// </summary>
        public List<ReturnTakeOrderInputDetail> Dtls { get; set; }
    }

    /// <summary>
    /// 领料下架给yigo系统的详情参数
    /// </summary>
    public class ReturnTakeOrderInputDetail
    {
        /// <summary>
        /// OID  
        /// </summary>
        public long OID { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string Materialname { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        public string Materialcode { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Qty { get; set; }

        /// <summary>
        /// 库区    
        /// </summary>
        public string DestStoreareacode { get; set; }

        /// <summary>
        /// 储位    
        /// </summary>
        public string DestLocationcode { get; set; }

        /// <summary>
        /// SCM批次号    
        /// </summary>
        public string Batchno_SCM { get; set; }

        /// <summary>
        /// WMS批次号    
        /// </summary>
        public string Batchno_WMS { get; set; }
    }
}
