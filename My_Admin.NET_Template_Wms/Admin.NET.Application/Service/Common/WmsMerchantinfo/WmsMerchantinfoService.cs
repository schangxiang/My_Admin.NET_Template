﻿using Furion.DatabaseAccessor;
using Furion.DatabaseAccessor.Extensions;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using Admin.NET.Core;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Microsoft.AspNetCore.Http;
using System.Text;
using System.Web;

namespace Admin.NET.Application
{
    /// <summary>
    /// 客商信息表服务
    /// </summary>
    [ApiDescriptionSettings("自己的业务", Name = "WmsMerchantinfo", Order = 100)]
    [Route("api/[Controller]")]
    public class WmsMerchantinfoService : IWmsMerchantinfoService, IDynamicApiController, ITransient
    {
        private readonly IRepository<WmsMerchantinfo,MasterDbContextLocator> _wmsMerchantinfoRep;
        private readonly IRepository<SysDictType, MasterDbContextLocator> _sysDictTypeRep;
        private readonly IRepository<SysDictData, MasterDbContextLocator> _sysDictDataRep;
        private readonly ISysExcelTemplateService _sysExcelTemplateService;
        private readonly static object _lock = new();

        public WmsMerchantinfoService(
            IRepository<WmsMerchantinfo,MasterDbContextLocator> wmsMerchantinfoRep
            ,IRepository<SysDictType, MasterDbContextLocator> sysDictTypeRep
            ,IRepository<SysDictData, MasterDbContextLocator> sysDictDataRep
            ,ISysExcelTemplateService sysExcelTemplateService
        )
        {
            _wmsMerchantinfoRep = wmsMerchantinfoRep;
         _sysDictTypeRep = sysDictTypeRep;
         _sysDictDataRep = sysDictDataRep;
         _sysExcelTemplateService = sysExcelTemplateService;
        }

        /// <summary>
        /// 分页查询客商信息表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("page")]
        public async Task<PageResult<WmsMerchantinfoOutput>> Page([FromQuery] WmsMerchantinfoSearch input)
        {
            var wmsMerchantinfos = await _wmsMerchantinfoRep.DetachedEntities
                                     .Where(!string.IsNullOrEmpty(input.MerchantNo), u => EF.Functions.Like(u.MerchantNo, $"%{input.MerchantNo.Trim()}%"))
                                     .Where(!string.IsNullOrEmpty(input.MerchantName), u => EF.Functions.Like(u.MerchantName, $"%{input.MerchantName.Trim()}%"))
                                     .Where(input.MerchantType != null, u => u.MerchantType == input.MerchantType)
                                     .Where(input.MerchantGrade != null, u => u.MerchantGrade == input.MerchantGrade)
                                     .Where(!string.IsNullOrEmpty(input.MerchantTmt), u => EF.Functions.Like(u.MerchantTmt, $"%{input.MerchantTmt.Trim()}%"))
                                     .OrderBy(PageInputOrder.OrderBuilder<WmsMerchantinfoSearch>(input))
                                     .ProjectToType<WmsMerchantinfoOutput>()
                                     .ToADPagedListAsync(input.PageNo, input.PageSize);
            return wmsMerchantinfos;
        }

        /// <summary>
        /// 不分页查询客商信息表列表
        /// </summary>
        /// <param name="input">客商信息表查询参数</param>
        /// <returns>(客商信息表)实例列表</returns>
        [HttpGet("listNonPage")]
        public async Task<List<WmsMerchantinfoOutput>> ListNonPageAsync([FromQuery] WmsMerchantinfoSearchNonPage input)
        {
            var pMerchantNo = input.MerchantNo?.Trim() ?? "";
            var pMerchantName = input.MerchantName?.Trim() ?? "";
            var pMerchantType = input.MerchantType;
            var pMerchantGrade = input.MerchantGrade;
            var pMerchantTmt = input.MerchantTmt?.Trim() ?? "";
            var wmsMerchantinfos = await _wmsMerchantinfoRep.DetachedEntities
                .Where(!string.IsNullOrEmpty(pMerchantNo), u => EF.Functions.Like(u.MerchantNo, $"%{pMerchantNo}%")) 
                .Where(!string.IsNullOrEmpty(pMerchantName), u => EF.Functions.Like(u.MerchantName, $"%{pMerchantName}%")) 
                .Where(pMerchantType != null, u => u.MerchantType == pMerchantType)
                .Where(pMerchantGrade != null, u => u.MerchantGrade == pMerchantGrade)
                .Where(!string.IsNullOrEmpty(pMerchantTmt), u => EF.Functions.Like(u.MerchantTmt, $"%{pMerchantTmt}%")) 
            .OrderBy(PageInputOrder.OrderNonPageBuilder(input))
            .ProjectToType<WmsMerchantinfoOutput>()
            .ToListAsync();
            return wmsMerchantinfos;
        }


        /// <summary>
        /// 增加客商信息表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("add")]
        public async Task Add(AddWmsMerchantinfoInput input)
        {
            var wmsMerchantinfo = input.Adapt<WmsMerchantinfo>();
            await _wmsMerchantinfoRep.InsertAsync(wmsMerchantinfo);
        }

        /// <summary>
        /// 删除客商信息表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("delete")]
        public async Task Delete(DeleteWmsMerchantinfoInput input)
        {
            var wmsMerchantinfo = await _wmsMerchantinfoRep.FirstOrDefaultAsync(u => u.Id == input.Id);
            await _wmsMerchantinfoRep.DeleteAsync(wmsMerchantinfo);
        }

        /// <summary>
        /// 更新客商信息表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("edit")]
        public async Task Update(UpdateWmsMerchantinfoInput input)
        {
            var isExist = await _wmsMerchantinfoRep.AnyAsync(u => u.Id == input.Id, false);
            if (!isExist) throw Oops.Oh(ErrorCode.D3000);

            var wmsMerchantinfo = input.Adapt<WmsMerchantinfo>();
            await _wmsMerchantinfoRep.UpdateAsync(wmsMerchantinfo,ignoreNullValues:true);
        }

        /// <summary>
        /// 获取客商信息表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("detail")]
        public async Task<WmsMerchantinfoOutput> Get([FromQuery] QueryeWmsMerchantinfoInput input)
        {
            return (await _wmsMerchantinfoRep.DetachedEntities.FirstOrDefaultAsync(u => u.Id == input.Id)).Adapt<WmsMerchantinfoOutput>();
        }

        /// <summary>
        /// 获取客商信息表列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("list")]
        public async Task<List<WmsMerchantinfoOutput>> List([FromQuery] WmsMerchantinfoInput input)
        {
            return await _wmsMerchantinfoRep.DetachedEntities.ProjectToType<WmsMerchantinfoOutput>().ToListAsync();
        }    
         /// <summary>
        /// Excel模板导入客商信息表功能
        /// </summary>
        /// <param name="file">Excel模板文件</param>
        /// <param name="importExcelType">Excel导入方式</param>
        /// <returns>导入的记录数</returns>
        [HttpPost("fromExcel")]
        public async Task<int> FromExcelAsync(IFormFile file, [FromQuery] ImportExcelType importExcelType)
        {
            int size = 200;
            var excelTemplate = await _sysExcelTemplateService.GetByAppNameAndClassNameAndVersionAsync("WmsMerchantinfo", "v2");
            if (excelTemplate == null) throw Oops.Oh(ErrorCode.Excel002);
            var keys = excelTemplate.UnionUniqueFields.Split(",") ?? Array.Empty<string>();
            for (var i = 0; i < keys.Length; i++) 
            {
                keys[i] = keys[i]?.Trim() ?? string.Empty;
            }
            ExcelUtil.FromExcel(file, excelTemplate.HeadStartLine, excelTemplate.DataStartLine, out List<string> headers, out List<List<object?>> data, out string sheetName);
            List<WmsMerchantinfoOutput> wmsMerchantinfoList = DataConvertUtil.ToObjectList(headers, data, sheetName, keys, excelTemplate?.DataStartLine ?? 2, out Dictionary<string, WmsMerchantinfoOutput> dict);
            List<Dictionary<string, object>> uniqueKeyValueDictList = wmsMerchantinfoList.ParseUniqueKeyValueDictList(keys.ToList(), excelTemplate?.DataStartLine ?? 2, sheetName);
            var filters = DataConvertUtil.GetExpressionListByUniqueDict<WmsMerchantinfo>(keys.ToList(), uniqueKeyValueDictList, size);
            var selectKeys = keys.ToList();
            if(!selectKeys.Contains("Id")) selectKeys.Add("Id");
            var selector = DataConvertUtil.GetSelectExpressionListByUniqueDict<WmsMerchantinfo, WmsMerchantinfoOutput>(selectKeys);
            List<WmsMerchantinfo> updates = new();
            List<WmsMerchantinfo> adds = new();

            lock (_lock)
            {
                foreach (var filter in filters)
                {
                    var wmsMerchantinfoExistSubList = _wmsMerchantinfoRep.Where(filter).Select(selector).ToList();
                    wmsMerchantinfoExistSubList.ForEach(x => 
                    {
                        var k = DataConvertUtil.GetKey(x, keys);
                        if (dict.ContainsKey(k)) dict[k].Id = x.Id;
                    });
                }
                foreach (var wmsMerchantinfo in wmsMerchantinfoList) 
                {
                    if (wmsMerchantinfo.Id > 0)
                    {
                        if (importExcelType == ImportExcelType.ADD_AND_UPDATE) updates.Add(wmsMerchantinfo.Adapt<WmsMerchantinfo>());
                    }
                    else 
                    {
                        adds.Add(wmsMerchantinfo.Adapt<WmsMerchantinfo>());
                    }
                }

                if (importExcelType == ImportExcelType.ADD_AND_UPDATE) updates.ForEach(x => _wmsMerchantinfoRep.Update(x));
                

                var maxId = _wmsMerchantinfoRep.DetachedEntities.OrderByDescending(x => x.Id).Select(x => x.Id).FirstOrDefault();
                adds.ForEach(x => x.Id = ++maxId);    
                Db.GetDbContext().Set<WmsMerchantinfo>().AddRange(adds);  
                Db.GetDbContext().SaveChanges();
               
            }
            await Task.CompletedTask;
            return adds.Count;
        }

        /// <summary>
        /// 根据版本下载客商信息表的Excel导入模板
        /// </summary>
        /// <param name="version">模板版本</param>
        /// <returns>下载的模板文件</returns>
        [HttpGet("downloadExcelTemplate")]
        public async Task<IActionResult> DownloadExcelTemplate([FromQuery] string version)
        {
            var excelTemplate = await _sysExcelTemplateService.GetByAppNameAndClassNameAndVersionAsync("WmsMerchantinfo", version);
            if (excelTemplate == null) throw Oops.Oh(ErrorCode.Excel002);
            var path = Path.Combine(@"\", excelTemplate.TemplateFileName);
            Stream ms = FileUtil.Download(path, excelTemplate.TemplateFileName);
            var fileName = HttpUtility.UrlEncode($"{excelTemplate.Name}导入模板.xlsx", Encoding.GetEncoding("UTF-8"));
            return new FileStreamResult(ms, "application/octet-stream") { FileDownloadName = fileName };
        }
        /// <summary>
        /// 根据客商信息表查询参数导出Excel
        /// </summary>
        /// <param name="input">客商信息表查询参数</param>
        /// <returns>导出的Excel文件</returns>
        [HttpGet("toExcel")]
        public async Task<IActionResult> ToExcelAsync([FromQuery] WmsMerchantinfoSearchNonPage input)
        {
            var wmsMerchantinfoList = await ListNonPageAsync(input);
            MemoryStream ms = new();
            DataConvertUtil.ToExcelData(wmsMerchantinfoList, _sysDictTypeRep, _sysDictDataRep, out List<string> headers, 
                out List<List<object>> data, out string sheetName);
            var excelTemplate = await _sysExcelTemplateService.GetByAppNameAndClassNameAndVersionAsync("WmsMerchantinfo", "v1");
            if (excelTemplate != null)
            {
                ExcelUtil.ToExcel(excelTemplate.TemplateFileName, headers, data, sheetName, excelTemplate.HeadStartLine, excelTemplate.DataStartLine, ms);
            }
            else 
            {
                ExcelUtil.ToExcel(headers, data, sheetName, ms);
            }
            ms.Position = 0;
            var fileName = HttpUtility.UrlEncode($"{sheetName}[{DateTimeOffset.Now:yyyy-MM-dd}].xlsx", Encoding.GetEncoding("UTF-8"));
            return new FileStreamResult(ms, "application/octet-stream") { FileDownloadName = fileName };
        }
    }
}
