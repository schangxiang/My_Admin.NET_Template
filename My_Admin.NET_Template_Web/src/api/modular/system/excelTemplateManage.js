﻿import { axios } from '@/utils/request'

/**
 * 查询Excel模板
 *
 * @author zhangzhan
 */
export function sysExcelTemplatePage (parameter) {
  return axios({
    url: '/sysExcelTemplate/page',
    method: 'get',
    params: parameter
  })
}

/**
 * Excel模板列表
 *
 * @author zhangzhan
 */
export function sysExcelTemplateList (parameter) {
  return axios({
    url: '/sysExcelTemplate/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加Excel模板
 *
 * @author zhangzhan
 */
export function sysExcelTemplateAdd (parameter) {
  return axios({
    url: '/sysExcelTemplate/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑Excel模板
 *
 * @author zhangzhan
 */
export function sysExcelTemplateEdit (parameter) {
  return axios({
    url: '/sysExcelTemplate/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除Excel模板
 *
 * @author zhangzhan
 */
export function sysExcelTemplateDelete (parameter) {
  return axios({
    url: '/sysExcelTemplate/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出Excel模板的Excel文件
 *
 * @author zhangzhan
 */
export function sysExcelTemplateToExcel (parameter) {
  return axios({
    url: '/sysExcelTemplate/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入Excel模板的Excel文件
 *
 * @author zhangzhan
 */
export function sysExcelTemplateFromExcel (data, parameter) {
  return axios({
    url: '/sysExcelTemplate/fromExcel',
    method: 'post',
    data: data,
    params: parameter
  })
}

/**
 * 下载Excel模板的Excel导入模板
 *
 * @author zhangzhan
 */
export function sysExcelTemplateDownloadExcelTemplate(parameter) {
  return axios({
    url: '/sysExcelTemplate/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 修改Excel模板状态
 *
 * @author zhangzhan
 */
export function sysExcelTemplateChangeStatus(parameter) {
  return axios({
    url: '/sysExcelTemplate/changeStatus',
    method: 'post',
    data: parameter
  })
}

/**
 * 获取实体名称获取属性集合
 *
 * @author zhangzhan
 */
export function sysExcelTemplateGetColumnList(parameter) {
  return axios({
    url: '/sysExcelTemplate/getColumnList',
    method: 'get',
    params: parameter,
  })
}