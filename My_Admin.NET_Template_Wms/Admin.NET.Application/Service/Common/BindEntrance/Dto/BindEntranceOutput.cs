﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 组盘物料信息
    /// </summary>
    public class BindEntranceOutput
    {

        /// <summary>
        /// 周转箱号
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 料箱号信息
        /// </summary>
        public WmsContainerDto WmsContainer { get; set; }
        /// <summary>
        /// 库位编号
        /// </summary>
        public string PlaceCode { get; set; }

        /// <summary>
        /// 物料明细信息
        /// </summary>
        public List<GetMaterialContainerOutput> WmsMaterials { get; set; }
    }

    /// <summary>
    ///  物料明细信息输出参数
    /// </summary>
    public class GetMaterialContainerOutput
    {
        /// <summary>
        /// 物料名称
        /// </summary>
        public string MaterialName { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        public string MaterialNo { get; set; }

        /// <summary>
        /// 物料批次
        /// </summary>
        public string MaterialBatch { get; set; }

        /// <summary>
        /// 物料类别;数据字典
        /// </summary>
        public MaterialType MaterialType { get; set; }

        /// <summary>
        /// 物料检验
        /// </summary>
        public MaterialInspection InspectionMethod { get; set; }

        /// <summary>
        /// 物料规格
        /// </summary>
        public string MaterialSpec { get; set; }
    }


    public class MaterialOutput
    {
        /// <summary>
        /// 物料名称
        /// </summary>
        public string MaterialName { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        public string MaterialNo { get; set; }

        /// <summary>
        /// 物料ID
        /// </summary>
        public long MaterialId { get; set; }
        /// <summary>
        /// 物料批次
        /// </summary>
        public string MaterialBatch { get; set; }

        /// <summary>
        /// 物料类别;数据字典
        /// </summary>
        public MaterialType MaterialType { get; set; }

        /// <summary>
        /// 物料检验
        /// </summary>
        public MaterialInspection InspectionMethod { get; set; }

        /// <summary>
        /// 物料规格
        /// </summary>
        public string MaterialSpec { get; set; }

        /// <summary>
        /// 物料密度
        /// </summary>
        public string MaterialDensity { get; set; }

        /// <summary>
        /// 物料单位
        /// </summary>
        public string MaterialUnit { get; set; }

        /// <summary>
        /// 工段
        /// </summary>
        public LesWorkshopSection LesWorkshopSection { get; set; }
    }
}
