﻿using Admin.NET.Core;
using Admin.NET.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace Admin.NET.Application
{    
    
    /// <summary>
    /// 学生视图查询参数
    /// </summary>
    public class VStudentSearch : PageInputBase
    {
        /// <summary>
        /// Id主键
        /// </summary>
        public virtual long? Id { get; set; }
        
        /// <summary>
        /// 学生姓名
        /// </summary>
        public virtual string Name { get; set; }
        
        /// <summary>
        /// 学生年龄
        /// </summary>
        public virtual int? Age { get; set; }
        
        /// <summary>
        /// 是否在校
        /// </summary>
        public virtual bool? StartName { get; set; }
        
        /// <summary>
        /// 性别
        /// </summary>
        public virtual int? Gender { get; set; }
        
        /// <summary>
        /// 出生日期
        /// </summary>
        public virtual DateTime? BrithDate { get; set; }
        
        /// <summary>
        /// 关联老师
        /// </summary>
        public virtual long? TeacherId { get; set; }
        
        /// <summary>
        /// 老师名称
        /// </summary>
        public virtual string teacherName { get; set; }
        
    }

        /// <summary>
    /// 学生视图不分页查询参数
    /// </summary>
    public class VStudentSearchNonPage : PageInputNonPageBase
    {
        /// <summary>
        /// Id主键
        /// </summary>
        public virtual long? Id { get; set; }
        
        /// <summary>
        /// 学生姓名
        /// </summary>
        public virtual string Name { get; set; }
        
        /// <summary>
        /// 学生年龄
        /// </summary>
        public virtual int? Age { get; set; }
        
        /// <summary>
        /// 是否在校
        /// </summary>
        public virtual bool? StartName { get; set; }
        
        /// <summary>
        /// 性别
        /// </summary>
        public virtual int? Gender { get; set; }
        
        /// <summary>
        /// 出生日期
        /// </summary>
        public virtual DateTime? BrithDate { get; set; }
        
        /// <summary>
        /// 关联老师
        /// </summary>
        public virtual long? TeacherId { get; set; }
        
        /// <summary>
        /// 老师名称
        /// </summary>
        public virtual string teacherName { get; set; }
        
    }

    /// <summary>
    /// 学生视图输入参数
    /// </summary>
    public class VStudentInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        public virtual long? Id { get; set; }
        
        /// <summary>
        /// 学生姓名
        /// </summary>
        public virtual string Name { get; set; }
        
        /// <summary>
        /// 学生年龄
        /// </summary>
        public virtual int? Age { get; set; }
        
        /// <summary>
        /// 是否在校
        /// </summary>
        public virtual bool? StartName { get; set; }
        
        /// <summary>
        /// 性别
        /// </summary>
        public virtual int? Gender { get; set; }
        
        /// <summary>
        /// 出生日期
        /// </summary>
        public virtual DateTime? BrithDate { get; set; }
        
        /// <summary>
        /// 关联老师
        /// </summary>
        public virtual long? TeacherId { get; set; }
        
        /// <summary>
        /// 老师名称
        /// </summary>
        public virtual string teacherName { get; set; }
        
    }

    /// <summary>
    /// 学生视图新增参数
    /// </summary>
    public class AddVStudentInput : VStudentInput
    {
    }

    /// <summary>
    /// 学生视图删除参数
    /// </summary>
    public class DeleteVStudentInput : BaseId
    {
    }

    /// <summary>
    /// 学生视图更新参数
    /// </summary>
    public class UpdateVStudentInput : VStudentInput
    {
    }

    /// <summary>
    /// 学生视图获取单个参数
    /// </summary>
    public class QueryeVStudentInput : BaseId
    {

    }
}
