﻿using Microsoft.AspNetCore.Mvc;

namespace Admin.NET.Application
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAuthService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<ClickWordCaptchaResult> GetCaptcha();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<bool> GetCaptchaOpen();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<LoginOutput> GetLoginUserAsync();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        string LoginAsync([FromBody] LoginInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task LogoutAsync();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ClickWordCaptchaResult> VerificationCode(ClickWordCaptchaInput input);
    }
}