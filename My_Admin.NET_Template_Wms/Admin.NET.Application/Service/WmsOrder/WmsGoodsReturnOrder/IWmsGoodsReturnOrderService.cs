﻿using Admin.NET.Core;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Admin.NET.Application
{
    public interface IWmsGoodsReturnOrderService
    {
        Task Add(AddWmsGoodsReturnOrderInput input);
        Task Delete(DeleteWmsGoodsReturnOrderInput input);
        Task<WmsGoodsReturnOrderOutput> Get([FromQuery] QueryeWmsGoodsReturnOrderInput input);
        Task<List<WmsGoodsReturnOrderOutput>> List([FromQuery] WmsGoodsReturnOrderInput input);
        Task<PageResult<WmsGoodsReturnOrderOutput>> Page([FromQuery] WmsGoodsReturnOrderSearch input);
        Task Update(UpdateWmsGoodsReturnOrderInput input);

        Task<List<WmsGoodsReturnOrderOutput>> ListNonPageAsync([FromQuery] WmsGoodsReturnOrderSearchNonPage input);

        Task<int> FromExcelAsync(IFormFile file, [FromQuery] ImportExcelType importExcelType);

        Task<IActionResult> DownloadExcelTemplate(string version);
    }
}