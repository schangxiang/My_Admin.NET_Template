﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 事务类型
    /// </summary>
    public enum MoveType
    {
        /// <summary>
        /// 生产领料:Z221
        /// </summary>
        SHENGCHANLINGLIAO = 1,
        /// <summary>
        /// 销售领料：Z201
        /// </summary>
        XIAOSHOULINGLIAO = 2
    }
}
