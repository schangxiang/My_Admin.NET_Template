﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    /// <summary>
    /// 领料单
    /// </summary>
    [Table("wms_takematerialorder")]
    [Comment("领料单")]
    public class WmsTakeMaterialOrder : DEntityBase, IEntityTypeBuilder<WmsTakeMaterialOrder>
    {
        /// <summary>
        ///  Billdate
        /// </summary>
        [Comment("单据大类")]
        [Required]
        public long OrderLargeCategory { get; set; }

        /// <summary>
        /// 单据小类
        /// </summary>
        [Comment("单据小类")]
        [Required]
        public long OrderSubclass { get; set; }

        /// <summary>
        ///事务类型
        ///销售领料：Z201 生产领料：Z221
        /// </summary>
        [Comment("事务类型")]
        public string MoveType { get; set; }

        /// <summary>
        /// SCM 领 料 申请单头表唯一标识
        /// </summary>
        [Comment("上位系统单据唯一识别码")]
        public long SOID { get; set; }

        /// <summary>
        /// 单据编号  
        /// </summary>
        [Comment("单据编号")]
        [Required]
        [MaxLength(255)]
        public string NO { get; set; }

        /// <summary>
        /// 领料单申请日期    
        /// </summary>
        [Comment("领料单申请日期")]
        public DateTimeOffset? Billdate { get; set; }

        /// <summary>
        /// 领用项目号
        /// </summary>
        [Comment("领用项目号")]
        [MaxLength(255)]
        public string WBSElementcode { get; set; }

        /// <summary>
        /// 领料部门    
        /// </summary>
        [Comment("领料部门")]
        [MaxLength(255)]
        public string BenefitingDepartcode { get; set; }

        /// <summary>
        /// 受益部门    
        /// </summary>
        [Comment("受益部门")]
        [MaxLength(255)]
        public string CostCenterID { get; set; }

        /// <summary>
        /// 客户
        /// </summary>
        [Comment("客户")]
        [MaxLength(255)]
        public string FI_Client_Analysis_H { get; set; }

        /// <summary>
        /// 是否公司间交易 1.是 2.否
        /// </summary>
        [Comment("是否公司间交易")]
        public long IsInnerCompany { get; set; }

        /// <summary>
        /// 领料人
        /// </summary>
        [Comment("领料人")]
        [MaxLength(255)]
        public string PickerID { get; set; }

        /// <summary>
        /// 仓储中心
        /// </summary>
        [Comment("仓储中心")]
        [MaxLength(255)]
        public string WarehouseCentername { get; set; }

        /// <summary>
        /// 公司
        /// </summary>
        [Comment("公司")]
        [MaxLength(255)]
        public string Companyname { get; set; }

        /// <summary>
        /// 施工队
        /// </summary>
        [Comment("施工队")]
        [MaxLength(255)]
        public string ConstructionTeamID { get; set; }

        /// <summary>
        /// 单据状态
        /// </summary>
        [Comment("单据状态")]
        [Required]
        public OrderStatusEnum OrderStatus { get; set; } = OrderStatusEnum.WEIXIAFA;

        /// <summary>
        /// 单据明细
        /// </summary>
        public ICollection<WmsTakeMaterialOrderDetail> WmsTakeMaterialOrderDetail { get; set; }

        /// <summary>
        /// 1对多配置关系
        /// </summary>
        /// <param name="entityBuilder"></param>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        public void Configure(EntityTypeBuilder<WmsTakeMaterialOrder> entityBuilder, DbContext dbContext, Type dbContextLocator)
        {
            // 一对多配置
            entityBuilder.HasMany(x => x.WmsTakeMaterialOrderDetail)
                .WithOne(x => x.WmsTakeMaterialOrder)
                .HasForeignKey(x => x.OrderId);
        }
    }
}
