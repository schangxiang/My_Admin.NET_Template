﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 
    /// </summary>
    public class BindEntranceInput : PageInputBase
    {
        /// <summary>
        /// 托盘号
        /// </summary>
        [Required(ErrorMessage = "托盘号不能为空")]
        public string ContainerCode { get; set; }
    }

    /// <summary>
    /// 组盘输入参数
    /// </summary>
    public class WarehousingInput
    {
        /// <summary>
        /// 托盘号
        /// </summary>
        [Required(ErrorMessage = "托盘号不能为空")]
        public string ContainerCode { get; set; }

        /// <summary>
        /// 库位号
        /// </summary>
        public string PlaceCode { get; set; }

        /// <summary>
        /// 入库起始位置
        /// </summary>
        public string SourcePlace { get; set; }

        /// <summary>
        /// 物料信息
        /// </summary>
        public List<WarehousingMaterialInput> WmsMaterials { get; set; }
    }

    /// <summary>
    /// 物料明细输入参数
    /// </summary>
    public class WarehousingMaterialInput
    {
        /// <summary>
        /// 物料名称
        /// </summary>
        public string MaterialName { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        public string MaterialNo { get; set; }

        /// <summary>
        /// 物料ID
        /// </summary>
        public long MaterialId { get; set; }
        /// <summary>
        /// 物料批次
        /// </summary>
        public string MaterialBatch { get; set; }

        /// <summary>
        /// 物料类别;数据字典
        /// </summary>
        public MaterialType MaterialType { get; set; }

        /// <summary>
        /// 物料检验
        /// </summary>
        public MaterialInspection InspectionMethod { get; set; }

        /// <summary>
        /// 物料规格
        /// </summary>
        public string MaterialSpec { get; set; }

        /// <summary>
        /// 物料密度
        /// </summary>
        public string MaterialDensity { get; set; }

        /// <summary>
        /// 物料单位
        /// </summary>
        public string MaterialUnit { get; set; }

        /// <summary>
        /// 工段
        /// </summary>
        public LesWorkshopSection LesWorkshopSection { get; set; }
    }

    /// <summary>
    /// 获取物料输入参数
    /// </summary>
    public class GetMaterialInput
    {
        /// <summary>
        /// 物料编码
        /// </summary>
        public string MaterialNo { get; set; }
        
    }
}
