﻿using Mapster;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    public class TestStudent3Mapper : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<AddTestStudent3Input, TestStudent3>()
            ;
            config.ForType<UpdateTestStudent3Input, TestStudent3>()
            ;
            config.ForType<TestStudent3, TestStudent3Output>()
            ;
        }
    }
}
