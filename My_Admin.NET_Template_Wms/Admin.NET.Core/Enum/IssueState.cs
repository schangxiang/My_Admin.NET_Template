﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 签核状态
    /// </summary>
    public enum IssueState
    {
        /// <summary>
        /// 不使用
        /// </summary>
        [Description("不使用")]
        Unused = 1,

        /// <summary>
        /// 未签核
        /// </summary>
        [Description("未签核")]
        Unfrozen = 2,

        /// <summary>
        /// 签核中
        /// </summary>
        [Description("签核中")]
        Pending = 3,

        /// <summary>
        /// 已签核
        /// </summary>
        [Description("已签核")]
        Active = 4
    }
}