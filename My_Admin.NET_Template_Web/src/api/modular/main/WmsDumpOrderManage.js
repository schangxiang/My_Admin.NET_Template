﻿import { axios } from '@/utils/request'

/**
 * 查询转储单
 *
 * @author XJF
 */
export function WmsDumpOrderPage (parameter) {
  return axios({
    url: '/WmsDumpOrder/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 转储单列表
 *
 * @author XJF
 */
export function WmsDumpOrderList (parameter) {
  return axios({
    url: '/WmsDumpOrder/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加转储单
 *
 * @author XJF
 */
export function WmsDumpOrderAdd (parameter) {
  return axios({
    url: '/WmsDumpOrder/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑转储单
 *
 * @author XJF
 */
export function WmsDumpOrderEdit (parameter) {
  return axios({
    url: '/WmsDumpOrder/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除转储单
 *
 * @author XJF
 */
export function WmsDumpOrderDelete (parameter) {
  return axios({
    url: '/WmsDumpOrder/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出转储单的Excel文件
 *
 * @author XJF
 */
export function WmsDumpOrderToExcel (parameter) {
  return axios({
    url: '/WmsDumpOrder/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入转储单的Excel文件
 *
 * @author XJF
 */
export function WmsDumpOrderFromExcel (data, parameter) {
  return axios({
    url: '/WmsDumpOrder/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载转储单的Excel导入模板
 *
 * @author XJF
 */
export function WmsDumpOrderDownloadExcelTemplate(parameter) {
  return axios({
    url: '/WmsDumpOrder/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}



