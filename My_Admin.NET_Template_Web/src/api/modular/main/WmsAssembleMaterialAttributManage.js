﻿import { axios } from '@/utils/request'

/**
 * 查询组装车间物料信息
 *
 * @author XJF
 */
export function WmsAssembleMaterialAttributPage (parameter) {
  return axios({
    url: '/WmsAssembleMaterialAttribut/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 组装车间物料信息列表
 *
 * @author XJF
 */
export function WmsAssembleMaterialAttributList (parameter) {
  return axios({
    url: '/WmsAssembleMaterialAttribut/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加组装车间物料信息
 *
 * @author XJF
 */
export function WmsAssembleMaterialAttributAdd (parameter) {
  return axios({
    url: '/WmsAssembleMaterialAttribut/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑组装车间物料信息
 *
 * @author XJF
 */
export function WmsAssembleMaterialAttributEdit (parameter) {
  return axios({
    url: '/WmsAssembleMaterialAttribut/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除组装车间物料信息
 *
 * @author XJF
 */
export function WmsAssembleMaterialAttributDelete (parameter) {
  return axios({
    url: '/WmsAssembleMaterialAttribut/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出组装车间物料信息的Excel文件
 *
 * @author XJF
 */
export function WmsAssembleMaterialAttributToExcel (parameter) {
  return axios({
    url: '/WmsAssembleMaterialAttribut/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入组装车间物料信息的Excel文件
 *
 * @author XJF
 */
export function WmsAssembleMaterialAttributFromExcel (data, parameter) {
  return axios({
    url: '/WmsAssembleMaterialAttribut/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载组装车间物料信息的Excel导入模板
 *
 * @author XJF
 */
export function WmsAssembleMaterialAttributDownloadExcelTemplate(parameter) {
  return axios({
    url: '/WmsAssembleMaterialAttribut/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}




/**
* 获取WmsAssembleMaterial列表
* @author XJF
*/
export function WmsAssembleMaterialAttributFkWmsAssembleMaterialList() {
  return axios({
    url: '/WmsAssembleMaterialAttribut/fkWmsAssembleMaterial',
    method: 'get'
  })
}
