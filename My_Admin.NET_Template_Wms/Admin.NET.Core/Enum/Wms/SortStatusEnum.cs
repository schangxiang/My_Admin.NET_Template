﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Core
{
    /// <summary>
    /// 分拣单状态
    /// </summary>
    public enum SortStatusEnum
    {

        /// <summary>
        /// 未分拣
        /// </summary>
        [Description("未分拣")]
        WEIFENJIAN = 1,

        /// <summary>
        /// 分拣中
        /// </summary>
        [Description("分拣中")]
        FENJIANZHONG = 2,

        /// <summary>
        /// 分拣完成
        /// </summary>
        [Description("分拣完成")]
        FENJIANWANCHENG = 3,
    }
}
