﻿using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 库区信息输出参数
    /// </summary>
    public class WmsAreaOutput
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string AreaName { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string AreaDesc { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public CommonStatus AreaStatus { get; set; }

        /// <summary>
        /// 分类
        /// </summary>
        public AreaType AreaType { get; set; }

        /// <summary>
        /// 是否为钢平台
        /// </summary>
        public bool IsSteel { get; set; }

        /// <summary>
        /// 所属车间
        /// </summary>
        public LesWorkShopType WorkShopType { get; set; }

        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 签核状态
        /// </summary>
        public IssueState IssueState { get; set; }
    }
}
