﻿import { axios } from '@/utils/request'

/**
 * 查询打包信息
 *
 * @author XJF
 */
export function LesFoamingPackWarehousePage (parameter) {
  return axios({
    url: '/LesFoamingPackWarehouse/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 打包信息列表
 *
 * @author XJF
 */
export function LesFoamingPackWarehouseList (parameter) {
  return axios({
    url: '/LesFoamingPackWarehouse/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加打包信息
 *
 * @author XJF
 */
export function LesFoamingPackWarehouseAdd (parameter) {
  return axios({
    url: '/LesFoamingPackWarehouse/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑打包信息
 *
 * @author XJF
 */
export function LesFoamingPackWarehouseEdit (parameter) {
  return axios({
    url: '/LesFoamingPackWarehouse/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除打包信息
 *
 * @author XJF
 */
export function LesFoamingPackWarehouseDelete (parameter) {
  return axios({
    url: '/LesFoamingPackWarehouse/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出打包信息的Excel文件
 *
 * @author XJF
 */
export function LesFoamingPackWarehouseToExcel (parameter) {
  return axios({
    url: '/LesFoamingPackWarehouse/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入打包信息的Excel文件
 *
 * @author XJF
 */
export function LesFoamingPackWarehouseFromExcel (data, parameter) {
  return axios({
    url: '/LesFoamingPackWarehouse/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载打包信息的Excel导入模板
 *
 * @author XJF
 */
export function LesFoamingPackWarehouseDownloadExcelTemplate(parameter) {
  return axios({
    url: '/LesFoamingPackWarehouse/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}



