﻿#nullable enable
using Admin.NET.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Admin.NET.Application
{
    /// <summary>
    /// Excel模板输出服务接口
    /// </summary>
    public interface ISysExcelTemplateService
    {
        /// <summary>
        /// 添加Excel模板
        /// </summary>
        /// <param name="input">Excel模板添加参数</param>
        /// <returns></returns>
        Task AddAsync(AddSysExcelTemplateInput input);

        /// <summary>
        /// 删除Excel模板
        /// </summary>
        /// <param name="input">Excel模板删除参数</param>
        /// <returns></returns>
        Task DeleteAsync(DeleteSysExcelTemplateInput input);
        
        /// <summary>
        /// 获取单个Excel模板
        /// </summary>
        /// <param name="input">Excel模板查询参数</param>
        /// <returns>Excel模板实例</returns>
        Task<SysExcelTemplateOutput?> GetAsync([FromQuery] QuerySysExcelTemplateInput input);
        
        /// <summary>
        /// 不分页查询所有Excel模板列表
        /// </summary>
        /// <param name="input">Excel模板查询参数</param>
        /// <returns>(Excel模板)实例列表</returns>
        Task<List<SysExcelTemplateOutput>> ListAsync([FromQuery] SysExcelTemplateInput input);
        
        /// <summary>
        /// 分页查询Excel模板列表
        /// </summary>
        /// <param name="input">Excel模板查询参数</param>
        /// <returns>(Excel模板)实例列表</returns>
        Task<PageResult<SysExcelTemplateOutput>> PageAsync([FromQuery] SysExcelTemplateSearch input);

        /// <summary>
        /// 不分页查询Excel模板列表
        /// </summary>
        /// <param name="input">Excel模板查询参数</param>
        /// <returns>(Excel模板)实例列表</returns>
        Task<List<SysExcelTemplateOutput>> ListNonPageAsync([FromQuery] SysExcelTemplateSearchNonPage input);

        /// <summary>
        /// 更新Excel模板
        /// </summary>
        /// <param name="input">Excel模板更新参数</param>
        /// <returns></returns>
        Task UpdateAsync(UpdateSysExcelTemplateInput input);

        /// <summary>
        /// 根据Excel模板查询参数导出Excel
        /// </summary>
        /// <param name="input">Excel模板查询参数</param>
        /// <returns>导出的Excel文件</returns>
        Task<IActionResult> ToExcelAsync([FromQuery] SysExcelTemplateSearchNonPage input);


        /// <summary>
        /// 根据Excel模板查询参数导出Excel
        /// </summary>
        /// <param name="input">Excel模板查询参数</param>
        /// <param name="appName"></param>
        /// <returns>导出的Excel文件</returns>
        Task<IActionResult> ToExcelAsync([FromQuery] SysExcelTemplateSearchNonPage input, [FromQuery] string appName);



        /// <summary>
        /// 修改Excel模板状态
        /// </summary>
        /// <param name="input">Excel模板状态参数</param>
        /// <returns></returns>
        Task ChangeSysExcelTemplateStatusAsync(UpdateSysExcelTemplateStatusInput input);

        /// <summary>
        /// 根据类名及版本号获取单个Excel模板
        /// </summary>
        /// <param name="className">类名</param>
        /// <param name="version">版本号</param>
        /// <returns>Excel模板实例</returns>
        Task<SysExcelTemplateOutput?> GetByAppNameAndClassNameAndVersionAsync([FromQuery] string className, [FromQuery] string version);

        /// <summary>
        /// Excel模板导入功能
        /// </summary>
        /// <param name="file"> Excel模板文件</param>
        /// <param name="importExcelType">Excel导入方式</param>
        /// <returns>导入的记录数</returns>
        Task<int> FromExcelAsync(IFormFile file, [FromQuery] ImportExcelType importExcelType);


        /// <summary>
        /// 根据版本下载Excel模板的Excel导入模板
        /// </summary>
        /// <param name="version">模板版本</param>
        /// <returns>下载的模板文件</returns>
        Task<IActionResult> DownloadExcelTemplate(string version);

        /// <summary>
        /// 获取实体名称获取属性集合
        /// </summary>
        /// <param name="className">实体名称</param>
        /// <returns></returns>
        Task<List<TableColumn>> GetColumnListAsync([FromQuery] string className);

        /// <summary>
        /// 获取模板提示字符
        /// </summary>
        /// <param name="className">实体名称</param>
        /// <returns></returns>
        Task<string> ParseTemplateHintAsync([FromQuery] string className);


        /// <summary>
        /// 根据类名查询其查询唯一类型
        /// </summary>
        /// <param name="className">类型名</param>
        /// <returns></returns>
        Task<List<TableColumn>> QueryUniqueColumns(string className);


        /// <summary>
        /// 根据类名查询其查询表信息
        /// </summary>
        /// <param name="className">类型名</param>
        /// <returns></returns>
        Task<TableInfo?> QueryTable(string className);


        /// <summary>
        /// 根据类名生成相应的种子代码
        /// </summary>
        /// <param name="className">类名</param>
        /// <param name="prefix">前缀</param>
        /// <returns></returns>
        Task<string> ParseSeedData([FromQuery] string className, string prefix);

        /// <summary>
        /// 根据前缀获取最大ID加1
        /// </summary>
        /// <param name="prefix">前缀</param>
        /// <returns></returns>
        Task<long> ParseId([FromQuery] string prefix);



    }
}