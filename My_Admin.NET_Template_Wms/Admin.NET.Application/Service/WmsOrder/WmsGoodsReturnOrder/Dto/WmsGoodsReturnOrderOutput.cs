﻿using Admin.NET.Core.Util.LowCode.Dto;
using System;
using Admin.NET.Core;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace Admin.NET.Application
{
    /// <summary>
    /// 退货单输出参数
    /// </summary>
    public class WmsGoodsReturnOrderOutput
    {
        /// <summary>
        /// 单据大类
        /// </summary>
        public long OrderLargeCategory { get; set; }
        
        /// <summary>
        /// 单据小类
        /// </summary>
        public long OrderSubclass { get; set; }
        
        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        public long SOID { get; set; }
        
        /// <summary>
        /// 单据编号
        /// </summary>
        public string NO { get; set; }
        
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTimeOffset Billdate { get; set; }
        
        /// <summary>
        /// 供应商编码
        /// </summary>
        public string Vendorcode { get; set; }
        
        /// <summary>
        /// 供应商名称
        /// </summary>
        public string Vendorname { get; set; }
        
        /// <summary>
        /// 公司名称
        /// </summary>
        public string Companyname { get; set; }
        
        /// <summary>
        /// 仓管员
        /// </summary>
        public string WarehouseKeepername { get; set; }
        
        /// <summary>
        /// 仓储中心
        /// </summary>
        public string WarehouseCentername { get; set; }
        
        /// <summary>
        /// 退货状态
        /// </summary>
        public Admin.NET.Core.GoodsReturnStatus GoodsReturnStatus { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public Admin.NET.Core.IssueState IssueState { get; set; }

        /// <summary>
        /// 对应YiGo系统
        /// </summary>
        public List<WmsGoodsReturnOrderDetailOutput> Dtls { get; set; }

    }

    /// <summary>
    /// 退货单详情输出参数
    /// </summary>
    public class WmsGoodsReturnOrderDetailOutput
    {
        /// <summary>
        /// Id  
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 上位系统单据明细唯一识别码
        /// </summary>
        public long OID { get; set; }

        /// <summary>
        /// 单据Id  
        /// </summary>
        public long OrderId { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string Materialname { get; set; }
        /// <summary>
        /// 物料编号
        /// </summary>
        public string Materialcode { get; set; }
        /// <summary>
        /// 基本单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Qty { get; set; }

        /// <summary>
        /// 单根长度 
        /// </summary>
        public decimal SingleLength { get; set; }

        /// <summary>
        /// 理论重量    
        /// </summary>
        public decimal TheoreticalWeight { get; set; }

        /// <summary>
        /// 批次号    
        /// </summary>
        public string Batchno_WMS { get; set; }

        /// <summary>
        /// 不含税单价    
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// 成本金额    
        /// </summary>
        public decimal SumPrice { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 合同编号
        /// </summary>
        public string ContractCode { get; set; }

        /// <summary>
        /// TA号
        /// </summary>
        public string TACode { get; set; }

        /// <summary>
        /// 仓间
        /// </summary>
        public string StoreRoomname { get; set; }

        /// <summary>
        /// 报关单号
        /// </summary>
        public string DeclarationCode { get; set; }

        /// <summary>
        /// 报关品名
        /// </summary>
        public string TradeName { get; set; }

        /// <summary>
        /// 报关项号
        /// </summary>
        public string ItemNo { get; set; }

        /// <summary>
        /// 报关单位
        /// </summary>
        public string CustomsUnitName { get; set; }

        /// <summary>
        /// 报关数量   
        /// </summary>
        public decimal CustomsNum { get; set; }

        /// <summary>
        /// 报关金额 
        /// </summary>
        public decimal CustomsPrices { get; set; }

        /// <summary>
        /// 退货状态
        /// </summary>
        public ReceivingStatus ReceivingStatus { get; set; }
    }

    /// <summary>
    /// yigo获取退货单返回参数
    /// </summary>
    public class GetGoodsReturnOrderOutputByYiGo
    {
        public bool Status { get; set; }

        public List<WmsGoodsReturnOrderOutput> Data { get; set; }

        public string Result { get; set; }
    }

    /// <summary>
    /// 退货下架返回参数
    /// </summary>
    public class ReturnGoodsReturnOrderOutput
    {

        /// <summary>
        /// 结果描述
        /// </summary>
        public string Result { get; set; }

        /// <summary>
        /// 请求状态
        /// </summary>
        public bool Status { get; set; }
    }
}
