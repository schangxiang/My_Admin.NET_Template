﻿import { axios } from '@/utils/request'

/**
 * 查询采购单
 *
 * @author XJF
 */
export function PurchaseOrderPage (parameter) {
  return axios({
    url: '/PurchaseOrder/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 采购单列表
 *
 * @author XJF
 */
export function PurchaseOrderList (parameter) {
  return axios({
    url: '/PurchaseOrder/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加采购单
 *
 * @author XJF
 */
export function PurchaseOrderAdd (parameter) {
  return axios({
    url: '/PurchaseOrder/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑采购单
 *
 * @author XJF
 */
export function PurchaseOrderEdit (parameter) {
  return axios({
    url: '/PurchaseOrder/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除采购单
 *
 * @author XJF
 */
export function PurchaseOrderDelete (parameter) {
  return axios({
    url: '/PurchaseOrder/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出采购单的Excel文件
 *
 * @author XJF
 */
export function PurchaseOrderToExcel (parameter) {
  return axios({
    url: '/PurchaseOrder/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入采购单的Excel文件
 *
 * @author XJF
 */
export function PurchaseOrderFromExcel (data, parameter) {
  return axios({
    url: '/PurchaseOrder/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载采购单的Excel导入模板
 *
 * @author XJF
 */
export function PurchaseOrderDownloadExcelTemplate(parameter) {
  return axios({
    url: '/PurchaseOrder/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 单据大类下拉
 *
 * @author yhh
 */
export function PurchaseOrderLargeCategories() {
  return axios({
    url: '/PurchaseOrder/OrderTypeLargeCategory',
    method: 'get'
  })
}

/**
 * 单据小类下拉
 *
 * @author yhh
 */
export function PurchaseOrderSubclass(id) {
  return axios({
    url: '/PurchaseOrder/OrderTypeSubclass',
    method: 'get',
    params:{id}
  })
}


/**
 * 查询采购单详情
 *
 * @author yhh
 */
export function PurchaseOrderDetailPage (parameter) {
  return axios({
    url: '/PurchaseOrder/detailPage',
    method: 'get',
    params: parameter
  })
}


