﻿using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 创建主任务、AGV任务输入参数
    /// </summary>
    public class InteractiveInput
    {
        /// <summary>
        /// 下线口
        /// </summary>
        public BlankingProductionLineEnum BlankingProductionLine { get; set; }
        /// <summary>
        /// 
        /// </summary>
        //public List<DestinationsInput> destinations { get; set; }
    }

    /// <summary>
    /// 查询所有空闲库位输入参数
    /// </summary>
    public class GetPlaceInput
    {
        /// <summary>
        /// 任务号
        /// </summary>
        public string TaskNo { get; set; }
    }

    /// <summary>
    /// 更新任务输入参数
    /// </summary>
    public class UpdateTaskInput
    {
        /// <summary>
        /// 任务号
        /// </summary>
        public string TaskNo { get; set; }

        /// <summary>
        /// 任务状态
        /// </summary>
        //public TaskStatusEnum TaskStatus { get; set; }

        /// <summary>
        /// 设备任务状态
        /// </summary>
        public TaskDodeviceStatusEnum TaskDodeviceStatus { get; set; }

        /// <summary>
        /// 巷道
        /// </summary>
        //public int Aisle { get; set; }

        /// <summary>
        /// 称重重量
        /// </summary>
        public decimal weight { get; set; }
    }
    /// <summary>
    /// 强制完成
    /// </summary>
    public class FinishTaskInput
    {
        /// <summary>
        /// 任务号
        /// </summary>
        public string TaskNo { get; set; }
    }


    /// <summary>
    /// 更新任务输入参数
    /// </summary>
    public class CreateRelocationInput
    {
        /// <summary>
        /// 任务起点
        /// </summary>
        public string SourcePlace { get; set; }

        /// <summary>
        /// 巷道
        /// </summary>
        public int Aisle { get; set; }

    }

    /// <summary>
    /// 添加设备报警信息输入参数
    /// </summary>
    public class AddDeviceWaringInput
    {
        /// <summary>
        /// WcsId
        /// </summary>
        public int WcsId { get; set; }

        /// <summary>
        /// 设备名称
        /// </summary>
        public string DeviceName { get; set; }

        /// <summary>
        /// 故障名称
        /// </summary>
        public string FaultName { get; set; }

        /// <summary>
        /// 发生时间
        /// </summary>
        public DateTimeOffset? StartTime { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTimeOffset? EndTime { get; set; }

        /// <summary>
        /// 持续时间
        /// </summary>
        public int RunningTime { get; set; }

    }
}
