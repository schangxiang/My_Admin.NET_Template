import { axios } from '@/utils/request'

/**
 * 胶合板备料单查询
 *
 * @author yhh
 */
export function RsbPreparationPage (parameter) {
  return axios({
    url: '/rsbpreparation/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 胶合板备料单据详情
 *
 * @author yhh
 */
export function RsbPreparationDetailPage (params) {
  return axios({
    url: '/rsbpreparation/detailPage',
    method: 'get',
    params:params
  })
}


/**
 * 胶合板备料计划下发
 *
 * @author yhh
 */
export function Distribute (ids) {
  return axios({
    url: '/rsbpreparation/BatchPreparationMaterials',
    method: 'post',
    data:{id:ids}
  })
}