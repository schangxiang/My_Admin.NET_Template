﻿import { axios } from '@/utils/request'

/**
 * 查询发泡车间任务管理表（熟化库）
 *
 * @author yrj
 */
export function FoamingTaskPage (parameter) {
  return axios({
    url: '/FoamingTask/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 强制完成
 *
 * @author yuhuanhuan 
 */
export function FoamingTaskForceComplete (id) {
  return axios({
    url: '/FoamingTask/finish',
    method: 'post',
    data: {id}
  })
}

/**
 * 任务取消
 *
 * @author yuhuanhuan 
 */
export function FoamingTaskCancel (ids) {
  return axios({
    url: '/FoamingTask/batchCancel',
    method: 'post',
    data: {id:ids}
  })
}

/**
 * 任务暂停
 *
 * @author yuhuanhuan 
 */
export function FoamingTaskPause (ids) {
  return axios({
    url: '/FoamingTask/batchBreak',
    method: 'post',
    data: {id:ids}
  })
}

/**
 * 任务继续
 *
 * @author yuhuanhuan 
 */
export function FoamingTaskContinue (ids) {
  return axios({
    url: '/FoamingTask/batchContinue',
    method: 'post',
    data: {id:ids}
  })
}


/**
 * 更新优先级
 *
 * @author yuhuanhuan 
 */
export function FoamingTaskLevelUpdate (parameter) {
  return axios({
    url: '/FoamingTask/updateTaskLevel',
    method: 'post',
    data: parameter
  })
}


/**
 * 更新优先级
 *
 * @author yuhuanhuan 
 */
export function FoamingTaskUpLevel (id) {
  return axios({
    url: '/FoamingTask/upwardTaskLevel',
    method: 'post',
    data: {id}
  })
}

/**
 * 更新优先级
 *
 * @author yuhuanhuan 
 */
export function FoamingTaskDownLevel (id) {
  return axios({
    url: '/FoamingTask/downTaskLevel',
    method: 'post',
    data: {id}
  })
}

