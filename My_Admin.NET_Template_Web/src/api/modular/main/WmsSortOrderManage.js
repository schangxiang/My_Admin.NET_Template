﻿import { axios } from '@/utils/request'

/**
 * 查询分拣单
 *
 * @author XJF
 */
export function WmsSortOrderPage (parameter) {
  return axios({
    url: '/WmsSortOrder/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 分拣单列表
 *
 * @author XJF
 */
export function WmsSortOrderList (parameter) {
  return axios({
    url: '/WmsSortOrder/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加分拣单
 *
 * @author XJF
 */
export function WmsSortOrderAdd (parameter) {
  return axios({
    url: '/WmsSortOrder/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑分拣单
 *
 * @author XJF
 */
export function WmsSortOrderEdit (parameter) {
  return axios({
    url: '/WmsSortOrder/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除分拣单
 *
 * @author XJF
 */
export function WmsSortOrderDelete (parameter) {
  return axios({
    url: '/WmsSortOrder/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出分拣单的Excel文件
 *
 * @author XJF
 */
export function WmsSortOrderToExcel (parameter) {
  return axios({
    url: '/WmsSortOrder/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入分拣单的Excel文件
 *
 * @author XJF
 */
export function WmsSortOrderFromExcel (data, parameter) {
  return axios({
    url: '/WmsSortOrder/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载分拣单的Excel导入模板
 *
 * @author XJF
 */
export function WmsSortOrderDownloadExcelTemplate(parameter) {
  return axios({
    url: '/WmsSortOrder/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}



