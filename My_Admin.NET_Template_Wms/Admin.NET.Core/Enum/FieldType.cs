﻿using System.ComponentModel;
 
namespace Admin.NET.Core
{
    /// <summary>
    /// 文件存储字段
    /// </summary>
    [Description("字段类型")]
    public enum FieldType
    {
        /// <summary>
        /// 标量类型
        /// </summary>
        [Description("标量类型")]
        SCALAR = 1,

        /// <summary>
        /// 导航类型
        /// </summary>
        [Description("导航类型")]
        NAVIGATION = 2,

        /// <summary>
        /// 外键类型
        /// </summary>
        [Description("外键类型")]
        FK = 3,

        /// <summary>
        /// 指示类型
        /// </summary>
        [Description("指示类型")]
        NO_MAP = 4
    }
}