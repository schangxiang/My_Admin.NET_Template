﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    /// <summary>
    /// 任务管理表
    /// </summary>
    [Table("wms_task")]
    [Comment("任务管理表")]
    public class WmsTask : DEntityBase
    {
        /// <summary>
        /// 任务号
        /// </summary>
        [Comment("任务号")]
        [Required]
        [MaxLength(50)]
        public string TaskNo { get; set; }

        /// <summary>
        /// 任务方式;数据字典
        /// </summary>
        [Comment("任务方式")]
        [Required]
        public TaskModel TaskModel { get; set; }

        /// <summary>
        /// 任务类型;数据字典
        /// </summary>
        [Comment("任务类型")]
        [Required]
        public TaskType TaskType { get; set; }

        /// <summary>
        /// 任务级别
        /// </summary>
        [Comment("任务级别")]
        [Required]
        public int TaskLevel { get; set; } = 1;

        /// <summary>
        /// 任务状态;数据字典
        /// </summary>
        [Comment("任务状态")]
        [Required]
        public TaskStatusEnum TaskStatus { get; set; }

        /// <summary>
        /// WCS是否可以读取
        /// </summary>
        [Comment("WCS是否可以读取")]
        [Required]
        public bool IsRead { get; set; }

        /// <summary>
        /// 托盘编号
        /// </summary>
        [Comment("托盘编号")]
        [Required]
        [MaxLength(50)]
        public string ContainerCode { get; set; }

        /// <summary>
        /// 起始库位
        /// </summary>
        [Comment("起始库位")]
        [Required]
        [MaxLength(50)]
        public string SourcePlace { get; set; }

        /// <summary>
        /// 目标库位
        /// </summary>
        [Comment("目标库位")]
        [Required]
        [MaxLength(50)]
        public string ToPlace { get; set; }

        /// <summary>
        /// 巷道
        /// </summary>
        [Comment("巷道")]
        [Required]
        public int Aisle { get; set; }


        /// <summary>
        /// 是否空托
        /// </summary>
        [Comment("是否空托")]
        public YesOrNot EmptyContainer { get; set; }

        /// <summary>
        /// 库区名称
        /// </summary>
        [Comment("库区名称")]
        [Required]
        [MaxLength(50)]
        public string AreaName { get; set; }

        /// <summary>
        /// 单据明细Id
        /// </summary>
        [Comment("单据明细Id")]
        [Required]
        public long OrderDetailsId { get; set; }

        /// <summary>
        /// 组盘记录单据号
        /// </summary>
        [Comment("组盘记录单据号")]
        [Required]
        public string OrderNo { get; set; }

        /// <summary>
        /// 发送次数
        /// </summary>
        [Comment("发送次数")]
        public int SendTimes { get; set; }

        /// <summary>
        /// 任务执行堆垛机Id
        /// </summary>
        [Comment("任务执行堆垛机Id")]
        public int DodeviceId { get; set; }

        /// <summary>
        /// 设备执行节点Id
        /// </summary>
        [Comment("设备执行节点Id")]
        [MaxLength(50)]
        public string DodevicenodeId { get; set; }

        /// <summary>
        /// 设备类型
        /// </summary>
        [Comment("设备类型")]
        [MaxLength(50)]
        public DodeviceType Dodevicetype { get; set; }

        /// <summary>
        /// 任务设备状态;数据字典
        /// </summary>
        [Comment("任务设备状态")]
        [Required]
        public TaskDodeviceStatusEnum TaskDodeviceStatus { get; set; }

        /// <summary>
        /// 入库来源
        /// </summary>
        [Comment("入库来源")]
        [MaxLength(50)]
        public string Description { get; set; }
    }
}
