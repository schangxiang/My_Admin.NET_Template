﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 单据类型维护表服务
    /// </summary>
    public interface IWmsOrderTypeService
    {
        /// <summary>
        /// 增加单据类型维护表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task Add(AddWareOrderTypeInput input);

        /// <summary>
        /// 删除单据类型维护表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task Delete(DeleteWareOrderTypeInput input);

        /// <summary>
        /// 获取单据类型维护表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<WmsOrderTypeOutput> Get([FromQuery] QueryeWareOrderTypeInput input);

        /// <summary>
        /// 获取单据类型维护表列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<List<WmsOrderTypeOutput>> List([FromQuery] WmsOrderTypeInput input);

        /// <summary>
        /// 不分页查询单据类型维护表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<List<WmsOrderTypeOutput>> Page([FromQuery] WareOrderTypeSearch input);

        /// <summary>
        /// 更新单据类型维护表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task Update(UpdateWareOrderTypeInput input);

        /// <summary>
        /// 不分页查询单据类型维护表列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<List<WmsOrderTypeOutput>> ListNonPageAsync([FromQuery] WareOrderTypeSearchNonPage input);

        /// <summary>
        /// Excel模板导入单据类型维护表功能
        /// </summary>
        /// <param name="file"></param>
        /// <param name="importExcelType"></param>
        /// <returns></returns>
        Task<int> FromExcelAsync(IFormFile file, [FromQuery] ImportExcelType importExcelType);

        /// <summary>
        /// 根据版本下载单据类型维护表的Excel导入模板
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        Task<IActionResult> DownloadExcelTemplate(string version);

        /// <summary>
        /// 根据单据类型维护表查询参数导出Excel
        /// </summary>
        /// <param name="input">单据类型维护表查询参数</param>
        /// <returns>导出的Excel文件</returns>
        Task<IActionResult> ToExcelAsync([FromQuery] WareOrderTypeSearchNonPage input);
    }
}