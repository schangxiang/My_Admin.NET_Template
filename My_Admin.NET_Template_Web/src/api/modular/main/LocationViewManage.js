﻿import { axios } from '@/utils/request'

/**
 * 获取库区列表
 *
 * @author yhh
 */
export function GetArea () {
  return axios({
    url: '/LocationView/GetArea',
    method: 'get'
  })
}
/**
 * 根据库区获取巷道
 *
 * @author yhh
 */
export function GetAisle (parameter) {
  return axios({
    url: '/LocationView/GetAisle',
    method: 'get',
    params: parameter
  })
}

/**
 * 根据库区获取排列表
 *
 * @author yhh
 */
export function GetPalceRowno (parameter) {
  return axios({
    url: '/LocationView/GetPalceRowno',
    method: 'get',
    params: parameter
  })
}

/**
 * 获取一个排中的库位列表
 *
 * @author yhh
 */
export function GetPalceList (parameter) {
  return axios({
    url: '/LocationView/GetPalceList',
    method: 'get',
    params: parameter
  })
}

/**
 * 获取库区详情
 *
 * @author yhh
 */
export function GetLocationDetail (parameter) {
  return axios({
    url: '/LocationView/GetMaterialDetail',
    method: 'get',
    params: parameter
  })
}

/**
 * 更新库位锁定状态
 *
 * @author yhh
 */
export function UpdateLock (parameter) {
  return axios({
    url: '/LocationView/UpdatePalceIslock',
    method: 'get',
    params: parameter
  })
}

/**
 * 更新库位空闲状态
 *
 * @author yhh
 */
export function UpdateEmpty (parameter) {
  return axios({
    url: '/LocationView/UpdatePalceKongXian',
    method: 'get',
    params: parameter
  })
}
