﻿import { axios } from '@/utils/request'

/**
 * 查询出入库记录
 *
 * @author lidunanping
 */
export function FoamingAccessDetailsPage (parameter) {
  return axios({
    url: '/FoamingAccessDetails/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 出入库记录列表
 *
 * @author lidunanping
 */
export function FoamingAccessDetailsList (parameter) {
  return axios({
    url: '/FoamingAccessDetails/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加出入库记录
 *
 * @author lidunanping
 */
export function FoamingAccessDetailsAdd (parameter) {
  return axios({
    url: '/FoamingAccessDetails/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑出入库记录
 *
 * @author lidunanping
 */
export function FoamingAccessDetailsEdit (parameter) {
  return axios({
    url: '/FoamingAccessDetails/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除出入库记录
 *
 * @author lidunanping
 */
export function FoamingAccessDetailsDelete (parameter) {
  return axios({
    url: '/FoamingAccessDetails/delete',
    method: 'post',
    data: parameter
  })
}
