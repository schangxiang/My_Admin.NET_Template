﻿using System.ComponentModel.DataAnnotations;
using Admin.NET.Core;
using Admin.NET.Core.Service;

namespace Admin.NET.Application
{
    /// <summary>
    /// 库位信息查询参数
    /// </summary>
    public class WmsPlaceSearch : PageInputBase
    {
        /// <summary>
        /// 编码
        /// </summary>
        public virtual string Placecode { get; set; }
        
        /// <summary>
        /// 库位状态
        /// </summary>
        public virtual PlaceStatus? Placestatus { get; set; }
        
        /// <summary>
        /// 所在库区
        /// </summary>
        public virtual long? Areaid { get; set; }
        
        /// <summary>
        /// 排
        /// </summary>
        public virtual int? Rowno { get; set; }
        
        /// <summary>
        /// 列
        /// </summary>
        public virtual int? Columnno { get; set; }
        
        /// <summary>
        /// 层
        /// </summary>
        public virtual int? Layerno { get; set; }
        
        /// <summary>
        /// 进深号
        /// </summary>
        public virtual int? Deepcellno { get; set; }
        
        /// <summary>
        /// 巷道
        /// </summary>
        public virtual int? Aisle { get; set; }
        
        /// <summary>
        /// 线号
        /// </summary>
        public virtual int? Line { get; set; }
        
        /// <summary>
        /// 巷道左右
        /// </summary>
        //public virtual string Aisleside { get; set; }
        
        /// <summary>
        /// 是否锁定
        /// </summary>
        public virtual YesOrNot? Islock { get; set; }

        /// <summary>
        /// 
        /// </summary>
        //public virtual YesOrNot? Isfull { get; set; }

        /// <summary>
        /// 否正在操作
        /// </summary>
        //public virtual YesOrNot? Hastaskdoing { get; set; }

        /// <summary>
        /// 是否空托
        /// </summary>
        public virtual YesOrNot EmptyContainer { get; set; }

        /// <summary>
        /// 堆垛机内部的位置
        /// </summary>
        public virtual string PositionnoForSrm { get; set; }
        
        /// <summary>
        /// 库位X坐标
        /// </summary>
        public virtual string Xzb { get; set; }
        
        /// <summary>
        /// 库位Y坐标
        /// </summary>
        public virtual string Yzb { get; set; }
        
        /// <summary>
        /// 库位Z坐标
        /// </summary>
        public virtual string Zzb { get; set; }
        
        /// <summary>
        /// 库位长度
        /// </summary>
        public virtual decimal? Length { get; set; }
        
        /// <summary>
        /// 库位宽度
        /// </summary>
        public virtual decimal? Width { get; set; }
        
        /// <summary>
        /// 库位高度
        /// </summary>
        public virtual decimal? Height { get; set; }
        
        /// <summary>
        /// 最大承重
        /// </summary>
        public virtual decimal? Maxweight { get; set; }
        
        /// <summary>
        /// 库位高度
        /// </summary>
        public virtual Heightlevel? Heightlevel { get; set; }
        
        /// <summary>
        /// 优先级
        /// </summary>
        public virtual int? Priority { get; set; }
    }

    /// <summary>
    /// 库位信息输入参数
    /// </summary>
    public class WmsPlaceInput
    {
        /// <summary>
        /// 编码
        /// </summary>
        public virtual string Placecode { get; set; }
        
        /// <summary>
        /// 库位状态
        /// </summary>
        public virtual PlaceStatus Placestatus { get; set; }
        
        /// <summary>
        /// 所在库区
        /// </summary>
        public virtual long Areaid { get; set; }
        
        /// <summary>
        /// 排
        /// </summary>
        public virtual int Rowno { get; set; }
        
        /// <summary>
        /// 列
        /// </summary>
        public virtual int Columnno { get; set; }
        
        /// <summary>
        /// 层
        /// </summary>
        public virtual int Layerno { get; set; }
        
        /// <summary>
        /// 位
        /// </summary>
        public virtual int Deepcellno { get; set; }
        
        /// <summary>
        /// 巷道
        /// </summary>
        public virtual int Aisle { get; set; }
        
        /// <summary>
        /// 线号
        /// </summary>
        public virtual int Line { get; set; }
        
        /// <summary>
        /// 巷道左右
        /// </summary>
        //public virtual string Aisleside { get; set; }
        
        /// <summary>
        /// 是否锁定
        /// </summary>
        public virtual YesOrNot Islock { get; set; }

        /// <summary>
        /// 是否
        /// </summary>
        //public virtual YesOrNot Isfull { get; set; }

        /// <summary>
        /// 否正在操作
        /// </summary>
        //public virtual YesOrNot Hastaskdoing { get; set; }

        /// <summary>
        /// 是否空托
        /// </summary>
        public virtual YesOrNot EmptyContainer { get; set; }


        /// <summary>
        /// 堆垛机内部的位置
        /// </summary>
        public virtual string PositionnoForSrm { get; set; }
        
        /// <summary>
        /// 库位X坐标
        /// </summary>
        public virtual string Xzb { get; set; }
        
        /// <summary>
        /// 库位Y坐标
        /// </summary>
        public virtual string Yzb { get; set; }
        
        /// <summary>
        /// 库位Z坐标
        /// </summary>
        public virtual string Zzb { get; set; }
        
        /// <summary>
        /// 库位长度
        /// </summary>
        public virtual decimal Length { get; set; }
        
        /// <summary>
        /// 库位宽度
        /// </summary>
        public virtual decimal Width { get; set; }
        
        /// <summary>
        /// 库位高度
        /// </summary>
        public virtual decimal Height { get; set; }
        
        /// <summary>
        /// 最大承重
        /// </summary>
        public virtual decimal Maxweight { get; set; }
        
        /// <summary>
        /// 库位高度
        /// </summary>
        public virtual Heightlevel Heightlevel { get; set; }
        
        /// <summary>
        /// 优先级
        /// </summary>
        public virtual int Priority { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class AddWmsPlaceInput : WmsPlaceInput
    {
        /// <summary>
        /// 所在库区
        /// </summary>
        [Required(ErrorMessage = "所在库区不能为空")]
        public override long Areaid { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DeleteWmsPlaceInput : BaseId
    {
    }

    /// <summary>
    /// 
    /// </summary>
    public class UpdateWmsPlaceInput : WmsPlaceInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class QueryeWmsPlaceInput : BaseId
    {

    }
    /// <summary>
    /// 批量锁定参数
    /// </summary>
    public class MoreLockInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public List<long> Id { get; set; }
    }

}
