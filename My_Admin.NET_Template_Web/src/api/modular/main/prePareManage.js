import { axios } from '@/utils/request'

/**
 * 分页查询备料单
 *
 * @author yhh
 */
export function PrePareManagePage (parameter) {
  return axios({
    url: '/lespreparation/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加备料单
 *
 * @author yhh
 */
export function PrePareManageAdd (data) {
  return axios({
    url: '/lespreparation/AddPreparation',
    method: 'post',
    data: data
  })
}

/**
 * 删除备料单
 *
 * @author yhh
 */
export function PrePareManageDelete (ids) {
  return axios({
    url: '/lespreparation/delete',
    method: 'post',
    data: {id:ids}
  })
}

/**
 * 获取备料单号
 *
 * @author yhh
 */
export function GetBillNumber () {
  return axios({
    url: '/lespreparation/GetTakeMaterialsNo',
    method: 'get'
  })
}


/**
 * 备料物料查询
 *
 * @author yhh
 */
export function QueryMaterials (params) {
  return axios({
    url: '/lespreparation/MaterialSourcepage',
    method: 'get',
    params:params
  })
}

/**
 * 单据详情
 *
 * @author yhh
 */
export function CncTakeMaterialsDetailPage (params) {
  return axios({
    url: '/lespreparation/detailPage',
    method: 'get',
    params:params
  })
}