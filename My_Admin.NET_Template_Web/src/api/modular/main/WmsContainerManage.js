﻿import { axios } from '@/utils/request'

/**
 * 查询托盘信息
 *
 * @author XJF
 */
export function WmsContainerPage (parameter) {
  return axios({
    url: '/WmsContainer/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 托盘信息列表
 *
 * @author XJF
 */
export function WmsContainerList (parameter) {
  return axios({
    url: '/WmsContainer/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加托盘信息
 *
 * @author XJF
 */
export function WmsContainerAdd (parameter) {
  return axios({
    url: '/WmsContainer/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑托盘信息
 *
 * @author XJF
 */
export function WmsContainerEdit (parameter) {
  return axios({
    url: '/WmsContainer/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除托盘信息
 *
 * @author XJF
 */
export function WmsContainerDelete (parameter) {
  return axios({
    url: '/WmsContainer/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出托盘信息的Excel文件
 *
 * @author XJF
 */
export function WmsContainerToExcel (parameter) {
  return axios({
    url: '/WmsContainer/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入托盘信息的Excel文件
 *
 * @author XJF
 */
export function WmsContainerFromExcel (data, parameter) {
  return axios({
    url: '/WmsContainer/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载托盘信息的Excel导入模板
 *
 * @author XJF
 */
export function WmsContainerDownloadExcelTemplate(parameter) {
  return axios({
    url: '/WmsContainer/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}



