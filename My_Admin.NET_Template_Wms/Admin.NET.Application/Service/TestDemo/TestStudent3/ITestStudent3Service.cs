﻿using Admin.NET.Core;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Admin.NET.Application
{
    public interface ITestStudent3Service
    {
        Task<TestStudent3Output> Get([FromQuery] QueryeTestStudent3Input input);
        Task<List<TestStudent3Output>> List([FromQuery] TestStudent3Input input);
        Task<PageResult<TestStudent3Output>> Page([FromQuery] TestStudent3Search input);
        Task<List<TestStudent3Output>> ListNonPageAsync([FromQuery] TestStudent3SearchNonPage input);
        
    }
}