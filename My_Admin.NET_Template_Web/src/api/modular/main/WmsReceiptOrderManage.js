﻿import { axios } from '@/utils/request'

/**
 * 查询收货单
 *
 * @author XJF
 */
export function WmsReceiptOrderPage (parameter) {
  return axios({
    url: '/WmsReceiptOrder/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 收货单列表
 *
 * @author XJF
 */
export function WmsReceiptOrderList (parameter) {
  return axios({
    url: '/WmsReceiptOrder/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加收货单
 *
 * @author XJF
 */
export function WmsReceiptOrderAdd (parameter) {
  return axios({
    url: '/WmsReceiptOrder/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑收货单
 *
 * @author XJF
 */
export function WmsReceiptOrderEdit (parameter) {
  return axios({
    url: '/WmsReceiptOrder/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除收货单
 *
 * @author XJF
 */
export function WmsReceiptOrderDelete (parameter) {
  return axios({
    url: '/WmsReceiptOrder/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出收货单的Excel文件
 *
 * @author XJF
 */
export function WmsReceiptOrderToExcel (parameter) {
  return axios({
    url: '/WmsReceiptOrder/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入收货单的Excel文件
 *
 * @author XJF
 */
export function WmsReceiptOrderFromExcel (data, parameter) {
  return axios({
    url: '/WmsReceiptOrder/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载收货单的Excel导入模板
 *
 * @author XJF
 */
export function WmsReceiptOrderDownloadExcelTemplate(parameter) {
  return axios({
    url: '/WmsReceiptOrder/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}




/**
 * 物料详情
 */
 export function WmsOrderLldDetail (parameter) {
  return axios({
    url: '/WmsReceiptOrder/PageDetail',
    method: 'get',
    params: parameter
  })
}

// 确认收货
export function WmsReceiptOrderSh(parameter) {
  return axios({
    url: '/wmsreceiptorder/PcConfirmReceipt',
    method: 'post',
    data: parameter
  })
}


/**
 * 报检
 *
 */
 export function WmsOrderBaojian(parameter) {
  return axios({
    url: '/wmsinspectorder/InspectionDeclaration',
    method: 'post',
    data: parameter
  })
}