﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// WMS公共服务
    /// </summary>
    [ApiDescriptionSettings("WMS公共服务", Name = "WmsCommon", Order = 100)]
    [Route("api")]
    public class WmsCommonService : IWmsCommonService, IDynamicApiController, ITransient
    {

        private static readonly SemaphoreSlim semaphore_GetMnemonicCode = new SemaphoreSlim(1, 1);//SemaphoreSlim实例，并设置最大并发访问数为1。    

        /// <summary>
        /// 构造函数
        /// </summary>
        public WmsCommonService(

        )
        {
        }

        /// <summary>
        /// 获取通用编号
        /// </summary>
        /// <returns></returns>
        [HttpGet("WmsCommon/getSerial")]
        [AllowAnonymous]
        public async Task<string> GetSerial([FromQuery] EnumSerialType SerialType)
        {
            var returnstr = await SerialUtil.GetSerial(SerialType);
            return returnstr;
        }


        /// <summary>
        /// 生成助记码
        /// 注意：只能调这个接口，不能自己写方法调，否则可能会导致文件读取冲突造成错误！
        /// </summary>
        /// <returns></returns>
        [HttpGet("WmsCommon/getMnemonicCode")]
        [AllowAnonymous]
        public async Task<string> GetMnemonicCode([FromQuery] string name)
        {

            await semaphore_GetMnemonicCode.WaitAsync();
            try
            {
                MnemonicCodeUtil.fun_LoadWords();
                var retMsg = MnemonicCodeUtil.funChineseSpell(name, false);
                return retMsg;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                semaphore_GetMnemonicCode.Release();
            }
        }
    }

}
