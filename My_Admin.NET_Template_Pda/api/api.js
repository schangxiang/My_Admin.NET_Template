// 同时发送异步请求的次数，防止一次点击有多次请求
let ajaxTime = 0;
// export const baseUrl = 'http://172.27.96.26:5566' // 牧野
export const baseUrl = 'http://localhost:8090'
// export const baseUrl = 'http://192.168.20.51:7788'
 // export const baseUrl = 'http://10.135.75.70:7788'
 // export const baseUrl = 'http://192.168.216.203:7788'
//export const baseUrl = 'http://192.168.0.102:7788'
// 公共的request方法
function request(option) {
	//请求头.
	const header = {};
	// 返回方式
	header['Content-Type'] = option.contentType || 'application/json';
	// 判断是否是登录请求，不是在header中加上token
	if (option.url != '/login') {
		header['token'] = uni.getStorageSync('user_token') ? 'Bearer ' + uni.getStorageSync('user_token') : '';
		header.Authorization = uni.getStorageSync('user_token') ? 'Bearer ' + uni.getStorageSync('user_token') : ''
	}
	ajaxTime++;
	// 显示加载中效果
	uni.showLoading({
		title: '加载中...',
		mask: true
	});

	return new Promise((resolve, reject) => {
		uni.request({
			url: baseUrl + option.url,
			method: option.method || 'POST',
			data: option.data || {},
			header,
			timeout: 10000,
			success: (res) => {
				if(res.statusCode == 200) {
					const result = res.data;
					if (result.code === 200||!Boolean(result.code)) {
						// 请求状态正常，返回数据
						resolve(result)
					} else if (result.code === 401) { 
						//token失效，清除token关闭当前页面，跳转到登录
						uni.showModal({
							title: '提示',
							content: 'token失效，请重新登陆',
							showCancel: false,
							success:()=>{
								// 清楚token
								uni.removeStorageSync('token')
								// 跳转到登录
								uni.redirectTo({
									url: '/pages/login/index'
								})
							}
						})
						
					} else {
						uni.showModal({
							title: '提示',
							content: `${result.code}:${JSON.stringify(result.message)}`,
							showCancel: false
						})
						// 异常，返回异常code和message
						reject(result)
					}
					
				}else {
					uni.showModal({
						title: '提示',
						content: `${res.statusCode}:${JSON.stringify(res.errMsg)}`,
						showCancel: false
					})
					
					const {statusCode,errMsg} = res;
					reject({statusCode,errMsg})
				}
			},
			fail: (err) => {
				// 请求失败
				reject(err);
			},
			complete: () => {
				// 请求完成后判断状态
				ajaxTime--;
				// 只有值等于0，才清除转圈效果
				if (ajaxTime == 0) {
					uni.hideLoading()
				}
			}
		})
	})
}

export default request
