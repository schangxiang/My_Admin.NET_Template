﻿using Admin.NET.Core;
using Admin.NET.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace Admin.NET.Application
{    
    
    /// <summary>
    /// PDA菜单查询参数
    /// </summary>
    public class WmsPdaPowerSearch : PageInputBase
    {
        /// <summary>
        /// 图标
        /// </summary>
        public virtual string Icon { get; set; }
        
        /// <summary>
        /// 编码
        /// </summary>
        public virtual string Code { get; set; }
        
        /// <summary>
        /// 名称
        /// </summary>
        public virtual string Name { get; set; }
        
        /// <summary>
        /// 颜色
        /// </summary>
        public virtual string Clolor { get; set; }
        
        /// <summary>
        /// 所属车间
        /// </summary>
        public virtual Admin.NET.Core.LesWorkShopType? WorkShopType { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState IssueState { get; set; }
        
    }

        /// <summary>
    /// PDA菜单不分页查询参数
    /// </summary>
    public class WmsPdaPowerSearchNonPage : PageInputNonPageBase
    {
        /// <summary>
        /// 图标
        /// </summary>
        public virtual string Icon { get; set; }
        
        /// <summary>
        /// 编码
        /// </summary>
        public virtual string Code { get; set; }
        
        /// <summary>
        /// 名称
        /// </summary>
        public virtual string Name { get; set; }
        
        /// <summary>
        /// 颜色
        /// </summary>
        public virtual string Clolor { get; set; }
        
        /// <summary>
        /// 所属车间
        /// </summary>
        public virtual Admin.NET.Core.LesWorkShopType? WorkShopType { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState? IssueState { get; set; }
        
    }

    /// <summary>
    /// PDA菜单输入参数
    /// </summary>
    public class WmsPdaPowerInput
    {
        /// <summary>
        /// 图标
        /// </summary>
        public virtual string Icon { get; set; }
        
        /// <summary>
        /// 编码
        /// </summary>
        public virtual string Code { get; set; }
        
        /// <summary>
        /// 名称
        /// </summary>
        public virtual string Name { get; set; }
        
        /// <summary>
        /// 颜色
        /// </summary>
        public virtual string Clolor { get; set; }
        
        /// <summary>
        /// 所属车间
        /// </summary>
        public virtual Admin.NET.Core.LesWorkShopType WorkShopType { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState IssueState { get; set; }
        
    }

    public class AddWmsPdaPowerInput : WmsPdaPowerInput
    {
    }

    public class DeleteWmsPdaPowerInput : BaseId
    {
    }

    public class UpdateWmsPdaPowerInput : WmsPdaPowerInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
        
    }

    public class QueryeWmsPdaPowerInput : BaseId
    {

    }
}
