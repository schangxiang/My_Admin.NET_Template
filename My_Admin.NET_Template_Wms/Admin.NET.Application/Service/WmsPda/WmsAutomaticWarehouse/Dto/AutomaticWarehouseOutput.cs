﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 根据单据编号获取任务返回值
    /// </summary>
    public class AutomaticWarehouseOutput
    {
        /// <summary>
        /// 物料编号
        /// </summary>
        public string MaterialNo { get; set; }

        /// <summary>
        /// 批次    
        /// </summary>
        public string Batch { get; set; }

        /// <summary>
        /// 流水号
        /// </summary>
        public string SerialNo { get; set; }

        /// <summary>
        /// 长    
        /// </summary>
        public int Long { get; set; }

        /// <summary>
        /// 宽    
        /// </summary>
        public int Wide { get; set; }

        /// <summary>
        /// 高    
        /// </summary>
        public int High { get; set; }

        /// <summary>
        /// 入库口
        /// </summary>
        public string PreferredPort { get; set; }

        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
    }

    
    /// <summary>
    /// 返回参数
    /// </summary>
    public class OutEntranceListDefaultCodeOutput
    {
        /// <summary>
        /// 默认入库口
        /// </summary>
        public string DefaultCode { get; set; }

        /// <summary>
        /// 入库口名称
        /// </summary>
        public List<OutEntranceListOutput> OutEntranceListOutputList { get; set; }
    }

    /// <summary>
    /// 获取入库口下拉框输出参数
    /// </summary>
    public class OutEntranceListOutput
    {
        /// <summary>
        /// 入库口Code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 入库口名称
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// 反馈任务状态输出参数
    /// </summary>
    public class CallBackOutput
    {
        /// <summary>
        /// 库位编码
        /// </summary>
        public virtual string LocationCode { get; set; }
    }
}
