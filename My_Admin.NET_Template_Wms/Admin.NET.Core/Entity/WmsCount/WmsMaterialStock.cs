﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    /// <summary>
    /// 库存表
    /// </summary>
    [Table("wms_materialstock")]
    [Comment("库存表")]
    public class WmsMaterialStock : DEntityBase
    {
        /// <summary>
        /// 物料编号
        /// </summary>
        [Comment("物料编号")]
        [Required]
        [MaxLength(50)]
        public string MaterialNo { get; set; }

        /// <summary>
        /// 物料类别;数据字典
        /// </summary>
        [Comment("物料类别")]
        [Required]
        public MaterialType MaterialType { get; set; }

        /// <summary>
        /// 物料批次
        /// </summary>
        [Comment("物料批次")]
        [Required]
        [MaxLength(50)]
        public string MaterialBatch { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        [Comment("物料名称")]
        [Required]
        [MaxLength(50)]
        public string MaterialName { get; set; }

        /// <summary>
        /// 物料规格
        /// </summary>
        [Comment("物料规格")]
        [MaxLength(50)]
        public string MaterialSpec { get; set; }

        /// <summary>
        /// 长    
        /// </summary>
        [Comment("长")]
        public int Long { get; set; }

        /// <summary>
        /// 宽    
        /// </summary>
        [Comment("宽")]
        public int Wide { get; set; }

        /// <summary>
        /// 高    
        /// </summary>
        [Comment("高")]
        public int High { get; set; }

        /// <summary>
        /// 物料密度
        /// </summary>
        [Comment("物料密度")]
        [Required]
        [MaxLength(50)]
        public string MaterialDensity { get; set; }

        /// <summary>
        /// 检验方式;数据字典
        /// </summary>
        [Comment("检验方式")]
        public MaterialInspection InspectionMethod { get; set; }

        /// <summary>
        /// 单位类别;数据字典
        /// </summary>
        [Comment("单位类别")]
        public UnitType UnitType { get; set; }

        /// <summary>
        /// 单位编号;数据字典
        /// </summary>
        [Comment("单位编号")]
        public UnitNoType UnitNo { get; set; }

        /// <summary>
        /// 实物库存数
        /// </summary>
        [Comment("实物库存数")]
        [Required]
        public decimal StockNumber { get; set; }

        /// <summary>
        /// 库位编码
        /// </summary>
        [Comment("库位编码")]
        [Required]
        [MaxLength(50)]
        public string PlaceCode { get; set; }

        /// <summary>
        /// 托盘Id
        /// </summary>
        [Comment("托盘Id")]
        [Required]
        public long ContainerId{ get; set; }

        /// <summary>
        /// 托盘编码
        /// </summary>
        [Comment("托盘编码")]
        [Required]
        [MaxLength(50)]
        public string ContainerCode { get; set; }

        /// <summary>
        /// 库区Id
        /// </summary>
        [Comment("库区Id")]
        [Required]
        public long AreaId { get; set; }

        /// <summary>
        /// 账面数量
        /// </summary>
        [Comment("账面数量")]
        [Required]
        public decimal QuantityOfBill { get; set; }

        /// <summary>
        /// 入库来源
        /// </summary>
        [Comment("入库来源")]
        public RuKuSourceEnum Source { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        [Comment("项目编号")]
        [MaxLength(255)]
        public string ProjectCode { get; set; }

        /// <summary>
        /// 合同编号
        /// </summary>
        [Comment("合同编号")]
        [MaxLength(255)]
        public string ContractCode { get; set; }

        /// <summary>
        /// TA号
        /// </summary>
        [Comment("TA号")]
        [MaxLength(255)]
        public string TACode { get; set; }
    }
}
