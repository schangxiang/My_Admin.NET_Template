﻿using Admin.NET.Core;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Admin.NET.Application
{
    public interface IVStudentService
    {
        Task<VStudentOutput> Get([FromQuery] QueryeVStudentInput input);
        Task<List<VStudentOutput>> List([FromQuery] VStudentInput input);
        Task<PageResult<VStudentOutput>> Page([FromQuery] VStudentSearch input);
        Task<List<VStudentOutput>> ListNonPageAsync([FromQuery] VStudentSearchNonPage input);
        

        Task<IActionResult> ToExcelAsync([FromQuery] VStudentSearchNonPage input);
    }
}