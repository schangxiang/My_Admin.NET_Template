﻿using Admin.NET.Core;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Admin.NET.Application
{
    public interface ILesDeviceWaringService
    {
        Task Add(AddLesDeviceWaringInput input);
        Task Delete(DeleteLesDeviceWaringInput input);
        Task<LesDeviceWaringOutput> Get([FromQuery] QueryeLesDeviceWaringInput input);
        Task<List<LesDeviceWaringOutput>> List([FromQuery] LesDeviceWaringInput input);
        Task<PageResult<LesDeviceWaringOutput>> Page([FromQuery] LesDeviceWaringSearch input);
        Task Update(UpdateLesDeviceWaringInput input);

        Task<List<LesDeviceWaringOutput>> ListNonPageAsync([FromQuery] LesDeviceWaringSearchNonPage input);

        Task<int> FromExcelAsync(IFormFile file, [FromQuery] ImportExcelType importExcelType);

        Task<IActionResult> DownloadExcelTemplate(string version);
    }
}