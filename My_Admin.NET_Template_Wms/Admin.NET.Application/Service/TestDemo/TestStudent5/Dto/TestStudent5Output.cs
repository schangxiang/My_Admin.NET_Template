﻿using Admin.NET.Core.Util.LowCode.Dto;
using System;

namespace Admin.NET.Application
{
    /// <summary>
    /// 测试学生表输出参数
    /// </summary>
    public class TestStudent5Output
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// 年龄
        /// </summary>
        public int Age { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTimeOffset CreatedTime { get; set; }
        
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTimeOffset UpdatedTime { get; set; }
        
        /// <summary>
        /// 创建者名称
        /// </summary>
        public string CreatedUserName { get; set; }
        
        /// <summary>
        /// 修改者名称
        /// </summary>
        public string UpdatedUserName { get; set; }
        
    }
}
