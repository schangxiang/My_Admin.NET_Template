﻿using Furion.DatabaseAccessor;
using Furion.DatabaseAccessor.Extensions;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using Admin.NET.Core;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Microsoft.AspNetCore.Http;
using System.Text;
using System.Web;

namespace Admin.NET.Application
{
    /// <summary>
    /// 分拣单服务
    /// </summary>
    [ApiDescriptionSettings("单据管理", Name = "WmsSortOrder", Order = 100)]
    [Route("api/[Controller]")]
    public class WmsSortOrderService : IWmsSortOrderService, IDynamicApiController, ITransient
    {
        private readonly IRepository<WmsSortOrder,MasterDbContextLocator> _wmsSortOrderRep;
        private readonly IRepository<SysDictType, MasterDbContextLocator> _sysDictTypeRep;
        private readonly IRepository<SysDictData, MasterDbContextLocator> _sysDictDataRep;
        private readonly ISysExcelTemplateService _sysExcelTemplateService;
        private readonly static object _lock = new();

        public WmsSortOrderService(
            IRepository<WmsSortOrder,MasterDbContextLocator> wmsSortOrderRep
            ,IRepository<SysDictType, MasterDbContextLocator> sysDictTypeRep
            ,IRepository<SysDictData, MasterDbContextLocator> sysDictDataRep
            ,ISysExcelTemplateService sysExcelTemplateService
        )
        {
            _wmsSortOrderRep = wmsSortOrderRep;
         _sysDictTypeRep = sysDictTypeRep;
         _sysDictDataRep = sysDictDataRep;
         _sysExcelTemplateService = sysExcelTemplateService;
        }

        /// <summary>
        /// 分页查询分拣单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("page")]
        public async Task<PageResult<WmsSortOrderOutput>> Page([FromQuery] WmsSortOrderSearch input)
        {
            var wmsSortOrders = await _wmsSortOrderRep.DetachedEntities
                                     .Where(!string.IsNullOrEmpty(input.OrderNo), u => EF.Functions.Like(u.OrderNo, $"%{input.OrderNo.Trim()}%"))
                                     .Where(!string.IsNullOrEmpty(input.ProjectCode), u => EF.Functions.Like(u.ProjectCode, $"%{input.ProjectCode.Trim()}%"))
                                     .Where(!string.IsNullOrEmpty(input.TACode), u => EF.Functions.Like(u.TACode, $"%{input.TACode.Trim()}%"))
                                     .Where(!string.IsNullOrEmpty(input.PartCode), u => EF.Functions.Like(u.PartCode, $"%{input.PartCode.Trim()}%"))
                                     .Where(!string.IsNullOrEmpty(input.PlaceCode), u => EF.Functions.Like(u.PlaceCode, $"%{input.PlaceCode.Trim()}%"))
                                     .Where(!string.IsNullOrEmpty(input.ContainerCode), u => EF.Functions.Like(u.ContainerCode, $"%{input.ContainerCode.Trim()}%"))
                                     .Where(input.SortStatus != null, u => u.SortStatus == input.SortStatus)
                                     .OrderBy(PageInputOrder.OrderBuilder<WmsSortOrderSearch>(input))
                                     .ProjectToType<WmsSortOrderOutput>()
                                     .ToADPagedListAsync(input.PageNo, input.PageSize);
            return wmsSortOrders;
        }

        /// <summary>
        /// 不分页查询分拣单列表
        /// </summary>
        /// <param name="input">分拣单查询参数</param>
        /// <returns>(分拣单)实例列表</returns>
        [HttpGet("listNonPage")]
        public async Task<List<WmsSortOrderOutput>> ListNonPageAsync([FromQuery] WmsSortOrderSearchNonPage input)
        {
            var pOrderNo = input.OrderNo?.Trim() ?? "";
            var pProjectCode = input.ProjectCode?.Trim() ?? "";
            var pTACode = input.TACode?.Trim() ?? "";
            var pPartCode = input.PartCode?.Trim() ?? "";
            var pPlaceCode = input.PlaceCode?.Trim() ?? "";
            var pContainerCode = input.ContainerCode?.Trim() ?? "";
            var pSortStatus = input.SortStatus;
            var wmsSortOrders = await _wmsSortOrderRep.DetachedEntities
                .Where(!string.IsNullOrEmpty(pOrderNo), u => EF.Functions.Like(u.OrderNo, $"%{pOrderNo}%")) 
                .Where(!string.IsNullOrEmpty(pProjectCode), u => EF.Functions.Like(u.ProjectCode, $"%{pProjectCode}%")) 
                .Where(!string.IsNullOrEmpty(pTACode), u => EF.Functions.Like(u.TACode, $"%{pTACode}%")) 
                .Where(!string.IsNullOrEmpty(pPartCode), u => EF.Functions.Like(u.PartCode, $"%{pPartCode}%")) 
                .Where(!string.IsNullOrEmpty(pPlaceCode), u => EF.Functions.Like(u.PlaceCode, $"%{pPlaceCode}%")) 
                .Where(!string.IsNullOrEmpty(pContainerCode), u => EF.Functions.Like(u.ContainerCode, $"%{pContainerCode}%")) 
                .Where(pSortStatus != null, u => u.SortStatus == pSortStatus)
            .OrderBy(PageInputOrder.OrderNonPageBuilder(input))
            .ProjectToType<WmsSortOrderOutput>()
            .ToListAsync();
            return wmsSortOrders;
        }


        /// <summary>
        /// 增加分拣单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("add")]
        public async Task Add(AddWmsSortOrderInput input)
        {
            var wmsSortOrder = input.Adapt<WmsSortOrder>();
            await _wmsSortOrderRep.InsertAsync(wmsSortOrder);
        }

        /// <summary>
        /// 删除分拣单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("delete")]
        public async Task Delete(DeleteWmsSortOrderInput input)
        {
            var wmsSortOrder = await _wmsSortOrderRep.FirstOrDefaultAsync(u => u.Id == input.Id);
            await _wmsSortOrderRep.DeleteAsync(wmsSortOrder);
        }

        /// <summary>
        /// 更新分拣单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("edit")]
        public async Task Update(UpdateWmsSortOrderInput input)
        {
            var isExist = await _wmsSortOrderRep.AnyAsync(u => u.Id == input.Id, false);
            if (!isExist) throw Oops.Oh(ErrorCode.D3000);

            var wmsSortOrder = input.Adapt<WmsSortOrder>();
            await _wmsSortOrderRep.UpdateAsync(wmsSortOrder,ignoreNullValues:true);
        }

        /// <summary>
        /// 获取分拣单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("detail")]
        public async Task<WmsSortOrderOutput> Get([FromQuery] QueryeWmsSortOrderInput input)
        {
            return (await _wmsSortOrderRep.DetachedEntities.FirstOrDefaultAsync(u => u.Id == input.Id)).Adapt<WmsSortOrderOutput>();
        }

        /// <summary>
        /// 获取分拣单列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("list")]
        public async Task<List<WmsSortOrderOutput>> List([FromQuery] WmsSortOrderInput input)
        {
            return await _wmsSortOrderRep.DetachedEntities.ProjectToType<WmsSortOrderOutput>().ToListAsync();
        }    
         /// <summary>
        /// Excel模板导入分拣单功能
        /// </summary>
        /// <param name="file">Excel模板文件</param>
        /// <param name="importExcelType">Excel导入方式</param>
        /// <returns>导入的记录数</returns>
        [HttpPost("fromExcel")]
        public async Task<int> FromExcelAsync(IFormFile file, [FromQuery] ImportExcelType importExcelType)
        {
            int size = 200;
            var excelTemplate = await _sysExcelTemplateService.GetByAppNameAndClassNameAndVersionAsync("WmsSortOrder", "v2");
            if (excelTemplate == null) throw Oops.Oh(ErrorCode.Excel002);
            var keys = excelTemplate.UnionUniqueFields.Split(",") ?? Array.Empty<string>();
            for (var i = 0; i < keys.Length; i++) 
            {
                keys[i] = keys[i]?.Trim() ?? string.Empty;
            }
            ExcelUtil.FromExcel(file, excelTemplate.HeadStartLine, excelTemplate.DataStartLine, out List<string> headers, out List<List<object?>> data, out string sheetName);
            List<WmsSortOrderOutput> wmsSortOrderList = DataConvertUtil.ToObjectList(headers, data, sheetName, keys, excelTemplate?.DataStartLine ?? 2, out Dictionary<string, WmsSortOrderOutput> dict);
            List<Dictionary<string, object>> uniqueKeyValueDictList = wmsSortOrderList.ParseUniqueKeyValueDictList(keys.ToList(), excelTemplate?.DataStartLine ?? 2, sheetName);
            var filters = DataConvertUtil.GetExpressionListByUniqueDict<WmsSortOrder>(keys.ToList(), uniqueKeyValueDictList, size);
            var selectKeys = keys.ToList();
            if(!selectKeys.Contains("Id")) selectKeys.Add("Id");
            var selector = DataConvertUtil.GetSelectExpressionListByUniqueDict<WmsSortOrder, WmsSortOrderOutput>(selectKeys);
            List<WmsSortOrder> updates = new();
            List<WmsSortOrder> adds = new();

            lock (_lock)
            {
                foreach (var filter in filters)
                {
                    var wmsSortOrderExistSubList = _wmsSortOrderRep.Where(filter).Select(selector).ToList();
                    wmsSortOrderExistSubList.ForEach(x => 
                    {
                        var k = DataConvertUtil.GetKey(x, keys);
                        if (dict.ContainsKey(k)) dict[k].Id = x.Id;
                    });
                }
                foreach (var wmsSortOrder in wmsSortOrderList) 
                {
                    if (wmsSortOrder.Id > 0)
                    {
                        if (importExcelType == ImportExcelType.ADD_AND_UPDATE) updates.Add(wmsSortOrder.Adapt<WmsSortOrder>());
                    }
                    else 
                    {
                        adds.Add(wmsSortOrder.Adapt<WmsSortOrder>());
                    }
                }

                if (importExcelType == ImportExcelType.ADD_AND_UPDATE) updates.ForEach(x => _wmsSortOrderRep.Update(x));
                

                var maxId = _wmsSortOrderRep.DetachedEntities.OrderByDescending(x => x.Id).Select(x => x.Id).FirstOrDefault();
                adds.ForEach(x => x.Id = ++maxId);    
                Db.GetDbContext().Set<WmsSortOrder>().AddRange(adds);  
                Db.GetDbContext().SaveChanges();
               
            }
            await Task.CompletedTask;
            return adds.Count;
        }

        /// <summary>
        /// 根据版本下载分拣单的Excel导入模板
        /// </summary>
        /// <param name="version">模板版本</param>
        /// <returns>下载的模板文件</returns>
        [HttpGet("downloadExcelTemplate")]
        public async Task<IActionResult> DownloadExcelTemplate([FromQuery] string version)
        {
            var excelTemplate = await _sysExcelTemplateService.GetByAppNameAndClassNameAndVersionAsync("WmsSortOrder", version);
            if (excelTemplate == null) throw Oops.Oh(ErrorCode.Excel002);
            var path = Path.Combine(@"\", excelTemplate.TemplateFileName);
            Stream ms = FileUtil.Download(path, excelTemplate.TemplateFileName);
            var fileName = HttpUtility.UrlEncode($"{excelTemplate.Name}导入模板.xlsx", Encoding.GetEncoding("UTF-8"));
            return new FileStreamResult(ms, "application/octet-stream") { FileDownloadName = fileName };
        }
        /// <summary>
        /// 根据分拣单查询参数导出Excel
        /// </summary>
        /// <param name="input">分拣单查询参数</param>
        /// <returns>导出的Excel文件</returns>
        [HttpGet("toExcel")]
        public async Task<IActionResult> ToExcelAsync([FromQuery] WmsSortOrderSearchNonPage input)
        {
            var wmsSortOrderList = await ListNonPageAsync(input);
            MemoryStream ms = new();
            DataConvertUtil.ToExcelData(wmsSortOrderList, _sysDictTypeRep, _sysDictDataRep, out List<string> headers, 
                out List<List<object>> data, out string sheetName);
            var excelTemplate = await _sysExcelTemplateService.GetByAppNameAndClassNameAndVersionAsync("WmsSortOrder", "v1");
            if (excelTemplate != null)
            {
                ExcelUtil.ToExcel(excelTemplate.TemplateFileName, headers, data, sheetName, excelTemplate.HeadStartLine, excelTemplate.DataStartLine, ms);
            }
            else 
            {
                ExcelUtil.ToExcel(headers, data, sheetName, ms);
            }
            ms.Position = 0;
            var fileName = HttpUtility.UrlEncode($"{sheetName}[{DateTimeOffset.Now:yyyy-MM-dd}].xlsx", Encoding.GetEncoding("UTF-8"));
            return new FileStreamResult(ms, "application/octet-stream") { FileDownloadName = fileName };
        }
    }
}
