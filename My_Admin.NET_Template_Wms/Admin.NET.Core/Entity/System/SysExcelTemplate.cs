﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    /// <summary>
    /// Excel模板
    /// </summary>
    [Table("sys_excel_template")]
    [Comment("Excel模板")]
    public class SysExcelTemplate : DEntityBase, IEntityTypeBuilder<SysExcelTemplate>
    {
        /// <summary>
        /// 模板名称
        /// </summary>
        [Comment("模板名称")]
        [Required, MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// 版本
        /// </summary>
        [Comment("版本")]
        [Required, MaxLength(20)]
        public string Version { get; set; }

        /// <summary>
        /// 所属应用
        /// </summary>
        [Comment("所属应用")]
        [Required, MaxLength(20)]
        public string AppName { get; set; }


        /// <summary>
        /// 类名
        /// </summary> 
        [Comment("类名")]
        [Required, MaxLength(50)]
        public string ClassName { get; set; }

        /// <summary>
        /// 模板文件名称
        /// </summary> 
        [Comment("模板文件名称")]
        [Required, MaxLength(50)]
        public string TemplateFileName { get; set; }

        /// <summary>
        /// 唯一字段集
        /// </summary> 
        [Comment("唯一字段集")]
        [Required, MaxLength(300)]
        public string UnionUniqueFields { get; set; }


        /// <summary>
        /// 表头开始行
        /// </summary>
        [Comment("表头开始行")]
        public int HeadStartLine { get; set; }

        /// <summary>
        /// 数据开始行
        /// </summary>
        [Comment("数据开始行")]
        public int DataStartLine { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Comment("状态")]
        public CommonStatus Status { get; set; } = CommonStatus.ENABLE;

        /// <summary>
        /// 配置唯一键
        /// </summary>
        /// <param name="entityBuilder"></param>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        public void Configure(EntityTypeBuilder<SysExcelTemplate> entityBuilder, DbContext dbContext, Type dbContextLocator)
        {
            entityBuilder.HasAlternateKey(x => new { x.ClassName, x.Version, x.AppName });
        }
    }
}