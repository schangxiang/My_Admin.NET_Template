﻿#nullable enable
using Admin.NET.Core;
using Admin.NET.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace Admin.NET.Application
{
    /// <summary>
    /// Excel模板查询参数
    /// </summary>
    public class SysExcelTemplateSearch : PageInputBase
    {
        /// <summary>
        /// 模板名称
        /// </summary>
        public virtual string? Name { get; set; }

        /// <summary>
        /// 版本
        /// </summary>
        public virtual string? Version { get; set; }

        /// <summary>
        /// 所属应用
        /// </summary>
        public virtual string? AppName { get; set; }

        /// <summary>
        /// 类名
        /// </summary>
        public virtual string? ClassName { get; set; }

        /// <summary>
        /// 模板文件名称
        /// </summary>
        public virtual string? TemplateFileName { get; set; }

        /// <summary>
        /// 唯一字段集
        /// </summary>
        public virtual string? UnionUniqueFields { get; set; }

        /// <summary>
        /// 表头开始行
        /// </summary>
        public virtual int? HeadStartLine { get; set; }

        /// <summary>
        /// 数据开始行
        /// </summary>
        public virtual int? DataStartLine { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public virtual CommonStatus? Status { get; set; }

    }

    /// <summary>
    /// Excel模板不分页查询参数
    /// </summary>
    public class SysExcelTemplateSearchNonPage : PageInputNonPageBase
    {
        /// <summary>
        /// 模板名称
        /// </summary>
        public virtual string? Name { get; set; }

        /// <summary>
        /// 版本
        /// </summary>
        public virtual string? Version { get; set; }

        /// <summary>
        /// 所属应用
        /// </summary>
        public virtual string? AppName { get; set; }

        /// <summary>
        /// 类名
        /// </summary>
        public virtual string? ClassName { get; set; }

        /// <summary>
        /// 模板文件名称
        /// </summary>
        public virtual string? TemplateFileName { get; set; }

        /// <summary>
        /// 唯一字段集
        /// </summary>
        public virtual string? UnionUniqueFields { get; set; }

        /// <summary>
        /// 表头开始行
        /// </summary>
        public virtual int? HeadStartLine { get; set; }

        /// <summary>
        /// 数据开始行
        /// </summary>
        public virtual int? DataStartLine { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public virtual CommonStatus? Status { get; set; }

    }

    /// <summary>
    /// Excel模板输入参数
    /// </summary>
    public class SysExcelTemplateInput
    {
        /// <summary>
        /// 模板名称
        /// </summary>
        public virtual string Name { get; set; } = null!;

        /// <summary>
        /// 版本
        /// </summary>
        public virtual string Version { get; set; } = null!;

        /// <summary>
        /// 所属应用
        /// </summary>
        public virtual string AppName { get; set; } = null!;

        /// <summary>
        /// 类名
        /// </summary>
        public virtual string ClassName { get; set; } = null!;

        /// <summary>
        /// 模板文件名称
        /// </summary>
        public virtual string TemplateFileName { get; set; } = null!;

        /// <summary>
        /// 唯一字段集
        /// </summary>
        public virtual string UnionUniqueFields { get; set; } = null!;

        /// <summary>
        /// 表头开始行
        /// </summary>
        public virtual int HeadStartLine { get; set; }

        /// <summary>
        /// 数据开始行
        /// </summary>
        public virtual int DataStartLine { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public virtual CommonStatus Status { get; set; }

    }
    /// <summary>
    /// Excel模板添加输入参数
    /// </summary>
    public class AddSysExcelTemplateInput : SysExcelTemplateInput
    {
        /// <summary>
        /// 模板名称
        /// </summary>
        [Required(ErrorMessage = "模板名称不能为空")]
        public override string Name { get; set; } = null!;

        /// <summary>
        /// 版本
        /// </summary>
        [Required(ErrorMessage = "版本不能为空")]
        public override string Version { get; set; } = null!;

        /// <summary>
        /// 所属应用
        /// </summary>
        [Required(ErrorMessage = "所属应用不能为空")]
        public override string AppName { get; set; } = null!;

        /// <summary>
        /// 类名
        /// </summary>
        [Required(ErrorMessage = "类名不能为空")]
        public override string ClassName { get; set; } = null!;

        /// <summary>
        /// 模板文件名称
        /// </summary>
        [Required(ErrorMessage = "模板文件名称不能为空")]
        public override string TemplateFileName { get; set; } = null!;

        /// <summary>
        /// 唯一字段集
        /// </summary>
        [Required(ErrorMessage = "唯一字段集不能为空")]
        public override string UnionUniqueFields { get; set; } = null!;

        /// <summary>
        /// 表头开始行
        /// </summary>
        [Required(ErrorMessage = "表头开始行不能为空")]
        public override int HeadStartLine { get; set; }

        /// <summary>
        /// 数据开始行
        /// </summary>
        [Required(ErrorMessage = "数据开始行不能为空")]
        public override int DataStartLine { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Required(ErrorMessage = "状态不能为空")]
        public override CommonStatus Status { get; set; }

    }


    /// <summary>
    /// 更新Excel模板状态输入参数
    /// </summary>
    public class UpdateSysExcelTemplateStatusInput : BaseId
    {
        /// <summary>
        /// 状态-正常_0、停用_1、删除_2
        /// </summary>
        public CommonStatus Status { get; set; }
    }

    /// <summary>
    /// Excel模板删除输入参数
    /// </summary>
    public class DeleteSysExcelTemplateInput : BaseId
    {
    }

    /// <summary>
    /// Excel模板更新输入参数
    /// </summary>
    public class UpdateSysExcelTemplateInput : SysExcelTemplateInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }

    }

    /// <summary>
    /// Excel模板查询输入参数
    /// </summary>
    public class QuerySysExcelTemplateInput : BaseId
    {

    }
}

