﻿using Mapster;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    public class TestTeacherMapper : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<AddTestTeacherInput, TestTeacher>()
            ;
            config.ForType<UpdateTestTeacherInput, TestTeacher>()
            ;
            config.ForType<TestTeacher, TestTeacherOutput>()
            ;
        }
    }
}
