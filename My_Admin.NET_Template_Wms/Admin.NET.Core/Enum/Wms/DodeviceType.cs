﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 设备类型
    /// </summary>
    public enum DodeviceType
    {
        /// <summary>
        /// AGV
        /// </summary>
        [Description("AGV")]
        AGV = 1,

        /// <summary>
        /// 堆垛机
        /// </summary>
        [Description("堆垛机")]
        DUIDUOJI = 2,

        /// <summary>
        /// RGV
        /// </summary>
        [Description("RGV")]
        RGV = 3,

        /// <summary>
        /// 输送线
        /// </summary>
        [Description("SSX")]
        SSX = 4
    }
}
