﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 获取托盘物料分拣参数
    /// </summary>
    public class GetSortInfoOutput
    {
        /// <summary>
        /// 容器
        /// </summary>
        public WmsContainerDto WmsContainer { get; set; }

        /// <summary>
        /// 分拣信息
        /// </summary>
        public List<WmsSortOrder> WmsSortOrderList { get; set; }
    }

    /// <summary>
    /// 分页获取分拣单
    /// </summary>
    public class SortPdaPageOutput : PageInputBase
    {
        /// <summary>
        /// 单据大类
        /// </summary>
        public long OrderLargeCategory { get; set; }

        /// <summary>
        /// 单据小类
        /// </summary>
        public long OrderSubclass { get; set; }

        /// <summary>
        /// 事务类型
        /// </summary>
        public string MoveType { get; set; }

        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        public long SOID { get; set; }

        /// <summary>
        /// 单据编号
        /// </summary>
        public string NO { get; set; }

        /// <summary>
        /// 领料单申请日期
        /// </summary>
        public DateTimeOffset Billdate { get; set; }

        /// <summary>
        /// 领用项目号
        /// </summary>
        public string WBSElementcode { get; set; }

        /// <summary>
        /// 领料部门
        /// </summary>
        public string BenefitingDepartcode { get; set; }

        /// <summary>
        /// 受益部门
        /// </summary>
        public string CostCenterID { get; set; }

        /// <summary>
        /// 客户
        /// </summary>
        public string FI_Client_Analysis_H { get; set; }

        /// <summary>
        /// 是否公司间交易
        /// </summary>
        public long IsInnerCompany { get; set; }

        /// <summary>
        /// 领料人
        /// </summary>
        public string PickerID { get; set; }

        /// <summary>
        /// 仓储中心
        /// </summary>
        public string WarehouseCentername { get; set; }

        /// <summary>
        /// 公司
        /// </summary>
        public string Companyname { get; set; }

        /// <summary>
        /// 施工队
        /// </summary>
        public string ConstructionTeamID { get; set; }

        /// <summary>
        /// 单据状态
        /// </summary>
        public Admin.NET.Core.OrderStatusEnum OrderStatus { get; set; }

        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 时间
        /// </summary>
        public DateTimeOffset? CreatedTime { get; set; }
    }

    /// <summary>
    /// 根据出库单获取分拣单
    /// </summary>
    public class SortPdaDetailPageOutput
    {
        /// <summary>
        /// 来源单号
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        public string Materialcode { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string Materialname { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long OrderDetailID { get; set; }

        /// <summary>
        /// 分拣组盘单号
        /// </summary>
        public string ContainerOrderNo { get; set; }

        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// TA号
        /// </summary>
        public string TACode { get; set; }

        /// <summary>
        /// 分段号
        /// </summary>
        public string PartCode { get; set; }

        /// <summary>
        /// 库位编码
        /// </summary>
        public string PlaceCode { get; set; }

        /// <summary>
        /// 容器编号
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 分拣数
        /// </summary>
        public decimal SortQuantity { get; set; }

        /// <summary>
        /// 实际分拣数
        /// </summary>
        public decimal ActualQuantity { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// 状态-未分拣_1、分拣完成_2
        /// </summary>
        public Admin.NET.Core.SortStatusEnum SortStatus { get; set; }

        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
    }
}
