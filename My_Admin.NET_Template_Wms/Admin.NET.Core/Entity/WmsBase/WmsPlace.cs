﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    /// <summary>
    /// 库位表
    /// </summary>
    [Table("wms_place")]
    [Comment("库位表")]
    public class WmsPlace : DEntityBase
    {
        /// <summary>
        /// 库位编码
        /// </summary>
        [Comment("库位编码")]
        [Required]
        [MaxLength(50)]
        public string PlaceCode { get; set; }

        /// <summary>
        /// 库位对应AGV编码
        /// </summary>
        [Comment("库位对应AGV编码")]
        [MaxLength(50)]
        public string AgvCode { get; set; }

        /// <summary>
        /// 库位状态;数据字典
        /// </summary>
        [Comment("库位状态")]
        [Required]
        public PlaceStatus PlaceStatus { get; set; }

        /// <summary>
        /// 所在库区
        /// </summary>
        [Comment("所在库区")]
        [Required]
        public long AreaId { get; set; }

        /// <summary>
        /// 排
        /// </summary>
        [Comment("排")]
        [Required]
        public int RowNo { get; set; }

        /// <summary>
        /// 列
        /// </summary>
        [Comment("列")]
        [Required]
        public int ColumnNo { get; set; }

        /// <summary>
        /// 层
        /// </summary>
        [Comment("层")]
        [Required]
        public int LayerNo { get; set; }

        /// <summary>
        /// 进深号
        /// </summary>
        [Comment("进深号")]
        [Required]
        public int DeepcellNo { get; set; }

        /// <summary>
        /// 巷道
        /// </summary>
        [Comment("巷道")]
        [Required]
        public int Aisle { get; set; }

        /// <summary>
        /// 线号
        /// </summary>
        [Comment("线号")]
        [Required]
        public int Line { get; set; }

        /// <summary>
        /// 是否锁定;数据字典
        /// </summary>
        [Comment("是否锁定")]
        [Required]
        public YesOrNot Islock { get; set; }

        /// <summary>
        /// 是否空托;数据字典
        /// </summary>
        [Comment("是否空托")]
        public YesOrNot EmptyContainer { get; set; }

        /// <summary>
        /// 堆垛机内部的位置
        /// </summary>
        [Comment("堆垛机内部的位置")]
        [MaxLength(50)]
        public string PositionnoForSrm { get; set; }

        /// <summary>
        /// 库位X坐标
        /// </summary>
        [Comment("库位X坐标")]
        [MaxLength(50)]
        public string Xzb { get; set; }

        /// <summary>
        /// 库位Y坐标
        /// </summary>
        [Comment("库位Y坐标")]
        [MaxLength(50)]
        public string Yzb { get; set; }

        /// <summary>
        /// 库位Z坐标
        /// </summary>
        [Comment("库位Z坐标")]
        [MaxLength(50)]
        public string Zzb { get; set; }

        /// <summary>
        /// 库位长度
        /// </summary>
        [Comment("库位长度")]
        public decimal Length { get; set; }

        /// <summary>
        /// 库位宽度
        /// </summary>
        [Comment("库位宽度")]
        public decimal Width { get; set; }

        /// <summary>
        /// 库位高度
        /// </summary>
        [Comment("库位高度")]
        public decimal Height { get; set; }

        /// <summary>
        /// 最大承重
        /// </summary>
        [Comment("最大承重")]
        public decimal MaxWeight { get; set; }

        /// <summary>
        /// 库位高度属性;数据字典
        /// </summary>
        [Comment("库位高度属性")]
        public Heightlevel HeightLevel { get; set; }

        /// <summary>
        /// 优先级
        /// </summary>
        [Comment("优先级")]
        public int Priority { get; set; }

        /// <summary>
        /// 逻辑区域
        /// </summary>
        [Comment("逻辑区域")]
        public int LogicalName { get; set; }

        /// <summary>
        /// 库区表
        /// </summary>
        public WmsArea WmsArea { get; set; }
    }
}