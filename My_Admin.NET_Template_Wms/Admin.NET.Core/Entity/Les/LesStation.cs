﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    /// <summary>
    /// 工位表
    /// </summary>
    [Table("les_station")]
    [Comment("工位表")]
    public class LesStation : DEntityBase
    {
        /// <summary>
        /// 产线Id
        /// </summary>
        [Comment("产线Id")]
        public long ProductionlineId { get; set; }

        /// <summary>
        /// 工位名称
        /// </summary>
        [Comment("工位名称")]
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// 工位编码
        /// </summary>
        [Comment("工位编码")]
        [MaxLength(50)]
        public string Code { get; set; }

        /// <summary>
        /// 工位状态
        /// </summary>
        [Comment("工位状态")]
        public CommonStatus Status { get; set; } = CommonStatus.ENABLE;

        /// <summary>
        /// 运行状态
        /// </summary>
        [Comment("运行状态")]
        public PlaceStatus StataionStatus { get; set; } = PlaceStatus.KONGXIAN;

        /// <summary>
        /// 库位编码
        /// </summary>
        [Comment("库位编码")]
        [MaxLength(50)]
        public string ContainerCode { get; set; }

        /// <summary>
        /// 库位规格
        /// </summary>
        [Comment("库位规格")]
        public decimal PlaceSpec { get; set; }

        /// <summary>
        /// 产线表
        /// </summary>
        //public LesProductionline LesProductionline { get; set; }
    }
}
