﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;

namespace Admin.NET.Core
{
    /// <summary>
    /// 系统应用表种子数据
    /// </summary>
    public class SysAppSeedData : IEntitySeedData<SysApp>
    {
        /// <summary>
        /// 种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<SysApp> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new[]
            {
                new SysApp{Id=535823141748805, Name="TestDemo", Code="TestDemo", Active="N", Status=0, Sort=100 },
                new SysApp{Id=142307070898245, Name="开发管理", Code="system", Active="Y", Status=0, Sort=100 },
                new SysApp{Id=142307070902341, Name="系统管理", Code="manage", Active="N", Status=0, Sort=300 },
                new SysApp{Id=142307070922869, Name="业务应用", Code="busiapp", Active="N", Status=0, Sort=400 },
                new SysApp{Id=355686432948293, Name="仓库作业", Code="warejob", Active="N", Status=0, Sort=500 },
                new SysApp{Id=355686518018117, Name="统计查询", Code="warecount", Active="N", Status=0, Sort=600 }
            };
        }
    }
}