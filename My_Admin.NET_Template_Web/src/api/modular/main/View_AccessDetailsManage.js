﻿import { axios } from '@/utils/request'

/**
 * 查询出入库记录
 *
 * @author lidunanping
 */
export function View_AccessDetailsPage (parameter) {
  return axios({
    url: '/View_AccessDetails/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 出入库记录列表
 *
 * @author lidunanping
 */
export function View_AccessDetailsList (parameter) {
  return axios({
    url: '/View_AccessDetails/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加出入库记录
 *
 * @author lidunanping
 */
export function View_AccessDetailsAdd (parameter) {
  return axios({
    url: '/View_AccessDetails/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑出入库记录
 *
 * @author lidunanping
 */
export function View_AccessDetailsEdit (parameter) {
  return axios({
    url: '/View_AccessDetails/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除出入库记录
 *
 * @author lidunanping
 */
export function View_AccessDetailsDelete (parameter) {
  return axios({
    url: '/View_AccessDetails/delete',
    method: 'post',
    data: parameter
  })
}
