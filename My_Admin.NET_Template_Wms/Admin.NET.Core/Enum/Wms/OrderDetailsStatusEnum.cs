﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 单据明细表状态枚举
    /// </summary>
    public enum OrderDetailsStatusEnum
    {
        /// <summary>
        /// 未执行
        /// </summary>
        [Description("未执行")]
        WEIZHIXING = 1,

        /// <summary>
        /// 执行中
        /// </summary>
        [Description("执行中")]
        ZHIXINGZHONG = 2,

        /// <summary>
        /// 完成
        /// </summary>
        [Description("完成")]
        WANCHENG = 3,

        /// <summary>
        /// 强制完成
        /// </summary>
        [Description("强制完成")]
        QIANGZHIWANCHENG = 4
    }
}
