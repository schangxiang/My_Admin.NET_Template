﻿import { axios } from '@/utils/request'

/**
 * 查询销售退库
 *
 * @author yc
 */
export function SalesStockReturnOrderPage (parameter) {
  return axios({
    url: '/SalesStockReturnOrder/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 销售退库列表
 *
 * @author yc
 */
export function SalesStockReturnOrderList (parameter) {
  return axios({
    url: '/SalesStockReturnOrder/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加销售退库
 *
 * @author yc
 */
export function SalesStockReturnOrderAdd (parameter) {
  return axios({
    url: '/SalesStockReturnOrder/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑销售退库
 *
 * @author yc
 */
export function SalesStockReturnOrderEdit (parameter) {
  return axios({
    url: '/SalesStockReturnOrder/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除销售退库
 *
 * @author yc
 */
export function SalesStockReturnOrderDelete (parameter) {
  return axios({
    url: '/SalesStockReturnOrder/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出销售退库的Excel文件
 *
 * @author yc
 */
export function SalesStockReturnOrderToExcel (parameter) {
  return axios({
    url: '/SalesStockReturnOrder/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入销售退库的Excel文件
 *
 * @author yc
 */
export function SalesStockReturnOrderFromExcel (data, parameter) {
  return axios({
    url: '/SalesStockReturnOrder/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载销售退库的Excel导入模板
 *
 * @author yc
 */
export function SalesStockReturnOrderDownloadExcelTemplate(parameter) {
  return axios({
    url: '/SalesStockReturnOrder/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 销售退库单详情
 *
 * @author yhh
 */
export function SalesStockReturnOrderDetailPage (parameter) {
  return axios({
    url: '/SalesStockReturnOrder/DetailsPage',
    method: 'get',
    params: parameter
  })
}

/**
 * 获取销售退库单号
 *
 * @author yhh
 */
export function GetBillNumber () {
  return axios({
    url: '/SalesStockReturnOrder/GetSalesStockReturnOrderNo',
    method: 'get'
  })
}


/**
 * 获取未绑定销售退库单的出库单(计划)列表
 *
 * @author yhh
 */
export function GetStockOuts() {
  return axios({
    url: '/SalesStockReturnOrder/GetSalesStockOutboundOrderNo',
    method: 'get'
  })
}

/**
 * 编辑售退库单详情退库数
 *
 * @author yhh
 */
export function SalesStockReturnOrderEditDetailNumber (parameter) {
  return axios({
    url: '/SalesStockReturnOrder/UpdateSalesStockReturnOrderDetail',
    method: 'post',
    data: parameter
  })
}

/**
 * 获取可退库的物料列表
 *
 * @author yhh
 */
export function GetCouldReturnMaterials(params) {
  return axios({
    url: '/SalesStockReturnOrder/GetDetailBySalesStockOutboundOrderNo',
    method: 'get',
    params: params
  })
}

/**
 * 下发
 *
 * @author yhh
 */
export function SalesStockReturnOrderMulDistribute (ids) {
  return axios({
    url: '/SalesStockReturnOrder/BatchSalesStockReturnOrder',
    method: 'post',
    data: {id:ids}
  })
}


