﻿namespace Admin.NET.Application
{
    /// <summary>
    /// 系统帮助类
    /// </summary>
    public class SysHelper
    {

        /// <summary>
        /// 获取登录人的昵称
        /// </summary>
        /// <returns></returns>
        public static string GetUserName()
        {
            return CurrentUserInfo.Name;
        }

        /// <summary>
        /// 获取登录人的用户id
        /// </summary>
        /// <returns></returns>
        public static long GetUserId()
        {
            return CurrentUserInfo.UserId;
        }

        /// <summary>
        /// 获取当前时间
        /// </summary>
        /// <returns></returns>
        public static DateTime GetNowTime()
        {
            return DateTime.Now;
        }
    }
}
