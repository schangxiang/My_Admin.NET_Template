﻿using Admin.NET.Core;
using Admin.NET.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace Admin.NET.Application
{    
    
    /// <summary>
    /// 转储单查询参数
    /// </summary>
    public class WmsDumpOrderSearch : PageInputBase
    {
        /// <summary>
        /// 单据大类
        /// </summary>
        public virtual long? OrderLargeCategory { get; set; }
        
        /// <summary>
        /// 单据小类
        /// </summary>
        public virtual long? OrderSubclass { get; set; }
        
        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        public virtual long? SOID { get; set; }
        
        /// <summary>
        /// 单据编号
        /// </summary>
        public virtual string NO { get; set; }
        
        /// <summary>
        /// 创建日期
        /// </summary>
        public virtual DateTimeOffset? Billdate { get; set; }
        
        /// <summary>
        /// 创建人
        /// </summary>
        public virtual string Creator { get; set; }
        
        /// <summary>
        /// 公司
        /// </summary>
        public virtual string Companyname { get; set; }
        
        /// <summary>
        /// 单据状态
        /// </summary>
        public virtual Admin.NET.Core.OrderStatusEnum? OrderStatus { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState IssueState { get; set; }
        
    }

        /// <summary>
    /// 转储单不分页查询参数
    /// </summary>
    public class WmsDumpOrderSearchNonPage : PageInputNonPageBase
    {
        /// <summary>
        /// 单据大类
        /// </summary>
        public virtual long? OrderLargeCategory { get; set; }
        
        /// <summary>
        /// 单据小类
        /// </summary>
        public virtual long? OrderSubclass { get; set; }
        
        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        public virtual long? SOID { get; set; }
        
        /// <summary>
        /// 单据编号
        /// </summary>
        public virtual string? NO { get; set; }
        
        /// <summary>
        /// 创建日期
        /// </summary>
        public virtual DateTimeOffset? Billdate { get; set; }
        
        /// <summary>
        /// 创建人
        /// </summary>
        public virtual string? Creator { get; set; }
        
        /// <summary>
        /// 公司
        /// </summary>
        public virtual string? Companyname { get; set; }
        
        /// <summary>
        /// 单据状态
        /// </summary>
        public virtual Admin.NET.Core.OrderStatusEnum? OrderStatus { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState? IssueState { get; set; }
        
    }

    /// <summary>
    /// 转储单输入参数
    /// </summary>
    public class WmsDumpOrderInput
    {
        /// <summary>
        /// 单据大类
        /// </summary>
        public virtual long OrderLargeCategory { get; set; }
        
        /// <summary>
        /// 单据小类
        /// </summary>
        public virtual long OrderSubclass { get; set; }
        
        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        public virtual long SOID { get; set; }
        
        /// <summary>
        /// 单据编号
        /// </summary>
        public virtual string NO { get; set; }
        
        /// <summary>
        /// 创建日期
        /// </summary>
        public virtual DateTimeOffset Billdate { get; set; }
        
        /// <summary>
        /// 创建人
        /// </summary>
        public virtual string Creator { get; set; }
        
        /// <summary>
        /// 公司
        /// </summary>
        public virtual string Companyname { get; set; }
        
        /// <summary>
        /// 单据状态
        /// </summary>
        public virtual Admin.NET.Core.OrderStatusEnum OrderStatus { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState IssueState { get; set; }
        
    }

    public class AddWmsDumpOrderInput : WmsDumpOrderInput
    {
    }

    public class DeleteWmsDumpOrderInput : BaseId
    {
    }

    public class UpdateWmsDumpOrderInput : WmsDumpOrderInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
        
    }

    public class QueryeWmsDumpOrderInput : BaseId
    {

    }

    /// <summary>
    /// 获取明细参数
    /// </summary>
    public class DumpPageDetailInput : PageInputBase
    {
        /// <summary>
        /// Id
        /// </summary>
        public long? Id { get; set; }
    }
}
