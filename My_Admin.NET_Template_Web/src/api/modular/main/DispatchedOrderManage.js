﻿import { axios } from '@/utils/request'

/**
 * 查询发货单
 *
 * @author yc
 */
export function DispatchedOrderPage (parameter) {
  return axios({
    url: '/DispatchedOrder/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 发货单列表
 *
 * @author yc
 */
export function DispatchedOrderList (parameter) {
  return axios({
    url: '/DispatchedOrder/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加发货单
 *
 * @author yc
 */
export function DispatchedOrderAdd (parameter) {
  return axios({
    url: '/DispatchedOrder/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑发货单
 *
 * @author yc
 */
export function DispatchedOrderEdit (parameter) {
  return axios({
    url: '/DispatchedOrder/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除发货单
 *
 * @author yc
 */
export function DispatchedOrderDelete (parameter) {
  return axios({
    url: '/DispatchedOrder/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出发货单的Excel文件
 *
 * @author yc
 */
export function DispatchedOrderToExcel (parameter) {
  return axios({
    url: '/DispatchedOrder/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入发货单的Excel文件
 *
 * @author yc
 */
export function DispatchedOrderFromExcel (data, parameter) {
  return axios({
    url: '/DispatchedOrder/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载发货单的Excel导入模板
 *
 * @author yc
 */
export function DispatchedOrderDownloadExcelTemplate(parameter) {
  return axios({
    url: '/DispatchedOrder/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 单据大类下拉
 *
 * @author yhh
 */
export function DispatchedOrderLargeCategories() {
  return axios({
    url: '/DispatchedOrder/OrderTypeLargeCategory',
    method: 'get'
  })
}

/**
 * 单据小类下拉
 *
 * @author yhh
 */
export function DispatchedOrderSubclass(id) {
  return axios({
    url: '/DispatchedOrder/OrderTypeSubclass',
    method: 'get',
    params:{id}
  })
}


/**
 * 查询采购单详情
 *
 * @author yhh
 */
export function DispatchedOrderDetailPage (parameter) {
  return axios({
    url: '/DispatchedOrder/detailPage',
    method: 'get',
    params: parameter
  })
}


