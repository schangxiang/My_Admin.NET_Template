﻿import { axios } from '@/utils/request'

/**
 * 查询退货单
 *
 * @author XJF
 */
export function WmsGoodsReturnOrderPage (parameter) {
  return axios({
    url: '/WmsGoodsReturnOrder/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 退货单列表
 *
 * @author XJF
 */
export function WmsGoodsReturnOrderList (parameter) {
  return axios({
    url: '/WmsGoodsReturnOrder/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加退货单
 *
 * @author XJF
 */
export function WmsGoodsReturnOrderAdd (parameter) {
  return axios({
    url: '/WmsGoodsReturnOrder/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑退货单
 *
 * @author XJF
 */
export function WmsGoodsReturnOrderEdit (parameter) {
  return axios({
    url: '/WmsGoodsReturnOrder/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除退货单
 *
 * @author XJF
 */
export function WmsGoodsReturnOrderDelete (parameter) {
  return axios({
    url: '/WmsGoodsReturnOrder/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出退货单的Excel文件
 *
 * @author XJF
 */
export function WmsGoodsReturnOrderToExcel (parameter) {
  return axios({
    url: '/WmsGoodsReturnOrder/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入退货单的Excel文件
 *
 * @author XJF
 */
export function WmsGoodsReturnOrderFromExcel (data, parameter) {
  return axios({
    url: '/WmsGoodsReturnOrder/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载退货单的Excel导入模板
 *
 * @author XJF
 */
export function WmsGoodsReturnOrderDownloadExcelTemplate(parameter) {
  return axios({
    url: '/WmsGoodsReturnOrder/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}


/**
 * 物料详情
 */
 export function WmsOrderLldDetail (parameter) {
  return axios({
    url: '/WmsGoodsReturnOrder/PageDetail',
    method: 'get',
    params: parameter
  })
}