﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;

namespace YaKe.Les.Core.SeedData
{
    public class WmsContainerPlaceSeedData : IEntitySeedData<WmsContainerPlace>
    {
        /// <summary>
        /// 托盘库位关系表种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<WmsContainerPlace> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new[]
            {
                new WmsContainerPlace{Id=358853586010181,PlaceId=358852152045637,PlaceCode="1-1-1-1-1",ContainerId=358852634767429,ContainerCode="TM00001",ContainerPlaceStatus=CommonStatus.DELETED},
                new WmsContainerPlace{Id=358856088039493,PlaceId=358852152119365,PlaceCode="1-1-1-2-1",ContainerId=358852634767429,ContainerCode="TM00001",ContainerPlaceStatus=CommonStatus.DELETED},
                new WmsContainerPlace{Id=358856888660037,PlaceId=358852152045637,PlaceCode="1-1-1-1-1",ContainerId=358852767887429,ContainerCode="TM00002",ContainerPlaceStatus=CommonStatus.DELETED},
                new WmsContainerPlace{Id=358857243414597,PlaceId=358852152156229,PlaceCode="1-1-1-5-1",ContainerId=358852831047749,ContainerCode="TM00003",ContainerPlaceStatus=CommonStatus.ENABLE},
                new WmsContainerPlace{Id=358857693601867,PlaceId=358852152045637,PlaceCode="1-1-1-1-1",ContainerId=358853016055877,ContainerCode="TM00004",ContainerPlaceStatus=CommonStatus.ENABLE},
                new WmsContainerPlace{Id=358858262188101,PlaceId=358852152143941,PlaceCode="1-1-1-4-1",ContainerId=358853070798917,ContainerCode="TM00005",ContainerPlaceStatus=CommonStatus.ENABLE}
            };
        }
    }
}
