﻿import { axios } from '@/utils/request'

/**
 * 查询退库单
 *
 * @author XJF
 */
export function WmsStockReturnOrderPage (parameter) {
  return axios({
    url: '/WmsStockReturnOrder/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 退库单列表
 *
 * @author XJF
 */
export function WmsStockReturnOrderList (parameter) {
  return axios({
    url: '/WmsStockReturnOrder/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加退库单
 *
 * @author XJF
 */
export function WmsStockReturnOrderAdd (parameter) {
  return axios({
    url: '/WmsStockReturnOrder/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑退库单
 *
 * @author XJF
 */
export function WmsStockReturnOrderEdit (parameter) {
  return axios({
    url: '/WmsStockReturnOrder/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除退库单
 *
 * @author XJF
 */
export function WmsStockReturnOrderDelete (parameter) {
  return axios({
    url: '/WmsStockReturnOrder/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出退库单的Excel文件
 *
 * @author XJF
 */
export function WmsStockReturnOrderToExcel (parameter) {
  return axios({
    url: '/WmsStockReturnOrder/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入退库单的Excel文件
 *
 * @author XJF
 */
export function WmsStockReturnOrderFromExcel (data, parameter) {
  return axios({
    url: '/WmsStockReturnOrder/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载退库单的Excel导入模板
 *
 * @author XJF
 */
export function WmsStockReturnOrderDownloadExcelTemplate(parameter) {
  return axios({
    url: '/WmsStockReturnOrder/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}



/**
 * 物料详情
 */
 export function WmsOrderRDetail (parameter) {
  return axios({
    url: '/WmsStockReturnOrder/PageDetail',
    method: 'get',
    params: parameter
  })
}