﻿using Furion.DatabaseAccessor;

namespace Admin.NET.Core.Entity
{
    /// <summary>
    /// 出入库明细试图
    /// </summary>
    public class VAccessDetails : EntityNotKey
    {
        /// <summary>
        /// 配置视图名
        /// </summary>
        public VAccessDetails() : base("View_AccessDetails") { }

        /// <summary>
        /// 任务号
        /// </summary>
        public string TaskNo { get; set; }

        /// <summary>
        /// 任务方式
        /// </summary>
        public TaskModel TaskModel { get; set; }

        /// <summary>
        /// 任务类型
        /// </summary>
        public TaskType TaskType { get; set; }

        /// <summary>
        /// 任务级别
        /// </summary>
        public int TaskLevel { get; set; }

        /// <summary>
        /// 任务状态 字典 未执行，执行中，完成
        /// </summary>
        public TaskStatusEnum TaskStatus { get; set; }  

        /// <summary>
        /// 起始库位
        /// </summary>
        public string SourcePlace { get; set; }

        /// <summary>
        /// 目标库位
        /// </summary>
        public string ToPlace { get; set; }

        /// <summary>
        /// 托盘编码
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 来源
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        public string MaterialNo { get; set; }

        /// <summary>
        /// 物料密度
        /// </summary>
        public string MaterialDensity { get; set; }

        /// <summary>
        /// 物料批次
        /// </summary>
        public string Batch { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public virtual DateTimeOffset? CreatedTime { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public virtual DateTimeOffset? UpdatedTime { get; set; }

        /// <summary>
        /// 任务开始时间
        /// </summary>
        public DateTimeOffset? TaskCreatedTime { get; set; }

        /// <summary>
        /// 任务结束时间
        /// </summary>
        public DateTimeOffset? TaskUpdatedTime { get; set; }

        public int Long { get; set; }
        public int Wide { get; set; }
        public int High { get; set; }

        /// <summary>
        /// Id主键
        /// </summary>
        public long? Id { get; set; }
    }
}
