﻿using Admin.NET.Core;
using Admin.NET.Core.Service;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Admin.NET.Application
{    
    /// <summary>
    /// 报检返回参数
    /// </summary>
    public class InspectionDeclarationOutput
    {
       
        /// <summary>
        /// 结果描述
        /// </summary>
        public string Result { get; set; }

        /// <summary>
        /// 请求状态
        /// </summary>
        public bool Status { get; set; }
    }

    /// <summary>
    /// 获取报检结果返回参数
    /// </summary>
    public class GetInspectionResultOutput
    {

        /// <summary>
        /// 结果描述
        /// </summary>
        public string Result { get; set; }

        /// <summary>
        /// 请求状态
        /// </summary>
        public bool Status { get; set; }

        /// <summary>
        /// 数据
        /// </summary>
        public GetInspectionResultOutputData Data { get; set; }

    }

    /// <summary>
    /// 获取报检结果返回数据
    /// </summary>
    public class GetInspectionResultOutputData
    {

        /// <summary>
        /// SOID
        /// </summary>
        public long SOID { get; set; }

        /// <summary>
        /// 收货单号
        /// </summary>
        public string ON { get; set; }

        /// <summary>
        /// 质检日期
        /// </summary>
        public DateTimeOffset Billdate { get; set; }

        /// <summary>
        /// 质检单号
        /// </summary>
        public string QCNO { get; set; }

        /// <summary>
        /// 供应商编码
        /// </summary>
        public string Vendorcode { get; set; }

        /// <summary>
        /// 供应商名称
        /// </summary>
        public string Vendorname { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        public string Companyname { get; set; }

        /// <summary>
        /// 仓管员
        /// </summary>
        public string WarehouseKeepername { get; set; }

        /// <summary>
        /// 贸易方式
        /// </summary>
        public TradeMode TradeMode { get; set; }

        /// <summary>
        /// 仓储中心
        /// </summary>
        public string WarehouseCentername { get; set; }

        /// <summary>
        /// 仓间
        /// </summary>
        public string StoreRoomname { get; set; }

        /// <summary>
        /// 报关单号
        /// </summary>
        public string DeclarationCode { get; set; }

        /// <summary>
        /// 专业
        /// </summary>
        public string Majorname { get; set; }

        /// <summary>
        /// 详情
        /// </summary>
        public List<GetInspectionResultOutputDataDetail> Dtls { get; set; }
    }

    /// <summary>
    /// 获取报检结果返回数据详情
    /// </summary>
    public class GetInspectionResultOutputDataDetail
    {

        /// <summary>
        /// OID
        /// </summary>
        public long OID { get; set; }

        /// <summary>
        /// 质检状态
        /// </summary>
        public string FATQuality { get; set; }

        /// <summary>
        /// 物料编码
        /// </summary>
        public string Materialcode { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string Materialname { get; set; }

        /// <summary>
        /// 基本单位
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Qty { get; set; }

        /// <summary>
        /// 单根长度
        /// </summary>
        public decimal SingleLength { get; set; }

        /// <summary>
        /// 理论重量
        /// </summary>
        public decimal TheoreticalWeight { get; set; }

        /// <summary>
        /// 不含税单价
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// 成本金额
        /// </summary>
        public decimal SumPrice { get; set; }

        /// <summary>
        /// 项目编码
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 合同编码
        /// </summary>
        public string ContractCode { get; set; }

        /// <summary>
        /// TA号
        /// </summary>
        public string TACode { get; set; }

        /// <summary>
        /// 报关品名
        /// </summary>
        public string TradeName { get; set; }

        /// <summary>
        /// 报关项号
        /// </summary>
        public string ItemNo { get; set; }

        /// <summary>
        /// 报关单位
        /// </summary>
        public string CustomsUnitName { get; set; }

        /// <summary>
        /// 报关数量
        /// </summary>
        public decimal CustomsNum { get; set; }

        /// <summary>
        /// 报关金额
        /// </summary>
        public decimal CustomsPrices { get; set; }

        /// <summary>
        /// 特殊要求
        /// </summary>
        public string SpecialNeeds { get; set; }

        /// <summary>
        /// 供应链批次号
        /// </summary>
        public string BatchNO_SCM { get; set; }
    }
}
