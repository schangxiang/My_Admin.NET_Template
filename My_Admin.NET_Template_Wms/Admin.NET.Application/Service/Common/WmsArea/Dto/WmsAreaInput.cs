﻿using System.ComponentModel.DataAnnotations;
using Admin.NET.Core;
using Admin.NET.Core.Service;

namespace Admin.NET.Application
{
    /// <summary>
    /// 库区信息查询参数
    /// </summary>
    public class WmsAreaSearch : PageInputBase
    {
        /// <summary>
        /// 名称
        /// </summary>
        public virtual string AreaName { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public virtual string AreaDesc { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public virtual CommonStatus? AreaStatus { get; set; }

        /// <summary>
        /// 分类
        /// </summary>
        public virtual AreaType? AreaType { get; set; }

        /// <summary>
        /// 是否为钢平台
        /// </summary>
        public virtual bool? IsSteel { get; set; }

        /// <summary>
        /// 所属车间
        /// </summary>
        public virtual LesWorkShopType? WorkShopType { get; set; }

        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual IssueState? IssueState { get; set; }
    }

    /// <summary>
    /// 库区信息不分页查询参数
    /// </summary>
    public class WmsAreaSearchNonPage : PageInputNonPageBase
    {
        /// <summary>
        /// 名称
        /// </summary>
        public virtual string AreaName { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public virtual string AreaDesc { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public virtual CommonStatus? AreaStatus { get; set; }

        /// <summary>
        /// 分类
        /// </summary>
        public virtual AreaType? AreaType { get; set; }

        /// <summary>
        /// 是否为钢平台
        /// </summary>
        public virtual bool? IsSteel { get; set; }

        /// <summary>
        /// 所属车间
        /// </summary>
        public virtual LesWorkShopType? WorkShopType { get; set; }

        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual IssueState? IssueState { get; set; }
    }

    /// <summary>
    /// 库区信息输入参数
    /// </summary>
    public class WmsAreaInput
    {
        /// <summary>
        /// 名称
        /// </summary>
        public virtual string AreaName { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public virtual string AreaDesc { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public virtual CommonStatus AreaStatus { get; set; }

        /// <summary>
        /// 分类
        /// </summary>
        public virtual AreaType AreaType { get; set; }

        /// <summary>
        /// 是否为钢平台
        /// </summary>
        public virtual bool IsSteel { get; set; }

        /// <summary>
        /// 所属车间
        /// </summary>
        public virtual LesWorkShopType WorkShopType { get; set; }

        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual IssueState IssueState { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class AddWmsAreaInput : WmsAreaInput
    {
    }

    /// <summary>
    /// 
    /// </summary>
    public class DeleteWmsAreaInput : BaseId
    {
    }

    /// <summary>
    /// 
    /// </summary>
    public class UpdateWmsAreaInput : WmsAreaInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class QueryeWmsAreaInput : BaseId
    {

    }
}
