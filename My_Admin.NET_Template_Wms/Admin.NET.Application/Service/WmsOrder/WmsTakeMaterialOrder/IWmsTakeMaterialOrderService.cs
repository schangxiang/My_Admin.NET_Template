﻿using Admin.NET.Core;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Admin.NET.Application
{
    public interface IWmsTakeMaterialOrderService
    {
        Task Add(AddWmsTakeMaterialOrderInput input);
        Task Delete(DeleteWmsTakeMaterialOrderInput input);
        Task<WmsTakeMaterialOrderOutput> Get([FromQuery] QueryeWmsTakeMaterialOrderInput input);
        Task<List<WmsTakeMaterialOrderOutput>> List([FromQuery] WmsTakeMaterialOrderInput input);
        Task<PageResult<WmsTakeMaterialOrderOutput>> Page([FromQuery] WmsTakeMaterialOrderSearch input);
        Task Update(UpdateWmsTakeMaterialOrderInput input);

        Task<List<WmsTakeMaterialOrderOutput>> ListNonPageAsync([FromQuery] WmsTakeMaterialOrderSearchNonPage input);

        Task<int> FromExcelAsync(IFormFile file, [FromQuery] ImportExcelType importExcelType);

        Task<IActionResult> DownloadExcelTemplate(string version);
    }
}