export default function(options,$store,$headers={}) {
	let res = {flag:true,headers:$headers};
	if (process.env.NODE_ENV==='development') {
		res.headers.IsDevelopment = 'true'
	} else {
		res.headers.IsDevelopment = 'false'
		res.headers.AppVersion = $store.getters['system/getVersion']
	}
	if (options.needToken) {
		let token = $store.getters['user/getToken'];
		if (token) {
			res.headers.Authorization = 'Bearer ' + token;
			let rToken = $store.getters['user/getRefreshToken'];
			if (rToken) {
				res.headers['X-Authorization'] = 'Bearer ' + rToken;
			}
		} else {
			res.flag = false;
		}
	} 
	return res
}