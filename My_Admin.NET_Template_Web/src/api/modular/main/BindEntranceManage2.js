﻿import { axios } from '@/utils/request'

/**
 * 手动入库
 *
 * @author yhh
 */
export function BindEntranceManualWare (parameter) {
  return axios({
    url: '/PlywoodGroupDisk/PlywoodManualWare',
    method: 'post',
    data: parameter
  })
}

/**
 * 获取托盘信息
 *
 * @author yhh
 */
export function BindEntranceGetContainer (parameter) {
  return axios({
    url: '/PlywoodGroupDisk/PlywoodGetContainer',
    method: 'get',
    params: parameter
  })
}

/**
 * 组盘接口
 *
 * @author yhh
 */
export function BindAction (parameter) {
  return axios({
    url: '/PlywoodGroupDisk/PlywoodBindEntrance',
    method: 'post',
    data: parameter
  })
}