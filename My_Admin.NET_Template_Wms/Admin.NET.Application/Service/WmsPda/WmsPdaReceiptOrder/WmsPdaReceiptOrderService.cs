﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Admin.NET.Core;
using Yitter.IdGenerator;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc.RazorPages;
using StackExchange.Redis;

namespace Admin.NET.Application
{
    /// <summary>
    /// Pda收货
    /// </summary>
    [ApiDescriptionSettings("Pda收货", Name = "PdaReceiptOrder", Order = 100)]
    [Route("api/[Controller]")]
    public class WmsPdaReceiptOrderService : IDynamicApiController, ITransient
    {
        private readonly IRepository<WmsOrder, MasterDbContextLocator> _wmsOrderRep;
        private readonly IRepository<WmsOrderDetails, MasterDbContextLocator> _wmsOrderDetailsRep;
        private readonly IRepository<WmsTask, MasterDbContextLocator> _wmsTaskRep;
        private readonly IRepository<WmsPlace, MasterDbContextLocator> _wmsPlaceRep;
        private readonly IRepository<WmsArea, MasterDbContextLocator> _wmsAreaRep;
        private readonly IRepository<WmsWarehouseEntrance, MasterDbContextLocator> _wmsWarehouseEntranceRep;
        private readonly IRepository<WmsMaterialContainer, MasterDbContextLocator> _wmsMaterialContainerRep;
        private readonly IRepository<WmsContainer, MasterDbContextLocator> _wmsContainerRep;
        private readonly IRepository<WmsContainerPlace, MasterDbContextLocator> _wmsContainerPlaceRep;
        private readonly IRepository<WmsMaterialStock, MasterDbContextLocator> _wmsMaterialStockRep;
        private readonly IRepository<WmsOrderType, MasterDbContextLocator> _wmsOrderTypeRep;
        private readonly IRepository<WmsReceiptOrderDetails, MasterDbContextLocator> _wmsReceiptOrderDetailsRep;
        private readonly IRepository<WmsReceiptOrder, MasterDbContextLocator> _wmsReceiptOrderRep;

        /// <summary>
        /// 构造函数
        /// </summary>
        public WmsPdaReceiptOrderService(
            IRepository<WmsOrder, MasterDbContextLocator> wmsOrderRep,
            IRepository<WmsOrderDetails, MasterDbContextLocator> wmsOrderDetailsRep,
            IRepository<WmsTask, MasterDbContextLocator> wmsTaskRep,
            IRepository<WmsPlace, MasterDbContextLocator> wmsPlaceRep,
            IRepository<WmsArea, MasterDbContextLocator> wmsAreaRep,
            IRepository<WmsWarehouseEntrance, MasterDbContextLocator> wmsWarehouseEntranceRep,
            IRepository<WmsMaterialContainer, MasterDbContextLocator> wmsMaterialContainerRep,
            IRepository<WmsContainer, MasterDbContextLocator> wmsContainerRep,
            IRepository<WmsContainerPlace, MasterDbContextLocator> wmsContainerPlaceRep,
            IRepository<WmsMaterialStock, MasterDbContextLocator> wmsMaterialStockRep,
            IRepository<WmsOrderType, MasterDbContextLocator> wmsOrderTypeRep,
            IRepository<WmsReceiptOrderDetails, MasterDbContextLocator> wmsReceiptOrderDetailsRep,
            IRepository<WmsReceiptOrder, MasterDbContextLocator> wmsReceiptOrderRep
        )
        {
            _wmsOrderRep = wmsOrderRep;
            _wmsOrderDetailsRep = wmsOrderDetailsRep;
            _wmsTaskRep = wmsTaskRep;
            _wmsPlaceRep = wmsPlaceRep;
            _wmsAreaRep = wmsAreaRep;
            _wmsWarehouseEntranceRep = wmsWarehouseEntranceRep;
            _wmsMaterialContainerRep = wmsMaterialContainerRep;
            _wmsContainerRep = wmsContainerRep;
            _wmsContainerPlaceRep = wmsContainerPlaceRep;
            _wmsMaterialStockRep = wmsMaterialStockRep;
            _wmsOrderTypeRep = wmsOrderTypeRep;
            _wmsReceiptOrderDetailsRep = wmsReceiptOrderDetailsRep;
            _wmsReceiptOrderRep = wmsReceiptOrderRep;
        }

      
        /// <summary>
        /// 根据单据号获取明细
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetOrderDetail")]
        [UnitOfWork]
        [UnifyResult(typeof(List<GetOrderDetailOutput>))]
        public async Task<object> GetOrderDetail([FromQuery] GetOrderDetailInput input)
        {
            //获取主单据
            var wmsOrders = await _wmsReceiptOrderRep.DetachedEntities
                                     .FirstOrDefaultAsync(u => u.NO == input.NO);
            if (wmsOrders == null) return XnRestfulResultProvider.RESTfulMesaage("单据不存在!");
            //已收货的明细不显示
            var wmsOrderDetails = await _wmsReceiptOrderDetailsRep.DetachedEntities
                                     .Where(u => u.OrderId == wmsOrders.Id)
                                     .Where(u=>u.ReceivingStatus != ReceivingStatus.YISHOUHUO)
                                     .ProjectToType<WmsReceiptOrderDetailOutput>()
                                     .ToListAsync();     
            return wmsOrderDetails;
        }

        /// <summary>
        /// pda确认收货
        /// </summary>
        /// <returns></returns>
        [HttpPost("ConfirmReceipt")]
        [UnitOfWork]
        public async Task ConfirmReceipt([FromBody] ConfirmReceiptInput input)
        {
            if (input.GetOrderDetailOutputList.Count == 0) throw Oops.Oh("收货信息不能为空！");
            //获取主单据
            var wmsOrderModal = await _wmsReceiptOrderRep.DetachedEntities
                                     .ProjectToType<WmsReceiptOrder>()
                                     .FirstOrDefaultAsync(u => u.NO == input.NO);
            if (wmsOrderModal == null) throw Oops.Oh("单据不存在!");
            if (wmsOrderModal.ReceivingStatus == ReceivingStatus.YISHOUHUO) throw Oops.Oh("该单据已收货!");

            if (input.GetOrderDetailOutputList.Count == 0) throw Oops.Oh("收货信息不能为空！");
            //更新收货明细
            foreach (var item in input.GetOrderDetailOutputList)
            {
                var WmsReceiptOrderDetailsModal = item.Adapt<WmsReceiptOrderDetails>();
                if (WmsReceiptOrderDetailsModal.ReceivedQty < WmsReceiptOrderDetailsModal.DeliveryQty)
                {
                    WmsReceiptOrderDetailsModal.ReceivingStatus = ReceivingStatus.SHOUHUOZHONG;
                }
                else
                {
                    WmsReceiptOrderDetailsModal.ReceivingStatus = ReceivingStatus.YISHOUHUO;

                }
                await _wmsReceiptOrderDetailsRep.UpdateNowAsync(WmsReceiptOrderDetailsModal);
            }
            //获取该收货单所有单据明细数量
            int allCount = wmsOrderModal.WmsReceiptOrderDetails.Count;
            //获取该收货单所有完成的单据明细
            var wcCount = await _wmsReceiptOrderDetailsRep.Where(x => x.OrderId == wmsOrderModal.Id && x.ReceivingStatus == ReceivingStatus.YISHOUHUO).ToListAsync();
            if (allCount == wcCount.Count)
            {
                wmsOrderModal.ReceivingStatus = ReceivingStatus.YISHOUHUO;
            }
            else if (allCount > wcCount.Count && wcCount.Count > 0)
            {
                wmsOrderModal.ReceivingStatus = ReceivingStatus.SHOUHUOZHONG;
            }
            else
            {
                wmsOrderModal.ReceivingStatus = ReceivingStatus.WEISHOUHUO;
            }
            await _wmsReceiptOrderRep.UpdateAsync(wmsOrderModal);
        }
    }
}
