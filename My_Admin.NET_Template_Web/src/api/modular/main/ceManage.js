﻿import { axios } from '@/utils/request'

/**
 * 查询物料信息
 *
 * @author liduanping
 */
export function cePage (parameter) {
  return axios({
    url: '/ce/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 物料信息列表
 *
 * @author liduanping
 */
export function ceList (parameter) {
  return axios({
    url: '/ce/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加物料信息
 *
 * @author liduanping
 */
export function ceAdd (parameter) {
  return axios({
    url: '/ce/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑物料信息
 *
 * @author liduanping
 */
export function ceEdit (parameter) {
  return axios({
    url: '/ce/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除物料信息
 *
 * @author liduanping
 */
export function ceDelete (parameter) {
  return axios({
    url: '/ce/delete',
    method: 'post',
    data: parameter
  })
}
