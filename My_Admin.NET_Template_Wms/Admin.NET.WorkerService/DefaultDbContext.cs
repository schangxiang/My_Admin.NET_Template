using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;

namespace Admin.NET.WorkerService.DbContexts
{
    [AppDbContext("DefaultConnection", DbProvider.SqlServer)]
    public class DefaultDbContext : AppDbContext<DefaultDbContext>
    {
        public DefaultDbContext(DbContextOptions<DefaultDbContext> options) : base(options)
        {
        }
    }
}