﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 所属车间枚举 /组装车间
    /// </summary>
    
    ////所属车间 /胶合板车间/薄膜车间/组装车间（老）/组装车间（新）
    public enum LesWorkShopType
    {
        /// <summary>
        /// 入库
        /// </summary>
        [Description("入库")]
        FAPAOCHEJIAN = 1,

        /// <summary>
        /// 出库
        /// </summary>
        [Description("出库")]
        JIAOHEBANCHEJIAN = 2,

        /// <summary>
        /// 胶合板车间
        /// </summary>
        [Description("胶合板车间")]
        JHBCJ = 3,

        /// <summary>
        /// RSB车间
        /// </summary>
        [Description("RSB车间")]
        RSBCJ = 4,

        /// <summary>
        /// 原料库车间
        /// </summary>
        [Description("原料库车间")]
        YLKCJ = 5,

        /// <summary>
        /// 通用
        /// </summary>
        [Description("通用")]
        TY = 6,
    }
}
