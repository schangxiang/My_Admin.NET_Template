import {getSysEnumData} from '@/api/common.js'
const orderStatus = {
	data() {
		return {
			OrderStatus: []
		}
	},
	created() {
		this.getSysEnumData('OrderStatusEnum')
	},
	filters: {
		getStatus(orderStatus, OrderStatus) {
			const result = OrderStatus.filter(obj => obj.code == orderStatus)
			return result.length > 0 ? result[0].value : ''
		}
	},
	methods: {
		//获取单据状态枚举
		async getSysEnumData(enumName) {
			try {
				let {
					data
				} = await getSysEnumData({
					enumName
				})
				this.OrderStatus = data
				console.log(data);
			} catch (e) {
				//TODO handle the exception
				console.log(e);
			}
		}
	}
}

export {orderStatus}