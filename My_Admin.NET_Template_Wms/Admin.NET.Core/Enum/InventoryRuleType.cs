﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 盘点类型
    /// </summary>
    public enum RuleType
    {
        /// <summary>
        /// 动盘
        /// </summary>
        [Description("动盘")]
        DONGPANG = 1,

        /// <summary>
        /// 静盘
        /// </summary>
        [Description("静盘")]
        JINGPANG = 2
    }

    /// <summary>
    /// 盘点模式
    /// </summary>
    public enum RuleMode
    {
        /// <summary>
        /// 随机
        /// </summary>
        [Description("随机")]
        SUIJI = 1,

        /// <summary>
        /// 抽样
        /// </summary>
        [Description("抽样")]
        CHOUYANG = 2,

        /// <summary>
        /// 全盘
        /// </summary>
        [Description("全盘")]
        QUANPANG = 3
    }

    /// <summary>
    /// 盘点周期
    /// </summary>
    public enum RuleCycle
    {
        /// <summary>
        /// 月度
        /// </summary>
        [Description("月度")]
        YUEDU = 1,

        /// <summary>
        /// 季度
        /// </summary>
        [Description("季度")]
        JIDU = 2,

        /// <summary>
        /// 年度
        /// </summary>
        [Description("年度")]
        NIANGDU = 3
    }

    /// <summary>
    /// 审批状态
    /// </summary>
    public enum ExamineStateEnum
    {
        /// <summary>
        /// 未审批
        /// </summary>
        [Description("未审批")]
        WEISHENPI = 1,

        /// <summary>
        /// 通过
        /// </summary>
        [Description("通过")]
        TONGGUO = 2,

        /// <summary>
        /// 驳回
        /// </summary>
        [Description("驳回")]
        BOHUI = 3
    }
}