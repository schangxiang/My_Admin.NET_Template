﻿using Admin.NET.Core.Util.LowCode.Dto;
using System;

namespace Admin.NET.Application
{
    /// <summary>
    /// 客商信息表输出参数
    /// </summary>
    public class WmsMerchantinfoDto
    {
        /// <summary>
        /// 客商编号
        /// </summary>
        public string MerchantNo { get; set; }
        
        /// <summary>
        /// 客商名称
        /// </summary>
        public string MerchantName { get; set; }
        
        /// <summary>
        /// 客商分类
        /// </summary>
        public Admin.NET.Core.MerchantType MerchantType { get; set; }
        
        /// <summary>
        /// 客商简称
        /// </summary>
        public string MerchantShort { get; set; }
        
        /// <summary>
        /// 客商等级
        /// </summary>
        public Admin.NET.Core.MerchantGrade MerchantGrade { get; set; }
        
        /// <summary>
        /// 客商属性
        /// </summary>
        public string MerchantAttribute { get; set; }
        
        /// <summary>
        /// 客商行业
        /// </summary>
        public string MerchantTmt { get; set; }
        
        /// <summary>
        /// 客商联系人
        /// </summary>
        public string MerchantContact { get; set; }
        
        /// <summary>
        /// 客商电话
        /// </summary>
        public string MerchantPhone { get; set; }
        
        /// <summary>
        /// 客商传真
        /// </summary>
        public string MerchantFax { get; set; }
        
        /// <summary>
        /// 客商邮箱
        /// </summary>
        public string MerchantEmail { get; set; }
        
        /// <summary>
        /// 客商地址
        /// </summary>
        public string MerchantAddress { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public Admin.NET.Core.IssueState IssueState { get; set; }
        
    }
}
