﻿import { axios } from '@/utils/request'

/**
 * 查询备库计划
 *
 * @author yc
 */
export function BackupWarehouseOrderPage (parameter) {
  return axios({
    url: '/BackupWarehouseOrder/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 备库计划列表
 *
 * @author yc
 */
export function BackupWarehouseOrderList (parameter) {
  return axios({
    url: '/BackupWarehouseOrder/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加备库计划
 *
 * @author yc
 */
export function BackupWarehouseOrderAdd (parameter) {
  return axios({
    url: '/BackupWarehouseOrder/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑备库计划
 *
 * @author yc
 */
export function BackupWarehouseOrderEdit (parameter) {
  return axios({
    url: '/BackupWarehouseOrder/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除备库计划
 *
 * @author yc
 */
export function BackupWarehouseOrderDelete (parameter) {
  return axios({
    url: '/BackupWarehouseOrder/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出备库计划的Excel文件
 *
 * @author yc
 */
export function BackupWarehouseOrderToExcel (parameter) {
  return axios({
    url: '/BackupWarehouseOrder/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入备库计划的Excel文件
 *
 * @author yc
 */
export function BackupWarehouseOrderFromExcel (data, parameter) {
  return axios({
    url: '/BackupWarehouseOrder/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载备库计划的Excel导入模板
 *
 * @author yc
 */
export function BackupWarehouseOrderDownloadExcelTemplate(parameter) {
  return axios({
    url: '/BackupWarehouseOrder/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 单据小类下拉
 *
 * @author yhh
 */
export function BackupWarehouseOrderSubclass() {
  return axios({
    url: '/BackupWarehouseOrder/OrderTypeSubclass',
    method: 'get'
  })
}


/**
 * 备库计划详情
 *
 * @author yhh
 */
export function BackupWarehouseOrderDetailPage (parameter) {
  return axios({
    url: '/BackupWarehouseOrder/DetailsPage',
    method: 'get',
    params: parameter
  })
}



