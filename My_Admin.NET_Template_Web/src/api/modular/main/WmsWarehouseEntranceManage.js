﻿import { axios } from '@/utils/request'

/**
 * 查询库口表
 *
 * @author XJF
 */
export function WmsWarehouseEntrancePage (parameter) {
  return axios({
    url: '/WmsWarehouseEntrance/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 库口表列表
 *
 * @author XJF
 */
export function WmsWarehouseEntranceList (parameter) {
  return axios({
    url: '/WmsWarehouseEntrance/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加库口表
 *
 * @author XJF
 */
export function WmsWarehouseEntranceAdd (parameter) {
  return axios({
    url: '/WmsWarehouseEntrance/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑库口表
 *
 * @author XJF
 */
export function WmsWarehouseEntranceEdit (parameter) {
  return axios({
    url: '/WmsWarehouseEntrance/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除库口表
 *
 * @author XJF
 */
export function WmsWarehouseEntranceDelete (parameter) {
  return axios({
    url: '/WmsWarehouseEntrance/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出库口表的Excel文件
 *
 * @author XJF
 */
export function WmsWarehouseEntranceToExcel (parameter) {
  return axios({
    url: '/WmsWarehouseEntrance/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入库口表的Excel文件
 *
 * @author XJF
 */
export function WmsWarehouseEntranceFromExcel (data, parameter) {
  return axios({
    url: '/WmsWarehouseEntrance/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载库口表的Excel导入模板
 *
 * @author XJF
 */
export function WmsWarehouseEntranceDownloadExcelTemplate(parameter) {
  return axios({
    url: '/WmsWarehouseEntrance/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}



