﻿using Mapster;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    public class WmsGoodsReturnOrderMapper : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<AddWmsGoodsReturnOrderInput, WmsGoodsReturnOrder>()
            ;
            config.ForType<UpdateWmsGoodsReturnOrderInput, WmsGoodsReturnOrder>()
            ;
            config.ForType<WmsGoodsReturnOrder, WmsGoodsReturnOrderOutput>()
            ;
        }
    }
}
