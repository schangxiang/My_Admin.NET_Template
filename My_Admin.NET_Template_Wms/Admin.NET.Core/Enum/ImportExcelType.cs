﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 导入Excel方式
    /// </summary>
    [Description("导入Excel方式")]
    public enum ImportExcelType
    {
        /// <summary>
        /// 仅新增
        /// </summary>
        [Description("仅新增")]
        ADD_ONLY = 1,

        /// <summary>
        /// 新增并更新
        /// </summary>
        [Description("新增并更新")]
        ADD_AND_UPDATE = 2
    }
}