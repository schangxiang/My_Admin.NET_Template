﻿using Admin.NET.Core.Util.LowCode.Dto;
using System;

namespace Admin.NET.Application
{
    /// <summary>
    /// 学生信息表4输出参数
    /// </summary>
    public class TestStudents4Output
    {
        /// <summary>
        /// 学生姓名
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// 学生年龄
        /// </summary>
        public int? Age { get; set; }
        
        /// <summary>
        /// 零花钱
        /// </summary>
        public decimal? HasMoney { get; set; }
        
        /// <summary>
        /// 是否在校
        /// </summary>
        public bool? StartName { get; set; }
        
        /// <summary>
        /// 性别
        /// </summary>
        public int? Gender { get; set; }
        
        /// <summary>
        /// 出生日期
        /// </summary>
        public DateTime? BrithDate { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTimeOffset? CreatedTime { get; set; }
        
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTimeOffset? UpdatedTime { get; set; }
        
        /// <summary>
        /// 创建者名称
        /// </summary>
        public string CreatedUserName { get; set; }
        
        /// <summary>
        /// 修改者名称
        /// </summary>
        public string UpdatedUserName { get; set; }
        
    }
}
