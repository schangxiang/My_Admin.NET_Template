﻿using Admin.NET.Core;
using Admin.NET.Core.Service;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Admin.NET.Application
{    
   

    /// <summary>
    /// 报检传递给yigo系统的详情参数
    /// </summary>
    public class InspectionDeclarationDetail
    {
        /// <summary>
        /// OID  
        /// </summary>
        public long OID { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string Materialname { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        public string Materialcode { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Qty { get; set; }

        /// <summary>
        /// 重量    
        /// </summary>
        public decimal SumWeight { get; set; }

    }

    /// <summary>
    /// 报检传递给yigo系统的参数
    /// </summary>
    public class InspectionDeclarationYiGo
    {
        /// <summary>
        /// Id
        /// </summary>
        public long? Id { get; set; }

        /// <summary>
        /// YIGO系统id
        /// </summary>
        public virtual long? SOID { get; set; }

        /// <summary>
        /// 入库通知单号
        /// </summary>
        public virtual string NO { get; set; }

        /// <summary>
        /// WMS收货单号
        /// </summary>
        public virtual string WMSNO { get; set; }

        /// <summary>
        /// 收货时间
        /// </summary>
        public virtual string Billdate { get; set; }

        /// <summary>
        /// 收货详情
        /// </summary>
        public ICollection<InspectionDeclarationDetail> Dtls { get; set; }
    }

    /// <summary>
    /// 获取报检结果参数
    /// </summary>
    public class GetInspectionResultInput
    {
        /// <summary>
        /// 编号
        /// </summary>
        public string NO { get; set; }

        /// <summary>
        /// SOID  
        /// </summary>
        public long SOID { get; set; }
    }
}
