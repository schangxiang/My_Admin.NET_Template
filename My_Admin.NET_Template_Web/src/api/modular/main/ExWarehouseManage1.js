import { axios } from '@/utils/request'

/**
 * 查询叫料
 *
 * @author XJF
 */
export function CncTakeMaterialsPage (parameter) {
  return axios({
    url: '/cnctakematerials/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加叫料
 *
 * @author XJF
 */
export function CncTakeMaterialsAdd (data) {
  return axios({
    url: '/cnctakematerials/AddCNCTakematerial',
    method: 'post',
    data: data
  })
}

/**
 * 删除叫料
 *
 * @author XJF
 */
export function CncTakeMaterialsDelete (parameter) {
  return axios({
    url: '/LesTakematerials/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 获取叫料单号
 *
 * @author yhh
 */
export function GetBillNumber () {
  return axios({
    url: '/cnctakematerials/CNCGetTakeMaterialsNo',
    method: 'get'
  })
}


/**
 * 物料查询
 *
 * @author yhh
 */
export function QueryMaterials (params) {
  return axios({
    url: '/cnctakematerials/CNCMaterialSourcepage',
    method: 'get',
    params:params
  })
}

/**
 * 叫料单下发
 *
 * @author yhh
 */
export function Distribute (ids) {
  return axios({
    url: '/cnctakematerials/CNCBatchTakeMaterials',
    method: 'post',
    data:{id:ids}
  })
}

/**
 * 叫料单详情下发
 *
 * @author yhh
 */
export function DetailDistribute (id) {
  return axios({
    url: '/cnctakematerials/CNCBatchTakeMaterialsDetail',
    method: 'post',
    data:{id}
  })
}

/**
 * 单据详情
 *
 * @author yhh
 */
export function CncTakeMaterialsDetailPage (params) {
  return axios({
    url: '/cnctakematerials/detailPage',
    method: 'get',
    params:params
  })
}

/**
 * 获取产线
 *
 * @author yhh
 */
export function GetLines() {
  return axios({
    url: '/cnctakematerials/GetProductionlineList',
    method: 'get'
  })
}

/**
 * 根据产线获取工位
 *
 * @author yhh
 */
export function GetStations(id) {
  return axios({
    url: '/cnctakematerials/GetStationList',
    method: 'get',
    params:{id}
  })
}