﻿using Microsoft.EntityFrameworkCore;
using Furion.DatabaseAccessor;

namespace YaKe.Les.Core.SeedData
{
    public class WmsTaskSeedData : IEntitySeedData<WmsTask>
    {
        /// <summary>
        /// 任务管理种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<WmsTask> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new[]
            {
                new WmsTask{Id=358853585506373,TaskNo="358853585506374",TaskModel=TaskModel.SHOUDONG,TaskType=TaskType.RUKU,TaskLevel=1,TaskStatus=Core.TaskStatus.WANCHENG,ContainerCode="TM00001",SourcePlace="N/A",ToPlace="1-1-1-1-1",Aisle=1,AreaName="A区",OrderNo="N/A",SendTimes=0},
                new WmsTask{Id=358856088019013,TaskNo="358856088019014",TaskModel=TaskModel.SHOUDONG,TaskType=TaskType.YIKU,TaskLevel=1,TaskStatus=Core.TaskStatus.WANCHENG,ContainerCode="TM00001",SourcePlace="1-1-1-1-1",ToPlace="1-1-1-2-1",Aisle=1,AreaName="A区",OrderNo="N/A",SendTimes=0},
                new WmsTask{Id=358856349995077,TaskNo="358856349995078",TaskModel=TaskModel.SHOUDONG,TaskType=TaskType.CHUKU,TaskLevel=1,TaskStatus=Core.TaskStatus.WANCHENG,ContainerCode="TM00001",SourcePlace="1-1-1-2-1",ToPlace="N/A",Aisle=1,AreaName="A区",OrderNo="N/A",SendTimes=0},
                new WmsTask{Id=358856845733957,TaskNo="358856845733958",TaskModel=TaskModel.ZIDONG,TaskType=TaskType.RUKU,TaskLevel=1,TaskStatus=Core.TaskStatus.WANCHENG,ContainerCode="TM00002",SourcePlace="100",ToPlace="1-1-1-1-1",Aisle=1,AreaName="A区",OrderNo="358856845353029",SendTimes=0},
                new WmsTask{Id=358856980553799,TaskNo="358856980553800",TaskModel=TaskModel.ZIDONG,TaskType=TaskType.CHUKU,TaskLevel=1,TaskStatus=Core.TaskStatus.WANCHENG,ContainerCode="TM00002",SourcePlace="1-1-1-1-1",ToPlace="101",Aisle=1,AreaName="A区",OrderNo="358856980553798",SendTimes=0},
                new WmsTask{Id=358857243410501,TaskNo="358857243410502",TaskModel=TaskModel.SHOUDONG,TaskType=TaskType.RUKU,TaskLevel=1,TaskStatus=Core.TaskStatus.WANCHENG,ContainerCode="TM00003",SourcePlace="N/A",ToPlace="1-1-1-5-1",Aisle=1,AreaName="A区",OrderNo="N/A",SendTimes=0},
                new WmsTask{Id=358857618247749,TaskNo="358857618247750",TaskModel=TaskModel.ZIDONG,TaskType=TaskType.RUKU,TaskLevel=1,TaskStatus=Core.TaskStatus.WANCHENG,ContainerCode="TM00004",SourcePlace="101",ToPlace="1-1-1-1-1",Aisle=1,AreaName="A区",OrderNo="358857618075717",SendTimes=0},
                new WmsTask{Id=358858262179909,TaskNo="358858262179910",TaskModel=TaskModel.SHOUDONG,TaskType=TaskType.RUKU,TaskLevel=1,TaskStatus=Core.TaskStatus.WANCHENG,ContainerCode="TM00005",SourcePlace="N/A",ToPlace="1-1-1-4-1",Aisle=1,AreaName="A区",OrderNo="358858261872712",SendTimes=0}
            };
        }
    }
}
