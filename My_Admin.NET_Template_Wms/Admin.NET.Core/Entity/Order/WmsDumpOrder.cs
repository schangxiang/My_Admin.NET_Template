﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    /// <summary>
    /// 转储单
    /// </summary>
    [Table("wms_dumporder")]
    [Comment("转储单")]
    public class WmsDumpOrder : DEntityBase, IEntityTypeBuilder<WmsDumpOrder>
    {
        /// <summary>
        ///  Billdate
        /// </summary>
        [Comment("单据大类")]
        [Required]
        public long OrderLargeCategory { get; set; }

        /// <summary>
        /// 单据小类
        /// </summary>
        [Comment("单据小类")]
        [Required]
        public long OrderSubclass { get; set; }

        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        [Comment("上位系统单据唯一识别码")]
        public long SOID { get; set; }

        /// <summary>
        /// 单据编号  
        /// </summary>
        [Comment("单据编号")]
        [Required]
        [MaxLength(255)]
        public string NO { get; set; }

        /// <summary>
        /// 创建日期    
        /// </summary>
        [Comment("创建日期")]
        public DateTimeOffset? Billdate { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        [Comment("创建人")]
        [MaxLength(255)]
        public string Creator { get; set; }

        /// <summary>
        /// 公司    
        /// </summary>
        [Comment("公司")]
        [MaxLength(255)]
        public string Companyname { get; set; }

        /// <summary>
        /// 单据状态
        /// </summary>
        [Comment("单据状态")]
        [Required]
        public OrderStatusEnum OrderStatus { get; set; } = OrderStatusEnum.WEIXIAFA;

        /// <summary>
        /// 单据明细
        /// </summary>
        public ICollection<WmsDumpOrderDetails> WmsDumpOrderDetails { get; set; }

        /// <summary>
        /// 1对多配置关系
        /// </summary>
        /// <param name="entityBuilder"></param>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        public void Configure(EntityTypeBuilder<WmsDumpOrder> entityBuilder, DbContext dbContext, Type dbContextLocator)
        {
            // 一对多配置
            entityBuilder.HasMany(x => x.WmsDumpOrderDetails)
                .WithOne(x => x.WmsDumpOrder)
                .HasForeignKey(x => x.OrderId);
        }
    }
}
