﻿using Admin.NET.Core.Util.LowCode.Dto;
using System;

namespace Admin.NET.Application
{
    /// <summary>
    /// 收货单输出参数
    /// </summary>
    public class WmsReceiptOrderDto
    {
        /// <summary>
        /// 单据大类
        /// </summary>
        public long OrderLargeCategory { get; set; }
        
        /// <summary>
        /// 单据小类
        /// </summary>
        public long OrderSubclass { get; set; }
        
        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        public long SOID { get; set; }
        
        /// <summary>
        /// 单据编号
        /// </summary>
        public string NO { get; set; }
        
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTimeOffset Billdate { get; set; }
        
        /// <summary>
        /// 送货通知单号
        /// </summary>
        public string SourceBillNo { get; set; }
        
        /// <summary>
        /// 供应商编码
        /// </summary>
        public string Vendorcode { get; set; }
        
        /// <summary>
        /// 供应商名称
        /// </summary>
        public string Vendorname { get; set; }
        
        /// <summary>
        /// 公司名称
        /// </summary>
        public string Companyname { get; set; }
        
        /// <summary>
        /// 仓管员
        /// </summary>
        public string WarehouseKeepername { get; set; }
        
        /// <summary>
        /// 贸易方式
        /// </summary>
        public Admin.NET.Core.TradeMode TradeMode { get; set; }
        
        /// <summary>
        /// 仓储中心
        /// </summary>
        public string WarehouseCentername { get; set; }
        
        /// <summary>
        /// 仓间
        /// </summary>
        public string StoreRoomname { get; set; }
        
        /// <summary>
        /// 报关单号
        /// </summary>
        public string DeclarationCode { get; set; }
        
        /// <summary>
        /// 专业
        /// </summary>
        public string Majorname { get; set; }
        
        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks { get; set; }
        
        /// <summary>
        /// 单据状态
        /// </summary>
        public Admin.NET.Core.OrderStatusEnum OrderStatus { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public Admin.NET.Core.IssueState IssueState { get; set; }
        
    }
}
