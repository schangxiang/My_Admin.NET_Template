﻿import { axios } from '@/utils/request'

/**
 * 查询库存物料视图
 *
 * @author liuwq
 */
export function View_Materialstock_MaterialPage (parameter) {
  return axios({
    url: '/View_Materialstock_Material/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 库存物料视图列表
 *
 * @author liuwq
 */
export function View_Materialstock_MaterialList (parameter) {
  return axios({
    url: '/View_Materialstock_Material/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加库存物料视图
 *
 * @author liuwq
 */
export function View_Materialstock_MaterialAdd (parameter) {
  return axios({
    url: '/View_Materialstock_Material/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑库存物料视图
 *
 * @author liuwq
 */
export function View_Materialstock_MaterialEdit (parameter) {
  return axios({
    url: '/View_Materialstock_Material/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除库存物料视图
 *
 * @author liuwq
 */
export function View_Materialstock_MaterialDelete (parameter) {
  return axios({
    url: '/View_Materialstock_Material/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出库存物料视图的Excel文件
 *
 * @author liuwq
 */
export function View_Materialstock_MaterialToExcel (parameter) {
  return axios({
    url: '/View_Materialstock_Material/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入库存物料视图的Excel文件
 *
 * @author liuwq
 */
export function View_Materialstock_MaterialFromExcel (data, parameter) {
  return axios({
    url: '/View_Materialstock_Material/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载库存物料视图的Excel导入模板
 *
 * @author liuwq
 */
export function View_Materialstock_MaterialDownloadExcelTemplate(parameter) {
  return axios({
    url: '/View_Materialstock_Material/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}



