﻿import { axios } from '@/utils/request'

/**
 * 查询泡沐车间完工入库单
 *
 * @author yc
 */
export function PMFinishedOrderPage (parameter) {
  return axios({
    url: '/PMFinishedOrder/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 泡沐车间完工入库单列表
 *
 * @author yc
 */
export function PMFinishedOrderList (parameter) {
  return axios({
    url: '/PMFinishedOrder/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加泡沐车间完工入库单
 *
 * @author yc
 */
export function PMFinishedOrderAdd (parameter) {
  return axios({
    url: '/PMFinishedOrder/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑泡沐车间完工入库单
 *
 * @author yc
 */
export function PMFinishedOrderEdit (parameter) {
  return axios({
    url: '/PMFinishedOrder/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除泡沐车间完工入库单
 *
 * @author yc
 */
export function PMFinishedOrderDelete (parameter) {
  return axios({
    url: '/PMFinishedOrder/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出泡沐车间完工入库单的Excel文件
 *
 * @author yc
 */
export function PMFinishedOrderToExcel (parameter) {
  return axios({
    url: '/PMFinishedOrder/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入泡沐车间完工入库单的Excel文件
 *
 * @author yc
 */
export function PMFinishedOrderFromExcel (data, parameter) {
  return axios({
    url: '/PMFinishedOrder/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载泡沐车间完工入库单的Excel导入模板
 *
 * @author yc
 */
export function PMFinishedOrderDownloadExcelTemplate(parameter) {
  return axios({
    url: '/PMFinishedOrder/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 单据大类下拉
 *
 * @author yhh
 */
export function PMFinishedOrderLargeCategories() {
  return axios({
    url: '/PMFinishedOrder/OrderTypeLargeCategory',
    method: 'get'
  })
}

/**
 * 单据小类下拉
 *
 * @author yhh
 */
export function PMFinishedOrderSubclass(id) {
  return axios({
    url: '/PMFinishedOrder/OrderTypeSubclass',
    method: 'get',
    params:{id}
  })
}


/**
 * 完工入库单详情
 *
 * @author yhh
 */
export function PMFinishedOrderDetailPage (parameter) {
  return axios({
    url: '/PMFinishedOrder/DetailsPage',
    method: 'get',
    params: parameter
  })
}




