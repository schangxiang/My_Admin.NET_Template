﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

/*
 * @author : shaocx
 * @date : 2024/4/19下午3:13:43
 * @desc : 测试学生表
 */
namespace Admin.NET.Core
{
    /// <summary>
    /// 测试学生表
    /// </summary>
    [Table("test_student5")]
    [Comment("测试学生表")]
    public class TestStudent5 : DEntityBase
    {

        /// <summary>
        /// 姓名
        /// </summary>
        [Comment("姓名")]
        [MaxLength(50)]
        public string Name { get; set; }


        /// <summary>
        /// 年龄
        /// </summary>
        [Comment("年龄")]

        public int? Age { get; set; }

    }
}