﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 下料产线枚举
    /// </summary>
    public  enum BlankingProductionLineEnum
    {
        /// <summary>
        /// 发泡一线
        /// </summary>
        [Description("发泡一线")]
        FAPAOYIXIAN = 1,

        /// <summary>
        /// 发泡二线
        /// </summary>
        [Description("发泡二线")]
        FAPAOERXIAN = 2
    }
}
