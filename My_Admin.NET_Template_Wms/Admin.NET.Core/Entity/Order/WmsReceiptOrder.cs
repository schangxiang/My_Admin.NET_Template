﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    /// <summary>
    /// 收货单
    /// </summary>
    [Table("wms_receiptorder")]
    [Comment("收货单")]
    public class WmsReceiptOrder : DEntityBase, IEntityTypeBuilder<WmsReceiptOrder>
    {
        /// <summary>
        ///  Billdate
        /// </summary>
        [Comment("单据大类")]
        [Required]
        public long OrderLargeCategory { get; set; }

        /// <summary>
        /// 单据小类
        /// </summary>
        [Comment("单据小类")]
        [Required]
        public long OrderSubclass { get; set; }

        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        [Comment("上位系统单据唯一识别码")]
        public long SOID { get; set; }

        /// <summary>
        /// 单据编号  
        /// </summary>
        [Comment("单据编号")]
        [Required]
        [MaxLength(255)]
        public string NO { get; set; }

        /// <summary>
        /// WMS收货单号
        /// </summary>
        [Comment("WMS收货单号")]
        [MaxLength(255)]
        public string WMSNO { get; set; }

        /// <summary>
        /// 创建日期    
        /// </summary>
        [Comment("创建日期")]
        public DateTimeOffset? Billdate { get; set; }

        /// <summary>
        /// 送货通知单号
        /// </summary>
        [Comment("送货通知单号")]
        [MaxLength(255)]
        public string SourceBillNo { get; set; }

        /// <summary>
        /// 供应商编码    
        /// </summary>
        [Comment("供应商编码")]
        [MaxLength(255)]
        public string Vendorcode { get; set; }

        /// <summary>
        /// 供应商名称    
        /// </summary>
        [Comment("供应商名称")]
        [MaxLength(255)]
        public string Vendorname { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        [Comment("公司名称")]
        [MaxLength(255)]
        public string Companyname { get; set; }

        /// <summary>
        /// 仓管员
        /// </summary>
        [Comment("仓管员")]
        [MaxLength(255)]
        public string WarehouseKeepername { get; set; }

        /// <summary>
        /// 贸易方式
        /// </summary>
        [Comment("贸易方式")]
        public TradeMode TradeMode { get; set; }

        /// <summary>
        /// 仓储中心
        /// </summary>
        [Comment("仓储中心")]
        [MaxLength(255)]
        public string WarehouseCentername { get; set; }

        /// <summary>
        /// 仓间
        /// </summary>
        [Comment("仓间")]
        [MaxLength(255)]
        public string StoreRoomname { get; set; }

        /// <summary>
        /// 报关单号
        /// </summary>
        [Comment("报关单号")]
        [MaxLength(255)]
        public string DeclarationCode { get; set; }

        /// <summary>
        /// 专业
        /// </summary>
        [Comment("专业")]
        [MaxLength(255)]
        public string Majorname { get; set; }

        /// <summary>
        /// 备注    
        /// </summary>
        [Comment("备注")]
        [MaxLength(500)]
        public string Remarks { get; set; }

        /// <summary>
        /// 收货状态
        /// </summary>
        [Comment("收货状态")]
        [Required]
        public ReceivingStatus ReceivingStatus { get; set; } = ReceivingStatus.WEISHOUHUO;

        /// <summary>
        /// 报检状态
        /// </summary>
        [Comment("报检状态")]
        [Required]
        public InspectionStatus InspectionStatus { get; set; } = InspectionStatus.WEIBAOJIAN;

        /// <summary>
        /// 单据明细
        /// </summary>
        public ICollection<WmsReceiptOrderDetails> WmsReceiptOrderDetails { get; set; }

        /// <summary>
        /// 1对多配置关系
        /// </summary>
        /// <param name="entityBuilder"></param>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        public void Configure(EntityTypeBuilder<WmsReceiptOrder> entityBuilder, DbContext dbContext, Type dbContextLocator)
        {
            // 一对多配置
            entityBuilder.HasMany(x => x.WmsReceiptOrderDetails)
                .WithOne(x => x.WmsReceiptOrder)
                .HasForeignKey(x => x.OrderId);
        }
    }
}
