﻿using Admin.NET.Core;
using Microsoft.AspNetCore.Mvc;

namespace Admin.NET.Application
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISysTimerService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task AddTimer(AddJobInput input);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        void AddTimerJob(AddJobInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task DeleteTimer(DeleteJobInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<SysTimer> GetTimer([FromQuery] QueryJobInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PageResult<JobOutput>> GetTimerPageList([FromQuery] JobPageInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        void StartTimerJob(AddJobInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        void StopTimerJob(StopJobInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task UpdateTimber(UpdateJobInput input);

        /// <summary>
        /// 
        /// </summary>
        void StartTimerJob();
    }
}