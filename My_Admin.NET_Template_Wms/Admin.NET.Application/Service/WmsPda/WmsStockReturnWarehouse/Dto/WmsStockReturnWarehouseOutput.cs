﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 获取托盘入参
    /// </summary>
    public class StockReturnGetContainerOutput
    {
        /// <summary>
        /// 托盘号
        /// </summary>
        [Required(ErrorMessage = "托盘号不能为空")]
        public string ContainerCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public WmsContainerDto WmsContainer { get; set; }

        /// <summary>
        /// 物料信息
        /// </summary>
        public List<WmsMaterialContainer> WmsMaterials { get; set; }
    }

    /// <summary>
    /// 仅收货（组盘）成功输出参数
    /// </summary>
    public class StockReturnGroupDiskOutput
    {
        /// <summary>
        /// 托盘
        /// </summary>
        public string Containercode { get; set; }

        /// <summary>
        /// 组盘单据
        /// </summary>
        public string OrderNo { get; set; }
    }
    /// <summary>
    /// 物料返回参数
    /// </summary>
    public class StockReturnPadGetMaterialOutput
    {
        /// <summary>
        /// 单据Id  
        /// </summary>
        public long OrderId { get; set; }

        /// <summary>
        /// 上位系统单据明细唯一识别码
        /// </summary>
        public long OID { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        public string Materialcode { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string Materialname { get; set; }

        /// <summary>
        /// 基本单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Qty { get; set; }

        /// <summary>
        /// 单根长度 
        /// </summary>
        public decimal Length { get; set; }

        /// <summary>
        /// 件数    
        /// </summary>
        public decimal Number { get; set; }

        /// <summary>
        /// SCM批次号    
        /// </summary>
        public string Batchno_SCM { get; set; }

        /// <summary>
        /// WMS批次号    
        /// </summary>
        public string Batchno_WMS { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// TA号
        /// </summary>
        public string TACode { get; set; }

        /// <summary>
        /// 分段号
        /// </summary>
        public string PartCode { get; set; }
    }
}
