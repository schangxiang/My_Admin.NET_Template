﻿import { axios } from '@/utils/request'

/**
 * 查询库存表
 *
 * @author liduanping
 */
export function WmsMaterialStockPage (parameter) {
  return axios({
    url: '/ExWarehouse/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 手动出库
 *
 * @author yhh
 */
export function IexwarehouseManual (parameter) {
  return axios({
    url: '/ExWarehouse/manualExWarehouse',
    method: 'post',
    data: parameter
  })
}

/**
 * 自动出库
 *
 * @author yhh
 */
export function IexwarehouseAuto (parameter) {
  return axios({
    url: '/ExWarehouse/AutoExWarehouse',
    method: 'post',
    data: parameter
  })
}

