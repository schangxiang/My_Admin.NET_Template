﻿namespace Admin.NET.Core
{
    /// <summary>
    /// Desk数据同步常量
    /// </summary>
    public class DeskDataConst
    {
        /// <summary>
        /// Desk AccessToken 缓存
        /// </summary>
        public const string DESK_ACCESSTOKEN_CODE = "desk_accesstoken";

        /// <summary>
        /// 同步客户URL
        /// </summary>
        public const string TONGBU_KEHU_URL = "/api/desk/v1.1/deskCustomer/listNonPage";

        /// <summary>
        /// 同步物料URL
        /// </summary>
        public const string TONGBU_WULIAO_URL = "/api/desk/v1.1/deskMaterial/listNonPage";
    }
}
