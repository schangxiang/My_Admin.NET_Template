﻿using Mapster;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    public class WmsDumpOrderMapper : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<AddWmsDumpOrderInput, WmsDumpOrder>()
            ;
            config.ForType<UpdateWmsDumpOrderInput, WmsDumpOrder>()
            ;
            config.ForType<WmsDumpOrder, WmsDumpOrderOutput>()
            ;
        }
    }
}
