﻿namespace Admin.NET.Application
{
    /// <summary>
    /// AuthToken参数
    /// </summary>
    public class AuthToken
    {
        /// <summary>
        /// 
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int ExpireIn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string RefreshToken { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Uid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string OpenId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AccessCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string UnionId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Scope { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string TokenType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string IdToken { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MacAlgorithm { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MacKey { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string OauthToken { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string OauthTokenSecret { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ScreenName { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public bool OauthCallbackConfirmed { get; set; }
    }
}