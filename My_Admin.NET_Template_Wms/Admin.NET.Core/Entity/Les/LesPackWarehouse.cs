﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Core
{
    /// <summary>
    /// 打包表
    /// </summary>
    [Table("les_packwarehouse")]
    [Comment("打包表")]
    public class LesPackWarehouse : DEntityBase
    {
        /// <summary>
        /// 标签码
        /// </summary>
        [Comment("标签码")]
        [MaxLength(50)]
        public string PackCode { get; set; }

        /// <summary>
        /// 物料码
        /// </summary>
        [Comment("物料码")]
        [MaxLength(50)]
        public string MaterialNo { get; set; }

        /// <summary>
        /// 长    
        /// </summary>
        [Comment("长")]
        [Required]
        public int Long { get; set; }

        /// <summary>
        /// 宽    
        /// </summary>
        [Comment("宽")]
        [Required]
        public int Wide { get; set; }

        /// <summary>
        /// 高    
        /// </summary>
        [Comment("高")]
        [Required]
        public int High { get; set; }

        /// <summary>
        /// 出库口
        /// </summary>
        [Comment("出库口")]
        [MaxLength(50)]
        public string OutputEntrance { get; set; }
        

        /// <summary>
        /// 打包状态
        /// </summary>
        [Comment("打包状态")]
        public LesPackState LesPackState { get; set; }

        /// <summary>
        /// 单据号
        /// </summary>
        [Comment("单据号")]
        public string OrderNo { get; set; }
    }
}
