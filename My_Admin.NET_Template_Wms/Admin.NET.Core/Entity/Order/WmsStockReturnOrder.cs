﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    /// <summary>
    /// 退料入库单
    /// </summary>
    [Table("wms_stockreturn_order")]
    [Comment("退料入库单")]
    public class WmsStockReturnOrder : DEntityBase, IEntityTypeBuilder<WmsStockReturnOrder>
    {
        /// <summary>
        ///  Billdate
        /// </summary>
        [Comment("单据大类")]
        [Required]
        public long OrderLargeCategory { get; set; }

        /// <summary>
        /// 单据小类
        /// </summary>
        [Comment("单据小类")]
        [Required]
        public long OrderSubclass { get; set; }

        /// <summary>
        /// 事务类型
        /// </summary>
        [Comment("事务类型")]
        public MoveType MoveType { get; set; }

        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        [Comment("上位系统单据唯一识别码")]
        public long SOID { get; set; }

        /// <summary>
        /// 单据编号  
        /// </summary>
        [Comment("单据编号")]
        [Required]
        [MaxLength(255)]
        public string NO { get; set; }

        /// <summary>
        /// 退料申请日期    
        /// </summary>
        [Comment("退料申请日期")]
        public DateTimeOffset? Billdate { get; set; }

        /// <summary>
        /// 领用项目号
        /// </summary>
        [Comment("领用项目号")]
        [MaxLength(255)]
        public string WBSElementcode { get; set; }

        /// <summary>
        /// 领料部门    
        /// </summary>
        [Comment("领料部门")]
        [MaxLength(255)]
        public string BenefitingDepartcode { get; set; }

        /// <summary>
        /// 收益部门    
        /// </summary>
        [Comment("收益部门")]
        [MaxLength(255)]
        public string CostCenterID { get; set; }

        /// <summary>
        /// 客户
        /// </summary>
        [Comment("客户")]
        [MaxLength(255)]
        public string FI_Client_Analysis_H { get; set; }

        /// <summary>
        /// 领取人
        /// </summary>
        [Comment("领取人")]
        [MaxLength(255)]
        public string PickerID { get; set; }

        /// <summary>
        /// 公司
        /// </summary>
        [Comment("公司")]
        [MaxLength(255)]
        public string Companyname { get; set; }

        /// <summary>
        /// 施工队
        /// </summary>
        [Comment("施工队")]
        [MaxLength(255)]
        public string ConstructionTeamID { get; set; }  

        /// <summary>
        /// 单据状态
        /// </summary>
        [Comment("单据状态")]
        [Required]
        public OrderStatusEnum OrderStatus { get; set; } = OrderStatusEnum.WEIXIAFA;

        /// <summary>
        /// 单据明细
        /// </summary>
        public ICollection<WmsStockReturnOrderDetails> WmsStockReturnOrderDetails { get; set; }

        /// <summary>
        /// 1对多配置关系
        /// </summary>
        /// <param name="entityBuilder"></param>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        public void Configure(EntityTypeBuilder<WmsStockReturnOrder> entityBuilder, DbContext dbContext, Type dbContextLocator)
        {
            // 一对多配置
            entityBuilder.HasMany(x => x.WmsStockReturnOrderDetails)
                .WithOne(x => x.WmsStockReturnOrder)
                .HasForeignKey(x => x.OrderId);
        }
    }
}
