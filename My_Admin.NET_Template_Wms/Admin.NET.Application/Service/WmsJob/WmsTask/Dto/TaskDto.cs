﻿using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 出入库任务管理（熟化库）输出参数
    /// </summary>
    public class TaskDto
    {
        /// <summary>
        /// 任务号
        /// </summary>
        public string TaskNo { get; set; }
        
        /// <summary>
        /// 任务方式
        /// </summary>
        public TaskModel TaskModel { get; set; }
        
        /// <summary>
        /// 任务类型
        /// </summary>
        public TaskType TaskType { get; set; }
        
        /// <summary>
        /// 任务级别
        /// </summary>
        public int TaskLevel { get; set; }
        
        /// <summary>
        /// 任务状态
        /// </summary>
        public TaskStatusEnum TaskStatus { get; set; }
        
        /// <summary>
        /// WCS是否可以读取
        /// </summary>
        public bool IsRead { get; set; }
        
        /// <summary>
        /// 托盘编号
        /// </summary>
        public string ContainerCode { get; set; }
        
        /// <summary>
        /// 起始库位
        /// </summary>
        public string SourcePlace { get; set; }
        
        /// <summary>
        /// 目标库位
        /// </summary>
        public string ToPlace { get; set; }
        
        /// <summary>
        /// 巷道
        /// </summary>
        public int Aisle { get; set; }
        
        /// <summary>
        /// 库区名称
        /// </summary>
        public string AreaName { get; set; }
        
        /// <summary>
        /// 单据明细Id
        /// </summary>
        public long OrderDetailsId { get; set; }
        
        /// <summary>
        /// 组盘记录单据号
        /// </summary>
        public string OrderNo { get; set; }
        
        /// <summary>
        /// 发送次数
        /// </summary>
        public int SendTimes { get; set; }
        
        /// <summary>
        /// 任务执行堆垛机Id
        /// </summary>
        public int DodeviceId { get; set; }
        
        /// <summary>
        /// 设备执行节点Id
        /// </summary>
        public string DodevicenodeId { get; set; }
        
        /// <summary>
        /// 设备类型
        /// </summary>
        public DodeviceType Dodevicetype { get; set; }
        
        /// <summary>
        /// 任务设备状态
        /// </summary>
        public TaskDodeviceStatusEnum TaskDodeviceStatus { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public IssueState IssueState { get; set; }
    }
}
