﻿import { axios } from '@/utils/request'

/**
 * 查询Agv报警
 *
 * @author yc
 */
export function AgvWaringPage (parameter) {
  return axios({
    url: '/AgvWaring/page',
    method: 'get',
    params: parameter
  })
}

/**
 * Agv报警列表
 *
 * @author yc
 */
export function AgvWaringList (parameter) {
  return axios({
    url: '/AgvWaring/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加Agv报警
 *
 * @author yc
 */
export function AgvWaringAdd (parameter) {
  return axios({
    url: '/AgvWaring/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑Agv报警
 *
 * @author yc
 */
export function AgvWaringEdit (parameter) {
  return axios({
    url: '/AgvWaring/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除Agv报警
 *
 * @author yc
 */
export function AgvWaringDelete (parameter) {
  return axios({
    url: '/AgvWaring/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出Agv报警的Excel文件
 *
 * @author yc
 */
export function AgvWaringToExcel (parameter) {
  return axios({
    url: '/AgvWaring/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入Agv报警的Excel文件
 *
 * @author yc
 */
export function AgvWaringFromExcel (data, parameter) {
  return axios({
    url: '/AgvWaring/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载Agv报警的Excel导入模板
 *
 * @author yc
 */
export function AgvWaringDownloadExcelTemplate(parameter) {
  return axios({
    url: '/AgvWaring/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}


/**
 * Agv复位
 *
 * @author yhh
 */
export function AgvWaringReset (id) {
  return axios({
    url: '/AgvWaring/PcResetAgvFault',
    method: 'post',
    data: {id}
  })
}



