﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 库口类型
    /// </summary>
    public enum LesEntranceType
    {
        /// <summary>
        /// 入库口
        /// </summary>
        [Description("入库口")]
        RUKU = 1,

        /// <summary>
        /// 出库口
        /// </summary>
        [Description("出库口")]
        CHUKU = 2
    }
}
