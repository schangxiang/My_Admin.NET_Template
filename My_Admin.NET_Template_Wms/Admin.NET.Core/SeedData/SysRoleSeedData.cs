﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;

namespace Admin.NET.Core
{
    /// <summary>
    /// 系统角色表种子数据
    /// </summary>
    public class SysRoleSeedData : IEntitySeedData<SysRole>
    {
        /// <summary>
        /// 种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<SysRole> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new[]
            {
                new SysRole{Id=142307070910554, Name="系统管理员", Code="sys_manager_role", Sort=100, DataScopeType=DataScopeType.ALL, Remark="系统管理员", Status=0 },
                //new SysRole{Id=142307070910555, Name="普通用户", Code="common_role", Sort=101, DataScopeType=DataScopeType.DEFINE, Remark="普通用户", Status=0 },
                //new SysRole{Id=142307070910556, Name="系统管理员", Code="sys_manager_role", Sort=100, DataScopeType=DataScopeType.DEFINE, Remark="系统管理员", Status=0 },
                //new SysRole{Id=142307070910557, Name="普通用户", Code="common_role", Sort=101, DataScopeType=DataScopeType.DEFINE, Remark="普通用户", Status=0 },

                new SysRole{Id=356094617575493, Name="管理员", Code="admin", Sort=100, DataScopeType=DataScopeType.ALL, Remark="", Status=0 },
                new SysRole{Id=358509609758789, Name="仓库用户", Code="ware", Sort=100, DataScopeType=DataScopeType.ALL, Remark="", Status=0 },
                new SysRole{Id=358536344862789, Name="研发用户", Code="iWare", Sort=100, DataScopeType=DataScopeType.DEFINE, Remark="", Status=0 },
            };
        }
    }
}