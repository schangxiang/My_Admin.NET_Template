﻿#nullable enable

using System.Text;
using System.Text.RegularExpressions;

namespace Admin.NET.Core
{
    public static class StringUtil
    {
        /// <summary>
        /// 获取下划线表达式
        /// </summary>
        /// <param name="camel"></param>
        /// <returns></returns>
        public static string ToUnderLine(this string camel)
        {
            return Regex.Replace(camel, "([A-Z])", "_$1").ToLower().TrimStart('_');           
        }


        /// <summary>
        /// 获取下划线表达式
        /// </summary>
        /// <param name="camel"></param>
        /// <returns></returns>
        public static string ToUnderLine(this string camel, char trimChar)
        {
            return Regex.Replace(camel, "([A-Z])", trimChar + "$1").ToLower().TrimStart(trimChar);
        }


        /// <summary>
        /// 获取驼峰表达式
        /// </summary>
        /// <param name="camel"></param>
        /// <returns></returns>
        public static string ToCamel(this string str)
        {
            if(string.IsNullOrEmpty(str)) return "";
            return str[..1].ToLower() + str[1..];
        }


        /// <summary>
        /// 将类型进行转换
        /// </summary>
        /// <param name="dataType"></param>
        /// <returns></returns>
        public static string ParseTrueType(string dataType) 
        {
            if(!dataType.StartsWith("System.Nullable")) return dataType;

            return new Regex(@"(?i)(?<=\[)(.*)(?=\])").Match(dataType).Value;    
        }

        /// <summary>
        /// 将类型进行转换
        /// </summary>
        /// <param name="dataType"></param>
        /// <returns></returns>
        public static string ParseTrueType2(string dataType)
        {
            if (!dataType.StartsWith("System.Nullable")) return dataType;

            var str = new Regex(@"(?i)(?<=\[\[)(.*)(?=\]\])").Match(dataType).Value;

            var args = str.Split(',');

            return args.Length > 0 ? args[0] : "";
        }

        /// <summary>
        /// 分割字符
        /// </summary>
        /// <returns></returns>
        public static string SplitChar() 
        {
            return new ASCIIEncoding().GetString(new byte[] { 3 });
        }


    }
}
