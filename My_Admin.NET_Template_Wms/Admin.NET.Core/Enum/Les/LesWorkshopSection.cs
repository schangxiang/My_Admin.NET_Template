﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 工段
    /// </summary>
    public enum LesWorkshopSection
    {
        /// <summary>
        /// 切割
        /// </summary>
        [Description("切割")]
        QIEGE = 1,

        /// <summary>
        /// 压合
        /// </summary>
        [Description("压合")]
        YAHE = 2,

        /// <summary>
        /// CNC
        /// </summary>
        [Description("CNC")]
        CNC = 3
    }
}
