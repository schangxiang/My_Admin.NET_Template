import { axios } from '@/utils/request'

/**
 * 胶合板备料单查询
 *
 * @author yhh
 */
export function PlywoodPreparationPage (parameter) {
  return axios({
    url: '/plywoodpreparation/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 胶合板备料单据详情
 *
 * @author yhh
 */
export function PlywoodPreparationDetailPage (params) {
  return axios({
    url: '/plywoodpreparation/detailPage',
    method: 'get',
    params:params
  })
}


/**
 * 胶合板备料计划下发
 *
 * @author yhh
 */
export function Distribute (ids) {
  return axios({
    url: '/plywoodpreparation/BatchPreparationMaterials',
    method: 'post',
    data:{id:ids}
  })
}