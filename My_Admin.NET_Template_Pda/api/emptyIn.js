import request from '@/api/api.js'
import qs from 'qs'


// 获取托盘信息 /api/wmsinwarehouse/GetContainer
export function GetContainer (data) {
  return request({
    url: `/api/wmsinwarehouse/GetContainer?${qs.stringify(data)}`,
    method: 'get'
  })
}

// 获取单据信息 /api/wmsinwarehouse/PadGetMaterial
export function GetMaterialPad (data) {
  return request({
    url: `/api/wmsinwarehouse/PadGetMaterial?${qs.stringify(data)}`,
    method: 'get'
  })
}

// 组盘 /api/wmsinwarehouse/GroupDisk
export function GroupDisk (data) {
  return request({
    url: `/api/wmsinwarehouse/GroupDisk`,
    method: 'post',
	data
  })
}

// 入库 /api/wmsinwarehouse/PdaInWarehouse
export function PdaInWarehouse (data) {
  return request({
    url: `/api/wmsinwarehouse/PdaInWarehouse`,
    method: 'post',
	data
  })
}


