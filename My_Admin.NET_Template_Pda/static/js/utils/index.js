import colorModule from './modules/color.js'
import regModule from './modules/regexValidate.js'

export let color = colorModule;
export let regexValidate = regModule;
/* 生成随机uuid
*	n：uuid的长度
* type：类型（0-大小写字母+数字，1-小写字母+数字，2-纯数字，3-大小写字母，4-小写字母）
*/
export function uuid(n=6,type=0){
	let res='', uType=0;
	const __uuidLetter = function(){
		const __tempArr = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
		return __tempArr[Math.floor(Math.random()*26)];
	}
	for (let i=0;i<n;i++) {
		switch(type) {
			case 1:
				uType = Math.floor(Math.random()*2);
				if (uType===0) {
					res += Math.floor(Math.random()*10).toString()
				} else {
					res += __uuidLetter()
				}
				break;
			case 2:
				res += Math.floor(Math.random()*10).toString()
				break;
			case 3:
				uType = Math.floor(Math.random()*2);
				if (uType===0) {
					res += __uuidLetter()
				} else {
					res += __uuidLetter().toUpperCase()
				}
				break;
			case 4:
				res += __uuidLetter()
				break;
			default:
				uType = Math.floor(Math.random()*3);
				if (uType===0) {
					res += Math.floor(Math.random()*10).toString()
				} else if (uType===1) {
					res += __uuidLetter()
				} else {
					res += __uuidLetter().toUpperCase()
				}
				break;
		}
	}
	return res;
}
export function $alert(msg,title="系统提示",callback){
	uni.showModal({
		title:title,
		content:msg,
		showCancel:false,
		confirmText:'确定',
		success:function(res){
			if (res.confirm) {
				callback && callback()
			}
		}
	})
}
export function $successInfo(info){
	uni.showToast({
		title:info,
		mask:true,
		icon:'success'
	})
};

/* 
  获取字典列表
		code:字典集合码
	返回:字典列表
 */
export function getDicList(store,code) {
	let res = [];
	let dicDataSource = store.getters['system/getEnum'];
	if (dicDataSource) {
		for (let i=0;i<dicDataSource.length;i++) {
			if (dicDataSource[i].code===code) {
				res = dicDataSource[i].children || [];
				break;
			}
		}
	} 
	return res;
}

/* 
  解析字典值
		code:字典集合码
		value:字典值
	返回:字典值对应名称
 */
export function parseDic(store,code,value){
	let res = '';
	let dicDataSource = store.getters['system/getEnum'];
	if (dicDataSource) {
		let _arr = [];
		for (let i=0;i<dicDataSource.length;i++) {
			if (dicDataSource[i].code===code) {
				_arr = dicDataSource[i].children || [];
				break;
			}
		}
		if (_arr.length>0) {
			for (let j=0;j<_arr.length;j++) {
				if (_arr[j].code==value) {
					res = _arr[j].name;
					break;
				}
			}
			if (!res) res = '无此字典'
		} else {
			res = '无此字典'
		}
	} else {
		res = 'Failed'
	}
	return res;
};

/* 
  下载更新
 */
export function downloadApk($config){
	try{
		const _downFun = function(){
			try{
				plus.runtime.openURL($config.downUrl, function(res) {console.log(res);});  
			}catch(ex){
				console.log('_downFun Error!')
				console.log(ex)
			}
			setTimeout(()=>{
				uni.hideLoading();
			},10000)
		}
		
		uni.showLoading({
			title: '开启下载...',
			mask:true
		});
		if (plus.networkinfo.getCurrentType() != 3){
			uni.hideLoading();
			uni.showModal({
				title:'系统提示',
				content:'当前非WIFI环境，确认继续下载吗？',
				success:(res)=>{
					if (res.confirm) {
						uni.showLoading({
							title: '开启下载...',
							mask:true
						});
						_downFun()
					}
				}
			})
		} else {
			_downFun()
		}
	} catch(e){
		uni.hideLoading();
		uni.showModal({
			title:'系统提示',
			content:'下载失败',
			showCancel:false
		})
		console.log('DownloadApk Error!')
		console.log(e)
	}
};

export function getObjectType(obj){
	if (typeof obj !== 'object') {
		return null;
	} else {
		let objTypeStr = Object.prototype.toString.call(obj).toLowerCase().trim();
		objTypeStr = objTypeStr.substr(1,objTypeStr.length-2)
		let tempA = objTypeStr.split(" ");
		return tempA[1];
	}	
}

export function getUserDefaultArea($store,arr,valueField='code'){
  let res = ''
  const _default = $store.getters['user/getUserInfo']['workShopType']
  for (let i=0;i<arr.length;i++){
    if (String(arr[i][valueField])===Sring(_default)) {
      res = _default
      break;
    }
  }
  return res
}

export function getUserDefaultLine($store,arr,valueField='productionlineId'){
  let res = ''
  const _default = $store.getters['user/getUserInfo']['productionlineId']
  for (let i=0;i<arr.length;i++){
    if (arr[i][valueField]===_default) {
      res = _default
      break;
    }
  }
	return res
}