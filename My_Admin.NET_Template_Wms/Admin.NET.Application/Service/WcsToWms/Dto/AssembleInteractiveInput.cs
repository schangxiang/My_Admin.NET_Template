﻿using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 创建主任务、AGV任务输入参数
    /// </summary>
    public class AssembleInteractiveInput
    {
        /// <summary>
        /// 下线口
        /// </summary>
        public BlankingProductionLineEnum BlankingProductionLine { get; set; }
        /// <summary>
        /// 
        /// </summary>
        //public List<DestinationsInput> destinations { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class AssembleDestinationsInput
    {
        /// <summary>
        /// 位置（起点）/ 位置（终点）
        /// </summary>
        /// <example>001H01B0101</example>
        public string locationName { get; set; }

        /// <summary>
        /// 取货动作：层数 / 放货动作：层数  2
        /// </summary>
        /// <example>Load cargo:00</example>
        public string operation { get; set; }
    }

    /// <summary>
    /// 更新任务输入参数
    /// </summary>
    public class UpdateAssembleTaskInput
    {
        /// <summary>
        /// 任务号
        /// </summary>
        public string TaskNo { get; set; }

        /// <summary>
        /// 任务状态
        /// </summary>
        public TaskStatusEnum TaskStatus { get; set; }

        /// <summary>
        /// 设备任务状态
        /// </summary>
        public TaskAssembleDodeviceStatusEnum TaskAssembleDodeviceStatus { get; set; }

        /// <summary>
        /// 巷道
        /// </summary>
        public int Aisle { get; set; }
    }


    /// <summary>
    /// 强制完成
    /// </summary>
    public class FinishAssembleTaskInput
    {
        /// <summary>
        /// 任务号
        /// </summary>
        public string TaskNo { get; set; }
    }
    /// <summary>
    ///更新agv报错信息
    /// </summary>
    public class UpdateAgvWaring
    {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }
    }
    
}
