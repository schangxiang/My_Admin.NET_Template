﻿using Admin.NET.Core;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Admin.NET.Application
{
    public interface IWmsDumpOrderService
    {
        Task Add(AddWmsDumpOrderInput input);
        Task Delete(DeleteWmsDumpOrderInput input);
        Task<WmsDumpOrderOutput> Get([FromQuery] QueryeWmsDumpOrderInput input);
        Task<List<WmsDumpOrderOutput>> List([FromQuery] WmsDumpOrderInput input);
        Task<PageResult<WmsDumpOrderOutput>> Page([FromQuery] WmsDumpOrderSearch input);
        Task Update(UpdateWmsDumpOrderInput input);

        Task<List<WmsDumpOrderOutput>> ListNonPageAsync([FromQuery] WmsDumpOrderSearchNonPage input);

        Task<int> FromExcelAsync(IFormFile file, [FromQuery] ImportExcelType importExcelType);

        Task<IActionResult> DownloadExcelTemplate(string version);
    }
}