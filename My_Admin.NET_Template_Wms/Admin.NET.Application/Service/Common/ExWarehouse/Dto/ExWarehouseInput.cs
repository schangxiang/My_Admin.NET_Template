﻿using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    ///  物料出库查询参数
    /// </summary>
    public class ExWarehouseSearch: PageInputBase
    {
        /// <summary>
        /// 物料编号
        /// </summary>
        public string MaterialNo { get; set; }
        /// <summary>
        /// 物料名称
        /// </summary>
        public string  MaterialName { get; set; }
    }

    /// <summary>
    /// 出库输入参数
    /// </summary>
    public class ExWarehouseInput
    {
        /// <summary>
        /// 托盘号
        /// </summary>
        public virtual string ContainerCode { get; set; }
        /// <summary>
        /// 出库位置
        /// </summary>
        public string ToPlace { get; set; }
        /// <summary>
        /// 库位编号
        /// </summary>
        public string PlaceCode { get; set; }
    }

    /// <summary>
    /// 呼叫空托输入参数
    /// </summary>
    public class CallAirflareInput
    {
        /// <summary>
        /// 呼叫空托数量
        /// </summary>
        public int Quantity { get; set; }
    }
}
