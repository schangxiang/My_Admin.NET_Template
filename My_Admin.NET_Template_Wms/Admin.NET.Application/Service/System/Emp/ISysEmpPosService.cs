﻿namespace Admin.NET.Application
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISysEmpPosService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="empId"></param>
        /// <param name="posIdList"></param>
        /// <returns></returns>
        Task AddOrUpdate(long empId, List<long> posIdList);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        Task DeleteEmpPosInfoByUserId(long empId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="empId"></param>
        /// <returns></returns>
        Task<List<EmpPosOutput>> GetEmpPosList(long empId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="posId"></param>
        /// <returns></returns>
        Task<bool> HasPosEmp(long posId);
    }
}