﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    [Table("base_SerialSN")]
    [Comment("单据号流水号履历表")]
    public class BaseSerialSN : DEntityBase
    {
        /// <summary>
        /// 流水号时间
        /// </summary>
        [Comment("流水号时间")]
        [Required]
        public DateTime CurrentDate { get; set; }

        /// <summary>
        /// 流水号
        /// </summary>
        [Comment("流水号")]
        [Required]
        public int Sn { get; set; }

        /// <summary>
        /// 单据号类型
        /// </summary>
        [Comment("单据号类型")]
        [Required]
        public int SerialType { get; set; }
    }
}
