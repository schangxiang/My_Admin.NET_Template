﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    /// <summary>
    /// 退货单
    /// </summary>
    [Table("wms_goodsreturnorder")]
    [Comment("退货单")]
    public class WmsGoodsReturnOrder : DEntityBase, IEntityTypeBuilder<WmsGoodsReturnOrder>
    {
        /// <summary>
        ///  Billdate
        /// </summary>
        [Comment("单据大类")]
        [Required]
        public long OrderLargeCategory { get; set; }

        /// <summary>
        /// 单据小类
        /// </summary>
        [Comment("单据小类")]
        [Required]
        public long OrderSubclass { get; set; }

        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        [Comment("上位系统单据唯一识别码")]
        public long SOID { get; set; }

        /// <summary>
        /// 单据编号  
        /// </summary>
        [Comment("单据编号")]
        [Required]
        [MaxLength(255)]
        public string NO { get; set; }

        /// <summary>
        /// 创建日期    
        /// </summary>
        [Comment("创建日期")]
        public DateTimeOffset? Billdate { get; set; }

        /// <summary>
        /// 供应商编码    
        /// </summary>
        [Comment("供应商编码")]
        [MaxLength(255)]
        public string Vendorcode { get; set; }

        /// <summary>
        /// 供应商名称    
        /// </summary>
        [Comment("供应商名称")]
        [MaxLength(255)]
        public string Vendorname { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        [Comment("公司名称")]
        [MaxLength(255)]
        public string Companyname { get; set; }

        /// <summary>
        /// 仓管员
        /// </summary>
        [Comment("仓管员")]
        [MaxLength(255)]
        public string WarehouseKeepername { get; set; }

        /// <summary>
        /// 仓储中心
        /// </summary>
        [Comment("仓储中心")]
        [MaxLength(255)]
        public string WarehouseCentername { get; set; }

        /// <summary>
        /// 退货状态
        /// </summary>
        [Comment("退货状态")]
        [Required]
        public GoodsReturnStatus GoodsReturnStatus { get; set; } = GoodsReturnStatus.WEITUIHUO;

        /// <summary>
        /// 单据明细
        /// </summary>
        public ICollection<WmsGoodsReturnOrderDetails> WmsGoodsReturnOrderDetails { get; set; }

        /// <summary>
        /// 1对多配置关系
        /// </summary>
        /// <param name="entityBuilder"></param>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        public void Configure(EntityTypeBuilder<WmsGoodsReturnOrder> entityBuilder, DbContext dbContext, Type dbContextLocator)
        {
            // 一对多配置
            entityBuilder.HasMany(x => x.WmsGoodsReturnOrderDetails)
                .WithOne(x => x.WmsGoodsReturnOrder)
                .HasForeignKey(x => x.OrderId);
        }
    }
}
