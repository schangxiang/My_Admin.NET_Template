﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core.Entity.WmsBase
{
    /// <summary>
    /// 角色菜单表
    /// </summary>
    [Table("wms_role_pdamenu")]
    [Comment("角色菜单表")]
    public class WmsRolePdaMenu : IEntity
    {
        /// <summary>
        /// 角色Id
        /// </summary>
        [Comment("角色Id")]
        public long SysRoleId { get; set; }

        /// <summary>
        /// 一对一引用（系统用户）
        /// </summary>
        public SysRole SysRole { get; set; }

        /// <summary>
        /// 菜单Id
        /// </summary>
        [Comment("菜单Id")]
        public long PdaMenuId { get; set; }

        /// <summary>
        /// 一对一引用（系统菜单）
        /// </summary>
        public WmsPdaPower WmsPdaPower { get; set; }
    }
}