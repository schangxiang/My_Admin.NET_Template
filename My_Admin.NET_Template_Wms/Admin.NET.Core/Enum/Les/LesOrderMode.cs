﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 单据模式
    /// </summary>
   
    public enum LesOrderMode
    {
        /// <summary>
        /// 根据物料
        /// </summary>
        [Description("根据物料")]
        GENJUWULIAO = 1,

        /// <summary>
        /// 根据库位
        /// </summary>
        [Description("根据库位")]
        GENJUKUWEI = 2,
    }
}
