﻿import { axios } from '@/utils/request'

/**
 * 查询库存表
 *
 * @author XJF
 */
export function FoamingMaterialStockPage (parameter) {
  return axios({
    url: '/FoamingMaterialStock/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 库存表列表
 *
 * @author XJF
 */
export function FoamingMaterialStockList (parameter) {
  return axios({
    url: '/FoamingMaterialStock/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加库存表
 *
 * @author XJF
 */
export function FoamingMaterialStockAdd (parameter) {
  return axios({
    url: '/FoamingMaterialStock/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑库存表
 *
 * @author XJF
 */
export function FoamingMaterialStockEdit (parameter) {
  return axios({
    url: '/FoamingMaterialStock/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除库存表
 *
 * @author XJF
 */
export function FoamingMaterialStockDelete (parameter) {
  return axios({
    url: '/FoamingMaterialStock/delete',
    method: 'post',
    data: parameter
  })
}

/**
* 获取WmsArea列表
* @author XJF
*/
export function FoamingMaterialStockFkWmsAreaList() {
  return axios({
    url: '/FoamingMaterialStock/fkWmsArea',
    method: 'get'
  })
}
