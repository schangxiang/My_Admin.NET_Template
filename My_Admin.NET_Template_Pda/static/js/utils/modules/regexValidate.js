export default {
	/* 大陆手机号码 */
	mobile(val){
		return /^[1][3,4,5,6,7,8,9][0-9]{9}$/.test(val)
	},
	/* 大陆固定电话 */
	telephone(val){
		return /^(([0\+]\d{2,3}-)?(0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/.test(val)
	},
	/* 电子邮箱 */
	email(val){
		return /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(val)
	},
	/* 正整数 */
	positiveInteger(val){
		let tp = typeof val;
		if (tp==='number') {
			if (parseInt(val)!==val) {
				return false
			} else if (val<=0) {
				return false
			} else {
				return true
			}
		} else if (tp==='string') {
			if (!val) {
				return false;
			} else {
				return /^0*[1-9]\d*$/.test(val)
			}
		} else {
			return false;
		}
	},
	/* 自然数,包括0和正整数 */
	naturlNumber(val){
		let tp = typeof val;
		if (tp==='number') {
			if (parseInt(val)!==val) {
				return false
			} else if (val<0) {
				return false
			} else {
				return true
			}
		} else if (tp==='string') {
			if (!val) {
				return false;
			} else {
				return /^([1-9]\d*|[0]{1,1})$/.test(val)
			}
		} else {
			return false;
		}
	},
	/* 匹配输入，以字母开头，只能树字母和数字 */
	inputA(val){
		return /^[a-zA-Z]{1}[a-zA-Z0-9]*$/.test(val)
	},
	/* 匹配输入，以字母开头，只能树字母、数字、连字符、下划线*/
	inputB(val){
		return /^[a-zA-Z]{1}[a-zA-Z0-9\-_]*$/.test(val)
	}
}