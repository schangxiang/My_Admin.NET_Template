﻿import { axios } from '@/utils/request'

/**
 * 查询物料信息
 *
 * @author XJF
 */
export function WmsMaterialPage (parameter) {
  return axios({
    url: '/WmsMaterial/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 物料信息列表
 *
 * @author XJF
 */
export function WmsMaterialList (parameter) {
  return axios({
    url: '/WmsMaterial/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加物料信息
 *
 * @author XJF
 */
export function WmsMaterialAdd (parameter) {
  return axios({
    url: '/WmsMaterial/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑物料信息
 *
 * @author XJF
 */
export function WmsMaterialEdit (parameter) {
  return axios({
    url: '/WmsMaterial/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除物料信息
 *
 * @author XJF
 */
export function WmsMaterialDelete (parameter) {
  return axios({
    url: '/WmsMaterial/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 获取库区下拉
 *
 * @author yhh
 */
export function GetAreas (parameter) {
  return axios({
    url: '/WmsMaterial/GetAreaList',
    method: 'get'
  })
}

/**
 * 获取工位下拉
 *
 * @author yhh
 */
export function GetStations (parameter) {
  return axios({
    url: '/WmsMaterial/GetStationList',
    method: 'get'
  })
}

/**
 * 同步Desk数据
 *
 * @author dln
 */
export function Getdeskdata () {
  return axios({
    url: '/deskdatasynchronous/GetMomMaterial',
    method: 'post'
  })
}
