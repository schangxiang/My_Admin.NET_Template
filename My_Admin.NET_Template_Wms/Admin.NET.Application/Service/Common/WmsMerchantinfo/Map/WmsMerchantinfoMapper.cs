﻿using Mapster;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    public class WmsMerchantinfoMapper : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<AddWmsMerchantinfoInput, WmsMerchantinfo>()
            ;
            config.ForType<UpdateWmsMerchantinfoInput, WmsMerchantinfo>()
            ;
            config.ForType<WmsMerchantinfo, WmsMerchantinfoOutput>()
            ;
        }
    }
}
