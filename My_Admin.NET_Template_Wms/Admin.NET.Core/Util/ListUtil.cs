﻿using Furion.FriendlyException;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Reflection;

namespace Admin.NET.Core
{
    /// <summary>
    /// 列表扩展
    /// </summary>
    public static class ListUtil
    {
        /// <summary>
        /// 将一个数字拆分成若干个指定大小的数组
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="maxThreadCount"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static List<T>[] SplitList<T>(this List<T> list, int size ) 
        {
            var count = (int)Math.Ceiling((decimal)list.Count / size);
            if(count <= 0) return Array.Empty<List < T >> ();
            List<T>[] listArray = new List<T>[count];
            for (var i = 0; i < count; i++)
            {
                listArray[i] = list.GetRange(i * size, Math.Min(size, list.Count - i * size));
            }
            return listArray;
        }

        /// <summary>
        /// 展示列表数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public static string Show<T>(this List<T> list) 
        {
            string info = string.Empty;
            list.ForEach(item => info += item + ",");
            return info.EndsWith(",") ? info[..^1] : info;
        }
        




        

    }
}