﻿using Mapster;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    public class WmsTakeMaterialOrderMapper : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<AddWmsTakeMaterialOrderInput, WmsTakeMaterialOrder>()
            ;
            config.ForType<UpdateWmsTakeMaterialOrderInput, WmsTakeMaterialOrder>()
            ;
            config.ForType<WmsTakeMaterialOrder, WmsTakeMaterialOrderOutput>()
            ;
        }
    }
}
