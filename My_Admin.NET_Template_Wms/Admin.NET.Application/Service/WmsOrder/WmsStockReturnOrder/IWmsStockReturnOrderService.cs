﻿using Admin.NET.Core;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Admin.NET.Application
{
    public interface IWmsStockReturnOrderService
    {
        Task Add();
        Task Delete(DeleteWmsStockReturnOrderInput input);
        Task<WmsStockReturnOrderOutput> Get([FromQuery] QueryeWmsStockReturnOrderInput input);
        Task<List<WmsStockReturnOrderOutput>> List([FromQuery] WmsStockReturnOrderInput input);
        Task<PageResult<WmsStockReturnOrderOutput>> Page([FromQuery] WmsStockReturnOrderSearch input);
        Task Update(UpdateWmsStockReturnOrderInput input);

        Task<List<WmsStockReturnOrderOutput>> ListNonPageAsync([FromQuery] WmsStockReturnOrderSearchNonPage input);

        Task<int> FromExcelAsync(IFormFile file, [FromQuery] ImportExcelType importExcelType);

        Task<IActionResult> DownloadExcelTemplate(string version);
    }
}