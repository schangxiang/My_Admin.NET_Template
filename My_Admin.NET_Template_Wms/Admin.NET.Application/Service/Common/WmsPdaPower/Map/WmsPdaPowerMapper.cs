﻿using Mapster;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    public class WmsPdaPowerMapper : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<AddWmsPdaPowerInput, WmsPdaPower>()
            ;
            config.ForType<UpdateWmsPdaPowerInput, WmsPdaPower>()
            ;
            config.ForType<WmsPdaPower, WmsPdaPowerOutput>()
            ;
        }
    }
}
