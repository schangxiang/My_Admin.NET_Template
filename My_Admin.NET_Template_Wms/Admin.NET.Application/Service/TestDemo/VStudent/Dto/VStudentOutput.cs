﻿using Admin.NET.Core.Util.LowCode.Dto;
using System;

namespace Admin.NET.Application
{
    /// <summary>
    /// 学生视图输出参数
    /// </summary>
    public class VStudentOutput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        public long? Id { get; set; }
        
        /// <summary>
        /// 学生姓名
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// 学生年龄
        /// </summary>
        public int? Age { get; set; }
        
        /// <summary>
        /// 是否在校
        /// </summary>
        public bool? StartName { get; set; }
        
        /// <summary>
        /// 性别
        /// </summary>
        public int? Gender { get; set; }
        
        /// <summary>
        /// 出生日期
        /// </summary>
        public DateTime? BrithDate { get; set; }
        
        /// <summary>
        /// 关联老师
        /// </summary>
        public long? TeacherId { get; set; }
        
        /// <summary>
        /// 老师名称
        /// </summary>
        public string teacherName { get; set; }
        
    }
}
