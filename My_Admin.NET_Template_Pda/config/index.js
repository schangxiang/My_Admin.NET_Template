import ajax from './modules/ajax.js'
import pagination from './modules/pagination.js'

export default {
	ajax,
	pagination,
	downUrl:'http://localhost:81/1.0.0.apk',
	path: {
		home:'/pages/home/index',
		login:'/pages/login/index',
		bluetooth:'/pages/print/bluetoothConnection',
		host:'/pages/setting/host',
		online:'/pages/online/index',
		receive:'/pages/receiveIn/index',
		temporaryIn:'/pages/temporaryInstore/index',
		manualOut:'/pages/manual/out',
		unbind:'/pages/unbind/index',
		tasks:'/pages/tasks/index',
		notices:'/pages/notices/index',
		fapaoIn:'/pages/fapaoInStore/index',
		fapaoOut:'/pages/fapiaoOutStore/index',
		bind:'/pages/bind/index',
		slicingOn:'/pages/slicingOn/index',
		slicingOff:'/pages/slicingOff/index',
		plywoodBind:'/pages/plywoodBind/index',
		plywoodInstore:'/pages/plywoodInstore/index',
		plywoodOut:'/pages/plywoodOut/index',
		emptyOut:'/pages/emptyOut/index',
		rsbIn:'/pages/rsbIn/index',
		emptyIn:'/pages/emptyIn/index',
		materialback:'/pages/materialback/index', // 物料退库
		rsbOut:'/pages/rsbOut/index',
		sandingIn:'/pages/sandingInstore/index',
		endProductIn:'/pages/endProductInstore/index',
		innerTailBack:'/pages/innerTailBack/index',
		unstackingIn:'/pages/unstackingInstore/index',
		transport:'/pages/transport/index',
		changeStore:'/pages/changeStore/index',
		takeInStore:'/pages/takeInStore/index',
		checkStock:'/pages/checkStock/index',
		receiptExecution:'/pages/receiptExecution/index',
		sortingSearch:'/pages/sortingSearch/index'
	}
}
