import Vue from 'vue'
import axios from 'axios'
import store from '@/store'
// import router from './router'
import { message, Modal, notification } from 'ant-design-vue' /// es/notification
import { VueAxios } from './axios'
import { ACCESS_TOKEN } from '@/store/mutation-types'

// 创建 axios 实例
const service = axios.create({
 // baseURL: process.env.VUE_APP_API_BASE_URL + '/api', // api base_url
  baseURL: window._CONFIG.baseUrl+'/api', 
  timeout: 6000 // 请求超时时间
})

const err = error => {
  if (error.response) {
    const data = error.response.data
    const token = Vue.ls.get(ACCESS_TOKEN)

    if (error.response.status === 403) {
      notification.error({
        message: 'Forbidden',
        description: data.message
      })
    }
    if (error.response.status === 500) {
      if (data.message && data.message.length > 0) {
        message.error(data.message)
      }
    }
    if (error.response.status === 401 && !(data.result && data.result.isLogin)) {
      notification.error({
        message: 'Unauthorized',
        description: 'Authorization verification failed'
      })
      if (token) {
        store.dispatch('Logout').then(() => {
          setTimeout(() => {
            window.location.reload()
          }, 1500)
        })
      }
    }
  }
  return Promise.reject(error)
}

// request interceptor
service.interceptors.request.use(config => {
  const token = Vue.ls.get(ACCESS_TOKEN)
  const refreshToken = Vue.ls.get('X-Access-Token')
  if (token) {
    config.headers['Authorization'] = 'Bearer ' + token
  }
  if (refreshToken) {
    config.headers['X-Authorization'] = 'Bearer ' + refreshToken
  }

   ///-----------------------这里开始
   const params = config.params || {};
    // get参数编码
    if (config.method?.toUpperCase() === "GET" && params) {
      let url = config.url + "?";
      for (const propName of Object.keys(params)) {
        const value = params[propName];
        if (
          value !== void 0 &&
          value !== null &&
          typeof value !== "undefined"
        ) {
          if (typeof value === "object") {
            for (const val of Object.keys(value)) {
              const params = propName + "[" + val + "]";
              const subPart = encodeURIComponent(params) + "=";
              url += subPart + encodeURIComponent(value[val]) + "&";
            }
          } else {
            url += `${propName}=${encodeURIComponent(value)}&`;
          }
        }
      }
      // 给 get 请求加上时间戳参数，避免从缓存中拿数据
      // const now = new Date().getTime()
      // params = params.substring(0, url.length - 1) + `?_t=${now}`
      url = url.slice(0, -1);
      config.params = {};
      config.url = url;
    }

  return config
}, err)


const _requestErrAlert = function(msg,callback,oktext='确认') {
  Modal.error({
    title: '系统提示：',
    content: msg,
    keyboard: false,
    okText: oktext,
    onOk: () => {
      callback && callback()
    }
  })
}

const requerstErrMsg = function(msg){
  
}

/**
 * response interceptor
 * 所有请求统一返回
 */
service.interceptors.response.use(response => {
  // LocalStorage 存储的 token 和 refreshToken，不设定过期时间，由服务端统一处理
  if (response.headers['access-token'] && response.headers['access-token'] !== 'invalid_token') {
    Vue.ls.set(ACCESS_TOKEN, response.headers['access-token'] /*, 7 * 24 * 60 * 60 * 1000 */)
    store.commit('SET_TOKEN', response.headers['access-token'])
  }
  if (response.headers['x-access-token']) {
    Vue.ls.set('X-Access-Token', response.headers['x-access-token'] /*, 7 * 24 * 60 * 60 * 1000 */)
  }
  if (response.request.responseType === 'blob') {
    return response
  }
  
  if (response.status===200) {
    const resData = response.data
    const code = response.data.code
    if (resData.success && ((code && code === 200)||(response.data.repCode==='0000'))) {
      return resData
    } else if (!store.state.app.hasError && code === 401) {
      _requestErrAlert(resData.message,()=>{
        store.dispatch('SetHasError', false)
        window.location.reload()
      },'重新登录')
          
      // 授权过期，清理本地缓存的记录，不论 Modal.error 的 onOk 是否确认，先清理
      // 否则会在没按 OK 时，刷新网页或者重新访问，都会弹出“未授权的提示框”
      // 这样的调整后，TOKEN 为空直接重定向，SetHasError 的设置和判断其实已经用不上
      Vue.ls.remove(ACCESS_TOKEN)
      Vue.ls.remove('X-Access-Token')
      store.dispatch('SetHasError', true)
      return resData
    } else {
      let msg = resData.message;
      if (!msg) msg = '网络错误，请稍后再尝试！';
      message.error(msg)
      console.log(response)
      return Promise.reject(response)
    }
  } else {
    message.error('网络错误，请稍后再尝试！')
    console.log(response)
    return Promise.reject(response)
  }
}, err)

const installer = {
  vm: {},
  install(Vue) {
    Vue.use(VueAxios, service)
  }
}

export { installer as VueAxios, service as axios }
