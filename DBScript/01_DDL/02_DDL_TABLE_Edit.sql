/* 这里写创建或修改表字段的脚本 */

 ALTER TABLE sys_code_gen_config ADD   ShowTitleMinWidth     nvarchar(50)  -- 增加 页面列显示最小宽度【Editby liuwq,2024-04-18】
  ALTER TABLE sys_code_gen_config ADD   WhetherUnionKey     nvarchar(5)  -- 增加 是否是联合主键【Editby liuwq,2024-04-18】