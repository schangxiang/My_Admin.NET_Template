﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Admin.NET.Core.Entity.WmsBase;

namespace Admin.NET.Core
{
    /// <summary>
    /// PDA菜单表
    /// </summary>
    [Table("wms_pdapower")]
    [Comment("PDA菜单表")]
    public class WmsPdaPower : DEntityBase
    {
        /// <summary>
        /// 图标
        /// </summary>
        [Comment("图标")]
        [Required]
        [MaxLength(50)]
        public string Icon { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        [Comment("编码")]
        [MaxLength(50)]
        public string Code { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Comment("名称")]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// 颜色
        /// </summary>
        [Comment("颜色")]
        [Required]
        public string Clolor { get; set; }

        /// <summary>
        /// 所属车间
        /// </summary>
        [Comment("所属车间")]
        public LesWorkShopType WorkShopType { get; set; }

        /// <summary>
        /// 多对多（角色）
        /// </summary>
        public ICollection<SysRole> SysRoles { get; set; }

        /// <summary>
        /// 多对多中间表（用户角色）
        /// </summary>
        public List<WmsRolePdaMenu> WmsRolePdaMenus { get; set; }

    }
}
