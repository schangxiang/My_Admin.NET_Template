﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 任务区域
    /// </summary>
    public enum TaskArea
    {
        /// <summary>
        /// 一楼
        /// </summary>
        [Description("一楼")]
        YILOU = 1,

        /// <summary>
        /// 二楼
        /// </summary>
        [Description("二楼")]
        ERLOU = 2
    }

    /// <summary>
    /// 熟化库任务方式
    /// </summary>
    public enum TaskModel
    {
        /// <summary>
        /// 手动
        /// </summary>
        [Description("手动")]
        SHOUDONG = 1,

        /// <summary>
        /// 全自动
        /// </summary>
        [Description("全自动")]
        QUANZIDONG = 2,

        /// <summary>
        /// 半自动
        /// </summary>
        [Description("半自动")]
        BANZIDONG = 3,
    }

    /// <summary>
    /// 组装库任务方式
    /// </summary>
    public enum AssembleTaskModel
    {
        /// <summary>
        /// 手动
        /// </summary>
        [Description("手动")]
        SHOUDONG = 1,

        /// <summary>
        /// 全自动
        /// </summary>
        [Description("全自动")]
        QUANZIDONG = 2
    }

    /// <summary>
    /// 任务类型
    /// </summary>
    public enum TaskType
    {
        /// <summary>
        /// 入库
        /// </summary>
        [Description("入库")]
        RUKU = 1,

        /// <summary>
        /// 出库
        /// </summary>
        [Description("出库")]
        CHUKU = 2,

        /// <summary>
        /// 移库
        /// </summary>
        [Description("移库")]
        YIKU = 3,

        /// <summary>
        /// 搬运
        /// </summary>
        [Description("搬运")]
        BAIYUN =4
    }

    /// <summary>
    /// 任务状态
    /// </summary>
    public enum TaskStatusEnum
    {
        /// <summary>
        /// 未执行
        /// </summary>
        [Description("未执行")]
        WEIZHIXING = 1,

        /// <summary>
        /// 执行中
        /// </summary>
        [Description("执行中")]
        ZHIXINGZHONG = 2,

        /// <summary>
        /// 完成
        /// </summary>
        [Description("完成")]
        WANCHENG = 3,

        /// <summary>
        /// 取消
        /// </summary>
        [Description("取消")]
        QUXIAO = 4,

        /// <summary>
        /// 暂停
        /// </summary>
        [Description("暂停")]
        ZANTING = 5,

        /// <summary>
        /// 撤回
        /// </summary>
        [Description("撤回")]
        CHEHUI = 6,

        /// <summary>
        /// 待执行
        /// </summary>
        [Description("待执行")]
        DAIZHIXING =7
    }

    /// <summary>
    /// 任务设备状态
    /// </summary>
    public enum TaskDodeviceStatusEnum
    {
        /// <summary>
        /// 未执行
        /// </summary>
        [Description("未执行")]
        WZX= 1,

        /// <summary>
        /// AGV执行
        /// </summary>
        [Description("AGV执行")]
        ZX_AGV = 2,

        /// <summary>
        /// AGV完成
        /// </summary>
        [Description("AGV完成")]
        WC_AGV = 3,

        /// <summary>
        /// 入库输送线-执行
        /// </summary>
        [Description("入库输送线-执行")]
        ZX_RSSX = 4,

        /// <summary>
        /// 入库输送线-完成
        /// </summary>
        [Description("入库输送线-完成")]
        WC_RSSX = 5,

        /// <summary>
        /// 堆垛机-执行
        /// </summary>
        [Description("堆垛机-执行")]
        ZX_DDJ = 6,

        /// <summary>
        /// 堆垛机-完成
        /// </summary>
        [Description("堆垛机-完成")]
        WC_DDJ = 7,

        /// <summary>
        /// 出库输送线-执行
        /// </summary>
        [Description("出库输送线-执行")]
        ZX_CSSX = 8,

        /// <summary>
        /// 出库输送线-完成
        /// </summary>
        [Description("出库输送线-完成")]
        WC_CSSX = 9,


        /// <summary>
        /// RGV-执行
        /// </summary>
        [Description("RGV-执行")]
        ZX_RGV = 10,

        /// <summary>
        /// RGV-完成
        /// </summary>
        [Description("RGV-完成")]
        WC_RGV = 11,

        /// <summary>
        /// 等待
        /// </summary>
        [Description("等待")]
        DD = 12,

        /// <summary>
        /// 完成
        /// </summary>
        [Description("完成")]
        WC = 13,

        /// <summary>
        /// 无
        /// </summary>
        [Description("无")]
        W = 14,

        /// <summary>
        /// 入库称重
        /// </summary>
        [Description("入库称重")]
        ZX_RKCZ=15


    }

    /// <summary>
    /// AGV任务设备状态
    /// </summary>
    public enum AGVTaskDodeviceStatusEnum
    {
        /// <summary>
        /// 未执行
        /// </summary>
        [Description("未执行")]
        WZX = 1,

        /// <summary>
        /// 1-AGV执行
        /// </summary>
        [Description("1-AGV执行")]
        ZX_AGVONE = 2,

        /// <summary>
        /// 1-AGV完成
        /// </summary>
        [Description("1-AGV完成")]
        WC_AGVONE = 3,

        /// <summary>
        /// 2-AGV执行
        /// </summary>
        [Description("2-AGV执行")]
        ZX_AGVTWO = 4,

        /// <summary>
        /// 2-AGV完成
        /// </summary>
        [Description("2-AGV完成")]
        WC_AGVTWO = 5,

        /// <summary>
        /// 无
        /// </summary>
        [Description("无")]
        W = 6
    }

    /// <summary>
    /// 组装车间任务设备状态
    /// </summary>
    public enum TaskAssembleDodeviceStatusEnum
    {
        /// <summary>
        /// 未执行
        /// </summary>
        [Description("未执行")]
        WZX = 1,

        /// <summary>
        /// 提升机执行
        /// </summary>
        [Description("提升机执行")]
        ZX_TXJ = 2,

        /// <summary>
        /// 提升机完成
        /// </summary>
        [Description("提升机完成")]
        WC_TXJ = 3,

        /// <summary>
        /// 无
        /// </summary>
        [Description("无")]
        W = 4,

        /// <summary>
        /// AGV执行
        /// </summary>
        [Description("AGV执行")]
        ZX_AGV = 5,

        /// <summary>
        /// AGV完成
        /// </summary>
        [Description("AGV完成")]
        WC_AGV = 6


    }
}
