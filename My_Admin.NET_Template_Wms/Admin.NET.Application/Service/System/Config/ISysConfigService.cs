﻿using Admin.NET.Core;
using Microsoft.AspNetCore.Mvc;

namespace Admin.NET.Application
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISysConfigService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task AddConfig(AddConfigInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task DeleteConfig(DeleteConfigInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<SysConfig> GetConfig([FromQuery] QueryConfigInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<List<SysConfig>> GetConfigList();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PageResult<SysConfig>> QueryConfigPageList([FromQuery] ConfigPageInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task UpdateConfig(UpdateConfigInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<bool> GetDemoEnvFlag();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<bool> GetCaptchaOpenFlag();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        Task UpdateConfigCache(string code, string value);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<bool> GetEnableSingleLoginFlag();
    }
}