﻿import { axios } from '@/utils/request'

/**
 * 查询学生信息表3-刘文奇
 *
 * @author liuwq
 */
export function TestStudent3Page (parameter) {
  return axios({
    url: '/TestStudent3/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 学生信息表3-刘文奇列表
 *
 * @author liuwq
 */
export function TestStudent3List (parameter) {
  return axios({
    url: '/TestStudent3/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加学生信息表3-刘文奇
 *
 * @author liuwq
 */
export function TestStudent3Add (parameter) {
  return axios({
    url: '/TestStudent3/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑学生信息表3-刘文奇
 *
 * @author liuwq
 */
export function TestStudent3Edit (parameter) {
  return axios({
    url: '/TestStudent3/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除学生信息表3-刘文奇
 *
 * @author liuwq
 */
export function TestStudent3Delete (parameter) {
  return axios({
    url: '/TestStudent3/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出学生信息表3-刘文奇的Excel文件
 *
 * @author liuwq
 */
export function TestStudent3ToExcel (parameter) {
  return axios({
    url: '/TestStudent3/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入学生信息表3-刘文奇的Excel文件
 *
 * @author liuwq
 */
export function TestStudent3FromExcel (data, parameter) {
  return axios({
    url: '/TestStudent3/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载学生信息表3-刘文奇的Excel导入模板
 *
 * @author liuwq
 */
export function TestStudent3DownloadExcelTemplate(parameter) {
  return axios({
    url: '/TestStudent3/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}



