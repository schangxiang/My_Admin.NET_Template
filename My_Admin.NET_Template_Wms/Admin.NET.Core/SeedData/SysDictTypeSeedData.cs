﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;

namespace Admin.NET.Core
{
    /// <summary>
    /// 系统字典类型种子数据
    /// </summary>
    public class SysDictTypeSeedData : IEntitySeedData<SysDictType>
    {
        /// <summary>
        /// 种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<SysDictType> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new[]
            {
                new SysDictType{Id=142307070906483, Name="通用状态", Code="common_status", Sort=100, Remark="通用状态", Status=0 },
                new SysDictType{Id=142307070906484, Name="性别", Code="sex", Sort=100, Remark="性别字典", Status=0 },
                new SysDictType{Id=142307070906485, Name="常量的分类", Code="consts_type", Sort=100, Remark="常量的分类，用于区别一组配置", Status=0 },
                new SysDictType{Id=142307070906486, Name="是否", Code="yes_or_no", Sort=100, Remark="是否", Status=0 },
                new SysDictType{Id=142307070906487, Name="访问类型", Code="vis_type", Sort=100, Remark="访问类型", Status=0 },
                new SysDictType{Id=142307070906488, Name="菜单类型", Code="menu_type", Sort=100, Remark="菜单类型", Status=0 },
                new SysDictType{Id=142307070906489, Name="发送类型", Code="send_type", Sort=100, Remark="发送类型", Status=0 },
                new SysDictType{Id=142307070906490, Name="打开方式", Code="open_type", Sort=100, Remark="打开方式", Status=0 },
                new SysDictType{Id=142307070906491, Name="菜单权重", Code="menu_weight", Sort=100, Remark="菜单权重", Status=0 },
                new SysDictType{Id=142307070906492, Name="数据范围类型", Code="data_scope_type", Sort=100, Remark="数据范围类型", Status=0 },
                //new SysDictType{Id=142307070906493, Name="短信发送来源", Code="sms_send_source", Sort=100, Remark="短信发送来源", Status=0 },
                new SysDictType{Id=142307070906494, Name="操作类型", Code="op_type", Sort=100, Remark="操作类型", Status=0 },
                new SysDictType{Id=142307070906495, Name="文件存储位置", Code="file_storage_location", Sort=100, Remark="文件存储位置", Status=0 },
                new SysDictType{Id=142307070910533, Name="运行状态", Code="run_status", Sort=100, Remark="定时任务运行状态", Status=0 },
                new SysDictType{Id=142307070910534, Name="通知公告类型", Code="notice_type", Sort=100, Remark="通知公告类型", Status=0 },
                new SysDictType{Id=142307070910535, Name="通知公告状态", Code="notice_status", Sort=100, Remark="通知公告状态", Status=0 },
                new SysDictType{Id=142307070910536, Name="是否boolean", Code="yes_true_false", Sort=100, Remark="是否boolean", Status=0 },
                new SysDictType{Id=142307070910537, Name="代码生成方式", Code="code_gen_create_type", Sort=100, Remark="代码生成方式", Status=0 },
                new SysDictType{Id=142307070910538, Name="请求方式", Code="request_type", Sort=100, Remark="请求方式", Status=0 },
                new SysDictType{Id=142307070922827, Name="代码生成作用类型", Code="code_gen_effect_type", Sort=100, Remark="代码生成作用类型", Status=0 },
                new SysDictType{Id=142307070922828, Name="代码生成查询类型", Code="code_gen_query_type", Sort=100, Remark="代码生成查询类型", Status=0 },
                new SysDictType{Id=142307070922829, Name="代码生成.NET类型", Code="code_gen_net_type", Sort=100, Remark="代码生成.NET类型", Status=0 },
                new SysDictType{Id=142307070926941, Name="角色类型", Code="role_type", Sort=100, Remark="角色类型", Status=0 },
                new SysDictType{Id=142307070926942, Name="机构类型", Code="org_type", Sort=100, Remark="机构类型", Status=0 },

                new SysDictType{Id=285605149831237, Name="表单类型", Code="form_type", Sort=100, Remark="表单类型", Status=0 },

                new SysDictType{Id=285641358889029, Name="流程分类", Code="workflow_group", Sort=100, Remark="流程分类", Status=0 },

                new SysDictType{Id=355694205325381, Name="托盘类型", Code="container_type", Sort=100, Remark="", Status=0 },
                new SysDictType{Id=355697881423941, Name="托盘状态", Code="container_status", Sort=100, Remark="", Status=0 },
                new SysDictType{Id=355697881423942, Name="托盘分类", Code="container_category", Sort=100, Remark="", Status=0 },


                new SysDictType{Id=355698224758853, Name="库区类型", Code="area_type", Sort=100, Remark="", Status=0 },
                new SysDictType{Id=355698441912389, Name="库位状态", Code="place_status", Sort=100, Remark="", Status=0 },
                new SysDictType{Id=355698661130309, Name="库位高度", Code="height_level", Sort=100, Remark="", Status=0 },

                new SysDictType{Id=355698855280709, Name="物料检验", Code="material_inspection", Sort=100, Remark="", Status=0 },
                new SysDictType{Id=355699003580485, Name="物料类别", Code="material_type", Sort=100, Remark="", Status=0 },

                new SysDictType{Id=355699189051461, Name="单位类别", Code="unit_type", Sort=100, Remark="", Status=0 },
                new SysDictType{Id=355699314061381, Name="单位编号", Code="unitno_type", Sort=100, Remark="", Status=0 },

                new SysDictType{Id=355699447029829, Name="任务方式", Code="task_model", Sort=100, Remark="", Status=0 },
                new SysDictType{Id=355699576467525, Name="任务类型", Code="task_type", Sort=100, Remark="", Status=0 },
                new SysDictType{Id=355699731099717, Name="任务状态", Code="task_status", Sort=100, Remark="", Status=0 },
                new SysDictType{Id=355699731099718, Name="任务设备状态", Code="taskdodevice_statusenum", Sort=100, Remark="", Status=0 },


                new SysDictType{Id=383290718294085, Name="所属车间", Code="les_workshop_type", Sort=100, Remark="", Status=0 },
                new SysDictType{Id=383317542490181, Name="叫料单状态", Code="les_takematerials_status", Sort=100, Remark="", Status=0 },
                new SysDictType{Id=384394636988485, Name="设备类型", Code="dodevice_type", Sort=100, Remark="", Status=0 },
                new SysDictType{Id=385412129075269, Name="工段", Code="lesworkshop_section", Sort=100, Remark="", Status=0 },
                new SysDictType{Id=386522984120389, Name="库口类型", Code="les_entrance_type", Sort=100, Remark="", Status=0 },

               
                new SysDictType{Id=386522984120391, Name="单据明细表状态", Code="orderdetails_statusenum", Sort=100, Remark="", Status=0 },
                new SysDictType{Id=386522984120392, Name="单据大类", Code="orderlargecategory_Enum", Sort=100, Remark="", Status=0 },
                new SysDictType{Id=386522984120393, Name="单据主表状态", Code="order_statusenum", Sort=100, Remark="", Status=0 },
               

            };
        }
    }
}