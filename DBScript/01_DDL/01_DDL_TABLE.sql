/* 这里写创建数据库的脚本 */



-- 编号生成表 【Editby shaocx,2023-07-27】

/****** Object:  Table [dbo].[Base_SerialRuleDetail]    Script Date: 2023/7/27 14:34:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Base_SerialRuleDetail](
	[Id] [bigint] NOT NULL,
	[SerialRuleNo] [nvarchar](50) NOT NULL,
	[SerialType] [int] NOT NULL,
	[ItemNo] [int] NOT NULL,
	[SourceType] [nvarchar](50) NOT NULL,
	[SerialLength] [int] NOT NULL,
	[SearchStart] [int] NULL,
	[DecimalType] [int] NULL,
	[UserDefine] [nvarchar](50) NULL,
	[SerialCodeFlag] [int] NULL,
	[Description] [nvarchar](250) NULL,
	[SerialTypeNo] [nvarchar](max) NULL,
	[GetData] [int] NULL,
	[PadLeft] [int] NOT NULL,
	[IssueStatus] [int] NULL,
	[CreatedTime] [datetimeoffset](7) NULL,
	[UpdatedTime] [datetimeoffset](7) NULL,
	[CreatedUserId] [bigint] NULL,
	[CreatedUserName] [nvarchar](50) NULL,
	[UpdatedUserId] [bigint] NULL,
	[UpdatedUserName] [nvarchar](50) NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Base_SerialRuleDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Base_SerialSN]    Script Date: 2023/7/27 14:34:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Base_SerialSN](
	[Id] [bigint] NOT NULL,
	[CurrentDate] [datetime2](7) NOT NULL,
	[Sn] [int] NOT NULL,
	[SerialType] [int] NOT NULL,
	[CreatedTime] [datetimeoffset](7) NULL,
	[UpdatedTime] [datetimeoffset](7) NULL,
	[CreatedUserId] [bigint] NULL,
	[CreatedUserName] [nvarchar](50) NULL,
	[UpdatedUserId] [bigint] NULL,
	[UpdatedUserName] [nvarchar](50) NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Base_SerialSN] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialRuleDetail', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据号编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialRuleDetail', @level2type=N'COLUMN',@level2name=N'SerialRuleNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据号类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialRuleDetail', @level2type=N'COLUMN',@level2name=N'SerialType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'生成顺序号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialRuleDetail', @level2type=N'COLUMN',@level2name=N'ItemNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'组合类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialRuleDetail', @level2type=N'COLUMN',@level2name=N'SourceType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'组合类型长度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialRuleDetail', @level2type=N'COLUMN',@level2name=N'SerialLength'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'字符串内容' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialRuleDetail', @level2type=N'COLUMN',@level2name=N'UserDefine'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'类型描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialRuleDetail', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文本靠边方向' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialRuleDetail', @level2type=N'COLUMN',@level2name=N'PadLeft'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialRuleDetail', @level2type=N'COLUMN',@level2name=N'CreatedTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialRuleDetail', @level2type=N'COLUMN',@level2name=N'UpdatedTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建者Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialRuleDetail', @level2type=N'COLUMN',@level2name=N'CreatedUserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建者名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialRuleDetail', @level2type=N'COLUMN',@level2name=N'CreatedUserName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialRuleDetail', @level2type=N'COLUMN',@level2name=N'UpdatedUserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialRuleDetail', @level2type=N'COLUMN',@level2name=N'UpdatedUserName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'软删除标记' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialRuleDetail', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据号生成规则配置表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialRuleDetail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Id主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialSN', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'流水号时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialSN', @level2type=N'COLUMN',@level2name=N'CurrentDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'流水号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialSN', @level2type=N'COLUMN',@level2name=N'Sn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据号类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialSN', @level2type=N'COLUMN',@level2name=N'SerialType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialSN', @level2type=N'COLUMN',@level2name=N'CreatedTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更新时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialSN', @level2type=N'COLUMN',@level2name=N'UpdatedTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建者Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialSN', @level2type=N'COLUMN',@level2name=N'CreatedUserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建者名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialSN', @level2type=N'COLUMN',@level2name=N'CreatedUserName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者Id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialSN', @level2type=N'COLUMN',@level2name=N'UpdatedUserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改者名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialSN', @level2type=N'COLUMN',@level2name=N'UpdatedUserName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'软删除标记' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialSN', @level2type=N'COLUMN',@level2name=N'IsDeleted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单据号流水号履历表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Base_SerialSN'
GO

