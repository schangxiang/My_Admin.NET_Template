﻿import { axios } from '@/utils/request'

/**
 * 查询库存表
 *
 * @author yhh
 */
export function PlywoodStockPage (parameter) {
  return axios({
    url: '/plywoodmaterialstock/page',
    method: 'get',
    params: parameter
  })
}

