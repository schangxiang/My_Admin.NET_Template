﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 贸易方式
    /// </summary>
    public enum TradeMode
    {
        /// <summary>
        /// 国产
        /// </summary>
        [Description("国产")]
        GUOCHAN = 1,

        /// <summary>
        /// 进料对口
        /// </summary>
        [Description("进料对口")]
        JINLIAODUIKOU = 2,

        /// <summary>
        /// 一般贸易
        /// </summary>
        [Description("一般贸易")]
        YIBANMAOYI = 3,

        /// <summary>
        /// 其他进出口免费
        /// </summary>
        [Description("其他进出口免费")]
        QITAJINCHUKOUMIANFEI = 4,

        /// <summary>
        /// 修理物品
        /// </summary>
        [Description("修理物品")]
        XIULIWUPIN = 5,

        /// <summary>
        ///进料料件内销
        /// </summary>
        [Description("进料料件内销")]
        JINLIAOLIAOJIANNEIXIAO = 6,

        /// <summary>
        ///货样广告品
        /// </summary>
        [Description("货样广告品")]
        HUOYANGGUANGGAOPIN = 7,

        /// <summary>
        ///进料边角料内销
        /// </summary>
        [Description("进料边角料内销")]
        JINLIAOBIANJIAOLIAONEIXIAO = 8,

        /// <summary>
        ///进料余料结转
        /// </summary>
        [Description("进料余料结转")]
        JINLIAOYULIAOJIEZHUAN = 9,

        /// <summary>
        ///进料深加工
        /// </summary>
        [Description("进料深加工")]
        JINLIAOSHENJIAOGONG = 10,

        /// <summary>
        ///保税仓库货物
        /// </summary>
        [Description("保税仓库货物")]
        BAOSHUICANGKUHUOWU = 11,

        /// <summary>
        ///进料料件退还
        /// </summary>
        [Description("进料料件退还")]
        JINLIAOLIAOJIANTUIHUAN = 12,

        /// <summary>
        ///暂时进出货物
        /// </summary>
        [Description("暂时进出货物")]
        ZANSHIJINCHUHUOWU = 13,

        /// <summary>
        ///进料料件复出
        /// </summary>
        [Description("进料料件复出")]
        JINLIAOLIAOJIANFUCHU = 14,
    }
}
