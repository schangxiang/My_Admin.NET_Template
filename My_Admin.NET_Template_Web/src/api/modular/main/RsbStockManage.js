﻿import { axios } from '@/utils/request'

/**
 * 查询库存表
 *
 * @author yhh
 */
export function RsbStockPage (parameter) {
  return axios({
    url: '/rsbmaterialstock/page',
    method: 'get',
    params: parameter
  })
}

