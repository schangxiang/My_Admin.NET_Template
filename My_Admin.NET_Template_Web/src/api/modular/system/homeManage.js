/**
 * 系统应用
 *
 * @author yhh
 */
import { axios } from '@/utils/request'

/**
 * 获取库位数据
 *
 * @author yhh
 */
export function GetPalceNumbers () {
  return axios({
    url: '/locationview/GetPalceDetail',
    method: 'get'
  })
}

/**
 * 获取出入库统计数据
 *
 * @author yhh
 */
export function GeInOutTaskNumber (params) {
  return axios({
    url: '/locationview/task-detail',
    method: 'get',
    params:params
  })
}

/**
 * 获取出入库总数
 *
 * @author yhh
 */
export function GeInOutTotalNumber () {
  return axios({
    url: '/locationview/GetTaskCount',
    method: 'get'
  })
}