﻿using Mapster;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    public class TestStudentMapper : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<AddTestStudentInput, TestStudent>()
            ;
            config.ForType<UpdateTestStudentInput, TestStudent>()
            ;
            config.ForType<TestStudent, TestStudentOutput>()
            ;
        }
    }
}
