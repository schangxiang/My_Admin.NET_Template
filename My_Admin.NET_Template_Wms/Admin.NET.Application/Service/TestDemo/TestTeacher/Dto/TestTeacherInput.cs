﻿using Admin.NET.Core;
using Admin.NET.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace Admin.NET.Application
{    
    
    /// <summary>
    /// 测试老师表查询参数
    /// </summary>
    public class TestTeacherSearch : PageInputBase
    {
        /// <summary>
        /// 名称
        /// </summary>
        public virtual string Name { get; set; }
        
    }

        /// <summary>
    /// 测试老师表不分页查询参数
    /// </summary>
    public class TestTeacherSearchNonPage : PageInputNonPageBase
    {
        /// <summary>
        /// 名称
        /// </summary>
        public virtual string Name { get; set; }
        
    }

    /// <summary>
    /// 测试老师表输入参数
    /// </summary>
    public class TestTeacherInput
    {
        /// <summary>
        /// 名称
        /// </summary>
        public virtual string Name { get; set; }
        
    }

    public class AddTestTeacherInput : TestTeacherInput
    {
    }

    public class DeleteTestTeacherInput : BaseId
    {
    }

    public class UpdateTestTeacherInput : TestTeacherInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
        
    }

    public class QueryeTestTeacherInput : BaseId
    {

    }
}
