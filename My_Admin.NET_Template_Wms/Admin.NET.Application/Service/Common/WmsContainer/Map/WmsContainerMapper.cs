﻿using Mapster;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 
    /// </summary>
    public class WmsContainerMapper : IRegister
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<AddWmsContainerInput, WmsContainer>()
            ;
            config.ForType<UpdateWmsContainerInput, WmsContainer>()
            ;
            config.ForType<WmsContainer, WmsContainerOutput>()
            ;
        }
    }
}
