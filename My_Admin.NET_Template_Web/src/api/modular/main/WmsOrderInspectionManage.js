﻿import { axios } from '@/utils/request'
// 报检单
/**
 * 查询单据表
 *
 * @author XJF
 */
export function WmsOrderPage (parameter) {
  return axios({
    url: '/wmsinspectorder/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 单据表列表
 *
 * @author XJF
 */
export function WmsOrderList (parameter) {
  return axios({
    url: '/WmsOrder/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加单据表
 *
 * @author XJF
 */
export function WmsOrderAdd (parameter) {
  return axios({
    url: '/WmsOrder/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑单据表
 *
 * @author XJF
 */
export function WmsOrderEdit (parameter) {
  return axios({
    url: '/WmsOrder/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除单据表
 *
 * @author XJF
 */
export function WmsOrderDelete (parameter) {
  return axios({
    url: '/WmsOrder/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出单据表的Excel文件
 *
 * @author XJF
 */
export function WmsOrderToExcel (parameter) {
  return axios({
    url: '/WmsOrder/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入单据表的Excel文件
 *
 * @author XJF
 */
export function WmsOrderFromExcel (data, parameter) {
  return axios({
    url: '/WmsOrder/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载单据表的Excel导入模板
 *
 * @author XJF
 */
export function WmsOrderDownloadExcelTemplate(parameter) {
  return axios({
    url: '/WmsOrder/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}



/**
 * 物料详情
 *
 */
 export function WmsOrderRukuDetail(parameter) {
  return axios({
    url: '/wmsinspectorder/PageDetail',
    method: 'get',
    params: parameter
  })
}

/**
 * 报检
 *
 */
 export function WmsOrderBaojian(parameter) {
  return axios({
    url: '/wmsinspectorder/InspectionDeclaration',
    method: 'post',
    data: parameter
  })
}