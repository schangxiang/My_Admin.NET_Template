﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    /// <summary>
    /// 学生信息表
    /// </summary>
    [Comment("学生信息表")]
    [Table("test_students")]
    public class TestStudent : DEntityBase
    {
        /// <summary>
        /// 学生姓名
        /// </summary>
        [Comment("学生姓名")]
        [MaxLength(10)]
        public string Name { get; set; }

        /// <summary>
        /// 学生年龄
        /// </summary>
        [Comment("学生年龄")]
        public int Age { get; set; }

        /// <summary>
        /// 是否在校
        /// </summary>
        [Comment("是否在校")]
        [MaxLength(50)]
        public bool StartName { get; set; }


        /// <summary>
        /// 性别
        /// </summary>
        [Comment("性别")]
        public GenderChinese Gender { get; set; }

        /// <summary>
        /// 出生日期
        /// </summary>
        [Comment("出生日期")]
        public DateTime BrithDate { get; set; }


        /// <summary>
        /// 关联老师
        /// </summary>
        [Comment("关联老师")]
        public long? TeacherId { get; set; }

    }
}
