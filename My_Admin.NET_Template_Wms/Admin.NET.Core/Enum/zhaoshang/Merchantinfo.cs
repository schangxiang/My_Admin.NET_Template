﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 商客分类
    /// </summary>
    public enum MerchantType
    {
        /// <summary>
        /// 客商
        /// </summary>
        GuestConsult = 1,
        /// <summary>
        /// 供应商
        /// </summary>
        Supplier = 2,
        /// <summary>
        /// 客户
        /// </summary>
        Customers = 3
    }
    /// <summary>
    /// 商客等级
    /// </summary>
    public enum MerchantGrade
    {
        /// <summary>
        /// 一级
        /// </summary>
        YIJI = 1,
        /// <summary>
        /// 二级
        /// </summary>
        ERJI = 2,
        /// <summary>
        /// 三级
        /// </summary>
        SANJI = 3
    }
}
