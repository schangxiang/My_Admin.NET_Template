﻿using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 物料信息
    /// </summary>
    public class WmsMaterialDto
    {
        /// <summary>
        /// 物料名称
        /// </summary>
        public string MaterialName { get; set; }
        
        /// <summary>
        /// 物料编号
        /// </summary>
        public string MaterialNo { get; set; }
        
        /// <summary>
        /// 物料批次
        /// </summary>
        public string MaterialBatch { get; set; }
        
        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }
        
        /// <summary>
        /// 物料类别
        /// </summary>
        public MaterialType? MaterialType { get; set; }
        
        /// <summary>
        /// 物料规格
        /// </summary>
        public string MaterialSpec { get; set; }
        
        /// <summary>
        /// 物料密度
        /// </summary>
        public string MaterialDensity { get; set; }
        
        /// <summary>
        /// 库区参数
        /// </summary>
        public string AreaNameParameter { get; set; }
        
        /// <summary>
        /// 工作区参数
        /// </summary>
        public string StationNameParameter { get; set; }
        
        /// <summary>
        /// 熟化时间
        /// </summary>
        public int MaturationTime { get; set; }
        
        /// <summary>
        /// 是否需要熟化
        /// </summary>
        public bool? IsMaturation { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
    }
}
