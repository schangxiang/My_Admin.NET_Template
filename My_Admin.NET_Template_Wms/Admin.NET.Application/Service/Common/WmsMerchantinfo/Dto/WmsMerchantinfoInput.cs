﻿using Admin.NET.Core;
using Admin.NET.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace Admin.NET.Application
{    
    
    /// <summary>
    /// 客商信息表查询参数
    /// </summary>
    public class WmsMerchantinfoSearch : PageInputBase
    {
        /// <summary>
        /// 客商编号
        /// </summary>
        public virtual string MerchantNo { get; set; }
        
        /// <summary>
        /// 客商名称
        /// </summary>
        public virtual string MerchantName { get; set; }
        
        /// <summary>
        /// 客商分类
        /// </summary>
        public virtual Admin.NET.Core.MerchantType? MerchantType { get; set; }
        
        /// <summary>
        /// 客商简称
        /// </summary>
        public virtual string MerchantShort { get; set; }
        
        /// <summary>
        /// 客商等级
        /// </summary>
        public virtual Admin.NET.Core.MerchantGrade? MerchantGrade { get; set; }
        
        /// <summary>
        /// 客商属性
        /// </summary>
        public virtual string MerchantAttribute { get; set; }
        
        /// <summary>
        /// 客商行业
        /// </summary>
        public virtual string MerchantTmt { get; set; }
        
        /// <summary>
        /// 客商联系人
        /// </summary>
        public virtual string MerchantContact { get; set; }
        
        /// <summary>
        /// 客商电话
        /// </summary>
        public virtual string MerchantPhone { get; set; }
        
        /// <summary>
        /// 客商传真
        /// </summary>
        public virtual string MerchantFax { get; set; }
        
        /// <summary>
        /// 客商邮箱
        /// </summary>
        public virtual string MerchantEmail { get; set; }
        
        /// <summary>
        /// 客商地址
        /// </summary>
        public virtual string MerchantAddress { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState IssueState { get; set; }
        
    }

        /// <summary>
    /// 客商信息表不分页查询参数
    /// </summary>
    public class WmsMerchantinfoSearchNonPage : PageInputNonPageBase
    {
        /// <summary>
        /// 客商编号
        /// </summary>
        public virtual string? MerchantNo { get; set; }
        
        /// <summary>
        /// 客商名称
        /// </summary>
        public virtual string? MerchantName { get; set; }
        
        /// <summary>
        /// 客商分类
        /// </summary>
        public virtual Admin.NET.Core.MerchantType? MerchantType { get; set; }
        
        /// <summary>
        /// 客商简称
        /// </summary>
        public virtual string? MerchantShort { get; set; }
        
        /// <summary>
        /// 客商等级
        /// </summary>
        public virtual Admin.NET.Core.MerchantGrade? MerchantGrade { get; set; }
        
        /// <summary>
        /// 客商属性
        /// </summary>
        public virtual string? MerchantAttribute { get; set; }
        
        /// <summary>
        /// 客商行业
        /// </summary>
        public virtual string? MerchantTmt { get; set; }
        
        /// <summary>
        /// 客商联系人
        /// </summary>
        public virtual string? MerchantContact { get; set; }
        
        /// <summary>
        /// 客商电话
        /// </summary>
        public virtual string? MerchantPhone { get; set; }
        
        /// <summary>
        /// 客商传真
        /// </summary>
        public virtual string? MerchantFax { get; set; }
        
        /// <summary>
        /// 客商邮箱
        /// </summary>
        public virtual string? MerchantEmail { get; set; }
        
        /// <summary>
        /// 客商地址
        /// </summary>
        public virtual string? MerchantAddress { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState? IssueState { get; set; }
        
    }

    /// <summary>
    /// 客商信息表输入参数
    /// </summary>
    public class WmsMerchantinfoInput
    {
        /// <summary>
        /// 客商编号
        /// </summary>
        public virtual string MerchantNo { get; set; }
        
        /// <summary>
        /// 客商名称
        /// </summary>
        public virtual string MerchantName { get; set; }
        
        /// <summary>
        /// 客商分类
        /// </summary>
        public virtual Admin.NET.Core.MerchantType? MerchantType { get; set; }
        
        /// <summary>
        /// 客商简称
        /// </summary>
        public virtual string MerchantShort { get; set; }
        
        /// <summary>
        /// 客商等级
        /// </summary>
        public virtual Admin.NET.Core.MerchantGrade? MerchantGrade { get; set; }
        
        /// <summary>
        /// 客商属性
        /// </summary>
        public virtual string MerchantAttribute { get; set; }
        
        /// <summary>
        /// 客商行业
        /// </summary>
        public virtual string MerchantTmt { get; set; }
        
        /// <summary>
        /// 客商联系人
        /// </summary>
        public virtual string MerchantContact { get; set; }
        
        /// <summary>
        /// 客商电话
        /// </summary>
        public virtual string MerchantPhone { get; set; }
        
        /// <summary>
        /// 客商传真
        /// </summary>
        public virtual string MerchantFax { get; set; }
        
        /// <summary>
        /// 客商邮箱
        /// </summary>
        public virtual string MerchantEmail { get; set; }
        
        /// <summary>
        /// 客商地址
        /// </summary>
        public virtual string MerchantAddress { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState IssueState { get; set; }
        
    }

    public class AddWmsMerchantinfoInput : WmsMerchantinfoInput
    {
    }

    public class DeleteWmsMerchantinfoInput : BaseId
    {
    }

    public class UpdateWmsMerchantinfoInput : WmsMerchantinfoInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
        
    }

    public class QueryeWmsMerchantinfoInput : BaseId
    {

    }
}
