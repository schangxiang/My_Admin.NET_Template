﻿import { axios } from '@/utils/request'

/**
 * 查询学生信息表4
 *
 * @author 邵长祥
 */
export function TestStudents4Page (parameter) {
  return axios({
    url: '/TestStudents4/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 学生信息表4列表
 *
 * @author 邵长祥
 */
export function TestStudents4List (parameter) {
  return axios({
    url: '/TestStudents4/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加学生信息表4
 *
 * @author 邵长祥
 */
export function TestStudents4Add (parameter) {
  return axios({
    url: '/TestStudents4/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑学生信息表4
 *
 * @author 邵长祥
 */
export function TestStudents4Edit (parameter) {
  return axios({
    url: '/TestStudents4/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除学生信息表4
 *
 * @author 邵长祥
 */
export function TestStudents4Delete (parameter) {
  return axios({
    url: '/TestStudents4/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出学生信息表4的Excel文件
 *
 * @author 邵长祥
 */
export function TestStudents4ToExcel (parameter) {
  return axios({
    url: '/TestStudents4/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入学生信息表4的Excel文件
 *
 * @author 邵长祥
 */
export function TestStudents4ImportExcel (data, parameter) {
  return axios({
    url: '/TestStudents4/importExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载学生信息表4的Excel导入模板
 *
 * @author 邵长祥
 */
export function TestStudents4DownloadExcelTemplate(parameter) {
  return axios({
    url: '/TestStudents4/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}



