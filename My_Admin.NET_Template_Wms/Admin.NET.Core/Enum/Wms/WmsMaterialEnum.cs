﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 物料检验
    /// </summary>
    public enum MaterialInspection
    {
        /// <summary>
        /// 免检
        /// </summary>
        [Description("免检")]
        MIANJIAN = 1,

        /// <summary>
        /// 抽检
        /// </summary>
        [Description("抽检")]
        CHOUJIAN = 2
    }

    /// <summary>
    /// 物料类别
    /// </summary>
    public enum MaterialType
    {
        /// <summary>
        /// 原材料
        /// </summary>
        [Description("原材料")]
        YUANCAILIAO = 1,

        /// <summary>
        /// 半成品
        /// </summary>
        [Description("半成品")]
        BANCHENGPING = 2,

        /// <summary>
        /// 成品
        /// </summary>
        [Description("成品")]
        CHENGPING = 3,

        /// <summary>
        /// 标准产品
        /// </summary>
        [Description("标准产品")]
        BIAOZHUANCHANPIN = 4,

        /// <summary>
        /// 包装材料
        /// </summary>
        [Description("包装材料")]
        BAOZHUANGCAILIAO = 5,

        /// <summary>
        /// 辅助材料
        /// </summary>
        [Description("包装材料")]
        FUZHUCAILIAO = 6,

        /// <summary>
        /// 工量具
        /// </summary>
        [Description("工量具")]
        GONGLIANGJU = 7,

        /// <summary>
        /// 空托
        /// </summary>
        [Description("空托")]
        KONGTUO = 8,

        /// <summary>
        /// 泡沫
        /// </summary>
        [Description("泡沫")]
        PAOMO = 9,

        /// <summary>
        /// 胶合板
        /// </summary>
        [Description("胶合板")]
        JIAOHEBAN = 10,

        /// <summary>
        /// 主板
        /// </summary>
        [Description("主板")]
        ZHUBAN = 11,

        /// <summary>
        /// RSB
        /// </summary>
        [Description("RSB")]
        RSB = 12,
    }

    /// <summary>
    /// 单位类别
    /// </summary>
    public enum UnitType
    {
        /// <summary>
        /// 数量
        /// </summary>
        [Description("数量")]
        SHULIANG = 0,

        /// <summary>
        /// 长度
        /// </summary>
        [Description("长度")]
        CHANGDU = 1,

        /// <summary>
        /// 重量
        /// </summary>
        [Description("重量")]
        ZHONGLIANG = 2
    }

    /// <summary>
    /// 单位编号
    /// </summary>
    public enum UnitNoType
    {
        /// <summary>
        /// cm
        /// </summary>
        [Description("cm")]
        CM = 1,

        /// <summary>
        /// t
        /// </summary>
        [Description("T")]
        T = 2
    }
}
