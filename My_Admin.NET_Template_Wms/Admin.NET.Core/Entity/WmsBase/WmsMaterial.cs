﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    /// <summary>
    /// 物料信息表
    /// </summary>
    [Table("wms_material")]
    [Comment("物料信息表")]
    public class WmsMaterial : DEntityBase, IEntityTypeBuilder<WmsMaterial>
    {
        /// <summary>
        /// 物料名称
        /// </summary>
        [Comment("物料名称")]
        [Required]
        [MaxLength(50)]
        public string MaterialName { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        [Comment("物料编号")]
        [Required]
        [MaxLength(50)]
        public string MaterialNo { get; set; }

        /// <summary>
        /// 物料批次
        /// </summary>
        [Comment("物料批次")]
        [MaxLength(50)]
        public string MaterialBatch { get; set; }

        /// <summary>
        /// 物料类别;数据字典
        /// </summary>
        [Comment("物料类别")]
        public MaterialType MaterialType { get; set; }

        /// <summary>
        /// 物料检验
        /// </summary>
        [Comment("物料检验")]
        public MaterialInspection InspectionMethod { get; set; }

        /// <summary>
        /// 物料规格
        /// </summary>
        [Comment("物料规格")]
        [MaxLength(50)]
        public string MaterialSpec { get; set; }

        /// <summary>
        /// 物料密度
        /// </summary>
        [Comment("物料密度")]
        [MaxLength(50)]
        public string MaterialDensity { get; set; }

        /// <summary>
        /// 物料单位
        /// </summary>
        [Comment("物料单位")]
        [MaxLength(50)]
        public string MaterialUnit { get; set; }

        /// <summary>
        /// 库区Ids
        /// </summary>
        [Comment("库区Ids")]
        public string AreaIds { get; set; }

        /// <summary>
        /// 库区参数
        /// </summary>
        [Comment("库区参数")]
        public string AreaNameParameter { get; set; }

        /// <summary>
        /// 工作区Ids
        /// </summary>
        [Comment("工作区Ids")]
        public string StationIds { get; set; }

        /// <summary>
        /// 工作区参数
        /// </summary>
        [Comment("工作区参数")]
        public string StationNameParameter { get; set; }

        /// <summary>
        /// 工段
        /// </summary>
        //[Comment("工段")]
        //[Required]
        //public LesWorkshopSection LesWorkshopSection { get; set; }

        //1215

        /// <summary>
        /// 描述
        /// </summary>
        [Comment("描述")]

        [MaxLength(500)]
        public string Description { get; set; }

        /// <summary>
        /// 安全存量
        /// </summary>
        [Comment("安全存量")]

        public decimal Safeqty { get; set; }

        /// <summary>
        /// 最大存量
        /// </summary>
        [Comment("最大存量")]

        public decimal MaxImumqty { get; set; }

        /// <summary>
        /// 最小库龄
        /// </summary>
        [Comment("最小库龄")]

        public decimal MinstorageAge { get; set; }

        /// <summary>
        /// 最大库龄
        /// </summary>
        [Comment("最大库龄")]

        public decimal MaxstorageAge { get; set; }

     




        /// <summary>
        /// 物料托盘关系表
        /// </summary>
        public ICollection<WmsMaterialContainer> WmsFoamingMaterialContainer { get; set; }

        /// <summary>
        /// 物料托盘历史表
        /// </summary>
        public ICollection<WmsHistoryMaterialContainer> WmsFoamingHistoryMaterialContainer { get; set; }

        /// <summary>
        /// 构建一对多的关系
        /// </summary>
        /// <param name="entityBuilder"></param>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        public void Configure(EntityTypeBuilder<WmsMaterial> entityBuilder, DbContext dbContext, Type dbContextLocator)
        {
            entityBuilder.HasMany(x => x.WmsFoamingMaterialContainer)
                .WithOne(x => x.WmsMaterial)
                .HasForeignKey(x => x.MaterialId).IsRequired(false);

            entityBuilder.HasMany(x => x.WmsFoamingHistoryMaterialContainer)
                .WithOne(x => x.WmsMaterial)
                .HasForeignKey(x => x.MaterialId).IsRequired(false);
        }
    }
}

