﻿using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 库位信息输出参数
    /// </summary>
    public class WmsPlaceDto
    {
        /// <summary>
        /// 所在库区
        /// </summary>
        public string WmsAreaAreaname { get; set; }
        
        /// <summary>
        /// 编码
        /// </summary>
        public string Placecode { get; set; }
        
        /// <summary>
        /// 库位状态
        /// </summary>
        public PlaceStatus Placestatus { get; set; }
        
        /// <summary>
        /// 所在库区
        /// </summary>
        public long Areaid { get; set; }
        
        /// <summary>
        /// 排
        /// </summary>
        public int Rowno { get; set; }
        
        /// <summary>
        /// 列
        /// </summary>
        public int Columnno { get; set; }
        
        /// <summary>
        /// 层
        /// </summary>
        public int Layerno { get; set; }
        
        /// <summary>
        /// 进深号
        /// </summary>
        public int Deepcellno { get; set; }
        
        /// <summary>
        /// 巷道
        /// </summary>
        public int Aisle { get; set; }
        
        /// <summary>
        /// 线号
        /// </summary>
        public int Line { get; set; }
        
        /// <summary>
        /// 巷道左右
        /// </summary>
        //public string Aisleside { get; set; }
        
        /// <summary>
        /// 是否锁定
        /// </summary>
        public YesOrNot Islock { get; set; }

        /// <summary>
        /// 
        /// </summary>
        //public YesOrNot Isfull { get; set; }

        /// <summary>
        /// 否正在操作
        /// </summary>
        //public YesOrNot Hastaskdoing { get; set; }

        /// <summary>
        /// 是否空托
        /// </summary>
        public virtual YesOrNot EmptyContainer { get; set; }

        /// <summary>
        /// 堆垛机内部的位置
        /// </summary>
        public string PositionnoForSrm { get; set; }
        
        /// <summary>
        /// 库位X坐标
        /// </summary>
        public string Xzb { get; set; }
        
        /// <summary>
        /// 库位Y坐标
        /// </summary>
        public string Yzb { get; set; }
        
        /// <summary>
        /// 库位Z坐标
        /// </summary>
        public string Zzb { get; set; }
        
        /// <summary>
        /// 库位长度
        /// </summary>
        public decimal Length { get; set; }
        
        /// <summary>
        /// 库位宽度
        /// </summary>
        public decimal Width { get; set; }
        
        /// <summary>
        /// 库位高度
        /// </summary>
        public decimal Height { get; set; }
        
        /// <summary>
        /// 最大承重
        /// </summary>
        public decimal Maxweight { get; set; }
        
        /// <summary>
        /// 库位高度
        /// </summary>
        public Heightlevel Heightlevel { get; set; }
        
        /// <summary>
        /// 优先级
        /// </summary>
        public int Priority { get; set; }

        /// <summary>
        /// 逻辑区域
        /// </summary>
        public int LogicalName { get; set; }

        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
    }
}
