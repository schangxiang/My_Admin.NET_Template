﻿namespace Admin.NET.Core
{
    public static class ExcelDateUtil
    {
        public static long ToInt(DateTime date) 
        {
            var current = Convert.ToDateTime("1900-01-01") ;         
            if (date.Ticks <= current.Ticks) return 1;
            var days = (date - current).Days;
            return days < 59 ? days + 1 : days + 2;
        }

        public static long ToInt(DateTimeOffset date)
        {
            var current = Convert.ToDateTime("1900-01-01");
            if (date.Ticks <= current.Ticks) return 1;
            var days = (date - current).Days;
            return days < 59 ? days + 1 : days + 2;
        }


        public static DateTime ToDateTime(long days)
        {
            var current = Convert.ToDateTime("1900-01-01");
            if(days <= 1) return current;
            return current.AddDays(days < 60 ? days -1 : days - 2);
        }

        public static DateTimeOffset ToDateTimeOffset(long days)
        {
            var current = new DateTimeOffset(Convert.ToDateTime("1900-01-01"));
            if (days <= 1) return current;
            return current.AddDays(days < 60 ? days - 1 : days - 2);
        }
    }
}