import Vuex from 'vuex'
import user from './modules/user.js'
import system from './modules/system.js'
import print from './modules/print.js'

export default function(Vue){
	Vue.use(Vuex);
	
	const store = new Vuex.Store({
	  modules:{
	  	user,system,print
	  }
	})
	
	return  store
}