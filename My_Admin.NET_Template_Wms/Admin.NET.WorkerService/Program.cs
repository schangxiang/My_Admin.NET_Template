using Admin.NET.WorkerService.DbContexts;

IHost host = Host.CreateDefaultBuilder(args).UseWindowsService().Inject()
    .ConfigureServices(services =>
    {
        // ע�����ݿ����
        services.AddDatabaseAccessor(options =>
        {
            options.AddDb<DefaultDbContext>();
        });
    })
    .Build();

await host.RunAsync();
