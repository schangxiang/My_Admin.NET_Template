﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 获取托盘物料分拣参数
    /// </summary>
    public class GetSortInfoInput
    {
        /// <summary>
        /// 容器编号
        /// </summary>
        public string ContainerCode { get; set; }
    }

    /// <summary>
    /// 分页获取出库单
    /// </summary>
    public class SortPdaPageInput : PageInputBase
    {
        /// <summary>
        /// 单据编号
        /// </summary>
        public string No { get; set; }
    }
    /// <summary>
    /// 根据出库单获取分拣明细
    /// </summary>
    public class SortPdaDetailPageInput : PageInputBase
    {
        /// <summary>
        /// 单据编号
        /// </summary>
        public string No { get; set; }
    }
    
}
