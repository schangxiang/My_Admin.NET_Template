﻿using Admin.NET.Core;
using Microsoft.AspNetCore.Mvc;

namespace Admin.NET.Application.CodeGen
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICodeGenService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task AddCodeGen(AddCodeGenInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputs"></param>
        /// <returns></returns>
        Task DeleteCodeGen(List<DeleteCodeGenInput> inputs);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<SysCodeGen> GetCodeGen([FromQuery] QueryCodeGenInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        List<TableColumnOuput> GetColumnList(AddCodeGenInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbContextLocatorName"></param>
        /// <returns></returns>
        List<TableOutput> GetTableList(string dbContextLocatorName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PageResult<SysCodeGen>> QueryCodeGenPageList([FromQuery] CodeGenPageInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<dynamic> RunLocal(SysCodeGen input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<IActionResult> RunDown(long id);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task UpdateCodeGen(UpdateCodeGenInput input);
    }
}