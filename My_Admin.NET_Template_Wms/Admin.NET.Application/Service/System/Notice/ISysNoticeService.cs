﻿using Admin.NET.Core;
using Microsoft.AspNetCore.Mvc;

namespace Admin.NET.Application.Notice
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISysNoticeService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task AddNotice(AddNoticeInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task ChangeStatus(ChangeStatusNoticeInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task DeleteNotice(DeleteNoticeInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<NoticeDetailOutput> GetNotice([FromQuery] QueryNoticeInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PageResult<SysNotice>> QueryNoticePageList([FromQuery] NoticePageInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PageResult<NoticeReceiveOutput>> ReceivedNoticePageList([FromQuery] NoticePageInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task UpdateNotice(UpdateNoticeInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<dynamic> UnReadNoticeList([FromQuery] NoticeInput input);
    }
}