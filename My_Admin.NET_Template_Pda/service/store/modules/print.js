export default {
	namespaced: true,
	state: () => ({
		deviceId:'',
		blueInfo:null
	}),
	mutations: {
		setDeviceId(state,val){
			state.deviceId = val;
		},
		setBlueInfo(state,obj){
			state.blueInfo = obj;
		}
	},
	actions: {
		
	},
	getters: {
		getDeviceId(state){
			return state.deviceId;
		},
		getBlueInfo(state){
			return state.blueInfo;
		}
	}
}