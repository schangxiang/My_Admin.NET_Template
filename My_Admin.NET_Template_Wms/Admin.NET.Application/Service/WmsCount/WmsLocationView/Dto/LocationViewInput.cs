﻿using System.ComponentModel.DataAnnotations;
using Admin.NET.Core.Service;

namespace Admin.NET.Application
{
    /// <summary>
    /// 
    /// </summary>
    public class LocationViewInput
    {
    }

    /// <summary>
    /// 获取库位对应的巷道
    /// </summary>
    public class GetPalceAisleInput
    {
        /// <summary>
        /// 库区Id
        /// </summary>
        [Required(ErrorMessage = "库区Id不能为空")]
        public long Areaid { get; set; }
    }
    /// <summary>
    /// 获取库位和巷道获取对应的排
    /// </summary>
    public class GetPalceAisleRownoInput
    {
        /// <summary>
        /// 库区Id
        /// </summary>
        [Required(ErrorMessage = "库区Id不能为空")]
        public long Areaid { get; set; }

        /// <summary>
        /// 巷道ID
        /// </summary>
        [Required(ErrorMessage = "巷道ID不能为空")]
        public long Aisleid { get; set; }
    }

    /// <summary>
    /// 获取库位列表
    /// </summary>
    public class GetPalceListInput
    {
        /// <summary>
        /// 库区Id
        /// </summary>
        [Required(ErrorMessage = "库区Id不能为空")]
        public long Areaid { get; set; }

        /// <summary>
        /// 巷道Id
        /// </summary>
        [Required(ErrorMessage = "库区Id不能为空")]
        public long Aisleid { get; set; }

        /// <summary>
        /// 排
        /// </summary>
        [Required(ErrorMessage = "库区排不能为空")]
        public int Rowno { get; set; }
    }

    /// <summary>
    /// 获取库位对应的信息
    /// </summary>
    public class GetMaterialDetailInput : BaseId
    {
    }
}
