﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    /// <summary>
    /// 退料入库单详情
    /// </summary>
    [Table("wms_stockreturn_order_details")]
    [Comment("退料入库单详情")]
    public class WmsStockReturnOrderDetails : DEntityBase
    {
        /// <summary>
        /// 单据Id  
        /// </summary>
        [Comment("单据Id")]
        [MaxLength(50)]
        public long OrderId { get; set; }

        /// <summary>
        /// 上位系统单据明细唯一识别码
        /// </summary>
        [Comment("上位系统单据明细唯一识别码")]
        public long OID { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        [Comment("物料编号")]
        [MaxLength(255)]
        public string Materialcode { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        [Comment("物料名称")]
        [MaxLength(255)]
        public string Materialname { get; set; }

        /// <summary>
        /// 基本单位    
        /// </summary>
        [Comment("基本单位")]
        [MaxLength(255)]
        public string Unit { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        [Comment("数量")]
        public decimal Qty { get; set; }

        /// <summary>
        /// 单根长度 
        /// </summary>
        [Comment("单根长度")]
        public decimal Length { get; set; }

        /// <summary>
        /// 件数    
        /// </summary>
        [Comment("件数")]
        public decimal Number { get; set; }

        /// <summary>
        /// SCM批次号    
        /// </summary>
        [Comment("SCM批次号")]
        [MaxLength(255)]
        public string Batchno_SCM { get; set; }

        /// <summary>
        /// WMS批次号    
        /// </summary>
        [Comment("WMS批次号")]
        [MaxLength(255)]
        public string Batchno_WMS { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        [Comment("项目编号")]
        [MaxLength(255)]
        public string ProjectCode { get; set; }

        /// <summary>
        /// TA号
        /// </summary>
        [Comment("TA号")]
        [MaxLength(255)]
        public string TACode { get; set; }

        /// <summary>
        /// 分段号
        /// </summary>
        [Comment("分段号")]
        [MaxLength(255)]
        public string PartCode { get; set; }

        /// <summary>
        /// 单据主表
        /// </summary>
        public WmsStockReturnOrder WmsStockReturnOrder { get; set; }
    }
}
