﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 获取托盘入参
    /// </summary>
    public class GetContainerOutput
    {
        /// <summary>
        /// 托盘号
        /// </summary>
        [Required(ErrorMessage = "托盘号不能为空")]
        public string ContainerCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public WmsContainerDto WmsContainer { get; set; }

        /// <summary>
        /// 物料信息
        /// </summary>
        public List<WmsMaterialContainer> WmsMaterials { get; set; }
    }

    /// <summary>
    /// 仅收货（组盘）成功输出参数
    /// </summary>
    public class GroupDiskOutput
    {
        /// <summary>
        /// 托盘
        /// </summary>
        public string Containercode { get; set; }

        /// <summary>
        /// 组盘单据
        /// </summary>
        public string OrderNo { get; set; }
    }
    /// <summary>
    /// 物料返回参数
    /// </summary>
    public class PadGetMaterialOutput
    {
        /// <summary>
        /// Id  
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 单据Id  
        /// </summary>
        public long OrderId { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string Materialname { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        public string Materialcode { get; set; }

        /// <summary>
        /// 基本单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Qty { get; set; }

        /// <summary>
        /// 单根长度 
        /// </summary>
        public decimal SingleLength { get; set; }

        /// <summary>
        /// 理论重量    
        /// </summary>
        public decimal TheoreticalWeight { get; set; }

        /// <summary>
        /// 不含税单价    
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// 成本金额    
        /// </summary>
        public decimal SumPrice { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 合同编号
        /// </summary>
        public string ContractCode { get; set; }

        /// <summary>
        /// TA号
        /// </summary>
        public string TACode { get; set; }

        /// <summary>
        /// 报关品名
        /// </summary>
        public string TradeName { get; set; }

        /// <summary>
        /// 报关项号
        /// </summary>
        public string ItemNo { get; set; }

        /// <summary>
        /// 报关单位
        /// </summary>
        public string CustomsUnitName { get; set; }

        /// <summary>
        /// 报关数量   
        /// </summary>
        public decimal CustomsNum { get; set; }

        /// <summary>
        /// 报关金额 
        /// </summary>
        public decimal CustomsPrices { get; set; }

        /// <summary>
        /// 特殊要求
        /// </summary>
        public string SpecialNeeds { get; set; }

        /// <summary>
        /// 单据状态
        /// </summary>
        public OrderDetailsStatusEnum OrderStatus { get; set; }
    }
}
