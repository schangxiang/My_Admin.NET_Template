﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;

namespace YaKe.Les.Core.SeedData
{
    //public class WmsMaterialSeedData : IEntitySeedData<WmsMaterial>
    //{
    //    /// <summary>
    //    /// 物料表种子数据
    //    /// </summary>
    //    /// <param name="dbContext"></param>
    //    /// <param name="dbContextLocator"></param>
    //    /// <returns></returns>
    //    public IEnumerable<WmsMaterial> HasData(DbContext dbContext, Type dbContextLocator)
    //    {
    //        return new[]
    //        {
    //            new WmsMaterial{Id=355707111780421,MaterialName="LAPP动力电缆",MaterialNo="3570100022",MaterialBatch="3570100022",InspectionMethod=MaterialInspection.MIANJIAN,MaterialType=MaterialType.YUANCAILIAO,MaterialSpec="OLFLEX CLASSIC 110 5G6",UnitType=UnitType.CHANGDU,UnitNo=UnitnoType.CM,Safeqty=100,Keymaterials=YesOrNot.N,Warranty=(decimal)100,MaxImumqty=(decimal)100,MinstorageAge=(decimal)1,MaxstorageAge=(decimal)1},
    //            new WmsMaterial{Id=358845906632773,MaterialName="气动拧松枪",MaterialNo="3070400120",MaterialBatch="3070400120",InspectionMethod=MaterialInspection.MIANJIAN,MaterialType=MaterialType.YUANCAILIAO,MaterialSpec="16SUP PRO",UnitType=UnitType.CHANGDU,UnitNo=UnitnoType.CM,Safeqty=100,Keymaterials=YesOrNot.N,Warranty=(decimal)10,MaxImumqty=(decimal)100,MinstorageAge=(decimal)1,MaxstorageAge=(decimal)1},
    //            new WmsMaterial{Id=358847125188677,MaterialName="定位销",MaterialNo="3110800160",MaterialBatch="3110800160",InspectionMethod=MaterialInspection.MIANJIAN,MaterialType=MaterialType.BANCHENGPING,MaterialSpec="YDL31-D8-P12-L18-B8-X",UnitType=UnitType.CHANGDU,UnitNo=UnitnoType.CM,Safeqty=100,Keymaterials=YesOrNot.N,Warranty=(decimal)1,MaxImumqty=(decimal)1,MinstorageAge=(decimal)1,MaxstorageAge=(decimal)1},
    //            new WmsMaterial{Id=358847583273029,MaterialName="防爆传感器",MaterialNo="3090200565",MaterialBatch="3090200565",InspectionMethod=MaterialInspection.CHOUJIAN,MaterialType=MaterialType.CHENGPING,MaterialSpec="BND-JA12GT4-24PH-25M-TL",UnitType=UnitType.ZHONGLIANG,UnitNo=UnitnoType.T,Safeqty=1,Keymaterials=YesOrNot.N,Warranty=(decimal)1,MaxImumqty=(decimal)1,MinstorageAge=(decimal)1,MaxstorageAge=(decimal)1},
    //            new WmsMaterial{Id=358847917588549,MaterialName="套筒收纳盒安装板",MaterialNo="2200074125",MaterialBatch="2200074125",InspectionMethod=MaterialInspection.CHOUJIAN,MaterialType=MaterialType.CHENGPING,MaterialSpec="4301015-40-110-04-01",UnitType=UnitType.CHANGDU,UnitNo=UnitnoType.CM,Safeqty=100,Keymaterials=YesOrNot.Y,Warranty=(decimal)1,MaxImumqty=(decimal)1,MinstorageAge=(decimal)1,MaxstorageAge=(decimal)1},
    //            new WmsMaterial{Id=358848226168901,MaterialName="传感器安装支架",MaterialNo="2200074102",MaterialBatch="2200074102",InspectionMethod=MaterialInspection.MIANJIAN,MaterialType=MaterialType.BANCHENGPING,MaterialSpec="40488-020-D086",UnitType=UnitType.ZHONGLIANG,UnitNo=UnitnoType.T,Safeqty=100,Keymaterials=YesOrNot.N,Warranty=(decimal)100,MaxImumqty=(decimal)1,MinstorageAge=(decimal)1,MaxstorageAge=(decimal)1},
    //            new WmsMaterial{Id=358851158249541,MaterialName="电机",MaterialNo="1222202451",MaterialBatch="1222202451",InspectionMethod=MaterialInspection.CHOUJIAN,MaterialType=MaterialType.CHENGPING,MaterialSpec="LA21038-020.01-01.10",UnitType=UnitType.ZHONGLIANG,UnitNo=UnitnoType.T,Safeqty=100,Keymaterials=YesOrNot.Y,Warranty=(decimal)1,MaxImumqty=(decimal)100,MinstorageAge=(decimal)1,MaxstorageAge=(decimal)1},
    //        };
    //    }
    //}
}
