﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 性别
    /// </summary>
    public enum Class
    {
        /// <summary>
        /// 一班
        /// </summary>
        [Description("一班")]
        一班 = 1,

        /// <summary>
        /// 二班
        /// </summary>
        [Description("二班")]
        二班 = 2
    }
    
    
}