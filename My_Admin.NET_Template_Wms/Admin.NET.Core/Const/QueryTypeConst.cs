﻿namespace Admin.NET.Core
{
    /// <summary>
    /// 查询方式
    /// </summary>
    public class QueryTypeConst
    {
        /// <summary>
        /// 等于
        /// </summary>
        public const string 等于 = "==";

        /// <summary>
        /// 模糊
        /// </summary>
        public const string 模糊 = "like";

        /// <summary>
        /// 大于
        /// </summary>
        public const string 大于 = ">";

        /// <summary>
        /// 小于
        /// </summary>
        public const string 小于 = "<";

        /// <summary>
        /// 	不等于
        /// </summary>
        public const string 不等于 = "!=";

        /// <summary>
        /// 大于等于
        /// </summary>
        public const string 大于等于 = ">=";

        /// <summary>
        /// 小于等于
        /// </summary>
        public const string 小于等于 = "<=";

        /// <summary>
        /// 不为空
        /// </summary>
        public const string 不为空 = "isNotNull";

        /// <summary>
        /// 介于
        /// </summary>
        public const string 介于 = "between";
    }
}