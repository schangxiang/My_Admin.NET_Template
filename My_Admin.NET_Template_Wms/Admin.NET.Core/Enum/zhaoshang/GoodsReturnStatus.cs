﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 退货状态
    /// </summary>
    public enum GoodsReturnStatus
    {
        /// <summary>
        /// 未退货
        /// </summary>
        WEITUIHUO = 1,
        /// <summary>
        /// 退货中
        /// </summary>
        TUIHUOZHONG = 2,
        /// <summary>
        /// 已退货
        /// </summary>
        YITUIHUO = 3
    }
}
