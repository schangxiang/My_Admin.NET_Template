﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;

namespace YaKe.Les.Core.SeedData
{
    public class WmsMaterialStockSeedData : IEntitySeedData<WmsMaterialStock>
    {
        /// <summary>
        /// 库存表种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<WmsMaterialStock> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new[]
            {
                new WmsMaterialStock{Id=358853586690117,MaterialNo="N/A",MaterialType=MaterialType.KONGTUO,MaterialBatch="N/A",MaterialName="N/A",MaterialSpec="N/A",InspectionMethod=MaterialInspection.MIANJIAN,UnitType=UnitType.ZHONGLIANG,UnitNo=UnitnoType.T,StockNumber=(decimal)0.00,PlaceCode="N/A",ContainerCode="TM00001",AreaId=0},
                new WmsMaterialStock{Id=358856888787013,MaterialNo="3070400120",MaterialType=MaterialType.YUANCAILIAO,MaterialBatch="3070400120",MaterialName="气动拧松枪",MaterialSpec="16SUP PRO",InspectionMethod=MaterialInspection.MIANJIAN,UnitType=UnitType.CHANGDU,UnitNo=UnitnoType.CM,StockNumber=(decimal)0.00,PlaceCode="N/A",ContainerCode="TM00002",AreaId=0},
                new WmsMaterialStock{Id=358857243504709,MaterialNo="N/A",MaterialType=MaterialType.KONGTUO,MaterialBatch="N/A",MaterialName="N/A",MaterialSpec="N/A",InspectionMethod=MaterialInspection.MIANJIAN,UnitType=UnitType.ZHONGLIANG,UnitNo=UnitnoType.T,StockNumber=(decimal)1.00,PlaceCode="1-1-1-5-1",ContainerCode="TM00003",AreaId=355701739634757},
                new WmsMaterialStock{Id=358857693638725,MaterialNo="3070400120",MaterialType=MaterialType.YUANCAILIAO,MaterialBatch="3070400120",MaterialName="气动拧松枪",MaterialSpec="16SUP PRO",InspectionMethod=MaterialInspection.MIANJIAN,UnitType=UnitType.CHANGDU,UnitNo=UnitnoType.CM,StockNumber=(decimal)100.00,PlaceCode="1-1-1-1-1",ContainerCode="TM00004",AreaId=355701739634757},
                new WmsMaterialStock{Id=358857693646917,MaterialNo="3110800160",MaterialType=MaterialType.BANCHENGPING,MaterialBatch="3110800160",MaterialName="定位销",MaterialSpec="YDL31-D8-P12-L18-B8-X",InspectionMethod=MaterialInspection.MIANJIAN,UnitType=UnitType.CHANGDU,UnitNo=UnitnoType.CM,StockNumber=(decimal)100.00,PlaceCode="1-1-1-1-1",ContainerCode="TM00004",AreaId=355701739634757},
                new WmsMaterialStock{Id=358858262392901,MaterialNo="1222202451",MaterialType=MaterialType.CHENGPING,MaterialBatch="1222202451",MaterialName="电机",MaterialSpec="LA21038-020.01-01.10",InspectionMethod=MaterialInspection.CHOUJIAN,UnitType=UnitType.ZHONGLIANG,UnitNo=UnitnoType.T,StockNumber=(decimal)10.00,PlaceCode="1-1-1-4-1",ContainerCode="TM00005",AreaId=355701739634757},
                new WmsMaterialStock{Id=358858262401093,MaterialNo="2200074102",MaterialType=MaterialType.BANCHENGPING,MaterialBatch="2200074102",MaterialName="传感器安装支架",MaterialSpec="40488-020-D086",InspectionMethod=MaterialInspection.MIANJIAN,UnitType=UnitType.ZHONGLIANG,UnitNo=UnitnoType.T,StockNumber=(decimal)1.00,PlaceCode="1-1-1-4-1",ContainerCode="TM00005",AreaId=355701739634757},
                new WmsMaterialStock{Id=358858262409285,MaterialNo="2200074125",MaterialType=MaterialType.CHENGPING,MaterialBatch="2200074125",MaterialName="套筒收纳盒安装板",MaterialSpec="4301015-40-110-04-01",InspectionMethod=MaterialInspection.CHOUJIAN,UnitType=UnitType.CHANGDU,UnitNo=UnitnoType.CM,StockNumber=(decimal)10.00,PlaceCode="1-1-1-4-1",ContainerCode="TM00005",AreaId=355701739634757}
            };
        }
    }
}
