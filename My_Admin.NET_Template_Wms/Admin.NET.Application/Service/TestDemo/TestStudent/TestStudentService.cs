﻿using Furion.DatabaseAccessor;
using Furion.DatabaseAccessor.Extensions;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using Admin.NET.Core;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Microsoft.AspNetCore.Http;
using System.Text;
using System.Web;
using System.ComponentModel;
using System.Data;
namespace Admin.NET.Application
{
    /// <summary>
    /// 学生信息表1-邵长祥服务
    /// </summary>
    [ApiDescriptionSettings("TestDemo", Name = "TestStudent", Order = 100)]
    [Route("api")]
    public class TestStudentService : ITestStudentService, IDynamicApiController, ITransient
    {
        private readonly IRepository<TestStudent,MasterDbContextLocator> _testStudentRep;
        private readonly IRepository<SysDictType, MasterDbContextLocator> _sysDictTypeRep;
        private readonly IRepository<SysDictData, MasterDbContextLocator> _sysDictDataRep;
        private readonly ISysExcelTemplateService _sysExcelTemplateService;
        private readonly static object _lock = new();

        public TestStudentService(
            IRepository<TestStudent,MasterDbContextLocator> testStudentRep
            ,IRepository<SysDictType, MasterDbContextLocator> sysDictTypeRep
            ,IRepository<SysDictData, MasterDbContextLocator> sysDictDataRep
            ,ISysExcelTemplateService sysExcelTemplateService
        )
        {
            _testStudentRep = testStudentRep;
         _sysDictTypeRep = sysDictTypeRep;
         _sysDictDataRep = sysDictDataRep;
         _sysExcelTemplateService = sysExcelTemplateService;
        }

        /// <summary>
        /// 分页查询学生信息表1-邵长祥
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("TestStudent/page")]
        public async Task<PageResult<TestStudentOutput>> Page([FromQuery] TestStudentSearch input)
        {
            var testStudents = await _testStudentRep.DetachedEntities
                                     .Where(!string.IsNullOrEmpty(input.Name), u => u.Name == input.Name)
                                     .Where(input.Age != null, u => u.Age == input.Age)
                                     .Where(input.StartName != null, u => u.StartName == input.StartName)
                                     .Where(input.Gender != null, u => u.Gender == input.Gender)
                                     .Where(SysCommUtil.ValidateListForQueryTimeArray(input.BrithDate), u => u.BrithDate > Convert.ToDateTime(input.BrithDate[0]).AddSeconds(-1) && u.BrithDate< Convert.ToDateTime(input.BrithDate[1]).AddSeconds(1))
                                     .Where(input.TeacherId != null, u => u.TeacherId == input.TeacherId)
                                     .Where(SysCommUtil.ValidateListForQueryTimeArray(input.CreatedTime), u => u.CreatedTime > Convert.ToDateTime(input.CreatedTime[0]).AddSeconds(-1) && u.CreatedTime< Convert.ToDateTime(input.CreatedTime[1]).AddSeconds(1))
                                     .Where(SysCommUtil.ValidateListForQueryTimeArray(input.UpdatedTime), u => u.UpdatedTime > Convert.ToDateTime(input.UpdatedTime[0]).AddSeconds(-1) && u.UpdatedTime< Convert.ToDateTime(input.UpdatedTime[1]).AddSeconds(1))
                                     .Where(!string.IsNullOrEmpty(input.CreatedUserName), u => u.CreatedUserName == input.CreatedUserName)
                                     .Where(!string.IsNullOrEmpty(input.UpdatedUserName), u => u.UpdatedUserName == input.UpdatedUserName)
                                     .OrderBy(PageInputOrder.OrderBuilder<TestStudentSearch>(input))
                                     .ProjectToType<TestStudentOutput>()
                                     .ToADPagedListAsync(input.PageNo, input.PageSize);
            return testStudents;
        }

        /// <summary>
        /// 不分页查询学生信息表1-邵长祥列表
        /// </summary>
        /// <param name="input">学生信息表1-邵长祥查询参数</param>
        /// <returns>(学生信息表1-邵长祥)实例列表</returns>
        [HttpGet("TestStudent/listNonPage")]
        public async Task<List<TestStudentOutput>> ListNonPageAsync([FromQuery] TestStudentSearchNonPage input)
        {
            var pName = input.Name?.Trim() ?? "";
            var pAge = input.Age;
            var pStartName = input.StartName;
            var pGender = input.Gender;
            var pBrithDate = input.BrithDate;
            var pTeacherId = input.TeacherId;
            var pCreatedTime = input.CreatedTime;
            var pUpdatedTime = input.UpdatedTime;
            var pCreatedUserName = input.CreatedUserName?.Trim() ?? "";
            var pUpdatedUserName = input.UpdatedUserName?.Trim() ?? "";
            var testStudents = await _testStudentRep.DetachedEntities
                .Where(!string.IsNullOrEmpty(pName), u => u.Name == pName)
                .Where(pAge != null, u => u.Age == pAge)
                .Where(pStartName != null, u => u.StartName == pStartName)
                .Where(pGender != null, u => u.Gender == pGender)
           .Where(SysCommUtil.ValidateListForQueryTimeArray(input.BrithDate), u => u.BrithDate > Convert.ToDateTime(input.BrithDate[0]).AddSeconds(-1) && u.BrithDate< Convert.ToDateTime(input.BrithDate[1]).AddSeconds(1))
                .Where(pTeacherId != null, u => u.TeacherId == pTeacherId)
           .Where(SysCommUtil.ValidateListForQueryTimeArray(input.CreatedTime), u => u.CreatedTime > Convert.ToDateTime(input.CreatedTime[0]).AddSeconds(-1) && u.CreatedTime< Convert.ToDateTime(input.CreatedTime[1]).AddSeconds(1))
           .Where(SysCommUtil.ValidateListForQueryTimeArray(input.UpdatedTime), u => u.UpdatedTime > Convert.ToDateTime(input.UpdatedTime[0]).AddSeconds(-1) && u.UpdatedTime< Convert.ToDateTime(input.UpdatedTime[1]).AddSeconds(1))
                .Where(!string.IsNullOrEmpty(pCreatedUserName), u => u.CreatedUserName == pCreatedUserName)
                .Where(!string.IsNullOrEmpty(pUpdatedUserName), u => u.UpdatedUserName == pUpdatedUserName)
            .OrderBy(PageInputOrder.OrderNonPageBuilder(input))
            .ProjectToType<TestStudentOutput>()
            .ToListAsync();
            return testStudents;
        }

         /// <summary>
        /// 获取单个学生信息表1-邵长祥
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("TestStudent/detail")]
        public async Task<TestStudentOutput> Get([FromQuery] QueryeTestStudentInput input)
        {
           return (await _testStudentRep.DetachedEntities.FirstOrDefaultAsync(u => u.Id == input.Id)).Adapt<TestStudentOutput>();
        }

        /// <summary>
        /// 获取学生信息表1-邵长祥列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("TestStudent/list")]
        public async Task<List<TestStudentOutput>> List([FromQuery] TestStudentInput input)
        {
            return await _testStudentRep.DetachedEntities.ProjectToType<TestStudentOutput>().ToListAsync();
        }    

        #region 增、删、改

        /// <summary>
        /// 增加学生信息表1-邵长祥
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("TestStudent/add")]
        public async Task Add(AddTestStudentInput input)
        {
            var testStudent = input.Adapt<TestStudent>();
                        //验证
            await CheckExisit(testStudent);

            testStudent.CreatedUserId = testStudent.UpdatedUserId = SysHelper.GetUserId();
            testStudent.CreatedUserName = testStudent.UpdatedUserName = SysHelper.GetUserName();
            testStudent.CreatedTime = testStudent.UpdatedTime = SysHelper.GetNowTime();
            await _testStudentRep.InsertAsync(testStudent);
        }

        /// <summary>
        /// 删除学生信息表1-邵长祥
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("TestStudent/delete")]
        public async Task Delete(DeleteTestStudentInput input)
        {
            var testStudent = await _testStudentRep.FirstOrDefaultAsync(u => u.Id == input.Id);
            await _testStudentRep.DeleteAsync(testStudent);
            
        }

        /// <summary>
        /// 更新学生信息表1-邵长祥
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("TestStudent/edit")]
        public async Task Update(UpdateTestStudentInput input)
        {
            var isExist = await _testStudentRep.AnyAsync(u => u.Id == input.Id, false);
            if (!isExist) throw Oops.Oh(ErrorCode.D1002);

            var testStudent = input.Adapt<TestStudent>();
            //验证
            await CheckExisit(testStudent,true);

            testStudent.UpdatedUserId = SysHelper.GetUserId();
            testStudent.UpdatedUserName = SysHelper.GetUserName();
            testStudent.UpdatedTime = SysHelper.GetNowTime();
            await _testStudentRep.UpdateAsync(testStudent,ignoreNullValues:true);
        }

        #endregion

        #region 导入

        /// <summary>
        /// Excel模板导入学生信息表1-邵长祥功能
        /// </summary>
        /// <param name="file">Excel模板文件</param>
        /// <returns>导入的记录数</returns>
        [HttpPost("TestStudent/importExcel")]
        public async Task<int> ImportExcelAsync(IFormFile file)
        { 
            int _HeadStartLine = 2;//第1行是说明,第2行是列名
            int _DataStartLine = 3;//第3行开始是数据

            DataTable importDataTable = ExcelUtil.ImportExcelToDataTable(file, _HeadStartLine, _DataStartLine);
            var addList =await CommonImport(importDataTable, _DataStartLine);

            lock (_lock)
            {
                _testStudentRep.InsertAsync(addList);
               
            }
            await Task.CompletedTask;
            return addList.Count;
        }

        /// <summary>
        ///  DataTable转换实体对象列表
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="dataStartLine">模版列名开始行</param>
        /// <returns></returns>
        private async Task<List<TestStudent>> CommonImport(DataTable dataTable, int dataStartLine)
        {

            var details = new List<TestStudent>();
            int index = dataStartLine;//模版列名开始行
            foreach (System.Data.DataRow row in dataTable.Rows)
            {
                index++;

               //导入模版定制化代码（替换模版使用）
                                          
                           var addItem = new TestStudent()
                            {
                               CreatedTime = SysHelper.GetNowTime(),
                               CreatedUserId = SysHelper.GetUserId(),
                               CreatedUserName = SysHelper.GetUserName(),
                               UpdatedTime = SysHelper.GetNowTime(),
                               UpdatedUserId = SysHelper.GetUserId(),
                               UpdatedUserName = SysHelper.GetUserName()
                             };
                          #region 定义变量
                           var _Name = "";//学生姓名
                           var _Age = "";//学生年龄
                           var _StartName = "";//是否在校
                           var _Gender = "";//性别
                           var _BrithDate = "";//出生日期
                           var _TeacherId = "";//关联老师
                          #endregion
                          
                          
                          #region 取值
                           _Name = row["学生姓名"]?.ToString() ;
                           _Age = row["学生年龄"]?.ToString() ;
                           _StartName = row["是否在校"]?.ToString() ;
                           _Gender = row["性别"]?.ToString() ;
                           _BrithDate = row["出生日期"]?.ToString() ;
                           _TeacherId = row["关联老师"]?.ToString() ;
                          #endregion
                          
                          
                          #region 验证
                          
                          if (string.IsNullOrEmpty(_Name))
                          {
                            throw Oops.Oh($"第{index}行[学生姓名]{_Name}不能为空！");
                          }
                          
                          if(!string.IsNullOrEmpty(_Name))
                          {
                                addItem.Name = (string)_Name;
                           }
                          
                          if (string.IsNullOrEmpty(_Age))
                          {
                            throw Oops.Oh($"第{index}行[学生年龄]{_Age}不能为空！");
                          }
                          
                          if(!string.IsNullOrEmpty(_Age))
                          {
                              if (!int.TryParse(_Age, out int outAge)&&!string.IsNullOrEmpty(_Age))
                              {
                                 throw Oops.Oh($"第{index}行[学生年龄]{_Age}值不正确！");
                              }
                              if (outAge <= 0&&!string.IsNullOrEmpty(_Age))
                              {
                                 throw Oops.Oh($"第{index}行[学生年龄]{_Age}值不能小于等于0！");
                              }
                              else
                              {
                                 addItem.Age = outAge;
                              }
                          
                          }
                          
                          if (string.IsNullOrEmpty(_StartName))
                          {
                            throw Oops.Oh($"第{index}行[是否在校]{_StartName}不能为空！");
                          }
                          
                          if(!string.IsNullOrEmpty(_StartName))
                          {
                            if(!_StartName.Equals("是") && !_StartName.Equals("否"))
                             {
                               throw Oops.Oh($"第{index}行[是否在校]{_StartName}值不正确！");
                             }
                             else
                             {
                               bool outStartName = _StartName.Equals("是") ? true : false;
                               addItem.StartName = outStartName;
                             }
                             }
                          
                          
                          if (string.IsNullOrEmpty(_Gender))
                          {
                            throw Oops.Oh($"第{index}行[性别]{_Gender}不能为空！");
                          }
                          
                          if(!string.IsNullOrEmpty(_Gender))
                          {
                          Admin.NET.Core.GenderChinese  enumGender = default(Admin.NET.Core.GenderChinese);
                          
                             if(!Enum.TryParse<Admin.NET.Core.GenderChinese>(_Gender, out enumGender)&&!string.IsNullOrEmpty(_Gender))
                              {
                                throw Oops.Oh($"第{index}行[性别]{_Gender}值不正确！");
                              }
                              else
                              {
                                 addItem.Gender = enumGender;
                              }
                          
                           }
                          
                          if (string.IsNullOrEmpty(_BrithDate))
                          {
                            throw Oops.Oh($"第{index}行[出生日期]{_BrithDate}不能为空！");
                          }
                          
                          if(!string.IsNullOrEmpty(_BrithDate))
                          {
                                addItem.BrithDate =   Convert.ToDateTime(_BrithDate);
                           }
                          if(!string.IsNullOrEmpty(_TeacherId))
                          {
                              if (!long.TryParse(_TeacherId, out long outTeacherId)&&!string.IsNullOrEmpty(_TeacherId))
                              {
                                 throw Oops.Oh($"第{index}行[关联老师]{_TeacherId}值不正确！");
                              }
                              if (outTeacherId <= 0&&!string.IsNullOrEmpty(_TeacherId))
                              {
                                 throw Oops.Oh($"第{index}行[关联老师]{_TeacherId}值不能小于等于0！");
                              }
                              else
                              {
                                 addItem.TeacherId = outTeacherId;
                              }
                          
                          }
                          #endregion
                          

              
                details.Add(addItem);
            }
              //验重
              await CheckExisitForImport(details);
            
            return details;
        }

        /// <summary>
        /// 根据版本下载学生信息表1-邵长祥的Excel导入模板
        /// </summary>
        /// <param name="version">模板版本</param>
        /// <returns>下载的模板文件</returns>
        [HttpGet("TestStudent/downloadExcelTemplate")]
        public IActionResult DownloadExcelTemplate([FromQuery] string version)
        {
            string _path = TemplateConst.EXCEL_TEMPLATEFILE_导入模版路径 + $"\\TestStudent{TemplateConst.EXCEL_TEMPLATEFILE_导入模版名称后缀}.xlsx";
            var fileName = HttpUtility.UrlEncode($"导入模板(学生信息表1-邵长祥).xlsx", Encoding.GetEncoding("UTF-8"));
            return new FileStreamResult(new FileStream(_path, FileMode.Open), "application/octet-stream") { FileDownloadName = fileName };
        }

        #endregion

        #region 私有方法

        /// <summary>
        /// 根据联合主键验证数据是否已存在-数据库
        /// </summary>
        /// <param name="input"></param>
        /// <param name="isEdit"></param>
        /// <returns></returns>
        private async Task CheckExisit( TestStudent input,bool isEdit=false)
        {
           
           bool isExist = false;
           if (!isEdit)//新增
           {
                   //数据是否存在重复
                   isExist = await _testStudentRep.AnyAsync(u =>
                                   u.Name.Equals(input.Name)
                                   &&u.Age.Equals(input.Age)
                   ,false);
          }
           else//编辑 
          {

          
                
                 //当前编辑数据以外是否存在重复
                  isExist = await _testStudentRep.AnyAsync(u => 
                                    u.Id != input.Id
                                    &&u.Name.Equals(input.Name)
                                    &&u.Age.Equals(input.Age)
                    ,false);
               }

            if (isExist) throw Oops.Oh($"验证失败,学生姓名[{input.Name}],学生年龄[{input.Age}]已存在");
       }
        
        /// <summary>
        /// 根据联合主键验证数据是否已存在-导入时验证
        /// </summary>
        /// <param name="inputs"></param>
        /// <returns></returns>
        private async Task CheckExisitForImport(List<TestStudent> inputs)
        { 
            //根据联合主键验证表格中中是否已存在相同数据  
                 if (inputs?.Count <= 0)
                 {
                     throw Oops.Oh($"导入数据不能为空");
                 }
                 //数据是否重复
                 var existExcelItem = inputs.GroupBy(g => new {
                                               g.Name
                                               ,g.Age
                                               })
                                               .Where(g => g.Count() > 1)
                                               .Select(s => new {
                                               s.Key.Name
                                               ,s.Key.Age
                                               }).FirstOrDefault();
                 if (existExcelItem != null)
                 {
                   var item= existExcelItem.Adapt<TestStudent>();
                   throw Oops.Oh($"导入的表格中,学生姓名[{item.Name}],学生年龄[{item.Age}]已存在");
                 }
                      



    //根据联合主键验证数据库中是否已存在相同数据
                 var existDBItem = await _testStudentRep.DetachedEntities.FirstOrDefaultAsync(w=> 
                                                                         inputs.Select(s=>""
                                                                           +s.Name
                                                                           +s.Age
                                                                        )
                                                                        .Contains(""
                                                                         +w.Name
                                                                         +w.Age
                  ));
                  if (existDBItem != null)
                 {
                   var item= existDBItem.Adapt<TestStudent>();
                   throw Oops.Oh($"系统中,学生姓名[{item.Name}],学生年龄[{item.Age}]已存在");
                 }
        }

        #endregion
    }
}
