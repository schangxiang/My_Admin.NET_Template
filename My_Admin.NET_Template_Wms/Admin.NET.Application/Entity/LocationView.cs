﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Furion.DatabaseAccessor;
using iWare.Wms.Core;
using Microsoft.EntityFrameworkCore;

namespace iWare.Wms.Application.Entity
{
    /// <summary>
    /// 
    /// </summary>
    [Table("LocationView")]
    [Comment("")]
    public class LocationView : DEntityBase<long, MasterDbContextLocator>
    {

        /// <summary>
        /// 输入框
        /// </summary>
        [Comment("输入框")][Required][Column(TypeName = "nvarchar(200)")]
        public String input_1668660943053 { get; set; }
        
        /// <summary>
        /// 文本框
        /// </summary>
        [Comment("文本框")][Required][Column(TypeName = "nvarchar(2000)")]
        public String textarea_1668660945922 { get; set; }
        
    }
}
