﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    /// <summary>
    /// 物料托盘关系表
    /// </summary>
    [Table("wms_materialcontainer")]
    [Comment("物料托盘关系表")]
    public class WmsMaterialContainer : DEntityBase
    {
        /// <summary>
        /// 托盘Id
        /// </summary>
        [Comment("托盘Id")]
        [Required]
        public long ContainerId { get; set; }

        /// <summary>
        /// 托盘编码
        /// </summary>
        [Comment("托盘编码")]
        [Required]
        [MaxLength(50)]
        public string ContainerCode { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        [Comment("物料名称")]
        [Required]
        [MaxLength(50)]
        public string MaterialName { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        [Comment("物料编号")]
        [Required]
        [MaxLength(50)]
        public string MaterialNo { get; set; }

        /// <summary>
        /// 物料批次
        /// </summary>
        [Comment("物料批次")]
        [Required]
        [MaxLength(50)]
        public string MaterialBatch { get; set; }

        /// <summary>
        /// 物料规格
        /// </summary>
        [Comment("物料规格")]
        [MaxLength(50)]
        public string MaterialSpec { get; set; }

        /// <summary>
        /// 物料Id
        /// </summary>
        [Comment("物料Id")]
        [Required]
        public long MaterialId { get; set; }

        /// <summary>
        /// 组盘数量
        /// </summary>
        [Comment("组盘数量")]
        [Required]
        public decimal BindQuantity { get; set; }

        /// <summary>
        /// 组盘状态;数据字典
        /// </summary>
        [Comment("组盘状态")]
        [Required]
        public CommonStatus BindStatus { get; set; }

        /// <summary>
        /// 抽检数量
        /// </summary>
        [Comment("抽检数量")]
        public decimal SampleQty { get; set; }

        /// <summary>
        /// 单号
        /// </summary>
        [Comment("单号")]
        [Required]
        [MaxLength(50)]
        public string OrderNo { get; set; }

        /// <summary>
        /// 单据明细Id
        /// </summary>
        [Comment("单据明细Id")]
        [Required]
        public long OrderDetailsId { get; set; }

        /// <summary>
        /// 物料密度
        /// </summary>
        [Comment("物料密度")]
        [Required]
        [MaxLength(50)]
        public string MaterialDensity { get; set; }

        /// <summary>
        /// 托盘信息表
        /// </summary>
        public WmsContainer WmsContainer { get; set; }

        /// <summary>
        /// 物料信息表
        /// </summary>
        public WmsMaterial WmsMaterial { get; set; }
    }
}
