﻿import { axios } from '@/utils/request'

/**
 * 查询叫料
 *
 * @author XJF
 */
export function LesTakematerialsPage (parameter) {
  return axios({
    url: '/LesTakematerials/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加叫料
 *
 * @author XJF
 */
export function LesTakematerialsAdd (data) {
  return axios({
    url: '/lesTakematerials/AddTakematerial',
    method: 'post',
    data: data
  })
}

/**
 * 删除叫料
 *
 * @author XJF
 */
export function LesTakematerialsDelete (ids) {
  return axios({
    url: '/LesTakematerials/delete',
    method: 'post',
    data:{id:ids}
  })
}

/**
 * 获取叫料单号
 *
 * @author yhh
 */
export function GetBillNumber () {
  return axios({
    url: '/lesTakematerials/GetTakeMaterialsNo',
    method: 'get'
  })
}


/**
 * 物料查询
 *
 * @author yhh
 */
export function QueryMaterials (params) {
  return axios({
    url: '/lesTakematerials/MaterialSourcepage',
    method: 'get',
    params:params
  })
}

/**
 * 领料单下发
 *
 * @author yhh
 */
export function Distribute (ids) {
  return axios({
    url: '/lesTakematerials/BatchTakeMaterials',
    method: 'post',
    data:{id:ids}
  })
}

/**
 * 领料单完成
 *
 * @author yhh
 */
export function ForceComplete (id) {
  return axios({
    url: '/lesTakematerials/CompleteOrder',
    method: 'post',
    data:{id:id}
  })
}


/**
 * 领料单详情下发
 *
 * @author yhh
 */
export function DetailDistribute (params) {
  return axios({
    url: '/lesTakematerials/BatchTakeMaterialsDetial',
    method: 'post',
    data:params
  })
}

/**
 * 单据详情
 *
 * @author yhh
 */
export function LesTakeMaterialsDetailPage (params) {
  return axios({
    url: '/lesTakematerials/detailPage',
    method: 'get',
    params:params
  })
}


/**
 * 获取产线
 *
 * @author yhh
 */
export function GetLines() {
  return axios({
    url: '/lesTakematerials/GetProductionlineList',
    method: 'get'
  })
}

/**
 * 根据产线获取工位
 *
 * @author yhh
 */
export function GetStations(id) {
  return axios({
    url: '/lesTakematerials/GetStationList',
    method: 'get',
    params:{id}
  })
}

/**
 * 获取库区下拉
 *
 * @author yhh
 */
export function GetAreas() {
  return axios({
    url: '/lesTakematerials/GetAreaList',
    method: 'get'
  })
}

/**
 * 获取图号下拉
 *
 * @author yhh
 */
export function GetDrawinNos() {
  return axios({
    url: '/lesTakematerials/GetDrawingNoList',
    method: 'get'
  })
}

/**
 * 获取船号下拉
 *
 * @author yhh
 */
export function GetSipNos() {
  return axios({
    url: '/lesTakematerials/GetShipNoList',
    method: 'get'
  })
}

/**
 * 按物料提交
 *
 * @author yhh
 */
export function LesTakematerialsAutoAdd (data) {
  return axios({
    url: '/lesTakematerials/AutoAddTakematerial',
    method: 'post',
    data: data
  })
}