﻿using Admin.NET.Core;
using Admin.NET.Core.Service;

namespace Admin.NET.Application
{
    /// <summary>
    /// 出入库记录查询参数
    /// </summary>
    public class View_AccessDetailsSearch : PageInputBase
    {
        /// <summary>
        /// 任务号
        /// </summary>
        public virtual string TaskNo { get; set; }

        /// <summary>
        /// 任务方式
        /// </summary>
        public virtual TaskModel? TaskModel { get; set; }

        /// <summary>
        /// 任务类型
        /// </summary>
        public virtual TaskType? TaskType { get; set; }

        /// <summary>
        /// 任务等级
        /// </summary>
        public virtual int? TaskLevel { get; set; }

        /// <summary>
        /// 任务状态
        /// </summary>
        public virtual TaskStatusEnum? TaskStatus { get; set; }

        /// <summary>
        /// 来源
        /// </summary>
        public virtual string SourcePlace { get; set; }

        /// <summary>
        /// 目标
        /// </summary>
        public virtual string ToPlace { get; set; }

        /// <summary>
        /// 托盘Id
        /// </summary>
        public virtual long? ContainerId { get; set; }

        /// <summary>
        /// 托盘编码
        /// </summary>
        public virtual string ContainerCode { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public virtual string MaterialName { get; set; }

        /// <summary>
        /// 物料编码
        /// </summary>
        public virtual string MaterialNo { get; set; }

        /// <summary>
        /// 物料批次
        /// </summary>
        public virtual string MaterialBatch { get; set; }

        /// <summary>
        /// 物料规格
        /// </summary>
        public virtual string MaterialSpec { get; set; }

        /// <summary>
        /// 物料Id
        /// </summary>
        public virtual long? MaterialId { get; set; }

        /// <summary>
        /// 操作数量
        /// </summary>
        public virtual decimal? BindQuantity { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public virtual CommonStatus? BindStatus { get; set; }
    }

    /// <summary>
    /// 出入库记录输入参数
    /// </summary>
    public class AccessDetailsInput
    {
        /// <summary>
        /// 任务号
        /// </summary>
        public virtual string TaskNo { get; set; }

        /// <summary>
        /// 任务方式
        /// </summary>
        public virtual TaskModel TaskModel { get; set; }

        /// <summary>
        /// 任务类型
        /// </summary>
        public virtual TaskType TaskType { get; set; }

        /// <summary>
        /// 任务等级
        /// </summary>
        public virtual int TaskLevel { get; set; }

        /// <summary>
        /// 任务状态
        /// </summary>
        public virtual TaskStatusEnum TaskStatus { get; set; }

        /// <summary>
        /// 来源
        /// </summary>
        public virtual string SourcePlace { get; set; }

        /// <summary>
        /// 目标
        /// </summary>
        public virtual string ToPlace { get; set; }

        /// <summary>
        /// 托盘Id
        /// </summary>
        public virtual long ContainerId { get; set; }

        /// <summary>
        /// 托盘编码
        /// </summary>
        public virtual string ContainerCode { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public virtual string MaterialName { get; set; }

        /// <summary>
        /// 物料编码
        /// </summary>
        public virtual string MaterialNo { get; set; }

        /// <summary>
        /// 物料批次
        /// </summary>
        public virtual string MaterialBatch { get; set; }

        /// <summary>
        /// 物料规格
        /// </summary>
        public virtual string MaterialSpec { get; set; }

        /// <summary>
        /// 物料Id
        /// </summary>
        public virtual long MaterialId { get; set; }

        /// <summary>
        /// 操作数量
        /// </summary>
        public virtual decimal BindQuantity { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public virtual CommonStatus BindStatus { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class AddAccessDetailsInput : AccessDetailsInput
    {

    }

    /// <summary>
    /// 
    /// </summary>
    public class DeleteAccessDetailsInput : BaseId
    {

    }

    /// <summary>
    /// 
    /// </summary>
    public class UpdateAccessDetailsInput : AccessDetailsInput
    {

    }

    /// <summary>
    /// 
    /// </summary>
    public class QueryeAccessDetailsInput : BaseId
    {

    }
}
