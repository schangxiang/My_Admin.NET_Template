﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Core
{
    /// <summary>
    /// 编号规则
    /// </summary> 
    [Table("les_serial_rule_detail")]
    [Comment("编号规则")]
    public class LesSerialRuleDetail : DEntityBase
   {
        /// <summary>
        /// 序号类别
        /// </summary>
        [Comment("序号类别")]
        [MaxLength(50)]
         public string SerialType { get; set; }

        /// <summary>
        /// 项次
        /// </summary>
        [Comment("项次")]
        public int ItemNo { get; set; }

        /// <summary>
        /// 资料来源
        /// </summary>
        [Comment("资料来源")]
        public string SourceType { get; set; }

        /// <summary>
        /// 长度
        /// </summary>
        [Comment("长度")]
        public int SerialLength { get; set; }

        /// <summary>
        /// 撷取码数(起)
        /// </summary>
        [Comment("撷取码数(起)")]
        public int SearchStart{ get; set; }

        /// <summary>
        /// 进位类型
        /// </summary>
        [Comment("进位类型")]
        public int DecimalType { get; set; }

        /// <summary>
        /// 使用者自定义
        /// </summary>
        [Comment("使用者自定义"), MaxLength(50)]
        public string UserDefine { get; set; }

        /// <summary>
        /// 序号编码
        /// </summary>
        [Comment("序号编码")]
        public YesOrNot SerialCodeFlag { get; set; } = YesOrNot.N;

        /// <summary>
        /// 说明
        /// </summary>
        [Comment("说明"), MaxLength(300)]
        public string Description { get; set; }

        /// <summary>
        /// 序号类别编号
        /// </summary>
        [Comment("序号类别编号")]
        [MaxLength(50)]
        public string SerialTypeNo { get; set; }

        /// <summary>
        /// 撷取
        /// </summary>
        [Comment("撷取")]
        [MaxLength(50)]
        public int GetData { get; set; }

        /// <summary>
        /// 自动补零
        /// </summary>
        [Comment("自动补零")]
        public int PadLeft { get; set; }   

}
}
