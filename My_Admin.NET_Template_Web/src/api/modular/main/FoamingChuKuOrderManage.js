﻿import { axios } from '@/utils/request'

/**
 * 查询发泡车间出库单据（熟化库）
 *
 * @author yrj
 */
export function FoamingChuKuOrderPage (parameter) {
  return axios({
    url: '/FoamingChuKuOrder/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 获取出库单据详情
 *
 * @author yuhuanhuan
 */
export function FoamingChuKuOrderDetailPage (parameter) {
  return axios({
    url: '/FoamingChuKuOrder/detailPage',
    method: 'get',
    params: parameter
  })
}

/**
 * 编辑时获取出库单信息
 *
 * @author yuhuanhuan
 */
export function FoamingChuKuOrderDetailEdit (parameter) {
  return axios({
    url: '/FoamingChuKuOrder/editDetail',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加发泡车间出库单据（熟化库）
 *
 * @author yrj
 */
export function FoamingChuKuOrderAdd (parameter) {
  return axios({
    url: '/FoamingChuKuOrder/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑发泡车间出库单据（熟化库）
 *
 * @author yrj
 */
export function FoamingChuKuOrderEdit (parameter) {
  return axios({
    url: '/FoamingChuKuOrder/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除发泡车间出库单据（熟化库）
 *
 * @author yrj
 */
export function FoamingChuKuOrderDelete (parameter) {
  return axios({
    url: '/FoamingChuKuOrder/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 批量下发
 *
 * @author yuhuanhuan
 */
export function FoamingChuKuOrderMulDistribute (ids) {
  return axios({
    url: '/FoamingChuKuOrder/batchDistribute',
    method: 'post',
    data: {id:ids}
  })
}

/**
 * 批量撤回
 *
 * @author yuhuanhuan
 */
export function FoamingChuKuOrderMulWithdraw (ids) {
  return axios({
    url: '/FoamingChuKuOrder/batchWithdraw',
    method: 'post',
    data: {id:ids}
  })
}


/**
 * 分页查询可出库物料信息
 *
 * @author yuhuanhuan
 */
export function FoamingChuKuOrderStockPage (parameter) {
  return axios({
    url: '/FoamingChuKuOrder/stockPage',
    method: 'get',
    params: parameter
  })
}


/**
 * 单据完成
 *
 * @author yuhuanhuan
 */
export function FoamingChuKuOrderOutForceComplete (id) {
  return axios({
    url: '/FoamingChuKuOrder/OutForceComplete',
    method: 'post',
    data: {id}
  })
}

/**
 * 获取单据号
 *
 * @author yuhuanhuan
 */
export function FoamingChuKuOrderGetNo () {
  return axios({
    url: '/FoamingChuKuOrder/GetChuKuOrderNo',
    method: 'post'
  })
}


