﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    [Table("base_SerialRuleDetail")]
    [Comment("单据号生成规则配置表")]
    public class BaseSerialRuleDetail : DEntityBase
    {
        /// <summary>
        /// 单据号编号,这个不是业务字段，纯粹是 标记下编号
        /// </summary>
        [Comment("单据号编号")]
        [Required, MaxLength(50)]
        public string SerialRuleNo { get; set; } = null!;

        /// <summary>
        /// 单据号类型
        /// </summary>
        [Comment("单据号类型")]
        [Required]
        public int SerialType { get; set; }

        /// <summary>
        /// 生成顺序号
        /// </summary>
        [Comment("生成顺序号")]
        [Required]
        public int ItemNo { get; set; }

        /// <summary>
        /// 组合类型
        /// </summary>
        [Comment("组合类型")]
        [Required, MaxLength(50)]
        public string SourceType { get; set; } = null!;

        /// <summary>
        /// 生成内容长度
        /// </summary>
        [Comment("组合类型长度")]
        [Required]
        public int SerialLength { get; set; }

        public int? SearchStart { get; set; }

        public int? DecimalType { get; set; }

        /// <summary>
        /// 单据号字符串内容
        /// </summary>
        [Comment("字符串内容")]
        [MaxLength(50)]
        public string? UserDefine { get; set; }

        public int? SerialCodeFlag { get; set; }

        /// <summary>
        /// 类型描述
        /// </summary>
        [Comment("类型描述")]
        [MaxLength(250)]
        public string? Description { get; set; }

        public string? SerialTypeNo { get; set; }

        /// <summary>
        /// 是否每天都重置（0：不重置，1：每天都重置）
        /// </summary>
        [Comment("是否每天都重置（0：不重置，1：每天都重置）")]
        public int? GetData { get; set; }

        /// <summary>
        /// 生成内容长度
        /// </summary>
        [Comment("文本靠边方向")]
        [Required]
        public int PadLeft { get; set; }


        public int? IssueStatus { get; set; }

    }
}
