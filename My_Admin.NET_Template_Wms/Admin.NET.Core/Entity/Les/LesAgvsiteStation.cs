﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace YaKe.Les.Core
{
    /// <summary>
    /// AGV站点工位关系表
    /// </summary>
    [Table("les_agvsitestation")]
    [Comment("AGV站点工位关系表")]
    public class LesAgvsiteStation : DEntityBase
    {
        /// <summary>
        /// AGV站点Id
        /// </summary>
        [Comment("AGV站点Id")]
        [Required]
        [MaxLength(50)]
        public long SiteId{ get; set; }

        /// <summary>
        /// AGV站点Code
        /// </summary>
        [Comment("AGV站点Code")]
        [Required]
        [MaxLength(50)]
        public string SiteCode { get; set; }

        /// <summary>
        /// 工位Id
        /// </summary>
        [Comment("工位Id")]
        [Required]
        [MaxLength(50)]
        public long StationId { get; set; }

        /// <summary>
        /// 工位编码
        /// </summary>
        [Comment("工位编码")]
        [Required]
        [MaxLength(50)]
        public string StationCode { get; set; }

        /// <summary>
        /// AGV站点表
        /// </summary>
        public LesAgvsite LesAgvsite { get; set; }

        /// <summary>
        /// 工位表
        /// </summary>
        public LesStation LesStation { get; set; }
    }
}
