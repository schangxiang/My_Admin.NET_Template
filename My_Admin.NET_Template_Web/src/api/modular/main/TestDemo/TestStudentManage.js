﻿import { axios } from '@/utils/request'

/**
 * 查询学生信息表1-邵长祥
 *
 * @author 邵长祥
 */
export function TestStudentPage (parameter) {
  return axios({
    url: '/TestStudent/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 学生信息表1-邵长祥列表
 *
 * @author 邵长祥
 */
export function TestStudentList (parameter) {
  return axios({
    url: '/TestStudent/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加学生信息表1-邵长祥
 *
 * @author 邵长祥
 */
export function TestStudentAdd (parameter) {
  return axios({
    url: '/TestStudent/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑学生信息表1-邵长祥
 *
 * @author 邵长祥
 */
export function TestStudentEdit (parameter) {
  return axios({
    url: '/TestStudent/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除学生信息表1-邵长祥
 *
 * @author 邵长祥
 */
export function TestStudentDelete (parameter) {
  return axios({
    url: '/TestStudent/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出学生信息表1-邵长祥的Excel文件
 *
 * @author 邵长祥
 */
export function TestStudentToExcel (parameter) {
  return axios({
    url: '/TestStudent/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入学生信息表1-邵长祥的Excel文件
 *
 * @author 邵长祥
 */
export function TestStudentImportExcel (data, parameter) {
  return axios({
    url: '/TestStudent/importExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载学生信息表1-邵长祥的Excel导入模板
 *
 * @author 邵长祥
 */
export function TestStudentDownloadExcelTemplate(parameter) {
  return axios({
    url: '/TestStudent/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}



