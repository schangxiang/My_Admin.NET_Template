﻿using Admin.NET.Core;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Admin.NET.Application
{
    public interface IWmsPdaPowerService
    {
        Task Add(AddWmsPdaPowerInput input);
        Task Delete(DeleteWmsPdaPowerInput input);
        Task<WmsPdaPowerOutput> Get([FromQuery] QueryeWmsPdaPowerInput input);
        Task<List<WmsPdaPowerOutput>> List([FromQuery] WmsPdaPowerInput input);
        Task<PageResult<WmsPdaPowerOutput>> Page([FromQuery] WmsPdaPowerSearch input);
        Task Update(UpdateWmsPdaPowerInput input);

        Task<List<WmsPdaPowerOutput>> ListNonPageAsync([FromQuery] WmsPdaPowerSearchNonPage input);

        Task<int> FromExcelAsync(IFormFile file, [FromQuery] ImportExcelType importExcelType);

        Task<IActionResult> DownloadExcelTemplate(string version);
    }
}