﻿import { axios } from '@/utils/request'

/**
 * 查询组装车间成品完工入库
 *
 * @author yc
 */
export function ZZProductFinishedOrderPage (parameter) {
  return axios({
    url: '/ZZProductFinishedOrder/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 组装车间成品完工入库列表
 *
 * @author yc
 */
export function ZZProductFinishedOrderList (parameter) {
  return axios({
    url: '/ZZProductFinishedOrder/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加组装车间成品完工入库
 *
 * @author yc
 */
export function ZZProductFinishedOrderAdd (parameter) {
  return axios({
    url: '/ZZProductFinishedOrder/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑组装车间成品完工入库
 *
 * @author yc
 */
export function ZZProductFinishedOrderEdit (parameter) {
  return axios({
    url: '/ZZProductFinishedOrder/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除组装车间成品完工入库
 *
 * @author yc
 */
export function ZZProductFinishedOrderDelete (parameter) {
  return axios({
    url: '/ZZProductFinishedOrder/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出组装车间成品完工入库的Excel文件
 *
 * @author yc
 */
export function ZZProductFinishedOrderToExcel (parameter) {
  return axios({
    url: '/ZZProductFinishedOrder/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入组装车间成品完工入库的Excel文件
 *
 * @author yc
 */
export function ZZProductFinishedOrderFromExcel (data, parameter) {
  return axios({
    url: '/ZZProductFinishedOrder/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载组装车间成品完工入库的Excel导入模板
 *
 * @author yc
 */
export function ZZProductFinishedOrderDownloadExcelTemplate(parameter) {
  return axios({
    url: '/ZZProductFinishedOrder/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 单据大类下拉
 *
 * @author yhh
 */
export function ZZProductFinishedOrderLargeCategories() {
  return axios({
    url: '/ZZProductFinishedOrder/OrderTypeLargeCategory',
    method: 'get'
  })
}

/**
 * 单据小类下拉
 *
 * @author yhh
 */
export function ZZProductFinishedOrderSubclass(id) {
  return axios({
    url: '/ZZProductFinishedOrder/OrderTypeSubclass',
    method: 'get',
    params:{id}
  })
}


/**
 * 完工入库单详情
 *
 * @author yhh
 */
export function ZZProductFinishedOrderDetailPage (parameter) {
  return axios({
    url: '/ZZProductFinishedOrder/DetailsPage',
    method: 'get',
    params: parameter
  })
}



