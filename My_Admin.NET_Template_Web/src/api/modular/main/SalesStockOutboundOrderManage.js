﻿import { axios } from '@/utils/request'

/**
 * 查询销售出库
 *
 * @author yc
 */
export function SalesStockOutboundOrderPage (parameter) {
  return axios({
    url: '/SalesStockOutboundOrder/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 销售出库列表
 *
 * @author yc
 */
export function SalesStockOutboundOrderList (parameter) {
  return axios({
    url: '/SalesStockOutboundOrder/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加销售出库
 *
 * @author yc
 */
export function SalesStockOutboundOrderAdd (parameter) {
  return axios({
    url: '/SalesStockOutboundOrder/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑销售出库
 *
 * @author yc
 */
export function SalesStockOutboundOrderEdit (parameter) {
  return axios({
    url: '/SalesStockOutboundOrder/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除销售出库
 *
 * @author yc
 */
export function SalesStockOutboundOrderDelete (parameter) {
  return axios({
    url: '/SalesStockOutboundOrder/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出销售出库的Excel文件
 *
 * @author yc
 */
export function SalesStockOutboundOrderToExcel (parameter) {
  return axios({
    url: '/SalesStockOutboundOrder/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入销售出库的Excel文件
 *
 * @author yc
 */
export function SalesStockOutboundOrderFromExcel (data, parameter) {
  return axios({
    url: '/SalesStockOutboundOrder/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载销售出库的Excel导入模板
 *
 * @author yc
 */
export function SalesStockOutboundOrderDownloadExcelTemplate(parameter) {
  return axios({
    url: '/SalesStockOutboundOrder/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 单据发货仓库列表
 *
 * @author yhh
 */
export function SalesStockOutboundOrderGetAreas() {
  return axios({
    url: '/SalesStockOutboundOrder/SalesStockOutboundOrderGetAreaList',
    method: 'get'
  })
}


/**
 * 销售出库单详情
 *
 * @author yhh
 */
export function SalesStockOutboundOrderDetailPage (parameter) {
  return axios({
    url: '/SalesStockOutboundOrder/DetailsPage',
    method: 'get',
    params: parameter
  })
}

/**
 * 获取单号
 *
 * @author yhh
 */
export function GetBillNumber () {
  return axios({
    url: '/SalesStockOutboundOrder/GetSalesStockOutboundOrderNoo',
    method: 'get'
  })
}

/**
 * 获取可出库的明细列表
 *
 * @author yhh
 */
export function GetCouldOutMaterials(params) {
  return axios({
    url: '/SalesStockOutboundOrder/GetMaterialList',
    method: 'get',
    params: params
  })
}

/**
 * 编辑单据详情数量
 *
 * @author yhh
 */
export function SalesStockOutboundOrderEditDetailNumber (parameter) {
  return axios({
    url: '/SalesStockOutboundOrder/UpdateSalesStockOutboundDetail',
    method: 'post',
    data: parameter
  })
}

/**
 * 下发
 *
 * @author yhh
 */
export function SalesStockOutboundOrderMulDistribute (ids) {
  return axios({
    url: '/SalesStockOutboundOrder/BatchSalesStockOutboundOrder',
    method: 'post',
    data: {id:ids}
  })
}



