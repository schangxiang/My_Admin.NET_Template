﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Admin.NET.Core;
using Admin.NET.Core.Entity;

namespace Admin.NET.Application
{
    /// <summary>
    /// 出入库记录服务
    /// </summary>
    [ApiDescriptionSettings("仓库作业", Name = "View_AccessDetails", Order = 105)]
    [Route("api/[Controller]")]
    public class AccessDetailsService : IDynamicApiController, ITransient
    {
        private readonly IRepository<VAccessDetails, MasterDbContextLocator> _vAccessDetailsRep;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="vAccessDetailsRep"></param>
        public AccessDetailsService(
            IRepository<VAccessDetails, MasterDbContextLocator> vAccessDetailsRep
        )
        {
            _vAccessDetailsRep = vAccessDetailsRep;
        }
        /// <summary>
        /// 分页查询出入库记录
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("page")]
        public async Task<PageResult<View_AccessDetailsOutput>> Page([FromQuery] View_AccessDetailsSearch input)
        {
            var view_AccessDetailss = await _vAccessDetailsRep.DetachedEntities
                .Where(!string.IsNullOrEmpty(input.TaskNo), u => EF.Functions.Like(u.TaskNo, $"%{input.TaskNo.Trim()}%"))
                .Where(input.TaskModel != null, u => u.TaskModel == input.TaskModel)
                .Where(input.TaskType != null, u => u.TaskType == input.TaskType)
                .Where(u => u.TaskStatus == TaskStatusEnum.WANCHENG || u.TaskStatus == TaskStatusEnum.ZHIXINGZHONG)
                .Where(u => u.TaskNo != null)
                .Where(!string.IsNullOrEmpty(input.SourcePlace), u => EF.Functions.Like(u.SourcePlace, $"%{input.SourcePlace.Trim()}%"))
                .Where(!string.IsNullOrEmpty(input.ToPlace), u => EF.Functions.Like(u.ToPlace, $"%{input.ToPlace.Trim()}%"))
                .Where(!string.IsNullOrEmpty(input.MaterialNo), u => EF.Functions.Like(u.MaterialNo, $"%{input.MaterialNo.Trim()}%"))
                .Where(input.SearchBeginTime != null && input.SearchEndTime != null, u => u.CreatedTime >= DateTime.Parse(input.SearchBeginTime.Trim()) && u.CreatedTime <= DateTime.Parse(input.SearchEndTime.Trim()))
                .OrderBy(PageInputOrder.OrderBuilder<View_AccessDetailsSearch>(input))
                .ProjectToType<View_AccessDetailsOutput>()
                .ToADPagedListAsync(input.PageNo, input.PageSize);
            return view_AccessDetailss;
        }
    }
}
