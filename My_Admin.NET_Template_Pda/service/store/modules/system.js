export default {
	namespaced: true,
	state: () => ({
		defaulthost:'',
		authhost:'',
		version:'',
		safety:{
			top:0,
			bottom:0,
			left:0,
			right:0
		},
		enum:null
	}),
	mutations: {
		setdefaultHost(state,val){
			uni.setStorageSync('system_host',val);
			state.defaulthost = val;
		},
		setauthHost(state,val){
			uni.setStorageSync('system_login_host',val);
			state.authhost = val;
		},
		setVersion(state,val){
			uni.setStorageSync('system_app_version',val);
			state.version = val;
		},
		setSafety(state,obj){
			state.safety = obj;
		},
		setEnum(state,obj){
			uni.setStorageSync('system_enum',JSON.stringify(obj));
			state.enum = obj;
		}
	},
	actions: {
		
	},
	getters: {
		getdefaultHost(state){
			let res = state.defaulthost;
			if (!res) {
				let temp = uni.getStorageSync('system_host');
				if (temp) {
					res = temp;
					state.defaulthost = res;
				}
			}
			return res;
		},
		getauthHost(state){
			let res = state.authhost;
			if (!res) {
				let temp = uni.getStorageSync('system_login_host');
				if (temp) {
					res = temp;
					state.authhost = res;
				}
			}
			return res;
		},
		getVersion(state){
			let res = state.version;
			if (!res) {
				let temp = uni.getStorageSync('system_app_version');
				if (temp) {
					res = temp;
					state.version = res;
				}
			}
			return res;
		},
		getSafety(state){
			return state.safety
		},
		getEnum(state){
			let res = state.enum;
			if (!res) {
				let temp = uni.getStorageSync('system_enum');
				if (temp) {
					try{
						res = JSON.parse(temp)
						state.enum = res;
					}catch(e){
						//TODO handle the exception
					}
				}
			}
			if (!res) res = {}
			return res;
		}
	}
}