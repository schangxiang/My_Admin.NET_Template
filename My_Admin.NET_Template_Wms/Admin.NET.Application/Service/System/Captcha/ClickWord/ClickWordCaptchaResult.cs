﻿namespace Admin.NET.Application
{
    /// <summary>
    /// 验证码输出参数
    /// </summary>
    public class ClickWordCaptchaResult
    {
        /// <summary>
        /// 
        /// </summary>
        public string RepCode { get; set; } = "0000";

        /// <summary>
        /// 
        /// </summary>
        public string RepMsg { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public RepData RepData { get; set; } = new RepData();

        /// <summary>
        /// 
        /// </summary>
        public bool Error { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Success { get; set; } = true;
    }
    
    /// <summary>
    /// 
    /// </summary>
    public class RepData
    {
        /// <summary>
        /// 
        /// </summary>
        public string CaptchaId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CaptchaType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CaptchaOriginalPath { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CaptchaFontType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CaptchaFontSize { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string SecretKey { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string OriginalImageBase64 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<PointPosModel> Point { get; set; } = new List<PointPosModel>();

        /// <summary>
        /// 
        /// </summary>
        public string JigsawImageBase64 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<string> WordList { get; set; } = new List<string>();

        /// <summary>
        /// 
        /// </summary>
        public string PointList { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string PointJson { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Token { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public bool Result { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CaptchaVerification { get; set; }
    }
}