﻿using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 单据类型维护表输出参数
    /// </summary>
    public class WmsOrderTypeDto
    {
        /// <summary>
        /// 父Id
        /// </summary>
        public long Pid { get; set; }
        
        /// <summary>
        /// 父Ids
        /// </summary>
        public string Pids { get; set; }
        
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }
        
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        
        /// <summary>
        /// 所属车间
        /// </summary>
        public LesWorkShopType LesWorkShopType { get; set; }
        
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        
        /// <summary>
        /// 状态
        /// </summary>
        public CommonStatus Status { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
    }
}
