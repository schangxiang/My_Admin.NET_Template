﻿import { axios } from '@/utils/request'

/**
 * 查询学生信息表Liuying
 *
 * @author liuying
 */
export function TestStudent2Page (parameter) {
  return axios({
    url: '/TestStudent2/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 学生信息表Liuying列表
 *
 * @author liuying
 */
export function TestStudent2List (parameter) {
  return axios({
    url: '/TestStudent2/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加学生信息表Liuying
 *
 * @author liuying
 */
export function TestStudent2Add (parameter) {
  return axios({
    url: '/TestStudent2/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑学生信息表Liuying
 *
 * @author liuying
 */
export function TestStudent2Edit (parameter) {
  return axios({
    url: '/TestStudent2/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除学生信息表Liuying
 *
 * @author liuying
 */
export function TestStudent2Delete (parameter) {
  return axios({
    url: '/TestStudent2/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出学生信息表Liuying的Excel文件
 *
 * @author liuying
 */
export function TestStudent2ToExcel (parameter) {
  return axios({
    url: '/TestStudent2/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入学生信息表Liuying的Excel文件
 *
 * @author liuying
 */
export function TestStudent2FromExcel (data, parameter) {
  return axios({
    url: '/TestStudent2/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载学生信息表Liuying的Excel导入模板
 *
 * @author liuying
 */
export function TestStudent2DownloadExcelTemplate(parameter) {
  return axios({
    url: '/TestStudent2/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}



