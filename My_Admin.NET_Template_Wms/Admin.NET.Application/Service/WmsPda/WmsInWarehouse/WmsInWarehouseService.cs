﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Admin.NET.Core;
using Yitter.IdGenerator;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Admin.NET.Application
{
    /// <summary>
    /// Pda物料入库
    /// </summary>
    [ApiDescriptionSettings("Pda物料入库", Name = "WmsInWarehouse", Order = 100)]
    [Route("api/[Controller]")]
    public class WmsInWarehouseService : IDynamicApiController, ITransient
    {
        private readonly IRepository<WmsOrder, MasterDbContextLocator> _wmsOrderRep;
        private readonly IRepository<WmsOrderDetails, MasterDbContextLocator> _wmsOrderDetailsRep;
        private readonly IRepository<WmsTask, MasterDbContextLocator> _wmsTaskRep;
        private readonly IRepository<WmsPlace, MasterDbContextLocator> _wmsPlaceRep;
        private readonly IRepository<WmsArea, MasterDbContextLocator> _wmsAreaRep;
        private readonly IRepository<WmsWarehouseEntrance, MasterDbContextLocator> _wmsWarehouseEntranceRep;
        private readonly IRepository<WmsMaterialContainer, MasterDbContextLocator> _wmsMaterialContainerRep;
        private readonly IRepository<WmsContainer, MasterDbContextLocator> _wmsContainerRep;
        private readonly IRepository<WmsContainerPlace, MasterDbContextLocator> _wmsContainerPlaceRep;
        private readonly IRepository<WmsMaterialStock, MasterDbContextLocator> _wmsMaterialStockRep;
        private readonly IRepository<WmsOrderType, MasterDbContextLocator> _wmsOrderTypeRep;

        /// <summary>
        /// 构造函数
        /// </summary>
        public WmsInWarehouseService(
            IRepository<WmsOrder, MasterDbContextLocator> wmsOrderRep,
            IRepository<WmsOrderDetails, MasterDbContextLocator> wmsOrderDetailsRep,
            IRepository<WmsTask, MasterDbContextLocator> wmsTaskRep,
            IRepository<WmsPlace, MasterDbContextLocator> wmsPlaceRep,
            IRepository<WmsArea, MasterDbContextLocator> wmsAreaRep,
            IRepository<WmsWarehouseEntrance, MasterDbContextLocator> wmsWarehouseEntranceRep,
            IRepository<WmsMaterialContainer, MasterDbContextLocator> wmsMaterialContainerRep,
            IRepository<WmsContainer, MasterDbContextLocator> wmsContainerRep,
            IRepository<WmsContainerPlace, MasterDbContextLocator> wmsContainerPlaceRep,
            IRepository<WmsMaterialStock, MasterDbContextLocator> wmsMaterialStockRep,
            IRepository<WmsOrderType, MasterDbContextLocator> wmsOrderTypeRep
        )
        {
            _wmsOrderRep = wmsOrderRep;
            _wmsOrderDetailsRep = wmsOrderDetailsRep;
            _wmsTaskRep = wmsTaskRep;
            _wmsPlaceRep = wmsPlaceRep;
            _wmsAreaRep = wmsAreaRep;
            _wmsWarehouseEntranceRep = wmsWarehouseEntranceRep;
            _wmsMaterialContainerRep = wmsMaterialContainerRep;
            _wmsContainerRep = wmsContainerRep;
            _wmsContainerPlaceRep = wmsContainerPlaceRep;
            _wmsMaterialStockRep = wmsMaterialStockRep;
            _wmsOrderTypeRep = wmsOrderTypeRep;
        }

        /// <summary>
        /// 获取托盘
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetContainer")]
        [UnifyResult(typeof(GetContainerOutput))]
        public async Task<object> CheckTray([FromQuery] GetContainerInput input)
        {
            var containerModel = await _wmsContainerRep.DetachedEntities.Where(z => z.ContainerCode == input.ContainerCode).FirstOrDefaultAsync();
            //如果托盘不存在则添加一个
            if (containerModel == null) {
                var ContainerInfo = input.Adapt<WmsContainer>();
                ContainerInfo.ContainerStatus = ContainerStatus.KOUXIAN;
                ContainerInfo.AssetNo = "N/A";
                ContainerInfo.ContainerCategory = ContainerCategory.GZP;
                ContainerInfo.ErpNo = "N/A";
                ContainerInfo.IsVirtually = YesOrNot.N;
                ContainerInfo.WorkShopType = LesWorkShopType.FAPAOCHEJIAN;
                await _wmsContainerRep.InsertNowAsync(ContainerInfo);
                containerModel = ContainerInfo;
            }

            var isExit = await _wmsTaskRep.AnyAsync(p => p.ContainerCode == containerModel.ContainerCode && p.TaskStatus != TaskStatusEnum.WANCHENG  && p.TaskStatus != TaskStatusEnum.QUXIAO);
            if (isExit) return XnRestfulResultProvider.RESTfulMesaage("托盘存在未完成任务!");

            isExit = await _wmsContainerPlaceRep.AnyAsync(p => p.ContainerCode == containerModel.ContainerCode && p.ContainerPlaceStatus == CommonStatus.ENABLE);
            if (isExit) return XnRestfulResultProvider.RESTfulMesaage("托盘存在库位中!");

            var GetContainerOutputModel = new GetContainerOutput()
            {
                WmsContainer = containerModel.Adapt<WmsContainerDto>(),
                ContainerCode = containerModel.ContainerCode,
            };
            //获取组盘数据
            GetContainerOutputModel.WmsMaterials = await _wmsMaterialContainerRep
                    .Where(p => p.ContainerCode == containerModel.ContainerCode && p.BindStatus == CommonStatus.ENABLE)
                    .OrderByDescending(p => p.CreatedTime)
                    .ProjectToType<WmsMaterialContainer>().ToListAsync();
            return XnRestfulResultProvider.RESTfulResult(GetContainerOutputModel);
        }

        /// <summary>
        /// 通过单据号获取明细
        /// </summary>
        /// <returns></returns>
        [HttpGet("PadGetMaterial")]
        [UnifyResult(typeof(List<PadGetMaterialOutput>))]
        public async Task<object> PadGetMaterial([FromQuery] PadGetMaterialInput input)
        {
            // 查询单据小类是“生产入库”的Id
            var wmsOrderType = await _wmsOrderTypeRep.FirstOrDefaultAsync(z => z.Name.Contains("生产入库"));
            //获取主表
            var wmsOrders = await _wmsOrderRep.DetachedEntities
                                     .Where(u => u.NO == input.NO && u.OrderStatus == OrderStatusEnum.YIXIAFA && u.OrderLargeCategory == wmsOrderType.Pid && u.OrderSubclass == wmsOrderType.Id)
                                     .FirstOrDefaultAsync();
            if (wmsOrders == null) return XnRestfulResultProvider.RESTfulMesaage("单据不存在或未下发!");
            var wmsOrderDetailList =await _wmsOrderDetailsRep.Where(x=>x.OrderId == wmsOrders.Id)
                                          .ProjectToType<PadGetMaterialOutput>()
                                          .ToADPagedListAsync(input.PageNo, input.PageSize);
            return XnRestfulResultProvider.RESTfulResult(wmsOrderDetailList);
        }


        /// <summary>
        /// 执行仅收货操作（组盘）
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("GroupDisk")]
        [UnitOfWork]
        public async Task<GroupDiskOutput> GroupDisk(GroupDiskInput input)
        {
            //托盘信息
            var containerModel = await _wmsContainerRep.FirstOrDefaultAsync(n => n.ContainerCode == input.ContainerCode && n.ContainerStatus != ContainerStatus.JINYONG);
            if (containerModel == null) throw Oops.Oh("托盘信息不存在!");

            if (containerModel.ContainerStatus == ContainerStatus.FENJIAN) throw Oops.Oh("容器存在分拣信息!");

            if (containerModel.ContainerStatus == ContainerStatus.KUWEI) throw Oops.Oh("托盘存在库位中!");

            var isExit = await _wmsTaskRep.AnyAsync(p => p.ContainerCode == containerModel.ContainerCode && p.TaskStatus != TaskStatusEnum.WANCHENG && p.TaskStatus != TaskStatusEnum.QUXIAO);
            if (isExit) throw Oops.Oh("托盘存在未完成任务中!");

            isExit = await _wmsContainerPlaceRep.AnyAsync(p => p.ContainerCode == containerModel.ContainerCode && p.ContainerPlaceStatus == CommonStatus.ENABLE);
            if (isExit) throw Oops.Oh("托盘存在库位中!");

            //组盘信息
            var WmsMaterialContainerList = await _wmsMaterialContainerRep.Where(p => p.ContainerCode == input.ContainerCode && p.BindStatus == CommonStatus.ENABLE).ToListAsync();

            if (WmsMaterialContainerList.Count > 0) await _wmsMaterialContainerRep.DeleteAsync(WmsMaterialContainerList);

            //修改托盘状态
            containerModel.ContainerStatus = ContainerStatus.ZUPANG;
            await _wmsContainerRep.UpdateAsync(containerModel);

            //组盘单据
            var orderNo = Yitter.IdGenerator.YitIdHelper.NextId().ToString();
            if (input.WmsMaterials.Count >0)//有物料的情况
            {
                foreach (var item in input.WmsMaterials)
                {
                    var bindentranceModel = item.Adapt<WmsMaterialContainer>();
                    bindentranceModel.MaterialName = item.Materialname;
                    bindentranceModel.MaterialNo = item.Materialcode;
                    bindentranceModel.MaterialId = 4567;//暂定
                    bindentranceModel.ContainerId = containerModel.Id;
                    bindentranceModel.ContainerCode = containerModel.ContainerCode;
                    bindentranceModel.MaterialBatch = "N/A";
                    bindentranceModel.MaterialSpec = "N/A";
                    bindentranceModel.BindQuantity = item.Qty;
                    bindentranceModel.BindStatus = CommonStatus.ENABLE;
                    bindentranceModel.OrderNo = orderNo;
                    bindentranceModel.MaterialDensity = "N/A";
                    bindentranceModel.Id = Yitter.IdGenerator.YitIdHelper.NextId();
                    await _wmsMaterialContainerRep.InsertAsync(bindentranceModel);
                }
            }
            //else//空托入库的情况
            //{
            //    // 创建新组盘记录
            //    var bindentranceModel = new WmsMaterialContainer();
            //    bindentranceModel.MaterialName = "空托";
            //    bindentranceModel.MaterialNo = containerModel.ContainerCode;
            //    bindentranceModel.MaterialId = 55555;//暂定
            //    bindentranceModel.ContainerId = containerModel.Id;
            //    bindentranceModel.ContainerCode = containerModel.ContainerCode;
            //    bindentranceModel.MaterialBatch = "N/A";
            //    bindentranceModel.MaterialSpec = containerModel.SpecLength + "*" + containerModel.SpecWidth;
            //    bindentranceModel.BindQuantity = 1;
            //    bindentranceModel.BindStatus = CommonStatus.ENABLE;
            //    bindentranceModel.OrderNo = orderNo;
            //    bindentranceModel.MaterialDensity = "N/A";
            //    bindentranceModel.Id = YitIdHelper.NextId();
            //    await _wmsMaterialContainerRep.InsertNowAsync(bindentranceModel);
            //}
            return new GroupDiskOutput()
            {
                Containercode = input.ContainerCode,
                OrderNo = orderNo
            };
        }

        /// <summary>
        /// 提交入库
        /// </summary>
        /// <returns></returns>
        [HttpPost("PdaInWarehouse")]
        [UnitOfWork]
        public async Task PdaInWarehouse([FromBody] PdaInWarehouseInput input)
        {

            //检查托盘
            var containerModel = await _wmsContainerRep.FirstOrDefaultAsync(p => p.ContainerCode == input.ContainerCode && p.ContainerStatus != ContainerStatus.JINYONG);
            if (containerModel == null) throw Oops.Oh("托盘不存在!");

            if (containerModel.ContainerStatus == ContainerStatus.FENJIAN) throw Oops.Oh("容器存在分拣信息!");

            var isExit = await _wmsTaskRep.AnyAsync(p => p.ContainerCode == containerModel.ContainerCode && p.TaskStatus != TaskStatusEnum.WANCHENG && p.TaskStatus != TaskStatusEnum.QUXIAO);
            if (isExit) throw Oops.Oh("托盘存在未完成任务中!");

            var isInPlace = await _wmsContainerRep.FirstOrDefaultAsync(n => n.ContainerCode == input.ContainerCode && n.ContainerStatus == ContainerStatus.KUWEI);
            if (isInPlace != null) throw Oops.Oh("托盘存在库位中!");

            isExit = await _wmsContainerPlaceRep.AnyAsync(p => p.ContainerCode == containerModel.ContainerCode && p.ContainerPlaceStatus == CommonStatus.ENABLE);
            if (isExit) throw Oops.Oh("托盘存在库位中!");

            //删除组盘信息
            var oldwmsMaterialContainerList = await _wmsMaterialContainerRep.Where(p => p.ContainerCode == input.ContainerCode && p.BindStatus == CommonStatus.ENABLE).ToListAsync();

            if (oldwmsMaterialContainerList.Count > 0) await _wmsMaterialContainerRep.DeleteAsync(oldwmsMaterialContainerList);

            //组盘单据号
            var orderNo = Yitter.IdGenerator.YitIdHelper.NextId().ToString();
            if (input.WmsMaterials!=null || input.WmsMaterials.Count <= 0)//有物料的情况
            {
                foreach (var item in input.WmsMaterials)
                {
                    var bindentranceModel = item.Adapt<WmsMaterialContainer>();
                    bindentranceModel.MaterialName = item.Materialname;
                    bindentranceModel.MaterialNo = item.Materialcode;
                    bindentranceModel.MaterialId = 4567;
                    bindentranceModel.ContainerId = containerModel.Id;
                    bindentranceModel.ContainerCode = containerModel.ContainerCode;
                    bindentranceModel.MaterialBatch = "N/A";
                    bindentranceModel.MaterialSpec = "N/A";
                    bindentranceModel.BindQuantity = item.Qty;
                    bindentranceModel.BindStatus = CommonStatus.ENABLE;
                    bindentranceModel.OrderNo = orderNo;
                    bindentranceModel.MaterialDensity = "N/A";
                    bindentranceModel.Id = Yitter.IdGenerator.YitIdHelper.NextId();
                    await _wmsMaterialContainerRep.InsertAsync(bindentranceModel);
                    ////更改明细入库数量
                    //var wmsOrderDetailsModal = await _wmsOrderDetailsRep.FirstOrDefaultAsync(x => x.Id == item.Id);
                    //wmsOrderDetailsModal.InventoryQty += item.Qty; 
                }
            }
            //else//空托入库的情况
            //{
            //    // 创建新组盘记录
            //    var bindentranceModel = new WmsMaterialContainer();
            //    bindentranceModel.MaterialName = "空托";
            //    bindentranceModel.MaterialNo = containerModel.ContainerCode;
            //    bindentranceModel.MaterialId = 55555;//暂定
            //    bindentranceModel.ContainerId = containerModel.Id;
            //    bindentranceModel.ContainerCode = containerModel.ContainerCode;
            //    bindentranceModel.MaterialBatch = "N/A";//根据物料码解析
            //    bindentranceModel.MaterialSpec = containerModel.SpecLength + "*" + containerModel.SpecWidth;
            //    bindentranceModel.BindQuantity = 1;
            //    bindentranceModel.BindStatus = CommonStatus.ENABLE;
            //    bindentranceModel.OrderNo = orderNo;
            //    bindentranceModel.MaterialDensity = "N/A";//根据物料码解析
            //    bindentranceModel.Id = YitIdHelper.NextId();
            //    await _wmsMaterialContainerRep.InsertNowAsync(bindentranceModel);
            //}

            ////修改托盘状态
            //containerModel.ContainerStatus = ContainerStatus.ZUPANG;
            //await _wmsContainerRep.UpdateAsync(containerModel);
            //分配巷道
            var Aisle = 1;

            //任务
            var takmodel = new WmsTask()
            {
                TaskNo = Yitter.IdGenerator.YitIdHelper.NextId().ToString(),
                TaskModel = TaskModel.QUANZIDONG,
                TaskType = TaskType.RUKU,
                TaskLevel = 1,
                TaskStatus = TaskStatusEnum.ZHIXINGZHONG,
                OrderNo = orderNo,
                OrderDetailsId = 0,
                ContainerCode = input.ContainerCode,
                SourcePlace = "入库口",//现场情况为空
                ToPlace = "", //目标位
                AreaName = "绝缘立库",
                IsRead = true, //WCS是否可以读取
                SendTimes = 1, //发送次数
                Aisle = Aisle,// 根据库区id获取几号提升机，测试使用一号
                TaskDodeviceStatus = TaskDodeviceStatusEnum.W 
            };
            if (input.WmsMaterials == null || input.WmsMaterials.Count <= 0) {
                takmodel.Description = "空托";
            }
            else
            {
                takmodel.Description = "物料";
            };
            
            await _wmsTaskRep.InsertAsync(takmodel);
        }
    }
}
