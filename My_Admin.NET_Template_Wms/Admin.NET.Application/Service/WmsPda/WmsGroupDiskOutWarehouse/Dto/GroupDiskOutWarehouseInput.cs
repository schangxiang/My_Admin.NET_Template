﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application
{
    /// <summary>
    /// 打包信息参数
    /// </summary>
    public class GroupDiskOutWarehouseInput
    {
        /// <summary>
        /// 出库口
        /// </summary>
        public virtual string OutputEntrance { get; set; }

        /// <summary>
        /// PLC物料编码
        /// </summary>
        public List<MaterialNoList> PlcMaterialNoList { get; set; }


        /// <summary>
        /// WMS物料编码
        /// </summary>
        public List<MaterialNoList> WcsMaterialNoList { get; set; }
    }

    /// <summary>
    /// 打包信息参数
    /// </summary>
    public class PackOutWarehouseInput
    {
        /// <summary>
        /// 打包标签
        /// </summary>
        public virtual string PackCode { get; set; }

        /// <summary>
        /// 物料编码
        /// </summary>
        public List<MaterialNoList> MaterialNoList { get; set; }
    }

    /// <summary>
    /// 物料编码list
    /// </summary>
    public class MaterialNoList
    {
        /// <summary>
        /// 物料编码
        /// </summary>
        public virtual string MaterialNo { get; set; }
    }
    /// <summary>
    /// 根据出库口获取码垛数据
    /// </summary>
    public class GetPackInfomationInput
    {
        /// <summary>
        /// 出库口
        /// </summary>
        [Required(ErrorMessage = "出库口不能为空")]
        public virtual string OutputEntrance { get; set; }
    }
}
