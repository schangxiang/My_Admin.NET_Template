﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;

namespace YaKe.Les.Core.SeedData
{
    public class WmsContainerSeedData : IEntitySeedData<WmsContainer>
    {
        /// <summary>
        /// 托盘实体种子
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<WmsContainer> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new[]
            {
                new WmsContainer{Id=358852634767429,ContainerCode="TM00001",ContainerType=ContainerType.SULIAO,ContainerStatus=ContainerStatus.KOUXIAN,SpecLength=(decimal)1,SpecWidth=(decimal)1,SpecHeight=(decimal)1,LimitLength=(decimal)1,LimitWidth=(decimal)1,LimitHeight=(decimal)1,MaxWeight=(decimal)1},
                new WmsContainer{Id=358852767887429,ContainerCode="TM00002",ContainerType=ContainerType.SULIAO,ContainerStatus=ContainerStatus.ZUPANG,SpecLength=(decimal)1,SpecWidth=(decimal)1,SpecHeight=(decimal)1,LimitLength=(decimal)1,LimitWidth=(decimal)1,LimitHeight=(decimal)1,MaxWeight=(decimal)1},
                new WmsContainer{Id=358852831047749,ContainerCode="TM00003",ContainerType=ContainerType.MUZHI,ContainerStatus=ContainerStatus.KUWEI,SpecLength=(decimal)1,SpecWidth=(decimal)1,SpecHeight=(decimal)1,LimitLength=(decimal)1,LimitWidth=(decimal)1,LimitHeight=(decimal)1,MaxWeight=(decimal)1},
                new WmsContainer{Id=358853016055877,ContainerCode="TM00004",ContainerType=ContainerType.MUZHI,ContainerStatus=ContainerStatus.KUWEI,SpecLength=(decimal)1,SpecWidth=(decimal)1,SpecHeight=(decimal)1,LimitLength=(decimal)1,LimitWidth=(decimal)1,LimitHeight=(decimal)1,MaxWeight=(decimal)1},
                new WmsContainer{Id=358853070798917,ContainerCode="TM00005",ContainerType=ContainerType.JINSHU,ContainerStatus=ContainerStatus.KUWEI,SpecLength=(decimal)1,SpecWidth=(decimal)1,SpecHeight=(decimal)1,LimitLength=(decimal)1,LimitWidth=(decimal)1,LimitHeight=(decimal)1,MaxWeight=(decimal)1},
                new WmsContainer{Id=358853146058821,ContainerCode="TM00006",ContainerType=ContainerType.JINSHU,ContainerStatus=ContainerStatus.KOUXIAN,SpecLength=(decimal)1,SpecWidth=(decimal)1,SpecHeight=(decimal)1,LimitLength=(decimal)1,LimitWidth=(decimal)1,LimitHeight=(decimal)1,MaxWeight=(decimal)1},
                new WmsContainer{Id=358853199671365,ContainerCode="TM00007",ContainerType=ContainerType.QITA,ContainerStatus=ContainerStatus.KOUXIAN,SpecLength=(decimal)1,SpecWidth=(decimal)1,SpecHeight=(decimal)1,LimitLength=(decimal)1,LimitWidth=(decimal)1,LimitHeight=(decimal)1,MaxWeight=(decimal)1},
                new WmsContainer{Id=358853255102533,ContainerCode="TM00008",ContainerType=ContainerType.SULIAO,ContainerStatus=ContainerStatus.KOUXIAN,SpecLength=(decimal)1,SpecWidth=(decimal)1,SpecHeight=(decimal)1,LimitLength=(decimal)1,LimitWidth=(decimal)1,LimitHeight=(decimal)1,MaxWeight=(decimal)1},
            };
        }
    }
}
