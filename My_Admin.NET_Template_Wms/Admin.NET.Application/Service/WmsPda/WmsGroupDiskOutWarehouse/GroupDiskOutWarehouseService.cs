﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Admin.NET.Core;
using Yitter.IdGenerator;

namespace Admin.NET.Application
{
    /// <summary>
    /// Pda组盘出库
    /// </summary>
    [ApiDescriptionSettings("Pda服务", Name = "GroupDiskOutWarehouse", Order = 100)]
    [Route("api/[Controller]")]
    public class GroupDiskOutWarehouseService : IDynamicApiController, ITransient
    {

        private readonly IRepository<WmsOrderDetails, MasterDbContextLocator> _wmsOrderDetailsRep;
        private readonly IRepository<LesPackWarehouse, MasterDbContextLocator> _lesPackWarehouseRep;
        private readonly IRepository<WmsTask, MasterDbContextLocator> _wmsTaskRep;
        private readonly IRepository<WmsPlace, MasterDbContextLocator> _wmsPlaceRep;
        private readonly IRepository<WmsArea, MasterDbContextLocator> _wmsAreaRep;
        private readonly IRepository<WmsOrderType, MasterDbContextLocator> _wmsOrderTypeRep;
        private readonly IRepository<WmsMaterialContainer, MasterDbContextLocator> _wmsMaterialContainerRep;
        private readonly IRepository<WmsContainer, MasterDbContextLocator> _wmsContainerRep;
        private readonly IRepository<WmsContainerPlace, MasterDbContextLocator> _wmsContainerPlaceRep;
        private readonly IRepository<WmsMaterialStock, MasterDbContextLocator> _wmsMaterialStockRep;

        /// <summary>
        /// 构造函数
        /// </summary>
        public GroupDiskOutWarehouseService(
            IRepository<WmsOrderDetails, MasterDbContextLocator> wmsOrderDetailsRep,
            IRepository<LesPackWarehouse, MasterDbContextLocator> lesPackWarehouseRep,
            IRepository<WmsTask, MasterDbContextLocator> wmsTaskRep,
            IRepository<WmsPlace, MasterDbContextLocator> wmsPlaceRep,
            IRepository<WmsArea, MasterDbContextLocator> wmsAreaRep,
            IRepository<WmsOrderType, MasterDbContextLocator> wmsOrderTypeRep,
            IRepository<WmsMaterialContainer, MasterDbContextLocator> wmsMaterialContainerRep,
            IRepository<WmsContainer, MasterDbContextLocator> wmsContainerRep,
            IRepository<WmsContainerPlace, MasterDbContextLocator> wmsContainerPlaceRep,
            IRepository<WmsMaterialStock, MasterDbContextLocator> wmsMaterialStockRep
        )
        {
            _wmsOrderDetailsRep = wmsOrderDetailsRep;
            _lesPackWarehouseRep = lesPackWarehouseRep;
            _wmsTaskRep = wmsTaskRep;
            _wmsPlaceRep = wmsPlaceRep;
            _wmsAreaRep = wmsAreaRep;
            _wmsOrderTypeRep = wmsOrderTypeRep;
            _wmsMaterialContainerRep = wmsMaterialContainerRep;
            _wmsContainerRep = wmsContainerRep;
            _wmsContainerPlaceRep = wmsContainerPlaceRep;
            _wmsMaterialStockRep = wmsMaterialStockRep;
        }

        
        ///// <summary>
        ///// 根据出库口获取码垛数据
        ///// </summary>
        ///// <param name="input"></param>
        ///// <returns></returns>
        //[HttpGet("GetPackInfomation")]
        //public async Task<GroupDiskOutWarehouseOutput> GetPackInfomation([FromQuery] GetPackInfomationInput input)
        //{
        //    //根据出库口获取打包表数据
        //    var lesPackWarehouseList = await _lesPackWarehouseRep.DetachedEntities
        //        .Where(u => u.OutputEntrance == input.OutputEntrance && u.LesPackState == LesPackState.WEICHUKU)
        //        .ToListAsync();
        //    if (lesPackWarehouseList.Count==0) throw Oops.Oh("暂无码垛信息!");

        //    //if(lesPackWarehouseList.Select(p=>p.OrderNo).Count()>=2) throw Oops.Oh("码垛信息存在多条!请到PC端进行处理!");

        //    //获取当前出库单据中执行中单据明细
        //    var wmsOrderDetailList = await _wmsOrderDetailsRep
        //        .Where(u=>u.WareOrder.OrderLargeCategory== 408734100951109 && u.WareOrder.ExitPort==input.OutputEntrance
        //        && lesPackWarehouseList.Select(n=>n.MaterialNo).ToList().Contains(u.MaterialNo)).ProjectToType<WmsOrderDetails>().ToListAsync();
        //    if (wmsOrderDetailList.Count == 0) throw Oops.Oh("暂无出库单据!");

        //    List<GroupDiskOutWarehouse> groupDiskOutWarehouseList = new List<GroupDiskOutWarehouse>();//打包信息list
        //    foreach (var item in wmsOrderDetailList)
        //    {
        //        GroupDiskOutWarehouse groupDiskOutWarehouse = new GroupDiskOutWarehouse();
        //        groupDiskOutWarehouse.MaterialNo = item.MaterialNo;
        //        groupDiskOutWarehouse.Long = item.Long;
        //        groupDiskOutWarehouse.Wide = item.Wide;
        //        groupDiskOutWarehouse.High = item.High;
        //        groupDiskOutWarehouseList.Add(groupDiskOutWarehouse);
        //    }

        //    return new GroupDiskOutWarehouseOutput()
        //    {
        //        OrderNo = wmsOrderDetailList.FirstOrDefault().WareOrder.OrderNo,
        //        OrderSubclass = (await _wmsOrderTypeRep.FirstOrDefaultAsync(u => u.Id == wmsOrderDetailList.FirstOrDefault().WareOrder.OrderSubclass)).Name,
        //        GroupDiskOutWarehouseList = groupDiskOutWarehouseList
        //    };
        //}

        ///// <summary>
        ///// 确认出库
        ///// </summary>
        ///// <param name="input"></param>
        ///// <returns></returns>
        //[HttpPost("Pack")]
        //[UnitOfWork]
        //public async Task PackOutWarehouse([FromBody] PackOutWarehouseInput input)
        //{
        //    if (input.PackCode == null) throw Oops.Oh("打包标签不能为空!");
        //    if (input.MaterialNoList.Count == 0) throw Oops.Oh("物料码不能为空!");
        //    //循环物料码
        //    foreach (var item in input.MaterialNoList)
        //    {
        //        //更改打包表中物料状态
        //        var lesPackWarehouse = await _lesPackWarehouseRep.FirstOrDefaultAsync(u => u.MaterialNo == item.MaterialNo && u.LesPackState == LesPackState.WEICHUKU);
        //        if (lesPackWarehouse == null) throw Oops.Oh("打包表中不存在此物料!");
        //        lesPackWarehouse.PackCode = input.PackCode;
        //        lesPackWarehouse.LesPackState = LesPackState.ZHUANYIKU;
        //        await _lesPackWarehouseRep.UpdateAsync(lesPackWarehouse);

        //        //更改单据明细的状态
        //        var wmsOrderDetail = await _wmsOrderDetailsRep.FirstOrDefaultAsync
        //            (u => u.MaterialNo == item.MaterialNo && u.WareOrder.OrderLargeCategory == 408734100951109);         
        //        if (wmsOrderDetail == null) throw Oops.Oh("单据明细中不存在此物料!");
        //        wmsOrderDetail.OrderStatus = OrderDetailsStatusEnum.WANCHENG;
        //        await _lesPackWarehouseRep.UpdateAsync(lesPackWarehouse);

        //        //更改任务状态
        //        var wmsTask = await _wmsTaskRep.FirstOrDefaultAsync
        //            (u => u.OrderDetailsId == wmsOrderDetail.Id && u.TaskStatus == TaskStatusEnum.ZHIXINGZHONG 
        //            && u.TaskType == TaskType.CHUKU);
        //        if (wmsTask == null) throw Oops.Oh("任务不存在!");
        //        wmsTask.TaskStatus = TaskStatusEnum.WANCHENG;
        //        await _wmsTaskRep.UpdateAsync(wmsTask);
        //    }
            
        //    //标签码当托盘码使用
        //    var oldwmsMaterialContainerList = await _wmsMaterialContainerRep
        //        .Where(p => p.ContainerCode == input.PackCode && p.BindStatus == CommonStatus.ENABLE).ToListAsync();
        //    if (oldwmsMaterialContainerList.Count > 0)
        //    {
        //        foreach (var item in oldwmsMaterialContainerList)
        //        {
        //            await _wmsMaterialContainerRep.DeleteAsync(item);
        //        }
        //    }
        //    //组盘单据 默认空托单据
        //    var orderNo = "N/A";
        //    if (input.MaterialNoList.Count > 0) orderNo =YitIdHelper.NextId().ToString();

        //    //新增组盘数据
        //    foreach (var item in input.MaterialNoList)
        //    {
        //        //获取明细
        //        var wmsOrderDetail = await _wmsOrderDetailsRep.FirstOrDefaultAsync
        //            (u => u.MaterialNo == item.MaterialNo && u.WareOrder.OrderLargeCategory == 408734100951109);
        //        if (wmsOrderDetail == null) throw Oops.Oh("单据信息异常!"+item.MaterialNo);
        //        WmsMaterialContainer bindentranceModel = new WmsMaterialContainer();
        //        bindentranceModel.ContainerCode = input.PackCode;
        //        bindentranceModel.ContainerId = 405636578041925;
        //        bindentranceModel.MaterialNo = item.MaterialNo;
        //        bindentranceModel.MaterialBatch = wmsOrderDetail.Batch;
        //        bindentranceModel.MaterialSpec = wmsOrderDetail.Long.ToString() + "*" + wmsOrderDetail.Wide.ToString() + "*" + wmsOrderDetail.High.ToString();
        //        bindentranceModel.BindQuantity = 1;
        //        bindentranceModel.OrderDetailsId = wmsOrderDetail.Id;
        //        bindentranceModel.MaterialDensity = wmsOrderDetail.MaterialDensity;
        //        bindentranceModel.BindStatus = CommonStatus.ENABLE;
        //        bindentranceModel.OrderNo = orderNo;
        //        bindentranceModel.MaterialName = "N/A";
        //        await _wmsMaterialContainerRep.InsertNowAsync(bindentranceModel);
        //    }
        //    //获取转移库库位
        //    var zyPlaceModel = await _wmsPlaceRep.DetachedEntities.Where(p => p.WmsArea.AreaType == AreaType.ZHUANYIKU
        //    && p.Islock == YesOrNot.N && p.PlaceStatus == PlaceStatus.KONGXIAN).FirstOrDefaultAsync();

        //    //创建任务  目前人工转移无需创建任务
        //    //创建托盘库位关系表
        //    var wmsContainerPlaceModel = new WmsContainerPlace()
        //    {
        //        PlaceId = zyPlaceModel.Id,
        //        PlaceCode = zyPlaceModel.PlaceCode,
        //        ContainerId = 405636578041925,   //给予一个特定的值
        //        ContainerCode = input.PackCode,
        //        ContainerPlaceStatus = CommonStatus.ENABLE
        //    };
        //    await _wmsContainerPlaceRep.InsertAsync(wmsContainerPlaceModel);

        //    //更新库存
        //    var wmsMaterialContainerList = await _wmsMaterialContainerRep.DetachedEntities
        //        .Where(p => p.OrderNo == orderNo && p.BindStatus == CommonStatus.ENABLE).ProjectToType<WmsMaterialContainer>().ToListAsync();

        //    //物料入库
        //    foreach (var item in wmsMaterialContainerList)
        //    {
        //        var StockModel = await _wmsMaterialStockRep
        //            .Where(p => p.ContainerCode == input.PackCode
        //            && p.MaterialName == item.MaterialName && p.MaterialNo == item.MaterialNo && p.MaterialBatch == item.MaterialBatch ).FirstOrDefaultAsync();
        //        if (StockModel != null)
        //        {
        //            StockModel.MaterialDensity = item.MaterialDensity;
        //            StockModel.PlaceCode = zyPlaceModel.PlaceCode;
        //            StockModel.AreaId = zyPlaceModel.AreaId;
        //            StockModel.StockNumber = item.BindQuantity;
        //            await _wmsMaterialStockRep.UpdateAsync(StockModel);
        //        }
        //        else
        //        {
        //            StockModel = new WmsMaterialStock()
        //            {
        //                MaterialNo = item.MaterialNo,
        //                //MaterialType = item.MaterialType,
        //                MaterialName = item.MaterialName,
        //                MaterialSpec = item.MaterialSpec,
        //                MaterialBatch = item.MaterialBatch,
        //                MaterialDensity = item.MaterialDensity,
        //                //InspectionMethod = item.WmsMaterial.InspectionMethod,
        //                //UnitType = item.WmsMaterial.UnitType,
        //                //UnitNo = item.WmsMaterial.UnitNo,
        //                StockNumber = item.BindQuantity,
        //                PlaceCode = zyPlaceModel.PlaceCode,
        //                ContainerCode = input.PackCode,
        //                AreaId = zyPlaceModel.AreaId,
        //            };
        //            await _wmsMaterialStockRep.InsertAsync(StockModel);
        //        }
        //    }

        //    //更新库位
        //    zyPlaceModel.PlaceStatus = PlaceStatus.CUNHUO;
        //    await _wmsPlaceRep.UpdateAsync(zyPlaceModel);
        //}
    }
}
