﻿using Admin.NET.Core;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Admin.NET.Application
{
    public interface ITestStudents4Service
    {
        Task<TestStudents4Output> Get([FromQuery] QueryeTestStudents4Input input);
        Task<List<TestStudents4Output>> List([FromQuery] TestStudents4Input input);
        Task<PageResult<TestStudents4Output>> Page([FromQuery] TestStudents4Search input);
        Task<List<TestStudents4Output>> ListNonPageAsync([FromQuery] TestStudents4SearchNonPage input);
        
        Task Add(AddTestStudents4Input input);
        Task Update(UpdateTestStudents4Input input);
        Task Delete(DeleteTestStudents4Input input);
        Task<int> ImportExcelAsync(IFormFile file);
        IActionResult DownloadExcelTemplate(string version);
    }
}