﻿using Admin.NET.Core;
using Furion.DatabaseAccessor;
using Furion.DatabaseAccessor.Extensions;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.Extras.Admin.NET.Entity;
using Furion.FriendlyException;
using Mapster;
using Microsoft.AspNetCore.Http.Metadata;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Admin.NET.Application
{
    /// <summary>
    /// 代码生成详细配置服务
    /// </summary>
    [ApiDescriptionSettings(Name = "CodeGenConfig", Order = 100)]
    [Route("api")]
    public class SysCodeGenerateConfigService : ICodeGenConfigService, IDynamicApiController, ITransient
    {
        private readonly IRepository<SysLowCodeDataBase> _sysLowCodeRep; // 代码生成器仓储
        private readonly IRepository<SysCodeGen> _sysCodeGenRep; // 代码生成器仓储
        private readonly IRepository<SysCodeGenConfig> _sysCodeGenConfigRep; // 代码生成详细配置仓储

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="sysCodeGenConfigRep"></param>
        /// <param name="sysCodeGenRep"></param>
        /// <param name="sysLowCodeRep"></param>
        public SysCodeGenerateConfigService(IRepository<SysCodeGenConfig> sysCodeGenConfigRep, IRepository<SysCodeGen> sysCodeGenRep
            , IRepository<SysLowCodeDataBase> sysLowCodeRep
            )
        {
            _sysLowCodeRep = sysLowCodeRep;
            _sysCodeGenConfigRep = sysCodeGenConfigRep;
            _sysCodeGenRep = sysCodeGenRep;
        }

        /// <summary>
        /// 代码生成详细配置列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("sysCodeGenerateConfig/list")]
        public async Task<List<CodeGenConfig>> List([FromQuery] CodeGenConfig input)
        {
            var result = await _sysCodeGenConfigRep.DetachedEntities
                                             .Where(u => u.CodeGenId == input.CodeGenId && u.WhetherCommon != YesOrNot.Y.ToString())
                                             .ProjectToType<CodeGenConfig>()
                                             .Distinct()
                                             .ToListAsync();

            var codeGen = await _sysCodeGenRep.FirstOrDefaultAsync(x => x.Id == input.CodeGenId);
            var codeGenOutput = codeGen.Adapt<CodeGenOutput>();
            result.ForEach(x => x.CodeGen = codeGenOutput);
            return result;
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="inputList"></param>
        /// <returns></returns>
        [HttpPost("sysCodeGenerateConfig/edit")]
        public async Task Update(List<CodeGenConfig> inputList)
        {
            if (inputList == null || inputList.Count < 1) return;
            var list = inputList.Adapt<List<SysCodeGenConfig>>();

            CodeGenHelper.ValidateCodeGenConfig(list);

            await _sysCodeGenConfigRep.UpdateAsync(list);
        }

        /// <summary>
        /// 详情
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("·/detail")]
        public async Task<SysCodeGenConfig> Detail([FromQuery] CodeGenConfig input)
        {
            return await _sysCodeGenConfigRep.FirstOrDefaultAsync(u => u.Id == input.Id);
        }

        /// <summary>
        /// 批量增加
        /// </summary>
        /// <param name="tableColumnOuputList"></param>
        /// <param name="codeGenerate"></param>
        [NonAction]
        public async Task DelAndAddList(List<TableColumnOuput> tableColumnOuputList, SysCodeGen codeGenerate)
        {
            if (tableColumnOuputList == null) return;
            var list = new List<SysCodeGenConfig>();

            List<SysLowCodeDataBase> list_LowCode = new List<SysLowCodeDataBase>();

            if (codeGenerate != null && codeGenerate.LowCodeId > 0 && _sysLowCodeRep.Where(x => x.SysLowCodeId == codeGenerate.LowCodeId).Any())
            {
                list_LowCode = _sysLowCodeRep.Where(x => x.SysLowCodeId == codeGenerate.LowCodeId).ToList();
            }

            foreach (var tableColumn in tableColumnOuputList)
            {
                var codeGenConfig = new SysCodeGenConfig();

                var YesOrNo = YesOrNot.Y.ToString();
                if (Convert.ToBoolean(tableColumn.ColumnKey))
                {
                    YesOrNo = YesOrNot.N.ToString();
                }

                if (CodeGenUtil.IsCommonColumn(tableColumn.ColumnName))
                {
                    codeGenConfig.WhetherCommon = YesOrNot.Y.ToString();
                    YesOrNo = YesOrNot.N.ToString();
                }
                else
                {
                    codeGenConfig.WhetherCommon = YesOrNot.N.ToString();
                }

                codeGenConfig.CodeGenId = codeGenerate.Id;
                codeGenConfig.ColumnName = tableColumn.ColumnName;
                codeGenConfig.ColumnComment = tableColumn.ColumnComment;
                codeGenConfig.NetType = CodeGenUtil.ConvertDataType(tableColumn.DataType);
                codeGenConfig.DtoNetType = list_LowCode.Where(x => x.FieldName == tableColumn.ColumnName).Select(x => x.DtoTypeName).FirstOrDefault();
                if (string.IsNullOrEmpty(codeGenConfig.DtoNetType)) codeGenConfig.DtoNetType = codeGenConfig.NetType;
                codeGenConfig.WhetherRetract = YesOrNot.N.ToString();

                codeGenConfig.WhetherRequired = tableColumn.IsNullable ? YesOrNot.N.ToString() : YesOrNot.Y.ToString();
                codeGenConfig.QueryWhether = YesOrNo;

                if (codeGenConfig.WhetherRequired == YesOrNot.Y.ToString())
                {//必填字段必须配置增改。
                    codeGenConfig.WhetherAddUpdate = YesOrNot.Y.ToString();
                    if (codeGenConfig.ColumnName.ToLower() == "id")
                    {
                        codeGenConfig.WhetherAddUpdate = YesOrNot.N.ToString();
                    }
                }
                else
                {//字段不必填
                    //ly-0419 生成新建 编辑页面 创建人 修改人创建时间 修改时间 也自动生成，需要去掉
                    if (codeGenConfig.ColumnName == "CreatedTime" || codeGenConfig.ColumnName == "UpdatedTime"
                        || codeGenConfig.ColumnName == "CreatedUserName" || codeGenConfig.ColumnName == "UpdatedUserName")
                    {
                        codeGenConfig.WhetherAddUpdate = YesOrNot.N.ToString();
                    }
                    else
                    {
                        codeGenConfig.WhetherAddUpdate = YesOrNo;
                    }
                }

                codeGenConfig.WhetherTable = YesOrNo;
                codeGenConfig.WhetherOrderBy = YesOrNo;
                //是否联合主键 update by liuwq 20240418 
                codeGenConfig.WhetherUnionKey = YesOrNot.N.ToString();
                codeGenConfig.ColumnKey = tableColumn.ColumnKey;

                codeGenConfig.DataType = tableColumn.DataType;
                codeGenConfig.EffectType = CodeGenUtil.DataTypeToEff(codeGenConfig.NetType);
                codeGenConfig.QueryType = QueryTypeConst.等于; // QueryTypeEnum.eq.ToString();
                if (codeGenConfig.DtoNetType.ToLower() == "datetime".ToLower() || codeGenConfig.DtoNetType.ToLower() == "DateTimeOffset".ToLower())
                {
                    codeGenConfig.QueryType = QueryTypeConst.介于;
                }
                codeGenConfig.ShowTitleMinWidth = tableColumn.ShowTitleMinWidth;
                list.Add(codeGenConfig);
            }

            _sysCodeGenConfigRep.Context.DeleteRange<SysCodeGenConfig>(x => x.CodeGenId == codeGenerate.Id);

            await _sysCodeGenConfigRep.InsertAsync(list);
        }

        /// <summary>
        /// 增加
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [NonAction]
        public async Task Add(CodeGenConfig input)
        {
            var codeGenConfig = input.Adapt<SysCodeGenConfig>();
            await codeGenConfig.InsertAsync();
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="codeGenId"></param>
        /// <returns></returns>
        [NonAction]
        public async Task Delete(long codeGenId)
        {
            var codeGenConfigList = await _sysCodeGenConfigRep.Where(u => u.CodeGenId == codeGenId).ToListAsync();
            await _sysCodeGenConfigRep.DeleteAsync(codeGenConfigList);
        }
    }
}