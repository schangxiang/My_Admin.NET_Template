﻿using Admin.NET.Core.Util.LowCode.Dto;
using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 领料单输出参数
    /// </summary>
    public class WmsTakeMaterialOrderDto
    {
        /// <summary>
        /// 单据大类
        /// </summary>
        public long OrderLargeCategory { get; set; }
        
        /// <summary>
        /// 单据小类
        /// </summary>
        public long OrderSubclass { get; set; }
        
        /// <summary>
        /// 事务类型
        /// </summary>
        public string MoveType { get; set; }
        
        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        public long SOID { get; set; }
        
        /// <summary>
        /// 单据编号
        /// </summary>
        public string NO { get; set; }
        
        /// <summary>
        /// 领料单申请日期
        /// </summary>
        public DateTimeOffset Billdate { get; set; }
        
        /// <summary>
        /// 领用项目号
        /// </summary>
        public string WBSElementcode { get; set; }
        
        /// <summary>
        /// 领料部门
        /// </summary>
        public string BenefitingDepartcode { get; set; }
        
        /// <summary>
        /// 受益部门
        /// </summary>
        public string CostCenterID { get; set; }
        
        /// <summary>
        /// 客户
        /// </summary>
        public string FI_Client_Analysis_H { get; set; }
        
        /// <summary>
        /// 是否公司间交易
        /// </summary>
        public long IsInnerCompany { get; set; }
        
        /// <summary>
        /// 领料人
        /// </summary>
        public string PickerID { get; set; }
        
        /// <summary>
        /// 仓储中心
        /// </summary>
        public string WarehouseCentername { get; set; }
        
        /// <summary>
        /// 公司
        /// </summary>
        public string Companyname { get; set; }
        
        /// <summary>
        /// 施工队
        /// </summary>
        public string ConstructionTeamID { get; set; }
        
        /// <summary>
        /// 单据状态
        /// </summary>
        public Admin.NET.Core.OrderStatusEnum OrderStatus { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public Admin.NET.Core.IssueState IssueState { get; set; }
        
    }

    /// <summary>
    /// 分拣库存
    /// </summary>
    public class FjStock
    {
        /// <summary>
        /// 物料编号
        /// </summary>
        public string MaterialNo { get; set; }

        /// <summary>
        /// 物料类别;数据字典
        /// </summary>
        public MaterialType MaterialType { get; set; }

        /// <summary>
        /// 物料批次
        /// </summary>
        public string MaterialBatch { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string MaterialName { get; set; }

        /// <summary>
        /// 物料规格
        /// </summary>
        public string MaterialSpec { get; set; }

        /// <summary>
        /// 长    
        /// </summary>
        public int Long { get; set; }

        /// <summary>
        /// 宽    
        /// </summary>
        public int Wide { get; set; }

        /// <summary>
        /// 高    
        /// </summary>
        public int High { get; set; }

        /// <summary>
        /// 物料密度
        /// </summary>
        public string MaterialDensity { get; set; }

        /// <summary>
        /// 检验方式;数据字典
        /// </summary>
        public MaterialInspection InspectionMethod { get; set; }

        /// <summary>
        /// 单位类别;数据字典
        /// </summary>
        public UnitType UnitType { get; set; }

        /// <summary>
        /// 单位编号;数据字典
        /// </summary>
        public UnitNoType UnitNo { get; set; }

        /// <summary>
        /// 实物库存数
        /// </summary>
        public decimal StockNumber { get; set; }

        /// <summary>
        /// 库位编码
        /// </summary>
        public string PlaceCode { get; set; }

        /// <summary>
        /// 托盘Id
        /// </summary>
        public long ContainerId { get; set; }

        /// <summary>
        /// 托盘编码
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 库区Id
        /// </summary>
        public long AreaId { get; set; }

        /// <summary>
        /// 账面数量
        /// </summary>
        public decimal QuantityOfBill { get; set; }

        /// <summary>
        /// 入库来源
        /// </summary>
        public RuKuSourceEnum Source { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>]
        public string ProjectCode { get; set; }

        /// <summary>
        /// 合同编号
        /// </summary>
        public string ContractCode { get; set; }

        /// <summary>
        /// TA号
        /// </summary>
        public string TACode { get; set; }

        /// <summary>
        /// 分拣数量
        /// </summary>
        public decimal FjQty { get; set; }
    }
         
}
