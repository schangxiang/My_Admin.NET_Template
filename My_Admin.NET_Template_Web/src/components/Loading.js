import Vue from "vue";
import { Spin } from "ant-design-vue"; 

const createFullLoading = function(){
  const instance = new Vue({
    data: {
      visible: false,
    },
    methods: {
      show() {
        this.visible = true;
      },
      hide() {
        this.visible = false;
      },
    },
    render(h, data) {
      const fullscreenStyle = {
        position: "fixed",
        left: 0,
        top: 0,
        width: "100%",
        height: "100%",
        display: this.visible?'block':'none',
        zIndex:99999999999
      }
      const maskStyle = {
        position: "absolute",
        left: 0,
        top: 0,
        width: "100%",
        height: "100%",
        zIndex:1,
        backgroundColor:'rgba(0,0,0,.2)'
      }
      const innerStyle = {
        position: "absolute",
        left: 0,
        top: 0,
        width: "100%",
        height: "100%",
        zIndex:2,
        backgroundColor:'transparent',
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
      }
      return (
        <div style={fullscreenStyle}>
          <div style={maskStyle}></div>
          <div style={innerStyle}><Spin /></div>
        </div>
      )
    },
  });
  const component = instance.$mount();
  document.body.appendChild(component.$el);
  return instance
}

export default createFullLoading



