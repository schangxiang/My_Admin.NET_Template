﻿using Admin.NET.Core.Util.LowCode.Front.Model;
using Furion.Extras.Admin.NET.Service.LowCode.Dto;

namespace Admin.NET.Application.Service.System.LowCode.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class Front_CodeGenerate
    {
        /// <summary>
        /// 
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string TableDesc { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AuthorName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string BusName { get; set; }

        /// <summary>
        /// 命名空间
        /// </summary>
        public string NameSpace { get; set; }

        /// <summary>
        /// 模块路径
        /// </summary>
        public string ModuleName { get; set; }

        /// <summary>
        /// 是否只是查询 
        /// </summary>
        public bool IsOnlyQuery { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ProName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DatabaseName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CamelizeClassName { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public List<CodeGenConfig> QueryWhetherList { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<CodeGenConfig> TableField { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<CodeGenConfig> FileTableField { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<GenEntity_Field> Fields { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long? LowCodeId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FormDesign { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DynamicData { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<Front_Dynamic> DynamicLoad_Dict { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsFile { get; set; }

        /// <summary>
        /// 菜单应用分类
        /// </summary>
        public string MenuApplication { get; set; }

        /// <summary>
        /// 导入模版定制化代码（替换模版使用）
        /// </summary>
        public string ImportExcelCustomizationContent { get; set; }
    }
}