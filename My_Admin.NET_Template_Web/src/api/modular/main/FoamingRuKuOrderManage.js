﻿import { axios } from '@/utils/request'

/**
 * 查询发泡车间入库单据（熟化库）
 *
 * @author yrj
 */
export function FoamingRuKuOrderPage (parameter) {
  return axios({
    url: '/FoamingRuKuOrder/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 获取入库单据详情
 *
 * @author yuhuanhuan
 */
export function FoamingRuKuOrderDetailPage (parameter) {
  return axios({
    url: '/FoamingRuKuOrder/detailPage',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加发泡车间入库单据（熟化库）
 *
 * @author yrj
 */
export function FoamingRuKuOrderAdd (parameter) {
  return axios({
    url: '/FoamingRuKuOrder/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑发泡车间入库单据（熟化库）
 *
 * @author yrj
 */
export function FoamingRuKuOrderEdit (parameter) {
  return axios({
    url: '/FoamingRuKuOrder/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除发泡车间入库单据（熟化库）
 *
 * @author yrj
 */
export function FoamingRuKuOrderDelete (parameter) {
  return axios({
    url: '/FoamingRuKuOrder/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 批量下发
 *
 * @author yuhuanhuan
 */
export function FoamingRuKuOrderMulDistribute (params) {
  return axios({
    url: '/FoamingRuKuOrder/batchDistribute',
    method: 'post',
    data: params
  })
}

/**
 * 批量撤回
 *
 * @author yuhuanhuan
 */
export function FoamingRuKuOrderMulWithdraw (ids) {
  return axios({
    url: '/FoamingRuKuOrder/batchWithdraw',
    method: 'post',
    data: {id:ids}
  })
}


/**
 * 强制完成
 *
 * @author yuhuanhuan
 */
export function FoamingRuKuOrderForceComplete (id) {
  return axios({
    url: '/FoamingRuKuOrder/ForceComplete',
    method: 'post',
    data: {id}
  })
}


/**
 * 增补数量
 *
 * @author yuhuanhuan
 */
export function FoamingRuKuOrderAddOrderDetails (params) {
  return axios({
    url: '/FoamingRuKuOrder/AddOrderDetails',
    method: 'post',
    data: params
  })
}

/**
 * 详情入库
 *
 * @author yuhuanhuan
 */
export function FoamingRuKuOrderExecuteTask (params) {
  return axios({
    url: '/FoamingRuKuOrder/ExecuteTask',
    method: 'post',
    data: params
  })
}


/**
 * 获取单据号
 *
 * @author yuhuanhuan
 */
export function FoamingRuKuOrderGetNo () {
  return axios({
    url: '/FoamingRuKuOrder/GetRuKuOrderNo',
    method: 'post'
  })
}


/**
 * 获取宽度下拉列表数据
 *
 * @author yuhuanhuan
 */
export function FoamingRuKuOrderGetMaterialWide () {
  return axios({
    url: '/FoamingRuKuOrder/GetMaterialWide',
    method: 'get'
  })
}


/**
 * 详情取消
 *
 * @author yuhuanhuan
 */
export function FoamingRuKuDetailCancel(id) {
  return axios({
    url: '/FoamingRuKuOrder/RuKuCancelTask',
    method: 'post',
    data:{id}
  })
}


