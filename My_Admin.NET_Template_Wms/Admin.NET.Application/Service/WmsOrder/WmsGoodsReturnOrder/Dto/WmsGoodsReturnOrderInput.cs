﻿using Admin.NET.Core;
using Admin.NET.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace Admin.NET.Application
{    
    
    /// <summary>
    /// 退货单查询参数
    /// </summary>
    public class WmsGoodsReturnOrderSearch : PageInputBase
    {
        /// <summary>
        /// 单据大类
        /// </summary>
        public virtual long? OrderLargeCategory { get; set; }
        
        /// <summary>
        /// 单据小类
        /// </summary>
        public virtual long? OrderSubclass { get; set; }
        
        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        public virtual long? SOID { get; set; }
        
        /// <summary>
        /// 单据编号
        /// </summary>
        public virtual string NO { get; set; }
        
        /// <summary>
        /// 创建日期
        /// </summary>
        public virtual DateTimeOffset? Billdate { get; set; }
        
        /// <summary>
        /// 供应商编码
        /// </summary>
        public virtual string Vendorcode { get; set; }
        
        /// <summary>
        /// 供应商名称
        /// </summary>
        public virtual string Vendorname { get; set; }
        
        /// <summary>
        /// 公司名称
        /// </summary>
        public virtual string Companyname { get; set; }
        
        /// <summary>
        /// 仓管员
        /// </summary>
        public virtual string WarehouseKeepername { get; set; }
        
        /// <summary>
        /// 仓储中心
        /// </summary>
        public virtual string WarehouseCentername { get; set; }
        
        /// <summary>
        /// 退货状态
        /// </summary>
        public virtual Admin.NET.Core.GoodsReturnStatus? GoodsReturnStatus { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState IssueState { get; set; }
        
    }

        /// <summary>
    /// 退货单不分页查询参数
    /// </summary>
    public class WmsGoodsReturnOrderSearchNonPage : PageInputNonPageBase
    {
        /// <summary>
        /// 单据大类
        /// </summary>
        public virtual long? OrderLargeCategory { get; set; }
        
        /// <summary>
        /// 单据小类
        /// </summary>
        public virtual long? OrderSubclass { get; set; }
        
        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        public virtual long? SOID { get; set; }
        
        /// <summary>
        /// 单据编号
        /// </summary>
        public virtual string? NO { get; set; }
        
        /// <summary>
        /// 创建日期
        /// </summary>
        public virtual DateTimeOffset? Billdate { get; set; }
        
        /// <summary>
        /// 供应商编码
        /// </summary>
        public virtual string? Vendorcode { get; set; }
        
        /// <summary>
        /// 供应商名称
        /// </summary>
        public virtual string? Vendorname { get; set; }
        
        /// <summary>
        /// 公司名称
        /// </summary>
        public virtual string? Companyname { get; set; }
        
        /// <summary>
        /// 仓管员
        /// </summary>
        public virtual string? WarehouseKeepername { get; set; }
        
        /// <summary>
        /// 仓储中心
        /// </summary>
        public virtual string? WarehouseCentername { get; set; }
        
        /// <summary>
        /// 退货状态
        /// </summary>
        public virtual Admin.NET.Core.GoodsReturnStatus? GoodsReturnStatus { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState? IssueState { get; set; }
        
    }

    /// <summary>
    /// 退货单输入参数
    /// </summary>
    public class WmsGoodsReturnOrderInput
    {
        /// <summary>
        /// 单据大类
        /// </summary>
        public virtual long OrderLargeCategory { get; set; }
        
        /// <summary>
        /// 单据小类
        /// </summary>
        public virtual long OrderSubclass { get; set; }
        
        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        public virtual long SOID { get; set; }
        
        /// <summary>
        /// 单据编号
        /// </summary>
        public virtual string NO { get; set; }
        
        /// <summary>
        /// 创建日期
        /// </summary>
        public virtual DateTimeOffset Billdate { get; set; }
        
        /// <summary>
        /// 供应商编码
        /// </summary>
        public virtual string Vendorcode { get; set; }
        
        /// <summary>
        /// 供应商名称
        /// </summary>
        public virtual string Vendorname { get; set; }
        
        /// <summary>
        /// 公司名称
        /// </summary>
        public virtual string Companyname { get; set; }
        
        /// <summary>
        /// 仓管员
        /// </summary>
        public virtual string WarehouseKeepername { get; set; }
        
        /// <summary>
        /// 仓储中心
        /// </summary>
        public virtual string WarehouseCentername { get; set; }
        
        /// <summary>
        /// 退货状态
        /// </summary>
        public virtual Admin.NET.Core.GoodsReturnStatus GoodsReturnStatus { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState IssueState { get; set; }
        
    }

    public class AddWmsGoodsReturnOrderInput : WmsGoodsReturnOrderInput
    {
    }

    public class DeleteWmsGoodsReturnOrderInput : BaseId
    {
    }

    public class UpdateWmsGoodsReturnOrderInput : WmsGoodsReturnOrderInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
        
    }

    public class QueryeWmsGoodsReturnOrderInput : BaseId
    {

    }

    /// <summary>
    /// 退货单获取明细参数
    /// </summary>
    public class GoodsReturnOrdePageDetailInput : PageInputBase
    {
        /// <summary>
        /// Id
        /// </summary>
        public long? Id { get; set; }
    }

    /// <summary>
    /// 退货下架给yigo系统的参数
    /// </summary>
    public class ReturnGoodsReturnOrderInput
    {
        /// <summary>
        /// Id
        /// </summary>
        public long? Id { get; set; }

        /// <summary>
        /// YIGO系统id
        /// </summary>
        public long? SOID { get; set; }

        /// <summary>
        /// 入库通知单号
        /// </summary>
        public string NO { get; set; }

        /// <summary>
        /// WMS退货单号
        /// </summary>
        public string WMSNO { get; set; }

        /// <summary>
        ///下架时间
        /// </summary>
        public DateTimeOffset? Billdate { get; set; }

        /// <summary>
        /// 退货详情
        /// </summary>
        public List<ReturnGoodsReturnOrderInputDetail> Dtls { get; set; }
    }

    /// <summary>
    /// 退货下架给yigo系统的详情参数
    /// </summary>
    public class ReturnGoodsReturnOrderInputDetail
    {
        /// <summary>
        /// OID  
        /// </summary>
        public long OID { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string Materialname { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        public string Materialcode { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Qty { get; set; }

        /// <summary>
        /// 重量    
        /// </summary>
        public decimal SumWeight { get; set; }

        /// <summary>
        /// 库区    
        /// </summary>
        public string DestStoreareacode { get; set; }

        /// <summary>
        /// 储位    
        /// </summary>
        public string DestLocationcode { get; set; }
    }
}
