﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application
{
    /// <summary>
    /// 打包信息返回参数
    /// </summary>
    public class GroupDiskOutWarehouseOutput
    {
        /// <summary>
        /// 单据编号  
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 单据小类
        /// </summary>
        public string OrderSubclass { get; set; }

        /// <summary>
        /// 打包信息list
        /// </summary>
        public List<GroupDiskOutWarehouse> GroupDiskOutWarehouseList { get; set; }
    }

    /// <summary>
    /// 打包信息
    /// </summary>
    public class GroupDiskOutWarehouse
    {
        /// <summary>
        /// 物料编码
        /// </summary>
        public virtual string MaterialNo { get; set; }

        /// <summary>
        /// 长    
        /// </summary>
        public int Long { get; set; }

        /// <summary>
        /// 宽    
        /// </summary>
        public int Wide { get; set; }

        /// <summary>
        /// 高    
        /// </summary>
        public int High { get; set; }
    }
}
