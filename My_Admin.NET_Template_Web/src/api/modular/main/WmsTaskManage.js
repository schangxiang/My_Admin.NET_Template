﻿import { axios } from '@/utils/request'

/**
 * 查询任务管理
 *
 * @author liduanping
 */
export function WmsTaskPage (parameter) {
  return axios({
    url: '/WmsTask/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 任务物料详情
 *
 * @author yhh
 */
export function WmsTaskDetail (parameter) {
  return axios({
    url: '/WmsTask/detail',
    method: 'get',
    params: parameter
  })
}

/**
 * 删除任务
 *
 * @author yhh
 */
export function WmsTaskCancel (id) {
  return axios({
    url: '/WmsTask/CancelTask',
    method: 'post',
    data: {id}
  })
}

/**
 * 强制完成
 *
 * @author yhh
 */
export function WmsTaskFinish (id) {
  return axios({
    url: '/WmsTask/finish',
    method: 'post',
    data: {id}
  })
}

/**
 * 更改优先级
 *
 * @author yhh
 */
export function WmsTaskUpdateLevel (parameter) {
  return axios({
    url: '/WmsTask/upwardTaskLevel',
    method: 'get',
    params: parameter
  })
}
