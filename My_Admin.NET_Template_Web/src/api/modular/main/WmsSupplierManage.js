﻿import { axios } from '@/utils/request'

/**
 * 查询供应商
 *
 * @author li
 */
export function WmsSupplierPage (parameter) {
  return axios({
    url: '/WmsSupplier/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 供应商列表
 *
 * @author li
 */
export function WmsSupplierList (parameter) {
  return axios({
    url: '/WmsSupplier/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加供应商
 *
 * @author li
 */
export function WmsSupplierAdd (parameter) {
  return axios({
    url: '/WmsSupplier/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑供应商
 *
 * @author li
 */
export function WmsSupplierEdit (parameter) {
  return axios({
    url: '/WmsSupplier/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除供应商
 *
 * @author li
 */
export function WmsSupplierDelete (parameter) {
  return axios({
    url: '/WmsSupplier/delete',
    method: 'post',
    data: parameter
  })
}
