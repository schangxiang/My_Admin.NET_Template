﻿import { axios } from '@/utils/request'

/**
 * 查询工位表
 *
 * @author li
 */
export function LesStationPage (parameter) {
  return axios({
    url: '/LesStation/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 工位表列表
 *
 * @author li
 */
export function LesStationList (parameter) {
  return axios({
    url: '/LesStation/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加工位表
 *
 * @author li
 */
export function LesStationAdd (parameter) {
  return axios({
    url: '/LesStation/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑工位表
 *
 * @author li
 */
export function LesStationEdit (parameter) {
  return axios({
    url: '/LesStation/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除工位表
 *
 * @author li
 */
export function LesStationDelete (parameter) {
  return axios({
    url: '/LesStation/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出工位表的Excel文件
 *
 * @author li
 */
export function LesStationToExcel (parameter) {
  return axios({
    url: '/LesStation/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入工位表的Excel文件
 *
 * @author li
 */
export function LesStationFromExcel (data, parameter) {
  return axios({
    url: '/LesStation/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载工位表的Excel导入模板
 *
 * @author li
 */
export function LesStationDownloadExcelTemplate(parameter) {
  return axios({
    url: '/LesStation/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}




/**
* 获取LesProductionline列表
* @author li
*/
export function LesStationFkLesProductionlineList() {
  return axios({
    url: '/LesStation/fkLesProductionline',
    method: 'get'
  })
}


/**
 * 获取产线数据
 *
 * @author yhh
 */
export function GetLines () {
  return axios({
    url: '/LesStation/GetProductionlineList',
    method: 'get'
  })
}
 
/**
 * 根据产线，获取工位数据
 *
 * @author yhh
 */
export function GetStations (id) {
  return axios({
    url: '/LesStation/GetStationList',
    method: 'get',
    params:{id}
  })
}

/**
 * 获取产线信息
 */
export function getProLineList() {
  return axios({
    url: '/lesstation/fkLesProductionline',
    method: 'get'
  })
}