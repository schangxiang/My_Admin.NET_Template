﻿using Admin.NET.Core;
using Admin.NET.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace Admin.NET.Application
{    
    
    /// <summary>
    /// 单据表查询参数
    /// </summary>
    public class WmsOrderSearch : PageInputBase
    {
        /// <summary>
        /// 单据大类
        /// </summary>
        public virtual long? OrderLargeCategory { get; set; }
        
        /// <summary>
        /// 单据小类
        /// </summary>
        public virtual long? OrderSubclass { get; set; }
        
        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        public virtual long? SOID { get; set; }
        
        /// <summary>
        /// 单据编号
        /// </summary>
        public virtual string NO { get; set; }
        
        /// <summary>
        /// 创建日期
        /// </summary>
        public virtual DateTimeOffset? Billdate { get; set; }
        
        /// <summary>
        /// 送货通知单号
        /// </summary>
        public virtual string SourceBillNo { get; set; }
        
        /// <summary>
        /// 供应商编码
        /// </summary>
        public virtual string Vendorcode { get; set; }
        
        /// <summary>
        /// 供应商名称
        /// </summary>
        public virtual string Vendorname { get; set; }
        
        /// <summary>
        /// 公司名称
        /// </summary>
        public virtual string Companyname { get; set; }
        
        /// <summary>
        /// 仓管员
        /// </summary>
        public virtual string WarehouseKeepername { get; set; }
        
        /// <summary>
        /// 贸易方式
        /// </summary>
        public virtual Admin.NET.Core.TradeMode? TradeMode { get; set; }
        
        /// <summary>
        /// 仓储中心
        /// </summary>
        public virtual string WarehouseCentername { get; set; }
        
        /// <summary>
        /// 仓间
        /// </summary>
        public virtual string StoreRoomname { get; set; }
        
        /// <summary>
        /// 报关单号
        /// </summary>
        public virtual string DeclarationCode { get; set; }
        
        /// <summary>
        /// 专业
        /// </summary>
        public virtual string Majorname { get; set; }
        
        /// <summary>
        /// 备注
        /// </summary>
        public virtual string Remarks { get; set; }
        
        /// <summary>
        /// 单据状态
        /// </summary>
        public virtual Admin.NET.Core.OrderStatusEnum? OrderStatus { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState IssueState { get; set; }
        
    }

        /// <summary>
    /// 单据表不分页查询参数
    /// </summary>
    public class WmsOrderSearchNonPage : PageInputNonPageBase
    {
        /// <summary>
        /// 单据大类
        /// </summary>
        public virtual long? OrderLargeCategory { get; set; }
        
        /// <summary>
        /// 单据小类
        /// </summary>
        public virtual long? OrderSubclass { get; set; }
        
        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        public virtual long? SOID { get; set; }
        
        /// <summary>
        /// 单据编号
        /// </summary>
        public virtual string? NO { get; set; }
        
        /// <summary>
        /// 创建日期
        /// </summary>
        public virtual DateTimeOffset? Billdate { get; set; }
        
        /// <summary>
        /// 送货通知单号
        /// </summary>
        public virtual string? SourceBillNo { get; set; }
        
        /// <summary>
        /// 供应商编码
        /// </summary>
        public virtual string? Vendorcode { get; set; }
        
        /// <summary>
        /// 供应商名称
        /// </summary>
        public virtual string? Vendorname { get; set; }
        
        /// <summary>
        /// 公司名称
        /// </summary>
        public virtual string? Companyname { get; set; }
        
        /// <summary>
        /// 仓管员
        /// </summary>
        public virtual string? WarehouseKeepername { get; set; }
        
        /// <summary>
        /// 贸易方式
        /// </summary>
        public virtual Admin.NET.Core.TradeMode? TradeMode { get; set; }
        
        /// <summary>
        /// 仓储中心
        /// </summary>
        public virtual string? WarehouseCentername { get; set; }
        
        /// <summary>
        /// 仓间
        /// </summary>
        public virtual string? StoreRoomname { get; set; }
        
        /// <summary>
        /// 报关单号
        /// </summary>
        public virtual string? DeclarationCode { get; set; }
        
        /// <summary>
        /// 专业
        /// </summary>
        public virtual string? Majorname { get; set; }
        
        /// <summary>
        /// 备注
        /// </summary>
        public virtual string? Remarks { get; set; }
        
        /// <summary>
        /// 单据状态
        /// </summary>
        public virtual Admin.NET.Core.OrderStatusEnum? OrderStatus { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState? IssueState { get; set; }
        
    }

    /// <summary>
    /// 单据表输入参数
    /// </summary>
    public class WmsOrderInput
    {
        /// <summary>
        /// 单据大类
        /// </summary>
        public virtual long OrderLargeCategory { get; set; }
        
        /// <summary>
        /// 单据小类
        /// </summary>
        public virtual long OrderSubclass { get; set; }
        
        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        public virtual long SOID { get; set; }
        
        /// <summary>
        /// 单据编号
        /// </summary>
        public virtual string NO { get; set; }
        
        /// <summary>
        /// 创建日期
        /// </summary>
        public virtual DateTimeOffset Billdate { get; set; }
        
        /// <summary>
        /// 送货通知单号
        /// </summary>
        public virtual string SourceBillNo { get; set; }
        
        /// <summary>
        /// 供应商编码
        /// </summary>
        public virtual string Vendorcode { get; set; }
        
        /// <summary>
        /// 供应商名称
        /// </summary>
        public virtual string Vendorname { get; set; }
        
        /// <summary>
        /// 公司名称
        /// </summary>
        public virtual string Companyname { get; set; }
        
        /// <summary>
        /// 仓管员
        /// </summary>
        public virtual string WarehouseKeepername { get; set; }
        
        /// <summary>
        /// 贸易方式
        /// </summary>
        public virtual Admin.NET.Core.TradeMode TradeMode { get; set; }
        
        /// <summary>
        /// 仓储中心
        /// </summary>
        public virtual string WarehouseCentername { get; set; }
        
        /// <summary>
        /// 仓间
        /// </summary>
        public virtual string StoreRoomname { get; set; }
        
        /// <summary>
        /// 报关单号
        /// </summary>
        public virtual string DeclarationCode { get; set; }
        
        /// <summary>
        /// 专业
        /// </summary>
        public virtual string Majorname { get; set; }
        
        /// <summary>
        /// 备注
        /// </summary>
        public virtual string Remarks { get; set; }
        
        /// <summary>
        /// 单据状态
        /// </summary>
        public virtual Admin.NET.Core.OrderStatusEnum OrderStatus { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState IssueState { get; set; }
        
    }

    public class AddWmsOrderInput : WmsOrderInput
    {
    }

    public class DeleteWmsOrderInput : BaseId
    {
    }

    public class UpdateWmsOrderInput : WmsOrderInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
        
    }

    public class QueryeWmsOrderInput : BaseId
    {

    }
    public class DistributeInOrderInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public List<long> Id { get; set; }
    }
}
