﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 库存信息（熟化库）输出参数
    /// </summary>
    public class MaterialStockDto
    {
        /// <summary>
        /// 库区名称
        /// </summary>
        public string WmsAreaAreaName { get; set; }
        
        /// <summary>
        /// 物料编号
        /// </summary>
        public string MaterialNo { get; set; }
        
        /// <summary>
        /// 物料类型
        /// </summary>
        public MaterialType MaterialType { get; set; }
        
        /// <summary>
        /// 物料批次
        /// </summary>
        public string MaterialBatch { get; set; }
        
        /// <summary>
        /// 物料名称
        /// </summary>
        public string MaterialName { get; set; }
        
        /// <summary>
        /// 物料规格
        /// </summary>
        public string MaterialSpec { get; set; }

        /// <summary>
        /// 长    
        /// </summary>
        public int Long { get; set; }

        /// <summary>
        /// 宽    
        /// </summary>
        public int Wide { get; set; }

        /// <summary>
        /// 高    
        /// </summary>
        public int High { get; set; }

        /// <summary>
        /// 物料密度
        /// </summary>
        public string MaterialDensity { get; set; }
        
        /// <summary>
        /// 检验方式
        /// </summary>
        public MaterialInspection InspectionMethod { get; set; }
        
        /// <summary>
        /// 单位类别
        /// </summary>
        public UnitType UnitType { get; set; }
        
        /// <summary>
        /// 单位编号
        /// </summary>
        public UnitNoType UnitNo { get; set; }
        
        /// <summary>
        /// 库存数
        /// </summary>
        public decimal StockNumber { get; set; }
        
        /// <summary>
        /// 库位编码
        /// </summary>
        public string PlaceCode { get; set; }
        
        /// <summary>
        /// 托盘编码
        /// </summary>
        public string ContainerCode { get; set; }
        
        /// <summary>
        /// 库区ID
        /// </summary>
        public long AreaId { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
    }
}
