﻿using Admin.NET.Core;
using Admin.NET.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace Admin.NET.Application
{
    /// <summary>
    /// 代码生成参数类
    /// </summary>
    public class CodeGenPageInput : PageInputBase
    {
        /// <summary>
        /// 业务名
        /// </summary>
        public string BusName { get; set; }

        /// <summary>
        /// 数据库表名
        /// </summary>
        public string TableName { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class AddCodeGenInput
    {
        /// <summary>
        /// 低代码模块来源
        /// </summary>
        public long? LowCodeId { get; set; }

        /// <summary>
        /// 数据库名
        /// </summary>
        [Required(ErrorMessage = "数据库名不能为空")]
        public string DatabaseName { get; set; }

        /// <summary>
        /// 数据库表名
        /// </summary>
        [Required(ErrorMessage = "数据库表名不能为空")]
        public string TableName { get; set; }

        /// <summary>
        /// 业务名（业务代码包名称）
        /// </summary>
        [Required(ErrorMessage = "业务名不能为空")]
        public string BusName { get; set; }

        /// <summary>
        /// 命名空间
        /// </summary>
        [Required(ErrorMessage = "命名空间不能为空")]
        public string NameSpace { get; set; }

        /// <summary>
        /// 作者姓名
        /// </summary>
        [Required(ErrorMessage = "作者姓名不能为空")]
        public string AuthorName { get; set; }

        /// <summary>
        /// 模块路径
        /// </summary>
        [Required(ErrorMessage = "模块路径不能为空")]
        public string ModuleName { get; set; }

        /// <summary>
        /// 是否只是查询
        /// </summary>
        [Required(ErrorMessage = "是否只是查询不能为空")]
        public bool IsOnlyQuery { get; set; }

        /// <summary>
        /// 前端项目名
        /// </summary>
        [Required(ErrorMessage = "前端项目名不能为空")]
        public string FrontProName { get; set; }

        /// <summary>
        /// 生成方式
        /// </summary>
        [Required(ErrorMessage = "生成方式不能为空")]
        public string GenerateType { get; set; }

        /// <summary>
        /// 菜单应用分类（应用编码）
        /// </summary>
        [Required(ErrorMessage = "菜单应用分类不能为空")]
        public string MenuApplication { get; set; }

        /// <summary>
        /// 菜单父级
        /// </summary>
        [Required(ErrorMessage = "菜单父级不能为空")]
        public long MenuPid { get; set; }

        /// <summary>
        /// 类名
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// 是否移除表前缀
        /// </summary>
        public string TablePrefix { get; set; }

        /// <summary>
        /// 功能名（数据库表名称）
        /// </summary>
        public string TableComment { get; set; }

        /// <summary>
        /// 是否支持导入
        /// </summary>
        [Required(ErrorMessage = "是否支持导入")]
        public bool IsWhetherImport { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DeleteCodeGenInput : BaseId
    {
    }

    /// <summary>
    /// 
    /// </summary>
    public class UpdateCodeGenInput
    {
        /// <summary>
        /// 代码生成器Id
        /// </summary>
        [Required(ErrorMessage = "代码生成器Id不能为空")]
        public long Id { get; set; }

        /// <summary>
        /// 数据库名
        /// </summary>
        [Required(ErrorMessage = "数据库名不能为空")]
        public string DatabaseName { get; set; }

        /// <summary>
        /// 数据库表名
        /// </summary>
        [Required(ErrorMessage = "数据库表名不能为空")]
        public string TableName { get; set; }

        /// <summary>
        /// 业务名（业务代码包名称）
        /// </summary>
        [Required(ErrorMessage = "业务名不能为空")]
        public string BusName { get; set; }

        /// <summary>
        /// 命名空间
        /// </summary>
        [Required(ErrorMessage = "命名空间不能为空")]
        public string NameSpace { get; set; }

        /// <summary>
        /// 模块路径
        /// </summary>
        [Required(ErrorMessage = "模块路径不能为空")]
        public string ModuleName { get; set; }


        /// <summary>
        /// 是否只是查询
        /// </summary>
        [Required(ErrorMessage = "是否只是查询不能为空")]
        public bool IsOnlyQuery { get; set; }

        /// <summary>
        /// 前端项目名
        /// </summary>
        [Required(ErrorMessage = "前端项目名不能为空")]
        public string FrontProName { get; set; }

        /// <summary>
        /// 作者姓名
        /// </summary>
        [Required(ErrorMessage = "作者姓名不能为空")]
        public string AuthorName { get; set; }

        /// <summary>
        /// 生成方式
        /// </summary>
        [Required(ErrorMessage = "生成方式不能为空")]
        public string GenerateType { get; set; }

        /// <summary>
        /// 菜单应用分类（应用编码）
        /// </summary>
        [Required(ErrorMessage = "菜单应用分类不能为空")]
        public string MenuApplication { get; set; }

        /// <summary>
        /// 菜单父级
        /// </summary>
        [Required(ErrorMessage = "菜单父级不能为空")]
        public long MenuPid { get; set; }

        /// <summary>
        /// 类名
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// 是否移除表前缀
        /// </summary>
        public string TablePrefix { get; set; }

        /// <summary>
        /// 功能名（数据库表名称）
        /// </summary>
        public string TableComment { get; set; }

        /// <summary>
        /// 是否支持导入
        /// </summary>
        [Required(ErrorMessage = "是否支持导入")]
        public bool IsWhetherImport { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class QueryCodeGenInput : BaseId
    {
    }
}