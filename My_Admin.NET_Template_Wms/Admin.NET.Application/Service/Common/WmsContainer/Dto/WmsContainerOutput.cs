﻿using Admin.NET.Core.Util.LowCode.Dto;
using System;

namespace Admin.NET.Application
{
    /// <summary>
    /// 托盘信息输出参数
    /// </summary>
    public class WmsContainerOutput
    {
        /// <summary>
        /// 编号
        /// </summary>
        public string ContainerCode { get; set; }
        
        /// <summary>
        /// 类型
        /// </summary>
        public Admin.NET.Core.ContainerType ContainerType { get; set; }
        
        /// <summary>
        /// 托盘状态
        /// </summary>
        public Admin.NET.Core.ContainerStatus ContainerStatus { get; set; }
        
        /// <summary>
        /// 长度
        /// </summary>
        public decimal SpecLength { get; set; }
        
        /// <summary>
        /// 宽度
        /// </summary>
        public decimal SpecWidth { get; set; }
        
        /// <summary>
        /// 高度
        /// </summary>
        public decimal SpecHeight { get; set; }
        
        /// <summary>
        /// 限长
        /// </summary>
        public decimal LimitLength { get; set; }
        
        /// <summary>
        /// 限宽
        /// </summary>
        public decimal LimitWidth { get; set; }
        
        /// <summary>
        /// 限高
        /// </summary>
        public decimal LimitHeight { get; set; }
        
        /// <summary>
        /// 载重上限
        /// </summary>
        public decimal MaxWeight { get; set; }
        
        /// <summary>
        /// 父托盘Id
        /// </summary>
        public long ParentContainerId { get; set; }
        
        /// <summary>
        /// 资产编号
        /// </summary>
        public string AssetNo { get; set; }
        
        /// <summary>
        /// 托盘分类
        /// </summary>
        public Admin.NET.Core.ContainerCategory ContainerCategory { get; set; }
        
        /// <summary>
        /// Erp单号
        /// </summary>
        public string ErpNo { get; set; }
        
        /// <summary>
        /// 是否虚拟
        /// </summary>
        public Admin.NET.Core.YesOrNot IsVirtually { get; set; }
        
        /// <summary>
        /// 所属车间
        /// </summary>
        public Admin.NET.Core.LesWorkShopType WorkShopType { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public Admin.NET.Core.IssueState IssueState { get; set; }
        
    }
}
