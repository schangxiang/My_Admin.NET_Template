﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    /// <summary>
    /// 转储单明细
    /// </summary>
    [Table("wms_dumporder_details")]
    [Comment("转储单明细")]
    public class WmsDumpOrderDetails : DEntityBase
    {
        /// <summary>
        /// 单据Id  
        /// </summary>
        [Comment("单据Id")]
        [MaxLength(50)]
        public long OrderId { get; set; }

        /// <summary>
        /// 上位系统单据明细唯一识别码
        /// </summary>
        [Comment("上位系统单据明细唯一识别码")]
        public long OID { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        [Comment("物料编号")]
        [MaxLength(255)]
        public string Materialcode { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        [Comment("物料名称")]
        [MaxLength(255)]
        public string Materialname { get; set; }

        /// <summary>
        /// 基本单位    
        /// </summary>
        [Comment("基本单位")]
        [MaxLength(255)]
        public string Unit { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        [Comment("数量")]
        public decimal Qty { get; set; }
        
        /// <summary>
        /// 长度 
        /// </summary>
        [Comment("长度")]
        public decimal Length { get; set; }

        /// <summary>
        /// SCM批次号    
        /// </summary>
        [Comment("SCM批次号")]
        [MaxLength(255)]
        public string Batchno_SCM { get; set; }

        /// <summary>
        /// WMS批次号    
        /// </summary>
        [Comment("WMS批次号")]
        [MaxLength(255)]
        public string Batchno_WMS { get; set; }

        /// <summary>
        /// 库存项目编码    
        /// </summary>
        [Comment("库存项目编码")]
        [MaxLength(255)]
        public string ProjectCode { get; set; }

        /// <summary>
        /// 新库存项目编码    
        /// </summary>
        [Comment("新库存项目编码")]
        [MaxLength(255)]
        public string Projectcode_To { get; set; }

        /// <summary>
        /// 单据状态
        /// </summary>
        [Comment("单据状态")]
        [Required]
        public OrderDetailsStatusEnum OrderStatus { get; set; } = OrderDetailsStatusEnum.WEIZHIXING;

        /// <summary>
        /// 单据主表
        /// </summary>
        public WmsDumpOrder WmsDumpOrder { get; set; }
    }
}
