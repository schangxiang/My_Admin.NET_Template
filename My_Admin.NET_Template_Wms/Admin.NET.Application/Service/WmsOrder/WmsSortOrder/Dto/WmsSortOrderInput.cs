﻿using Admin.NET.Core;
using Admin.NET.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace Admin.NET.Application
{    
    
    /// <summary>
    /// 分拣单查询参数
    /// </summary>
    public class WmsSortOrderSearch : PageInputBase
    {
        /// <summary>
        /// 来源单号
        /// </summary>
        public virtual string OrderNo { get; set; }


        /// <summary>
        /// 物料编号
        /// </summary>
        public string Materialcode { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string Materialname { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual long? OrderDetailID { get; set; }
        
        /// <summary>
        /// 分拣组盘单号
        /// </summary>
        public virtual string ContainerOrderNo { get; set; }
        
        /// <summary>
        /// 项目编号
        /// </summary>
        public virtual string ProjectCode { get; set; }
        
        /// <summary>
        /// TA号
        /// </summary>
        public virtual string TACode { get; set; }
        
        /// <summary>
        /// 分段号
        /// </summary>
        public virtual string PartCode { get; set; }
        
        /// <summary>
        /// 库位编码
        /// </summary>
        public virtual string PlaceCode { get; set; }
        
        /// <summary>
        /// 容器编号
        /// </summary>
        public virtual string ContainerCode { get; set; }
        
        /// <summary>
        /// 分拣数
        /// </summary>
        public virtual decimal? SortQuantity { get; set; }
        
        /// <summary>
        /// 实际分拣数
        /// </summary>
        public virtual decimal? ActualQuantity { get; set; }
        
        /// <summary>
        /// 备注
        /// </summary>
        public virtual string Remarks { get; set; }
        
        /// <summary>
        /// 状态-未分拣_1、分拣完成_2
        /// </summary>
        public virtual Admin.NET.Core.SortStatusEnum? SortStatus { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState IssueState { get; set; }
        
    }

        /// <summary>
    /// 分拣单不分页查询参数
    /// </summary>
    public class WmsSortOrderSearchNonPage : PageInputNonPageBase
    {
        /// <summary>
        /// 来源单号
        /// </summary>
        public virtual string? OrderNo { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public virtual long? OrderDetailID { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        public string Materialcode { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string Materialname { get; set; }

        /// <summary>
        /// 分拣组盘单号
        /// </summary>
        public virtual string? ContainerOrderNo { get; set; }
        
        /// <summary>
        /// 项目编号
        /// </summary>
        public virtual string? ProjectCode { get; set; }
        
        /// <summary>
        /// TA号
        /// </summary>
        public virtual string? TACode { get; set; }
        
        /// <summary>
        /// 分段号
        /// </summary>
        public virtual string? PartCode { get; set; }
        
        /// <summary>
        /// 库位编码
        /// </summary>
        public virtual string? PlaceCode { get; set; }
        
        /// <summary>
        /// 容器编号
        /// </summary>
        public virtual string? ContainerCode { get; set; }
        
        /// <summary>
        /// 分拣数
        /// </summary>
        public virtual decimal? SortQuantity { get; set; }
        
        /// <summary>
        /// 实际分拣数
        /// </summary>
        public virtual decimal? ActualQuantity { get; set; }
        
        /// <summary>
        /// 备注
        /// </summary>
        public virtual string? Remarks { get; set; }
        
        /// <summary>
        /// 状态-未分拣_1、分拣完成_2
        /// </summary>
        public virtual Admin.NET.Core.SortStatusEnum? SortStatus { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState? IssueState { get; set; }
        
    }

    /// <summary>
    /// 分拣单输入参数
    /// </summary>
    public class WmsSortOrderInput
    {
        /// <summary>
        /// 来源单号
        /// </summary>
        public virtual string OrderNo { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public virtual long OrderDetailID { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        public string Materialcode { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string Materialname { get; set; }

        /// <summary>
        /// 分拣组盘单号
        /// </summary>
        public virtual string ContainerOrderNo { get; set; }
        
        /// <summary>
        /// 项目编号
        /// </summary>
        public virtual string ProjectCode { get; set; }
        
        /// <summary>
        /// TA号
        /// </summary>
        public virtual string TACode { get; set; }
        
        /// <summary>
        /// 分段号
        /// </summary>
        public virtual string PartCode { get; set; }
        
        /// <summary>
        /// 库位编码
        /// </summary>
        public virtual string PlaceCode { get; set; }
        
        /// <summary>
        /// 容器编号
        /// </summary>
        public virtual string ContainerCode { get; set; }
        
        /// <summary>
        /// 分拣数
        /// </summary>
        public virtual decimal SortQuantity { get; set; }
        
        /// <summary>
        /// 实际分拣数
        /// </summary>
        public virtual decimal ActualQuantity { get; set; }
        
        /// <summary>
        /// 备注
        /// </summary>
        public virtual string Remarks { get; set; }
        
        /// <summary>
        /// 状态-未分拣_1、分拣完成_2
        /// </summary>
        public virtual Admin.NET.Core.SortStatusEnum SortStatus { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState IssueState { get; set; }
        
    }

    public class AddWmsSortOrderInput : WmsSortOrderInput
    {
    }

    public class DeleteWmsSortOrderInput : BaseId
    {
    }

    public class UpdateWmsSortOrderInput : WmsSortOrderInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
        
    }

    public class QueryeWmsSortOrderInput : BaseId
    {

    }
}
