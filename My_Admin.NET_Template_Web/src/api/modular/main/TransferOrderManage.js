import { axios } from '@/utils/request'

/**
 * 查询调拨单
 *
 * @author yc
 */
export function TransferOrderPage (parameter) {
  return axios({
    url: '/TransferOrder/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 调拨单列表
 *
 * @author yc
 */
export function TransferOrderList (parameter) {
  return axios({
    url: '/TransferOrder/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加调拨单
 *
 * @author yc
 */
export function TransferOrderAdd (parameter) {
  return axios({
    url: '/TransferOrder/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑调拨单
 *
 * @author yc
 */
export function TransferOrderEdit (parameter) {
  return axios({
    url: '/TransferOrder/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除调拨单
 *
 * @author yc
 */
export function TransferOrderDelete (parameter) {
  return axios({
    url: '/TransferOrder/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出调拨单的Excel文件
 *
 * @author yc
 */
export function TransferOrderToExcel (parameter) {
  return axios({
    url: '/TransferOrder/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入调拨单的Excel文件
 *
 * @author yc
 */
export function TransferOrderFromExcel (data, parameter) {
  return axios({
    url: '/TransferOrder/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载调拨单的Excel导入模板
 *
 * @author yc
 */
export function TransferOrderDownloadExcelTemplate(parameter) {
  return axios({
    url: '/TransferOrder/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 单据大类下拉
 *
 * @author yhh
 */
export function TransferOrderLargeCategories() {
  return axios({
    url: '/TransferOrder/OrderTypeLargeCategory',
    method: 'get'
  })
}

/**
 * 单据小类下拉
 *
 * @author yhh
 */
export function TransferOrderSubclass(id) {
  return axios({
    url: '/TransferOrder/OrderTypeSubclass',
    method: 'get',
    params:{id}
  })
}


/**
 * 调拨单详情
 *
 * @author yhh
 */
export function TransferOrderDetailPage (parameter) {
  return axios({
    url: '/TransferOrder/DetailsPage',
    method: 'get',
    params: parameter
  })
}




