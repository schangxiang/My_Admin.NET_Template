﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Core
{
    /// <summary>
    /// 公共方法
    /// </summary>
    public static class SysCommUtil
    {
        /// <summary>
        /// 生成出入库单号流水
        /// </summary>
        /// <param name="maxInvoiceNumber"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GenerateNewInvoiceNumber(string maxInvoiceNumber, int type)
        {

            if (string.IsNullOrEmpty(maxInvoiceNumber))
            {
                string today = DateTime.Today.ToString("yyyyMMdd");
                //判断是入库还是出库
                if (type == 1)
                {
                    return "RUKU" + today + "0001";
                }
                else
                {
                    return "CHUKU" + today + "0001";
                }

            }
            else
            {
                var before = maxInvoiceNumber.Substring(0, 12);
                var queen = maxInvoiceNumber.Substring(12);
                int maxNumber = int.Parse(queen);
                return before + (maxNumber + 1).ToString("D4");
            }
        }

        /// <summary>
        /// 验证日期查询范围字符串数组是否符合查询值
        /// </summary>
        /// <param name="list"></param>
        /// <returns>true:符合查询值，false：不符合查询值</returns>
        public static bool ValidateListForQueryTimeArray(List<string> list)
        {
            if (list == null) return false;
            if (list.Count != 2) return false;
            if (string.IsNullOrEmpty(list[0]) || string.IsNullOrEmpty(list[1])) return false;
            return true;
        }
    }
}
