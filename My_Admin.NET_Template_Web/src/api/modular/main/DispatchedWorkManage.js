﻿import { axios } from '@/utils/request'

/**
 * 查询发货作业
 *
 * @author yc
 */
export function DispatchedWorkPage (parameter) {
  return axios({
    url: '/DispatchedWork/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 发货作业列表
 *
 * @author yc
 */
export function DispatchedWorkList (parameter) {
  return axios({
    url: '/DispatchedWork/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加发货作业
 *
 * @author yc
 */
export function DispatchedWorkAdd (parameter) {
  return axios({
    url: '/DispatchedWork/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑发货作业
 *
 * @author yc
 */
export function DispatchedWorkEdit (parameter) {
  return axios({
    url: '/DispatchedWork/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除发货作业
 *
 * @author yc
 */
export function DispatchedWorkDelete (parameter) {
  return axios({
    url: '/DispatchedWork/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出发货作业的Excel文件
 *
 * @author yc
 */
export function DispatchedWorkToExcel (parameter) {
  return axios({
    url: '/DispatchedWork/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入发货作业的Excel文件
 *
 * @author yc
 */
export function DispatchedWorkFromExcel (data, parameter) {
  return axios({
    url: '/DispatchedWork/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载发货作业的Excel导入模板
 *
 * @author yc
 */
export function DispatchedWorkDownloadExcelTemplate(parameter) {
  return axios({
    url: '/DispatchedWork/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 单据详情
 *
 * @author yhh
 */
export function DispatchedWorkDetailPage (parameter) {
  return axios({
    url: '/DispatchedWork/DetailsPage',
    method: 'get',
    params: parameter
  })
}

