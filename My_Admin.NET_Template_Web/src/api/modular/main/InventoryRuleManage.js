﻿import { axios } from '@/utils/request'

/**
 * 查询盘点规则
 *
 * @author li
 */
export function InventoryRulePage (parameter) {
  return axios({
    url: '/InventoryRule/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 盘点规则列表
 *
 * @author li
 */
export function InventoryRuleList (parameter) {
  return axios({
    url: '/InventoryRule/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加盘点规则
 *
 * @author li
 */
export function InventoryRuleAdd (parameter) {
  return axios({
    url: '/InventoryRule/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑盘点规则
 *
 * @author li
 */
export function InventoryRuleEdit (parameter) {
  return axios({
    url: '/InventoryRule/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除盘点规则
 *
 * @author li
 */
export function InventoryRuleDelete (parameter) {
  return axios({
    url: '/InventoryRule/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出盘点规则的Excel文件
 *
 * @author li
 */
export function InventoryRuleToExcel (parameter) {
  return axios({
    url: '/InventoryRule/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入盘点规则的Excel文件
 *
 * @author li
 */
export function InventoryRuleFromExcel (data, parameter) {
  return axios({
    url: '/InventoryRule/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载盘点规则的Excel导入模板
 *
 * @author li
 */
export function InventoryRuleDownloadExcelTemplate(parameter) {
  return axios({
    url: '/InventoryRule/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}



