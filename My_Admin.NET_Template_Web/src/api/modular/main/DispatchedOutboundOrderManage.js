﻿import { axios } from '@/utils/request'

/**
 * 查询发货出库
 *
 * @author yc
 */
export function DispatchedOutboundOrderPage (parameter) {
  return axios({
    url: '/DispatchedOutboundOrder/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 发货出库列表
 *
 * @author yc
 */
export function DispatchedOutboundOrderList (parameter) {
  return axios({
    url: '/DispatchedOutboundOrder/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加发货出库
 *
 * @author yc
 */
export function DispatchedOutboundOrderAdd (parameter) {
  return axios({
    url: '/DispatchedOutboundOrder/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑发货出库
 *
 * @author yc
 */
export function DispatchedOutboundOrderEdit (parameter) {
  return axios({
    url: '/DispatchedOutboundOrder/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除发货出库
 *
 * @author yc
 */
export function DispatchedOutboundOrderDelete (parameter) {
  return axios({
    url: '/DispatchedOutboundOrder/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出发货出库的Excel文件
 *
 * @author yc
 */
export function DispatchedOutboundOrderToExcel (parameter) {
  return axios({
    url: '/DispatchedOutboundOrder/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入发货出库的Excel文件
 *
 * @author yc
 */
export function DispatchedOutboundOrderFromExcel (data, parameter) {
  return axios({
    url: '/DispatchedOutboundOrder/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载发货出库的Excel导入模板
 *
 * @author yc
 */
export function DispatchedOutboundOrderDownloadExcelTemplate(parameter) {
  return axios({
    url: '/DispatchedOutboundOrder/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 单据大类下拉
 *
 * @author yhh
 */
export function DispatchedOutboundOrderLargeCategories() {
  return axios({
    url: '/DispatchedOutboundOrder/OrderTypeLargeCategory',
    method: 'get'
  })
}

/**
 * 单据小类下拉
 *
 * @author yhh
 */
export function DispatchedOutboundOrderSubclass(id) {
  return axios({
    url: '/DispatchedOutboundOrder/OrderTypeSubclass',
    method: 'get',
    params:{id}
  })
}


/**
 * 发货出库单详情
 *
 * @author yhh
 */
export function DispatchedOutboundOrderDetailPage (parameter) {
  return axios({
    url: '/DispatchedOutboundOrder/DetailsPage',
    method: 'get',
    params: parameter
  })
}




