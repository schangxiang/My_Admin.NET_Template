﻿using Admin.NET.Core;
using Admin.NET.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace Admin.NET.Application
{    
    
    /// <summary>
    /// 测试学生表查询参数
    /// </summary>
    public class TestStudent5Search : PageInputBase
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public virtual string Name { get; set; }
        
        /// <summary>
        /// 年龄
        /// </summary>
        public virtual int? Age { get; set; }
        
        /// <summary>
        /// 创建时间
        /// </summary>
        public virtual List<string> CreatedTime { get; set; }
        
        /// <summary>
        /// 更新时间
        /// </summary>
        public virtual List<string> UpdatedTime { get; set; }
        
        /// <summary>
        /// 创建者名称
        /// </summary>
        public virtual string CreatedUserName { get; set; }
        
        /// <summary>
        /// 修改者名称
        /// </summary>
        public virtual string UpdatedUserName { get; set; }
        
    }

        /// <summary>
    /// 测试学生表不分页查询参数
    /// </summary>
    public class TestStudent5SearchNonPage : PageInputNonPageBase
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public virtual string Name { get; set; }
        
        /// <summary>
        /// 年龄
        /// </summary>
        public virtual int? Age { get; set; }
        
        /// <summary>
        /// 创建时间
        /// </summary>
         public virtual List<DateTimeOffset>? CreatedTime { get; set; }
        
        /// <summary>
        /// 更新时间
        /// </summary>
         public virtual List<DateTimeOffset>? UpdatedTime { get; set; }
        
        /// <summary>
        /// 创建者名称
        /// </summary>
        public virtual string CreatedUserName { get; set; }
        
        /// <summary>
        /// 修改者名称
        /// </summary>
        public virtual string UpdatedUserName { get; set; }
        
    }

    /// <summary>
    /// 测试学生表输入参数
    /// </summary>
    public class TestStudent5Input
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public virtual string Name { get; set; }
        
        /// <summary>
        /// 年龄
        /// </summary>
        public virtual int Age { get; set; }
        
        /// <summary>
        /// 创建时间
        /// </summary>
        public virtual DateTimeOffset CreatedTime { get; set; }
        
        /// <summary>
        /// 更新时间
        /// </summary>
        public virtual DateTimeOffset UpdatedTime { get; set; }
        
        /// <summary>
        /// 创建者名称
        /// </summary>
        public virtual string CreatedUserName { get; set; }
        
        /// <summary>
        /// 修改者名称
        /// </summary>
        public virtual string UpdatedUserName { get; set; }
        
    }

    /// <summary>
    /// 测试学生表新增参数
    /// </summary>
    public class AddTestStudent5Input : TestStudent5Input
    {
    }

    /// <summary>
    /// 测试学生表删除参数
    /// </summary>
    public class DeleteTestStudent5Input : BaseId
    {
    }

    /// <summary>
    /// 测试学生表更新参数
    /// </summary>
    public class UpdateTestStudent5Input : TestStudent5Input
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
        
    }

    /// <summary>
    /// 测试学生表获取单个参数
    /// </summary>
    public class QueryeTestStudent5Input : BaseId
    {

    }
}
