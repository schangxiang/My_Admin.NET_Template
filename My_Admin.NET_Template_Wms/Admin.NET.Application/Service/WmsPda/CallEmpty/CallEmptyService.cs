﻿using Furion.DatabaseAccessor;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Admin.NET.Core;
using Yitter.IdGenerator;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc.RazorPages;
using StackExchange.Redis;

namespace Admin.NET.Application
{
    /// <summary>
    /// Pda空托出库
    /// </summary>
    [ApiDescriptionSettings("Pda空托出库", Name = "CallEmpty", Order = 100)]
    [Route("api/[Controller]")]
    public class CallEmptyService : IDynamicApiController, ITransient
    {
        private readonly IRepository<WmsOrder, MasterDbContextLocator> _wmsOrderRep;
        private readonly IRepository<WmsOrderDetails, MasterDbContextLocator> _wmsOrderDetailsRep;
        private readonly IRepository<WmsTask, MasterDbContextLocator> _wmsTaskRep;
        private readonly IRepository<WmsPlace, MasterDbContextLocator> _wmsPlaceRep;
        private readonly IRepository<WmsArea, MasterDbContextLocator> _wmsAreaRep;
        private readonly IRepository<WmsWarehouseEntrance, MasterDbContextLocator> _wmsWarehouseEntranceRep;
        private readonly IRepository<WmsMaterialContainer, MasterDbContextLocator> _wmsMaterialContainerRep;
        private readonly IRepository<WmsContainer, MasterDbContextLocator> _wmsContainerRep;
        private readonly IRepository<WmsContainerPlace, MasterDbContextLocator> _wmsContainerPlaceRep;
        private readonly IRepository<WmsMaterialStock, MasterDbContextLocator> _wmsMaterialStockRep;
        private readonly IRepository<WmsOrderType, MasterDbContextLocator> _wmsOrderTypeRep;

        /// <summary>
        /// 构造函数
        /// </summary>
        public CallEmptyService(
            IRepository<WmsOrder, MasterDbContextLocator> wmsOrderRep,
            IRepository<WmsOrderDetails, MasterDbContextLocator> wmsOrderDetailsRep,
            IRepository<WmsTask, MasterDbContextLocator> wmsTaskRep,
            IRepository<WmsPlace, MasterDbContextLocator> wmsPlaceRep,
            IRepository<WmsArea, MasterDbContextLocator> wmsAreaRep,
            IRepository<WmsWarehouseEntrance, MasterDbContextLocator> wmsWarehouseEntranceRep,
            IRepository<WmsMaterialContainer, MasterDbContextLocator> wmsMaterialContainerRep,
            IRepository<WmsContainer, MasterDbContextLocator> wmsContainerRep,
            IRepository<WmsContainerPlace, MasterDbContextLocator> wmsContainerPlaceRep,
            IRepository<WmsMaterialStock, MasterDbContextLocator> wmsMaterialStockRep,
            IRepository<WmsOrderType, MasterDbContextLocator> wmsOrderTypeRep
        )
        {
            _wmsOrderRep = wmsOrderRep;
            _wmsOrderDetailsRep = wmsOrderDetailsRep;
            _wmsTaskRep = wmsTaskRep;
            _wmsPlaceRep = wmsPlaceRep;
            _wmsAreaRep = wmsAreaRep;
            _wmsWarehouseEntranceRep = wmsWarehouseEntranceRep;
            _wmsMaterialContainerRep = wmsMaterialContainerRep;
            _wmsContainerRep = wmsContainerRep;
            _wmsContainerPlaceRep = wmsContainerPlaceRep;
            _wmsMaterialStockRep = wmsMaterialStockRep;
            _wmsOrderTypeRep = wmsOrderTypeRep;
        }

      
        /// <summary>
        /// 提交出库
        /// </summary>
        /// <returns></returns>
        [HttpPost("PdaContainerOut")]
        [UnitOfWork]
        public async Task PdaContainerOut([FromBody] PdaContainerOutInput input)
        {
            //根据输入数量循环出库的托盘数量
            for (int i = 1; i < input.Qty+1; i++)
            {
                //获取库存中的空托盘
                var wmsMaterialStockModal = await _wmsMaterialStockRep.FirstOrDefaultAsync(x => x.Source == RuKuSourceEnum.KONGTUO);
                //获取库位信息
                var wmsPlacModal = await _wmsPlaceRep.FirstOrDefaultAsync(x => x.PlaceCode == wmsMaterialStockModal.PlaceCode);
                //任务
                var takmodel = new WmsTask()
                {
                    TaskNo = Yitter.IdGenerator.YitIdHelper.NextId().ToString(),
                    TaskModel = TaskModel.QUANZIDONG,
                    TaskType = TaskType.CHUKU,
                    TaskLevel = 1,
                    TaskStatus = TaskStatusEnum.WEIZHIXING,
                    OrderNo = "N/A",
                    OrderDetailsId = 0,
                    ContainerCode = wmsMaterialStockModal.ContainerCode,
                    SourcePlace = wmsMaterialStockModal.PlaceCode,
                    ToPlace = wmsPlacModal.Aisle.ToString(), //目标位
                    AreaName = "绝缘立库",
                    IsRead = true, //WCS是否可以读取
                    SendTimes = 1, //发送次数
                    Aisle = wmsPlacModal.Aisle,
                    TaskDodeviceStatus = TaskDodeviceStatusEnum.W,
                    Description = "空托"
                 };
                await _wmsTaskRep.InsertAsync(takmodel);
            }
        }
    }
}
