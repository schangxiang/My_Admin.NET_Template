import App from './App'
import Vue from 'vue'
import uView from '@/uni_modules/uview-ui'
import createStore from './service/store'
import config from './config/index.js'
import defineApi from './service/request'

const store = createStore(Vue);
const $api = new defineApi(store,config)

Vue.config.productionTip = false
App.mpType = 'app'
Vue.use(uView)

Vue.prototype.$modal = function(content,callback=()=>{},type = 0,title="提示") {
	uni.showModal({
		title,
		content,
		icon: type = 0 ? 'success' : 'error',
		showCancel:false,
		success:callback
	})
}

Vue.prototype.$config = config;
Vue.prototype.$api = $api;

const app = new Vue({
	...App,
	store: store
})
app.$mount()
