﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;

namespace YaKe.Les.Core.SeedData
{
    public class WmsAreaSeedData : IEntitySeedData<WmsArea>
    {
        /// <summary>
        /// 库区种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<WmsArea> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new[]
            {
                new WmsArea{Id=355701739634757,AreaName="A区",AreaDesc="",AreaType=AreaType.PUTONGK,AreaStatus=CommonStatus.ENABLE},
                new WmsArea{Id=358851237277765,AreaName="B区",AreaDesc="",AreaType=AreaType.LITIKU,AreaStatus=CommonStatus.ENABLE},
                new WmsArea{Id=358851309400133,AreaName="C区",AreaDesc="",AreaType=AreaType.GONGGONGKU,AreaStatus=CommonStatus.ENABLE}
            };
        }
    }
}
