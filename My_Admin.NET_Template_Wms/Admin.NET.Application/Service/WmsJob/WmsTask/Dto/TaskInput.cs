﻿using System.ComponentModel.DataAnnotations;
using Admin.NET.Core;
using Admin.NET.Core.Service;

namespace Admin.NET.Application
{
    /// <summary>
    /// 出入库任务管理（熟化库）查询参数
    /// </summary>
    public class WmsTaskSearch : PageInputBase
    {

        /// <summary>
        /// 批次    
        /// </summary>
        public string Batch { get; set; }

        /// <summary>
        /// 密度
        /// </summary>
        public string MaterialDensity { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        public string MaterialNo { get; set; }

        /// <summary>
        /// 任务号
        /// </summary>
        public virtual string TaskNo { get; set; }

        /// <summary>
        /// 任务方式
        /// </summary>
        public virtual TaskModel? TaskModel { get; set; }

        /// <summary>
        /// 任务类型
        /// </summary>
        public virtual TaskType? TaskType { get; set; }

        /// <summary>
        /// 任务级别
        /// </summary>
        public virtual int? TaskLevel { get; set; }

        /// <summary>
        /// 任务状态
        /// </summary>
        public virtual TaskStatusEnum? TaskStatus { get; set; }

        /// <summary>
        /// WCS是否可以读取
        /// </summary>
        public virtual bool? IsRead { get; set; }

        /// <summary>
        /// 托盘编号
        /// </summary>
        public virtual string ContainerCode { get; set; }

        /// <summary>
        /// 起始库位
        /// </summary>
        public virtual string SourcePlace { get; set; }

        /// <summary>
        /// 目标库位
        /// </summary>
        public virtual string ToPlace { get; set; }

        /// <summary>
        /// 巷道
        /// </summary>
        public virtual int? Aisle { get; set; }

        /// <summary>
        /// 库区名称
        /// </summary>
        public virtual string AreaName { get; set; }

        /// <summary>
        /// 单据明细Id
        /// </summary>
        public virtual long? OrderDetailsId { get; set; }

        /// <summary>
        /// 组盘记录单据号
        /// </summary>
        public virtual string OrderNo { get; set; }

        /// <summary>
        /// 发送次数
        /// </summary>
        public virtual int? SendTimes { get; set; }

        /// <summary>
        /// 任务执行堆垛机Id
        /// </summary>
        public virtual int? DodeviceId { get; set; }

        /// <summary>
        /// 设备执行节点Id
        /// </summary>
        public virtual string DodevicenodeId { get; set; }

        /// <summary>
        /// 设备类型
        /// </summary>
        public virtual DodeviceType? Dodevicetype { get; set; }

        /// <summary>
        /// 任务设备状态
        /// </summary>
        public virtual TaskDodeviceStatusEnum? TaskDodeviceStatus { get; set; }

        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual IssueState? IssueState { get; set; }
    }

    /// <summary>
    /// 强制完成输入参数
    /// </summary>
    public class WmsTaskFinishInput : BaseId
    {

    }

    /// <summary>
    /// 更新任务优先级
    /// </summary>
    public class UpdateTaskLevelInput : BaseId
    {
        /// <summary>
        /// 任务优先级
        /// </summary>
        //public int TaskLevel { get; set; } = 1;
    }

    /// <summary>
    /// 批量取消任务
    /// </summary>
    public class CancelInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 批量取消任务
    /// </summary>
    public class BatchCancelInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public List<long> Id { get; set; }
    }

    /// <summary>
    /// 批量暂停任务
    /// </summary>
    public class BatchBreakInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public List<long> Id { get; set; }
    }

    /// <summary>
    /// 批量继续任务
    /// </summary>
    public class BatchContinueInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public List<long> Id { get; set; }
    }
   
    /// <summary>
    /// 出入库任务管理（熟化库）不分页查询参数
    /// </summary>
    public class WmsTaskSearchNonPage : PageInputNonPageBase
    {
        /// <summary>
        /// 任务号
        /// </summary>
        public virtual string TaskNo { get; set; }

        /// <summary>
        /// 任务方式
        /// </summary>
        public virtual TaskModel? TaskModel { get; set; }

        /// <summary>
        /// 任务类型
        /// </summary>
        public virtual TaskType? TaskType { get; set; }

        /// <summary>
        /// 任务级别
        /// </summary>
        public virtual int? TaskLevel { get; set; }

        /// <summary>
        /// 任务状态
        /// </summary>
        public virtual TaskStatusEnum? TaskStatus { get; set; }

        /// <summary>
        /// WCS是否可以读取
        /// </summary>
        public virtual bool? IsRead { get; set; }

        /// <summary>
        /// 托盘编号
        /// </summary>
        public virtual string ContainerCode { get; set; }

        /// <summary>
        /// 起始库位
        /// </summary>
        public virtual string SourcePlace { get; set; }

        /// <summary>
        /// 目标库位
        /// </summary>
        public virtual string ToPlace { get; set; }

        /// <summary>
        /// 巷道
        /// </summary>
        public virtual int? Aisle { get; set; }

        /// <summary>
        /// 库区名称
        /// </summary>
        public virtual string AreaName { get; set; }

        /// <summary>
        /// 单据明细Id
        /// </summary>
        public virtual long? OrderDetailsId { get; set; }

        /// <summary>
        /// 组盘记录单据号
        /// </summary>
        public virtual string OrderNo { get; set; }

        /// <summary>
        /// 发送次数
        /// </summary>
        public virtual int? SendTimes { get; set; }

        /// <summary>
        /// 任务执行堆垛机Id
        /// </summary>
        public virtual int? DodeviceId { get; set; }

        /// <summary>
        /// 设备执行节点Id
        /// </summary>
        public virtual string DodevicenodeId { get; set; }

        /// <summary>
        /// 设备类型
        /// </summary>
        public virtual DodeviceType? Dodevicetype { get; set; }

        /// <summary>
        /// 任务设备状态
        /// </summary>
        public virtual TaskDodeviceStatusEnum? TaskDodeviceStatus { get; set; }

        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual IssueState? IssueState { get; set; }
    }

    /// <summary>
    /// 出入库任务管理（熟化库）输入参数
    /// </summary>
    public class TaskInput
    {
        /// <summary>
        /// 任务号
        /// </summary>
        public virtual string TaskNo { get; set; }

        /// <summary>
        /// 任务方式
        /// </summary>
        public virtual TaskModel TaskModel { get; set; }

        /// <summary>
        /// 任务类型
        /// </summary>
        public virtual TaskType TaskType { get; set; }

        /// <summary>
        /// 任务级别
        /// </summary>
        public virtual int TaskLevel { get; set; }

        /// <summary>
        /// 任务状态
        /// </summary>
        public virtual TaskStatusEnum TaskStatus { get; set; }

        /// <summary>
        /// WCS是否可以读取
        /// </summary>
        public virtual bool IsRead { get; set; }

        /// <summary>
        /// 托盘编号
        /// </summary>
        public virtual string ContainerCode { get; set; }

        /// <summary>
        /// 起始库位
        /// </summary>
        public virtual string SourcePlace { get; set; }

        /// <summary>
        /// 目标库位
        /// </summary>
        public virtual string ToPlace { get; set; }

        /// <summary>
        /// 巷道
        /// </summary>
        public virtual int Aisle { get; set; }

        /// <summary>
        /// 库区名称
        /// </summary>
        public virtual string AreaName { get; set; }

        /// <summary>
        /// 单据明细Id
        /// </summary>
        public virtual long OrderDetailsId { get; set; }

        /// <summary>
        /// 组盘记录单据号
        /// </summary>
        public virtual string OrderNo { get; set; }

        /// <summary>
        /// 发送次数
        /// </summary>
        public virtual int SendTimes { get; set; }

        /// <summary>
        /// 任务执行堆垛机Id
        /// </summary>
        public virtual int DodeviceId { get; set; }

        /// <summary>
        /// 设备执行节点Id
        /// </summary>
        public virtual string DodevicenodeId { get; set; }

        /// <summary>
        /// 设备类型
        /// </summary>
        public virtual DodeviceType Dodevicetype { get; set; }

        /// <summary>
        /// 任务设备状态
        /// </summary>
        public virtual TaskDodeviceStatusEnum TaskDodeviceStatus { get; set; }

        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual IssueState IssueState { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class AddWmsTaskInput : TaskInput
    {
    }

    /// <summary>
    /// 
    /// </summary>
    public class DeleteWmsTaskInput : BaseId
    {
    }

    /// <summary>
    /// 
    /// </summary>
    public class UpdateWmsTaskInput : TaskInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class QueryeWmsTaskInput : BaseId
    {

    }
}
