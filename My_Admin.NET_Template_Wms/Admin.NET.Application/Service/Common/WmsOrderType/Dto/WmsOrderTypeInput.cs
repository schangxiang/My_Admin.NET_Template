﻿using System.ComponentModel.DataAnnotations;
using Admin.NET.Core;
using Admin.NET.Core.Service;

namespace Admin.NET.Application
{
    /// <summary>
    /// 单据类型维护表查询参数
    /// </summary>
    public class WareOrderTypeSearch
    {
        /// <summary>
        /// 父Id
        /// </summary>
        public virtual long? Pid { get; set; }

        /// <summary>
        /// 父Ids
        /// </summary>
        public virtual string Pids { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public virtual int? Sort { get; set; }

        /// <summary>
        /// 所属车间
        /// </summary>
        public virtual LesWorkShopType? LesWorkShopType { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public virtual string Remark { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public virtual CommonStatus? Status { get; set; }
    }

    /// <summary>
    /// 单据类型维护表不分页查询参数
    /// </summary>
    public class WareOrderTypeSearchNonPage : PageInputNonPageBase
    {
        /// <summary>
        /// 父Id
        /// </summary>
        public virtual long? Pid { get; set; }

        ///// <summary>
        ///// 父Ids
        ///// </summary>
        //public virtual string? Pids { get; set; }

        ///// <summary>
        ///// 名称
        ///// </summary>
        //public virtual string? Name { get; set; }

        ///// <summary>
        ///// 编码
        ///// </summary>
        //public virtual string? Code { get; set; }

        ///// <summary>
        ///// 排序
        ///// </summary>
        //public virtual int? Sort { get; set; }

        ///// <summary>
        ///// 所属车间
        ///// </summary>
        //public virtual LesWorkShopType? LesWorkShopType { get; set; }

        ///// <summary>
        ///// 备注
        ///// </summary>
        //public virtual string? Remark { get; set; }

        ///// <summary>
        ///// 状态
        ///// </summary>
        //public virtual CommonStatus? Status { get; set; }
    }

    /// <summary>
    /// 单据类型维护表输入参数
    /// </summary>
    public class WmsOrderTypeInput
    {
        /// <summary>
        /// 父Id
        /// </summary>
        public virtual long Pid { get; set; }

        ///// <summary>
        ///// 父Ids
        ///// </summary>
        //public virtual string Pids { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public virtual int Sort { get; set; }

        /// <summary>
        /// 所属车间
        /// </summary>
        public virtual LesWorkShopType LesWorkShopType { get; set; }

        ///// <summary>
        ///// 备注
        ///// </summary>
        //public virtual string Remark { get; set; }

        ///// <summary>
        ///// 状态
        ///// </summary>
        //public virtual CommonStatus Status { get; set; }
    }

    /// <summary>
    /// 增加单据类型维护表输入参数
    /// </summary>
    public class AddWareOrderTypeInput : WmsOrderTypeInput
    {
    }

    /// <summary>
    /// 增加单据明细维护表输入参数
    /// </summary>
    public class AddWareOrderSubclassInput : WmsOrderTypeInput
    {
    }

    /// <summary>
    /// 删除单据明细维护表输入参数
    /// </summary>
    public class DeleteWareOrderTypeInput : BaseId
    {
    }

    /// <summary>
    /// 更新单据明细维护表输入参数
    /// </summary>
    public class UpdateWareOrderTypeInput : WmsOrderTypeInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 查询单据明细维护表输入参数
    /// </summary>
    public class QueryeWareOrderTypeInput : BaseId
    {

    }
}
