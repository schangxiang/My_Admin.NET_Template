﻿using Mapster;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    public class WmsSortOrderMapper : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<AddWmsSortOrderInput, WmsSortOrder>()
            ;
            config.ForType<UpdateWmsSortOrderInput, WmsSortOrder>()
            ;
            config.ForType<WmsSortOrder, WmsSortOrderOutput>()
            ;
        }
    }
}
