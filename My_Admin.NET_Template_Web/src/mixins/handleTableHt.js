/***
<div ref="boxOutWrap" class="boxOutWrap">

  <div ref="boxForm" class="boxForm"> 
      <a-card :bordered="false"

     <template class="table-operator" slot="operator" v-if="hasPerm('TestStudent2:add')" >
    <div ref="actionBar" class="actionBar">	


 <s-table	:scroll="{x: true,y:tableHeight}"

  //自定义table高度
  import setTableHtMixin from '@/mixins/handleTableHt.js'

    export default {
  mixins: [setTableHtMixin],


    this.handleTableHt() //设置主表表格高度
  window.addEventListener(
  'resize',
  () => {
  this.handleTableHt() // 监听屏幕大小改变表格高度
  },
  false
  )


    toggleAdvanced () {
  this.advanced = !this.advanced
  this.handleTableHt() //设置主表表格高度
  },


     customHeaderCell: () => {
            return {
              style: {
                'min-width': '120px' //最小列宽设置
              }
            }
          },
          customCell: () => {
            return {
              style: {
                'min-width': '120px' //最小列宽设置
              }
            }
          },
          
  {
          title: '序号',
          width: '60px',
          align: 'center',
          customRender: (text, record, index) => `${index + 1}`
        },

 * 
 */
export default {
  data() {
    return {
      tableHeight: 100
    }
  },
  // watch: {
  //   $route: {
  //     handler: function (route, oldRoute) {
  //       this.setTableHeight()
  //       window.addEventListener('resize', this.setTableHeight)
  //     },
  //     immediate: true
  //   }
  // },
  methods: {
    //设置主表表格高度
    handleTableHt() {
      this.$nextTick(() => {
        if (!this.$refs.boxOutWrap) {
          console.log("没有DOM:boxOutWrap,无法自动计算表格高度")
          return
        }
        if (!this.$refs.boxForm) {
          console.log("没有DOM:boxForm,无法自动计算表格高度")
          return
        }
        if (!this.$refs.actionBar) {
          console.log("没有DOM:actionBar,无法自动计算表格高度")
          return
        }
        let mainTableHt = 0
        // let t_footer = 40 //底部版权
        // let t_head = 55 //顶部栏
        let t_boxOutWrap = 0 //最大的盒子
        let t_tablePage = 24.5 //表格分页
        let t_formhead = 0 //form
        if (this.$refs.boxOutWrap) {
          // form大盒子的高度
          t_boxOutWrap = this.$refs.boxOutWrap.clientHeight
        }
        if (this.$refs.boxForm) {
          // form大盒子的高度
          t_formhead = this.$refs.boxForm.clientHeight || 0
        }
        let t_actionBar = 0
        if (this.$refs.actionBar) {
          //操作栏32
          t_actionBar = this.$refs.actionBar.clientHeight || 0
        }
        // mainTableHt = document.documentElement.clientHeight - t_head - t_formhead - t_tablePage - t_actionBar - t_footer
        mainTableHt = t_boxOutWrap - t_formhead - t_tablePage - t_actionBar
        mainTableHt = mainTableHt - 120 //减掉其他间隙 padding
        console.log('最大的盒子' + t_boxOutWrap)
        console.log('form高' + t_formhead)
        console.log('表格操作栏高' + t_actionBar)
        console.log('表格高度：' + mainTableHt)
        if (mainTableHt <= 0) {
          mainTableHt = 30
        }
        // this.tableHeight = mainTableHt + 'px'
        this.tableHeight = mainTableHt 

      })
    },
  }
  // async activated () {
  //   if (this.ready || this.$Config.ready === 0) {
  //     this.setTableHeight()
  //     window.addEventListener('resize', this.setTableHeight)
  //   }
  //   this.ready = true
  //   this.$Config.ready++
  // },
  // deactivated () {
  //   window.removeEventListener('resize', this.setTableHeight)
  // },
  // destroyed () {
  //   window.removeEventListener('resize', this.setTableHeight)
  // }
}

