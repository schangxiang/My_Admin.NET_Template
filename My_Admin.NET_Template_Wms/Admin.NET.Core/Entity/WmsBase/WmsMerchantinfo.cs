﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

/*
 * @author : http://www.chiner.com.cn
 * @date : 2023-8-17
 * @desc : 客商信息表
 */
namespace Admin.NET.Core
{
    /// <summary>
    /// 客商信息表
    /// </summary>
    [Table("wms_merchantinfo")]
    [Comment("客商信息表")]
    public class WmsMerchantinfo : DEntityBase
    {
        /// <summary>
        /// 客商编号
        /// </summary>
        [Comment("客商编号")]
        [MaxLength(50)]
        public string MerchantNo { get; set; }

        /// <summary>
        /// 客商名称
        /// </summary>
        [Comment("客商名称")]
        [MaxLength(50)]
        public string MerchantName { get; set; }

        /// <summary>
        /// 客商分类
        /// </summary>
        [Comment("客商分类")]
        public MerchantType MerchantType { get; set; }

        /// <summary>
        /// 客商简称
        /// </summary>
        [Comment("客商简称")]
        [MaxLength(50)]
        public string MerchantShort { get; set; }

        /// <summary>
        /// 客商等级
        /// </summary>
        [Comment("客商等级")]
        public MerchantGrade MerchantGrade { get; set; }

        /// <summary>
        /// 客商属性
        /// </summary>
        [Comment("客商属性")]
        [MaxLength(50)]
        public string MerchantAttribute { get; set; }

        /// <summary>
        /// 客商行业
        /// </summary>
        [Comment("客商行业")]
        [MaxLength(50)]
        public string MerchantTmt { get; set; }

        /// <summary>
        /// 客商联系人
        /// </summary>
        [Comment("客商联系人")]
        [MaxLength(50)]
        public string MerchantContact { get; set; }

        /// <summary>
        /// 客商电话
        /// </summary>
        [Comment("客商电话")]
        [MaxLength(50)]
        public string MerchantPhone { get; set; }

        /// <summary>
        /// 客商传真
        /// </summary>
        [Comment("客商传真")]
        [MaxLength(50)]
        public string MerchantFax { get; set; }

        /// <summary>
        /// 客商邮箱
        /// </summary>
        [Comment("客商邮箱")]
        [MaxLength(50)]
        public string MerchantEmail { get; set; }

        /// <summary>
        /// 客商地址
        /// </summary>
        [Comment("客商地址")]
        [MaxLength(50)]
        public string MerchantAddress { get; set; }
    }
}