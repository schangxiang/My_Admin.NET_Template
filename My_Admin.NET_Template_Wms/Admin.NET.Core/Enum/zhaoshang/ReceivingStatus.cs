﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 收货状态
    /// </summary>
    public enum ReceivingStatus
    {
        /// <summary>
        /// 未收货
        /// </summary>
        WEISHOUHUO = 1,
        /// <summary>
        /// 收货中
        /// </summary>
        SHOUHUOZHONG = 2,
        /// <summary>
        /// 已收货
        /// </summary>
        YISHOUHUO = 3
    }
}
