﻿using Admin.NET.Core.Util.LowCode.Dto;
using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 入库报表返回参数
    /// </summary>
    public class GetWareHouseInReportFormsOutput
    {
        /// <summary>
        /// SCM领料申请单明细行唯一标识
        /// </summary>
        public long OID { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        public string Materialcode { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string Materialname { get; set; }

        /// <summary>
        /// 基本单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Qty { get; set; }

        /// <summary>
        /// 下发数量
        /// </summary>
        public decimal DistributeQty { get; set; }

        /// <summary>
        /// 单根长度 
        /// </summary>
        public decimal SingleLength { get; set; }

        /// <summary>
        /// 件数    
        /// </summary>
        public decimal Number { get; set; }

        /// <summary>
        /// SCM批次号    
        /// </summary>
        public string Batchno_SCM { get; set; }

        /// <summary>
        /// WMS批次号    
        /// </summary>
        public string Batchno_WMS { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// TA号
        /// </summary>
        public string TACode { get; set; }

        /// <summary>
        /// 分段号
        /// </summary>
        public string PartCode { get; set; }

        /// <summary>
        /// 托盘编码
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 库位编码
        /// </summary>
        public string PlaceCode { get; set; }

        /// <summary>
        /// 出库时间
        /// </summary>
        public DateTimeOffset? CreatedTime { get; set; }

        /// <summary>
        /// 出库完成时间
        /// </summary>
        public DateTimeOffset? UpdatedTime { get; set; }

        /// <summary>
        /// 巷道
        /// </summary>
        public int Aisle { get; set; }

        /// <summary>
        /// 单据主表
        /// </summary>
        public WmsTakeMaterialOrder WmsTakeMaterialOrder { get; set; }

    }
}
