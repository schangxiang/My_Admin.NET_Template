﻿import { axios } from '@/utils/request'

/**
 * 查询泡沫车间Agv任务表
 *
 * @author yrj
 */
export function FoamingAgvTaskPage (parameter) {
  return axios({
    url: '/FoamingAgvTask/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 泡沫车间Agv任务表列表
 *
 * @author yrj
 */
export function FoamingAgvTaskList (parameter) {
  return axios({
    url: '/FoamingAgvTask/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加泡沫车间Agv任务表
 *
 * @author yrj
 */
export function FoamingAgvTaskAdd (parameter) {
  return axios({
    url: '/FoamingAgvTask/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑泡沫车间Agv任务表
 *
 * @author yrj
 */
export function FoamingAgvTaskEdit (parameter) {
  return axios({
    url: '/FoamingAgvTask/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除泡沫车间Agv任务表
 *
 * @author yrj
 */
export function FoamingAgvTaskDelete (parameter) {
  return axios({
    url: '/FoamingAgvTask/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出泡沫车间Agv任务表的Excel文件
 *
 * @author yrj
 */
export function FoamingAgvTaskToExcel (parameter) {
  return axios({
    url: '/FoamingAgvTask/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入泡沫车间Agv任务表的Excel文件
 *
 * @author yrj
 */
export function FoamingAgvTaskFromExcel (data, parameter) {
  return axios({
    url: '/FoamingAgvTask/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}

/**
 * 下载泡沫车间Agv任务表的Excel导入模板
 *
 * @author yrj
 */
export function FoamingAgvTaskDownloadExcelTemplate(parameter) {
  return axios({
    url: '/FoamingAgvTask/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 强制完成
 *
 * @author yrj 
 */
export function FoamingAgvTaskForceComplete (id) {
  return axios({
    url: '/FoamingAgvTask/finish',
    method: 'post',
    data: {id}
  })
}

/**
 * 生成AGB任务，方便测试
 *
 * @author yubaoshan
 * @date 2020/6/23 23:09
 */
export function foamingAgvTaskGenerate(parameter) {
  return axios({
    url: '/FoamingInteractive/CreateInTask',
    method: 'post',
    data: parameter
  })
}



