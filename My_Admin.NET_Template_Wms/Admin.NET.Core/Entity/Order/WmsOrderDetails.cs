﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    /// <summary>
    /// 单据明细表
    /// </summary>
    [Table("wms_order_details")]
    [Comment("单据明细表")]
    public class WmsOrderDetails : DEntityBase
    {
        /// <summary>
        /// 单据Id  
        /// </summary>
        [Comment("单据Id")]
        [MaxLength(50)]
        public long OrderId { get; set; }

        /// <summary>
        /// 上位系统单据明细唯一识别码
        /// </summary>
        [Comment("上位系统单据明细唯一识别码")]
        public long OID { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        [Comment("物料编号")]
        [MaxLength(255)]
        public string Materialcode { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        [Comment("物料名称")]
        [MaxLength(255)]
        public string Materialname { get; set; }

        /// <summary>
        /// 基本单位    
        /// </summary>
        [Comment("基本单位")]
        [MaxLength(255)]
        public string Unit { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        [Comment("数量")]
        public decimal Qty { get; set; }

        /// <summary>
        /// 入库数量
        /// </summary>
        [Comment("入库数量")]
        public decimal InventoryQty { get; set; }
        
        /// <summary>
        /// 单根长度 
        /// </summary>
        [Comment("单根长度")]
        public decimal SingleLength { get; set; }

        /// <summary>
        /// 理论重量    
        /// </summary>
        [Comment("理论重量")]
        public decimal TheoreticalWeight { get; set; }

        /// <summary>
        /// 不含税单价    
        /// </summary>
        [Comment("不含税单价")]
        public decimal Price { get; set; }

        /// <summary>
        /// 成本金额    
        /// </summary>
        [Comment("成本金额")]
        public decimal SumPrice { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        [Comment("项目编号")]
        [MaxLength(255)]
        public string ProjectCode { get; set; }

        /// <summary>
        /// 合同编号
        /// </summary>
        [Comment("合同编号")]
        [MaxLength(255)]
        public string ContractCode { get; set; }

        /// <summary>
        /// TA号
        /// </summary>
        [Comment("TA号")]
        [MaxLength(255)]
        public string TACode { get; set; }

        /// <summary>
        /// 报关品名
        /// </summary>
        [Comment("报关品名")]
        [MaxLength(255)]
        public string TradeName { get; set; }

        /// <summary>
        /// 报关项号
        /// </summary>
        [Comment("报关项号")]
        [MaxLength(255)]
        public string ItemNo { get; set; }

        /// <summary>
        /// 报关单位
        /// </summary>
        [Comment("报关单位")]
        [MaxLength(255)]
        public string CustomsUnitName { get; set; }

        /// <summary>
        /// 报关数量   
        /// </summary>
        [Comment("报关数量")]
        public decimal CustomsNum { get; set; }

        /// <summary>
        /// 报关金额 
        /// </summary>
        [Comment("报关金额")]
        public decimal CustomsPrices { get; set; }

        /// <summary>
        /// 特殊要求
        /// </summary>
        [Comment("特殊要求")]
        [MaxLength(255)]
        public string SpecialNeeds { get; set; }

        /// <summary>
        /// 单据状态
        /// </summary>
        [Comment("单据状态")]
        [Required]
        public OrderDetailsStatusEnum OrderStatus { get; set; } = OrderDetailsStatusEnum.WEIZHIXING;

        /// <summary>
        /// 检验结果
        /// </summary>
        [Comment("检验结果")]
        [Required]
        public InspectionResultsEnum InspectionResults { get; set; } = InspectionResultsEnum.WEIJIANYAN;

        /// <summary>
        /// 托盘编码
        /// </summary>
        [Comment("托盘编码")]
        [MaxLength(50)]
        public string ContainerCode { get; set; }

        /// <summary>
        /// 库位编码
        /// </summary>
        [Comment("库位编码")]
        [MaxLength(50)]
        public string PlaceCode { get; set; }

        /// <summary>
        /// 单据主表
        /// </summary>
        public WmsOrder WareOrder { get; set; }
    }
}
