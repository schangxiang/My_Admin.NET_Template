﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    [Comment("库存物料视图")]
    [Table("View_Materialstock_Material")]
    public class View_Materialstock_Material 
    {


        /// <summary>
        /// 物料名称
        /// </summary>
        [Comment("物料名称")]
        [Required]
        [MaxLength(50)]
        public string MaterialName { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        [Comment("物料编号")]
        [Required]
        [MaxLength(50)]
        public string MaterialNo { get; set; }

        /// <summary>
        /// 物料批次
        /// </summary>
        [Comment("物料批次")]
        [MaxLength(50)]
        public string MaterialBatch { get; set; }

        /// <summary>
        /// 物料类别;数据字典
        /// </summary>
        [Comment("物料类别")]
        public MaterialType MaterialType { get; set; }

        /// <summary>
        /// 实物库存数
        /// </summary>
        [Comment("实物库存数")]
        [Required]
        public decimal StockNumber { get; set; }
    }
}
