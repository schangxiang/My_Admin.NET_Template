﻿#nullable enable

namespace Admin.NET.Application
{
    /// <summary>
    /// Excel模板输出参数
    /// </summary>
    public class SysExcelTemplateDto
    {
        /// <summary>
        /// 模板名称
        /// </summary>
        public string Name { get; set; } = null!;

        /// <summary>
        /// 版本
        /// </summary>
        public string Version { get; set; } = null!;

        /// <summary>
        /// 所属应用
        /// </summary>
        public string AppName { get; set; } = null!;

        /// <summary>
        /// 类名
        /// </summary>
        public string ClassName { get; set; } = null!;

        /// <summary>
        /// 模板文件名称
        /// </summary>
        public string TemplateFileName { get; set; } = null!;

        /// <summary>
        /// 唯一字段集
        /// </summary>
        public string UnionUniqueFields { get; set; } = null!;

        /// <summary>
        /// 表头开始行
        /// </summary>
        public int HeadStartLine { get; set; }

        /// <summary>
        /// 数据开始行
        /// </summary>
        public int DataStartLine { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public Core.CommonStatus Status { get; set; }

        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }

    }
}
