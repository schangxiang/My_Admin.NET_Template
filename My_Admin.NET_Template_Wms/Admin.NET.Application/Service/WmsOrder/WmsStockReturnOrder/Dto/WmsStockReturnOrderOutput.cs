﻿using Admin.NET.Core.Util.LowCode.Dto;
using System;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace Admin.NET.Application
{
    /// <summary>
    /// 退库单输出参数
    /// </summary>
    public class WmsStockReturnOrderOutput
    {
        /// <summary>
        /// 单据大类
        /// </summary>
        public long OrderLargeCategory { get; set; }
        
        /// <summary>
        /// 单据小类
        /// </summary>
        public long OrderSubclass { get; set; }
        
        /// <summary>
        /// 事务类型
        /// </summary>
        public Admin.NET.Core.MoveType MoveType { get; set; }
        
        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        public long SOID { get; set; }
        
        /// <summary>
        /// 单据编号
        /// </summary>
        public string NO { get; set; }
        
        /// <summary>
        /// 退料申请日期
        /// </summary>
        public DateTimeOffset Billdate { get; set; }
        
        /// <summary>
        /// 领用项目号
        /// </summary>
        public string WBSElementcode { get; set; }
        
        /// <summary>
        /// 领料部门
        /// </summary>
        public string BenefitingDepartcode { get; set; }
        
        /// <summary>
        /// 收益部门
        /// </summary>
        public string CostCenterID { get; set; }
        
        /// <summary>
        /// 客户
        /// </summary>
        public string FI_Client_Analysis_H { get; set; }
        
        /// <summary>
        /// 领取人
        /// </summary>
        public string PickerID { get; set; }
        
        /// <summary>
        /// 公司
        /// </summary>
        public string Companyname { get; set; }
        
        /// <summary>
        /// 施工队
        /// </summary>
        public string ConstructionTeamID { get; set; }
        
        /// <summary>
        /// 单据状态
        /// </summary>
        public Admin.NET.Core.OrderStatusEnum OrderStatus { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public Admin.NET.Core.IssueState IssueState { get; set; }

        /// <summary>
        /// 对应YiGo系统
        /// </summary>
        public List<WmsStockReturnOrderDetailOutput> Dtls { get; set; }

    }

    /// <summary>
    /// 退库单获取明细返回参数
    /// </summary>
    public class WmsStockReturnOrderDetailOutput
    {
        /// <summary>
        /// Id  
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 单据Id  
        /// </summary>
        public long OrderId { get; set; }

        /// <summary>
        /// 上位系统单据明细唯一识别码
        /// </summary>
        public long OID { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        public string Materialcode { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string Materialname { get; set; }

        /// <summary>
        /// 基本单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Qty { get; set; }

        /// <summary>
        /// 单根长度 
        /// </summary>
        public decimal Length { get; set; }

        /// <summary>
        /// 件数    
        /// </summary>
        public decimal Number { get; set; }

        /// <summary>
        /// SCM批次号    
        /// </summary>
        public string Batchno_SCM { get; set; }

        /// <summary>
        /// WMS批次号    
        /// </summary>
        public string Batchno_WMS { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// TA号
        /// </summary>
        public string TACode { get; set; }

        /// <summary>
        /// 分段号
        /// </summary>
        public string PartCode { get; set; }
    }

    /// <summary>
    /// yigo获取退库单返回参数
    /// </summary>
    public class WmsStockReturnOrderOutputByYiGO
    {
        public bool Status { get; set; }

        public List<WmsStockReturnOrderOutput> Data { get; set; }

        public string Result { get; set; }
    }

    /// <summary>
    /// 退料下架返回参数
    /// </summary>
    public class StockReturnGoodsReturnOrderOutput
    {

        /// <summary>
        /// 结果描述
        /// </summary>
        public string Result { get; set; }

        /// <summary>
        /// 请求状态
        /// </summary>
        public bool Status { get; set; }
    }
}
