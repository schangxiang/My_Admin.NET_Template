// checkVersion.js
function fetchVersion() {
    // 使用 fetch API 来获取 index.html 文件
    return fetch('/')
      .then(response => response.text())
      .then(html => {
        // 使用正则表达式来提取版本号
        const versionMatch = html.match(/var version = (\d+);/);
        if (versionMatch && versionMatch.length > 1) {
          //console.log('找到的版本号是:'+versionMatch[1]);
          return versionMatch[1]; // 返回版本号
        }
        throw new Error('不能找到版本号。');
      });
  }
  
  function checkVersion() {
    let currentVersion; // 当前页面的版本号
  
    // 初始化时获取一次版本号
    fetchVersion().then(version => {
      currentVersion = version;
    });
  
    // 创建定时器，每10秒检查一次
    setInterval(() => {
      fetchVersion().then(newVersion => {
        // 检查版本号是否有变化
        if (newVersion !== currentVersion) {
          // 如果版本号变化，提示用户，并更新当前版本号
          if (confirm('检测到新版本，是否重新加载页面？如果信息还未提交完，请点击取消，继续填写.')) {
            window.location.reload(); // 用户确认后重新加载页面
            currentVersion = newVersion; // 更新当前版本号，避免重复提示
          }
        }
      }).catch(error => console.error('获取版本号失败：', error));
    }, 10000); // 设置时间间隔为 10000 毫秒（10 秒）
  }
  
  export default checkVersion;