﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 出库输入参数
    /// </summary>
    public class ExWareHouseOutput
    {
        /// <summary>
        /// 物料编号
        /// </summary>
        public string MaterialNo { get; set; }

        /// <summary>
        /// 物料类别;数据字典
        /// </summary>
        public MaterialType MaterialType { get; set; }

        /// <summary>
        /// 物料批次
        /// </summary>
        public string MaterialBatch { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string MaterialName { get; set; }

        /// <summary>
        /// 物料规格
        /// </summary>
        public string MaterialSpec { get; set; }

        /// <summary>
        /// 长    
        /// </summary>
        public int Long { get; set; }

        /// <summary>
        /// 宽    
        /// </summary>
        public int Wide { get; set; }

        /// <summary>
        /// 高    
        /// </summary>
        public int High { get; set; }

        /// <summary>
        /// 物料密度
        /// </summary>
        public string MaterialDensity { get; set; }

        /// <summary>
        /// 检验方式;数据字典
        /// </summary>
        public MaterialInspection InspectionMethod { get; set; }

        /// <summary>
        /// 单位类别;数据字典
        /// </summary>
        public UnitType UnitType { get; set; }

        /// <summary>
        /// 单位编号;数据字典
        /// </summary>
        public UnitNoType UnitNo { get; set; }

        /// <summary>
        /// 实物库存数
        /// </summary>
        public decimal StockNumber { get; set; }

        /// <summary>
        /// 库位编码
        /// </summary>
        public string PlaceCode { get; set; }

        /// <summary>
        /// 托盘Id
        /// </summary>
        public long ContainerId { get; set; }

        /// <summary>
        /// 托盘编码
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 库区Id
        /// </summary>
        public long AreaId { get; set; }

        /// <summary>
        /// 账面数量
        /// </summary>
        public decimal QuantityOfBill { get; set; }
    }
}
