﻿using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// OAuth用户参数
    /// </summary>
    public class AuthUserInput
    {
        /// <summary>
        /// 
        /// </summary>
        public string Uuid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Nickname { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Blog { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Eemark { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public AuthToken Token { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string RawUserInfo { get; set; }
    }
}