﻿using Admin.NET.Core.Util.LowCode.Dto;
using System;

namespace Admin.NET.Application
{
    /// <summary>
    /// 退库单输出参数
    /// </summary>
    public class WmsStockReturnOrderDto
    {
        /// <summary>
        /// 单据大类
        /// </summary>
        public long OrderLargeCategory { get; set; }
        
        /// <summary>
        /// 单据小类
        /// </summary>
        public long OrderSubclass { get; set; }
        
        /// <summary>
        /// 事务类型
        /// </summary>
        public Admin.NET.Core.MoveType MoveType { get; set; }
        
        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        public long SOID { get; set; }
        
        /// <summary>
        /// 单据编号
        /// </summary>
        public string NO { get; set; }
        
        /// <summary>
        /// 退料申请日期
        /// </summary>
        public DateTimeOffset Billdate { get; set; }
        
        /// <summary>
        /// 领用项目号
        /// </summary>
        public string WBSElementcode { get; set; }
        
        /// <summary>
        /// 领料部门
        /// </summary>
        public string BenefitingDepartcode { get; set; }
        
        /// <summary>
        /// 收益部门
        /// </summary>
        public string CostCenterID { get; set; }
        
        /// <summary>
        /// 客户
        /// </summary>
        public string FI_Client_Analysis_H { get; set; }
        
        /// <summary>
        /// 领取人
        /// </summary>
        public string PickerID { get; set; }
        
        /// <summary>
        /// 公司
        /// </summary>
        public string Companyname { get; set; }
        
        /// <summary>
        /// 施工队
        /// </summary>
        public string ConstructionTeamID { get; set; }
        
        /// <summary>
        /// 单据状态
        /// </summary>
        public Admin.NET.Core.OrderStatusEnum OrderStatus { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public Admin.NET.Core.IssueState IssueState { get; set; }
        
    }
}
