﻿using Admin.NET.Core;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Admin.NET.Application
{
    public interface IWmsReceiptOrderService
    {
        Task Add();
        Task Delete(DeleteWmsReceiptOrderInput input);
        Task<WmsReceiptOrderOutput> Get([FromQuery] QueryeWmsReceiptOrderInput input);
        Task<List<WmsReceiptOrderOutput>> List([FromQuery] WmsReceiptOrderInput input);
        Task<PageResult<WmsReceiptOrderOutput>> Page([FromQuery] WmsReceiptOrderSearch input);
        Task Update(UpdateWmsReceiptOrderInput input);

        Task<List<WmsReceiptOrderOutput>> ListNonPageAsync([FromQuery] WmsReceiptOrderSearchNonPage input);

        Task<int> FromExcelAsync(IFormFile file, [FromQuery] ImportExcelType importExcelType);

        Task<IActionResult> DownloadExcelTemplate(string version);
    }
}