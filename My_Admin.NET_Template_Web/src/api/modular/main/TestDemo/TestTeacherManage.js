﻿import { axios } from '@/utils/request'

/**
 * 查询测试老师表
 *
 * @author 邵长祥
 */
export function TestTeacherPage (parameter) {
  return axios({
    url: '/TestTeacher/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 测试老师表列表
 *
 * @author 邵长祥
 */
export function TestTeacherList (parameter) {
  return axios({
    url: '/TestTeacher/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加测试老师表
 *
 * @author 邵长祥
 */
export function TestTeacherAdd (parameter) {
  return axios({
    url: '/TestTeacher/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑测试老师表
 *
 * @author 邵长祥
 */
export function TestTeacherEdit (parameter) {
  return axios({
    url: '/TestTeacher/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除测试老师表
 *
 * @author 邵长祥
 */
export function TestTeacherDelete (parameter) {
  return axios({
    url: '/TestTeacher/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出测试老师表的Excel文件
 *
 * @author 邵长祥
 */
export function TestTeacherToExcel (parameter) {
  return axios({
    url: '/TestTeacher/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入测试老师表的Excel文件
 *
 * @author 邵长祥
 */
export function TestTeacherFromExcel (data, parameter) {
  return axios({
    url: '/TestTeacher/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载测试老师表的Excel导入模板
 *
 * @author 邵长祥
 */
export function TestTeacherDownloadExcelTemplate(parameter) {
  return axios({
    url: '/TestTeacher/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}



