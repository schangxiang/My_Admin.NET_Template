﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 托盘类型
    /// </summary>
    public enum ContainerType
    {
        /// <summary>
        /// 其他
        /// </summary>
        [Description("其他")]
        QITA = 0,

        /// <summary>
        /// 木质
        /// </summary>
        [Description("木质")]
        MUZHI = 1,

        /// <summary>
        /// 金属
        /// </summary>
        [Description("金属")]
        JINSHU = 2,

        /// <summary>
        /// 塑料
        /// </summary>
        [Description("塑料")]
        SULIAO = 3
    }

    /// <summary>
    /// 托盘状态
    /// </summary>
    public enum ContainerStatus
    {
        /// <summary>
        /// 禁用
        /// </summary>
        [Description("禁用")]
        JINYONG = 0,

        /// <summary>
        /// 空闲
        /// </summary>
        [Description("空闲")]
        KOUXIAN = 1,

        /// <summary>
        /// 组盘
        /// </summary>
        [Description("组盘")]
        ZUPANG = 2,

        /// <summary>
        /// 库位
        /// </summary>
        [Description("库位")]
        KUWEI = 3,

        /// <summary>
        /// 分拣
        /// </summary>
        [Description("分拣")]
        FENJIAN =4
    }

    /// <summary>
    /// 托盘分类
    /// </summary>
    public enum ContainerCategory
    {
        /// <summary>
        /// 通用
        /// </summary>
        [Description("通用")]
        TY = 0,

        /// <summary>
        /// 危化品
        /// </summary>
        [Description("危化品")]
        WXP = 1,

        /// <summary>
        /// 液体
        /// </summary>
        [Description("液体")]
        YT = 2,

        /// <summary>
        /// 贵重品
        /// </summary>
        [Description("贵重品")]
        GZP = 3,

        /// <summary>
        /// 气体
        /// </summary>
        [Description("气体")]
        QT = 4,

        /// <summary>
        /// 低温
        /// </summary>
        [Description("低温")]
        DW = 5
    }
}
