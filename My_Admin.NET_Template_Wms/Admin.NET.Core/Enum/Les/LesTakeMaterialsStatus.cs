﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 叫料单状态
    /// </summary>
    public enum LesTakeMaterialsStatus
    {
        /// <summary>
        /// 未开始
        /// </summary>
        [Description("未开始")]
        WEIKAISHI = 0,

        /// <summary>
        /// 运送中
        /// </summary>
        [Description("运送中")]
        YUNSONGZHONG = 1,

        /// <summary>
        /// 完成
        /// </summary>
        [Description("完成")]
        WANCHENG = 2
    }
}
