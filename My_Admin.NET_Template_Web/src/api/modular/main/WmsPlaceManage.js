﻿import { axios } from '@/utils/request'

/**
 * 查询库位信息
 *
 * @author liduanping
 */
export function WmsPlacePage (parameter) {
  return axios({
    url: '/WmsPlace/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 库位信息列表
 *
 * @author liduanping
 */
export function WmsPlaceList (parameter) {
  return axios({
    url: '/WmsPlace/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加库位信息
 *
 * @author liduanping
 */
export function WmsPlaceAdd (parameter) {
  return axios({
    url: '/WmsPlace/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑库位信息
 *
 * @author liduanping
 */
export function WmsPlaceEdit (parameter) {
  return axios({
    url: '/WmsPlace/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除库位信息
 *
 * @author liduanping
 */
export function WmsPlaceDelete (parameter) {
  return axios({
    url: '/WmsPlace/delete',
    method: 'post',
    data: parameter
  })
}

/**
* 获取WmsArea列表
* @author liduanping
*/
export function WmsPlaceFkWmsAreaList() {
  return axios({
    url: '/WmsPlace/fkWmsArea',
    method: 'get'
  })
}

/**
 * 批量锁定
 *
 * @author yuhuanhuan
 */
export function WmsPlaceLock (ids) {
  return axios({
    url: '/WmsPlace/MoreLock',
    method: 'post',
    data: {id:ids}
  })
}

/**
 * 批量解锁
 *
 * @author yuhuanhuan
 */
export function WmsPlaceUnlock (ids) {
  return axios({
    url: '/WmsPlace/MoreUnlock',
    method: 'post',
    data: {id:ids}
  })
}


/**
 * 无参获取巷道列表
 *
 * @author yuhuanhuan
 */
export function WmsPlaceGetFAisles () {
  return axios({
    url: '/WmsPlace/GetFAisle',
    method: 'get'
  })
}

