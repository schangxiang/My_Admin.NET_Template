/* 这里写 初始化的数据 */


-- 初始化  编号 【Editby shaocx,2023-08-18】
	DELETE Base_SerialRuleDetail where SerialType =10;
	GO
	
		INSERT INTO dbo.Base_SerialRuleDetail
(Id, SerialRuleNo, SerialType, ItemNo, SourceType, SerialLength, SearchStart, DecimalType, UserDefine, SerialCodeFlag, Description, SerialTypeNo, GetData, PadLeft, IssueStatus, CreatedTime, UpdatedTime, CreatedUserId, CreatedUserName, UpdatedUserId, UpdatedUserName, IsDeleted)
VALUES(10, N'1', 10, 1, N'UD', 15, 0, 1, N'UndoStoRec_', 0, N'测试编号编号-前缀', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO dbo.Base_SerialRuleDetail
(Id, SerialRuleNo, SerialType, ItemNo, SourceType, SerialLength, SearchStart, DecimalType, UserDefine, SerialCodeFlag, Description, SerialTypeNo, GetData, PadLeft, IssueStatus, CreatedTime, UpdatedTime, CreatedUserId, CreatedUserName, UpdatedUserId, UpdatedUserName, IsDeleted)
VALUES(11, N'1', 10, 2, N'Y4', 4, 0, 1, NULL, 0, N'测试编号编号-年份', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO dbo.Base_SerialRuleDetail
(Id, SerialRuleNo, SerialType, ItemNo, SourceType, SerialLength, SearchStart, DecimalType, UserDefine, SerialCodeFlag, Description, SerialTypeNo, GetData, PadLeft, IssueStatus, CreatedTime, UpdatedTime, CreatedUserId, CreatedUserName, UpdatedUserId, UpdatedUserName, IsDeleted)
VALUES(12, N'1', 10, 3, N'M2', 4, 0, 1, NULL, 0, N'测试编号编号-月份', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO dbo.Base_SerialRuleDetail
(Id, SerialRuleNo, SerialType, ItemNo, SourceType, SerialLength, SearchStart, DecimalType, UserDefine, SerialCodeFlag, Description, SerialTypeNo, GetData, PadLeft, IssueStatus, CreatedTime, UpdatedTime, CreatedUserId, CreatedUserName, UpdatedUserId, UpdatedUserName, IsDeleted)
VALUES(13, N'1', 10, 4, N'D2', 4, 0, 1, NULL, 0, N'测试编号编号-日', NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO dbo.Base_SerialRuleDetail
(Id, SerialRuleNo, SerialType, ItemNo, SourceType, SerialLength, SearchStart, DecimalType, UserDefine, SerialCodeFlag, Description, SerialTypeNo, GetData, PadLeft, IssueStatus, CreatedTime, UpdatedTime, CreatedUserId, CreatedUserName, UpdatedUserId, UpdatedUserName, IsDeleted)
VALUES(14, N'1', 10, 5, N'SN', 3, 0, 1, NULL, 0, N'测试编号编号-流水号', NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
