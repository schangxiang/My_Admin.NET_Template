﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 检验结果
    /// </summary>
    public enum InspectionResultsEnum
    {
        /// <summary>
        /// 未检验
        /// </summary>
        WEIJIANYAN = 1,
        /// <summary>
        /// 合格
        /// </summary>
        HEGE = 2,
        /// <summary>
        /// 不合格
        /// </summary>
        BUHEGE = 3,
        /// <summary>
        /// 带意见接收
        /// </summary>
        DAIYIJIANJIESHOU = 4
    }
}
