﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Core.Entity.WmsBase
{
    /// <summary>
    /// 仓库管理
    /// </summary>
    [Table("wms_warehouse_manage")]
    [Comment("库口表")]
    public class WmsWarehouseManage: DEntityBase
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Comment("名称")]
        [Required, MaxLength(100)]
        public string Name { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        [Comment("编码")]
        [Required, MaxLength(50)]
        public string Code { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        [Comment("类型")]
        public int Type { get; set; }


    }
}
