﻿import { axios } from '@/utils/request'

/**
 * 查询产线信息
 *
 * @author XJF
 */
export function LesProductionlinePage (parameter) {
  return axios({
    url: '/LesProductionline/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 产线信息列表
 *
 * @author XJF
 */
export function LesProductionlineList (parameter) {
  return axios({
    url: '/LesProductionline/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加产线信息
 *
 * @author XJF
 */
export function LesProductionlineAdd (parameter) {
  return axios({
    url: '/LesProductionline/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑产线信息
 *
 * @author XJF
 */
export function LesProductionlineEdit (parameter) {
  return axios({
    url: '/LesProductionline/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除产线信息
 *
 * @author XJF
 */
export function LesProductionlineDelete (parameter) {
  return axios({
    url: '/LesProductionline/delete',
    method: 'post',
    data: parameter
  })
}
