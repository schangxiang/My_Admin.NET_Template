﻿import { axios } from '@/utils/request'

/**
 * 查询ces
 *
 * @author liduanping
 */
export function cesPage (parameter) {
  return axios({
    url: '/ces/page',
    method: 'get',
    params: parameter
  })
}

/**
 * ces列表
 *
 * @author liduanping
 */
export function cesList (parameter) {
  return axios({
    url: '/ces/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加ces
 *
 * @author liduanping
 */
export function cesAdd (parameter) {
  return axios({
    url: '/ces/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑ces
 *
 * @author liduanping
 */
export function cesEdit (parameter) {
  return axios({
    url: '/ces/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除ces
 *
 * @author liduanping
 */
export function cesDelete (parameter) {
  return axios({
    url: '/ces/delete',
    method: 'post',
    data: parameter
  })
}
