﻿using Admin.NET.Core;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Admin.NET.Application
{
    public interface IWmsSortOrderService
    {
        Task Add(AddWmsSortOrderInput input);
        Task Delete(DeleteWmsSortOrderInput input);
        Task<WmsSortOrderOutput> Get([FromQuery] QueryeWmsSortOrderInput input);
        Task<List<WmsSortOrderOutput>> List([FromQuery] WmsSortOrderInput input);
        Task<PageResult<WmsSortOrderOutput>> Page([FromQuery] WmsSortOrderSearch input);
        Task Update(UpdateWmsSortOrderInput input);

        Task<List<WmsSortOrderOutput>> ListNonPageAsync([FromQuery] WmsSortOrderSearchNonPage input);

        Task<int> FromExcelAsync(IFormFile file, [FromQuery] ImportExcelType importExcelType);

        Task<IActionResult> DownloadExcelTemplate(string version);
    }
}