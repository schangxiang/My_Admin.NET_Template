﻿using Admin.NET.Core;
using Admin.NET.Core.Service;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Admin.NET.Application
{    
    
    /// <summary>
    /// 退库单查询参数
    /// </summary>
    public class WmsStockReturnOrderSearch : PageInputBase
    {
        /// <summary>
        /// 单据大类
        /// </summary>
        public virtual long? OrderLargeCategory { get; set; }
        
        /// <summary>
        /// 单据小类
        /// </summary>
        public virtual long? OrderSubclass { get; set; }
        
        /// <summary>
        /// 事务类型
        /// </summary>
        public virtual Admin.NET.Core.MoveType? MoveType { get; set; }
        
        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        public virtual long? SOID { get; set; }
        
        /// <summary>
        /// 单据编号
        /// </summary>
        public virtual string NO { get; set; }
        
        /// <summary>
        /// 退料申请日期
        /// </summary>
        public virtual DateTimeOffset? Billdate { get; set; }
        
        /// <summary>
        /// 领用项目号
        /// </summary>
        public virtual string WBSElementcode { get; set; }
        
        /// <summary>
        /// 领料部门
        /// </summary>
        public virtual string BenefitingDepartcode { get; set; }
        
        /// <summary>
        /// 收益部门
        /// </summary>
        public virtual string CostCenterID { get; set; }
        
        /// <summary>
        /// 客户
        /// </summary>
        public virtual string FI_Client_Analysis_H { get; set; }
        
        /// <summary>
        /// 领取人
        /// </summary>
        public virtual string PickerID { get; set; }
        
        /// <summary>
        /// 公司
        /// </summary>
        public virtual string Companyname { get; set; }
        
        /// <summary>
        /// 施工队
        /// </summary>
        public virtual string ConstructionTeamID { get; set; }
        
        /// <summary>
        /// 单据状态
        /// </summary>
        public virtual Admin.NET.Core.OrderStatusEnum? OrderStatus { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState IssueState { get; set; }
        
    }

        /// <summary>
    /// 退库单不分页查询参数
    /// </summary>
    public class WmsStockReturnOrderSearchNonPage : PageInputNonPageBase
    {
        /// <summary>
        /// 单据大类
        /// </summary>
        public virtual long? OrderLargeCategory { get; set; }
        
        /// <summary>
        /// 单据小类
        /// </summary>
        public virtual long? OrderSubclass { get; set; }
        
        /// <summary>
        /// 事务类型
        /// </summary>
        public virtual Admin.NET.Core.MoveType? MoveType { get; set; }
        
        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        public virtual long? SOID { get; set; }
        
        /// <summary>
        /// 单据编号
        /// </summary>
        public virtual string? NO { get; set; }
        
        /// <summary>
        /// 退料申请日期
        /// </summary>
        public virtual DateTimeOffset? Billdate { get; set; }
        
        /// <summary>
        /// 领用项目号
        /// </summary>
        public virtual string? WBSElementcode { get; set; }
        
        /// <summary>
        /// 领料部门
        /// </summary>
        public virtual string? BenefitingDepartcode { get; set; }
        
        /// <summary>
        /// 收益部门
        /// </summary>
        public virtual string? CostCenterID { get; set; }
        
        /// <summary>
        /// 客户
        /// </summary>
        public virtual string? FI_Client_Analysis_H { get; set; }
        
        /// <summary>
        /// 领取人
        /// </summary>
        public virtual string? PickerID { get; set; }
        
        /// <summary>
        /// 公司
        /// </summary>
        public virtual string? Companyname { get; set; }
        
        /// <summary>
        /// 施工队
        /// </summary>
        public virtual string? ConstructionTeamID { get; set; }
        
        /// <summary>
        /// 单据状态
        /// </summary>
        public virtual Admin.NET.Core.OrderStatusEnum? OrderStatus { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState? IssueState { get; set; }
        
    }

    /// <summary>
    /// 退库单输入参数
    /// </summary>
    public class WmsStockReturnOrderInput
    {
        /// <summary>
        /// 单据大类
        /// </summary>
        public virtual long OrderLargeCategory { get; set; }
        
        /// <summary>
        /// 单据小类
        /// </summary>
        public virtual long OrderSubclass { get; set; }
        
        /// <summary>
        /// 事务类型
        /// </summary>
        public virtual Admin.NET.Core.MoveType MoveType { get; set; }
        
        /// <summary>
        /// 上位系统单据唯一识别码
        /// </summary>
        public virtual long SOID { get; set; }
        
        /// <summary>
        /// 单据编号
        /// </summary>
        public virtual string NO { get; set; }
        
        /// <summary>
        /// 退料申请日期
        /// </summary>
        public virtual DateTimeOffset Billdate { get; set; }
        
        /// <summary>
        /// 领用项目号
        /// </summary>
        public virtual string WBSElementcode { get; set; }
        
        /// <summary>
        /// 领料部门
        /// </summary>
        public virtual string BenefitingDepartcode { get; set; }
        
        /// <summary>
        /// 收益部门
        /// </summary>
        public virtual string CostCenterID { get; set; }
        
        /// <summary>
        /// 客户
        /// </summary>
        public virtual string FI_Client_Analysis_H { get; set; }
        
        /// <summary>
        /// 领取人
        /// </summary>
        public virtual string PickerID { get; set; }
        
        /// <summary>
        /// 公司
        /// </summary>
        public virtual string Companyname { get; set; }
        
        /// <summary>
        /// 施工队
        /// </summary>
        public virtual string ConstructionTeamID { get; set; }
        
        /// <summary>
        /// 单据状态
        /// </summary>
        public virtual Admin.NET.Core.OrderStatusEnum OrderStatus { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public virtual Admin.NET.Core.IssueState IssueState { get; set; }
        
    }

    public class AddWmsStockReturnOrderInput : WmsStockReturnOrderInput
    {
        /// <summary>
        /// 退库明细
        /// </summary>
        public List<WmsStockReturnOrderDetail> WmsStockReturnOrderDetails { get; set; }
        

    }
    /// <summary>
    /// 退库明细
    /// </summary>
    public class WmsStockReturnOrderDetail
    {
        /// <summary>
        /// 单据Id  
        /// </summary>
        public long OrderId { get; set; }

        /// <summary>
        /// 上位系统单据明细唯一识别码
        /// </summary>
        public long OID { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        public string Materialcode { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string Materialname { get; set; }

        /// <summary>
        /// 基本单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Qty { get; set; }

        /// <summary>
        /// 单根长度 
        /// </summary>
        public decimal Length { get; set; }

        /// <summary>
        /// 件数    
        /// </summary>
        public decimal Number { get; set; }

        /// <summary>
        /// SCM批次号    
        /// </summary>
        public string Batchno_SCM { get; set; }

        /// <summary>
        /// WMS批次号    
        /// </summary>
        public string Batchno_WMS { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// TA号
        /// </summary>
        public string TACode { get; set; }

        /// <summary>
        /// 分段号
        /// </summary>
        public string PartCode { get; set; }
    }


    public class DeleteWmsStockReturnOrderInput : BaseId
    {
    }

    public class UpdateWmsStockReturnOrderInput : WmsStockReturnOrderInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
        
    }

    public class QueryeWmsStockReturnOrderInput : BaseId
    {

    }

    /// <summary>
    /// 获取明细参数
    /// </summary>
    public class PageDetailStockReturnOrderInput : PageInputBase
    {
        /// <summary>
        /// Id
        /// </summary>
        public long? Id { get; set; }
    }
    /// <summary>
    /// 获取出库完成明细参数
    /// </summary>
    public class GetTakeOrderDetailInput : PageInputBase
    {
        /// <summary>
        /// 物料编号
        /// </summary>
        public string Materialcode { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string Materialname { get; set; }

        /// <summary>
        /// SCM批次号    
        /// </summary>
        public string Batchno_SCM { get; set; }

        /// <summary>
        /// WMS批次号    
        /// </summary>
        public string Batchno_WMS { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// TA号
        /// </summary>
        public string TACode { get; set; }

        /// <summary>
        /// 分段号
        /// </summary>
        public string PartCode { get; set; }
    }


    /// <summary>
    /// 退料上架给yigo系统的参数
    /// </summary>
    public class StockReturnGoodsReturnOrderInput
    {
        /// <summary>
        /// Id
        /// </summary>
        public long? Id { get; set; }

        /// <summary>
        /// YIGO系统id
        /// </summary>
        public long? SOID { get; set; }

        /// <summary>
        /// 退料申请单号
        /// </summary>
        public string NO { get; set; }

        /// <summary>
        /// WMS入库单号
        /// </summary>
        public string WMSNO { get; set; }

        /// <summary>
        ///出库时间
        /// </summary>
        public DateTimeOffset? Billdate { get; set; }

        /// <summary>
        /// 退料详情
        /// </summary>
        public List<StockReturnGoodsReturnOrderInputDetail> Dtls { get; set; }
    }
    /// <summary>
    /// 退货下架给yigo系统的详情参数
    /// </summary>
    public class StockReturnGoodsReturnOrderInputDetail
    {
        /// <summary>
        /// OID  
        /// </summary>
        public long OID { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string Materialname { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        public string Materialcode { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Qty { get; set; }

        /// <summary>
        /// 库区    
        /// </summary>
        public string DestStoreareacode { get; set; }

        /// <summary>
        /// 储位    
        /// </summary>
        public string DestLocationcode { get; set; }

        /// <summary>
        /// SCM批次号    
        /// </summary>
        public string Batchno_SCM { get; set; }

        /// <summary>
        /// WMS批次号    
        /// </summary>
        public string Batchno_WMS { get; set; }
    }
}
