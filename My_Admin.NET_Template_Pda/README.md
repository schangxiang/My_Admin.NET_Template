> ***<span style="color:#00008B;">如果有新增修改的通用内容，请在本文件上更新使用方法，并注明更新人、更新时间</span>***  
> ***<span style="color:#00008B;">创建人：数瀛 高级前端 郁欢欢，创建时间：2022年11月11日</span>*** 

> ***<span style="color:#00008B;">更新1：</span>***  
> ***<span style="color:#00008B;">更新人：数瀛 高级前端 郁欢欢</span>***  
> ***<span style="color:#00008B;">更新时间：2022年11月25日</span>***  
> ***<span style="color:#00008B;">更新内容：ajax调用，第三参数增加fullRes设置</span>***    

> ***<span style="color:#00008B;">更新2：</span>***  
> ***<span style="color:#00008B;">更新人：数瀛 高级前端 郁欢欢</span>***  
> ***<span style="color:#00008B;">更新时间：2022年11月30日</span>***  
> ***<span style="color:#00008B;">更新内容：</span>***  
>> ***<span style="color:#00008B;">config配置增加downUrl</span>***   
>> ***<span style="color:#00008B;">utils增加parseDic方法、downloadApk方法</span>***   
>> ***<span style="color:#00008B;">增加 ActionUserRow 组件</span>***  
>> ***<span style="color:#00008B;">增加 ScanInputFormItem 组件</span>***  
>> ***<span style="color:#00008B;">增加 EasySelectFormItem 组件</span>***  
 
----

## 使用火狐浏览器FireFox阅读本文件
> 火狐浏览器安装Markdown Viewer扩展组件，安装之后在浏览器的地址栏输入本文件的地址（可以是本地的文件地址，如D:\README.md），回车确认即可。
----

## 使用技术梳理
### 一、uni-app，资料网站：<a href="https://uniapp.dcloud.net.cn/" target="_blank">https://uniapp.dcloud.net.cn/</a>
### 二、VUE2，资料网站：<a href="https://v2.cn.vuejs.org/" target="_blank">https://v2.cn.vuejs.org/</a>
### 三、vuex，store全局数据，资料网站：<a href="https://v3.vuex.vuejs.org/zh/" target="_blank">https://v3.vuex.vuejs.org/zh/</a>
### 四、uview，ui库，资料网站：<a href="https://www.uviewui.com/components/intro.html" target="_blank">https://www.uviewui.com/components/intro.html</a>
### 五、DCloud<a href="https://ext.dcloud.net.cn/" target="_blank">插件市场</a>，其中的绝大多数插件可以使用HbuilderX直接导入到项目中  
> HbuilderX会将插件导入到项目的uni_modules文件夹下  
> HbuilderX插件导入依赖uniModules核心插件，请先安装；若导入失败，很有可能是uniModules的版本问题，请卸载后再次安装后再导入。  
> 使用HbuilderX导入插件，请注意插件的平台支持以及vue语言版本
----

## 开发以及编译的工具和环境
### 一、开发工具HbuilderX，官网：<a href="https://www.dcloud.io/hbuilderx.html" target="_blank">https://www.dcloud.io/hbuilderx.html</a>，安装之后注意把一些常用插件也安装了。如何安装插件，官网有介绍。
### 二、nodejs  
#### 1、下载地址：<a href="https://nodejs.org/zh-cn/" target="_blank">https://nodejs.org/zh-cn/</a>  
#### 2、安装之后的配置
> 在nodejs的安装根目录下新建2个文件夹node_global和node_cache，然后以管理员身份打开命令行窗口（cmd）输入如下代码:
~~~
npm config set prefix "node_global文件夹地址"
npm config set cache "node_cache文件夹地址"
~~~
> 环境变量设置（如何打开计算机的环境变量窗口，这里不作阐述，不知道的去问度娘）：
>> NODE_PATH 用户变量，值为：nodejs根目录\node_modules  
>> Path 系统变量，新增2个属性：
>>> nodejs根目录\  
>>> nodejs根目录\node_global 
   
> cnpm镜像配置，输入如下命令：
~~~
npm install -g cnpm --registry=https://registry.npm.taobao.org
~~~
----

## 项目运行及打包
### 首次安装
> 在项目的根目录，输入命令
~~~
npm install 或者 cnpm install
~~~
### 浏览器运行 
> HbuilderX打开项目，菜单栏 运行 > 运行到浏览器 > 选择浏览器  
> 浏览器打开 开发者工具 ，将显示模式改为 移动设备 模式  
### 真机（手机或者pda）调式
> 1、使用数据线连接 开发电脑 和 真机  
> 2、真机的usb调试权限设置打开  
> 3、确保 开发电脑 和 真机 处于同一网络  
> 4、HbuilderX打开项目，菜单栏 运行 > 运行到手机或模拟器 > 选择调试真机  
>> 注意，首次运行的时候，HbuilderX会给真机安装调试app，需要在安装完成后再次运行  
### 云打包  
> 1、HbuilderX创建账号并登录  
> 2、如果你的HbuilderX是首次对项目进行打包，需要获取下AppId，在manifest.json的基础设置中  
<img src="./mdImgs/1664329468868.jpg" style="width:100%" />  
> 3、应用图标：准备一张图片，最好是1024*1024像素的，在manifest.json的App图标设置中进行配置。  
<img src="./mdImgs/1664329790970.jpg" style="width:100%" />  
> 4、自动生成安装apk：菜单栏->发行->原生App云打包。云打包需要等待一段时间。  
<img src="./mdImgs/1664330296712.png" style="width:100%" />   
> 5、打包完成，apk在 unpackage\release\apk 路径下自动生成
-----

## 项目目录结构
> components 自开发的通用组件 
 
> config 项目参数配置  

> node_modules 安装的第三方代码或组件  

> pages 开发页面的代码  
  
> service 封装的服务代码  
>> request 接口调用封装  
>> store vuex全局数据封装  

> static 静态资源  
>> fonts 字体样式库  
>> img  本地图片资源  
>> js  js资源  
>>> print  蓝牙打印  
>>> utils  公用js代码 
 
> uni_modules HbuilderX导入的第三方代码或组件 

> unpackage HbuilderX打包自动生成的文件夹。HbuilderX自动生成的应用图片也是被保存在这里的。
----

## 接口调用
### 1、代码封装在 \service\request  
### 2、使用  
> 已经在main.js中全局引入  
~~~
this.$api
~~~
### 3、可以调用的方法  
#### get、deletet、post、put，返回一个Promise，参数：  
> 第一个参数，string，后端的方法名，必传   

> 第二个参数，json object，接口传参，没有后续参数的时候，可不传  

> 第三个参数，string 或者 json object，接口调用配置设置，没有后续参数的时候，可不传  
>> string时，即json object结构的block值   
>> json object时的结构  
>>> warn: boolean，出错时是否提示错误信息，可不设置，默认true  
>>> host：string，接口地址的域名名称，对应config配置中ajax的host，可不设置，默认default  
>>> block：string，接口地址的模块名称，对应config配置中ajax的block，可不设置，默认default  
>>> needToken: boolean，headers是否传入token，可不设置，默认true  
>>> loading: boolean，接口调用过程中是否开启全屏loading，可不设置，默认true   
>>> fullRes: boolean，接口返回是否将后端的整个结构体返回，可不设置，默认false（只返回结构体的data）（更新1）   
#### post、put，参数： 
> 第四个参数，json object，post和put以get传参方式传递的参数，可不传  
#### base，返回一个Promise，参数：  
> 第一个参数，string，get、deletet、post、put中的一个，必传   
> 后续参数，沿用 get、deletet、post、put 各自的方法  
#### getHeaders，获取项目中的headers配置，参数
> 第一个参数，json object，可不传，结构如下  
>> needToken: boolean，headers是否传入token，可不设置，默认true  

> 第二个参数，json object，原有的headers，可不传  

> 返回一个json object，结构如下  
>> flag: boolean，设置headers是否通过  
>> headers: json object，项目配置后的headers  
#### getUrl，返回string，一个完整的url，参数：  
> 第一个参数，string，后端的方法名，必传  

> 第二个参数，string 或者 json object，接口调用配置设置，没有后续参数的时候，可不传  
>> string时，即json object结构的block值   
>> json object时的结构  
>>> host：string，接口地址的域名名称，对应config配置中ajax的host，可不设置，默认default  
>>> block：string，接口地址的模块名称，对应config配置中ajax的block，可不设置，默认default  
----

## config相关  
### 配置说明  
> ajax，接口调用相关配置  

>> errMsg，string，接口调用出错后的默认提示信息  

>> host，json object，接口调用域名配置  

>>> default，string，默认域名 

>> block，json object，接口调用模块配置  

>>> default，string，默认模块  

> pagination，json object，分页相关   

> path，json object，页面路径，关联pages.json，页面跳转的时候使用此处的配置，便于维护  

> downUrl，string，版本更新下载地址配置（更新2）  

### 开发使用  
> 已经在main.js中全局引入    
~~~
this.$config
~~~ 
-----

## utils相关   
### 使用范例  
~~~
import { $alert } from '@/static/js/utils'
~~~ 

### utils说明 
> $successInfo，Function类型，成功信息提示，参数：  
>> 第一个参数， string， 成功提示信息， 必传，文字尽量精简，因为只支持单行显示 

> $alert，Function类型，弹出一个信息提示窗口，参数：
>> 第一个参数， string， 提示信息， 必传  
>> 第二个参数， string， 弹窗标题，可不传，默认“系统提示”  
>> 第三个参数， Function， 点击确认的回调方法， 可不传 
 
> color，json对象类型，颜色处理相关

>> getRandomHexColor，Function类型，返回一个16进制的颜色值  

> regexValidate，json对象类型，正则校验  

> uuid，Function类型，创建一个随机码，参数：  
>> 第一个参数， number， 随机码长度， 默认6  
>> 第二个参数， number， 随机码组合方式，默认0：    
>>> 0：大小写字母+数字  
>>> 1：小写字母+数字  
>>> 2：纯数字  
>>> 3：大小写字母  
>>> 4：小写字母  

> parseDic，Function类型，解析数据字典（更新2）：  
>> 第一个参数， Object， vuex的store对象， 在vue文件中为this.$store  
>> 第二个参数， string， 字典集合码   
>> 第三个参数， string/number， 字典值   
>> 返回， string， 字典值对应名称   

> downloadApk，Function类型，新版本下载的方法（更新2）：  
>> 第一个参数， Object， config对象， 在vue文件中为this.$config    
-----

## 我们自己开发的组件说明  
###  一、PageHeader组件，这个是HeaderPageLayout组件的内置组件，不做详细说明  
###  二、FullPageLayout组件，页面外层组件，通常使用在页面的最外层
#### 组件属性：  
> gradient，boolean，底色渐变，即是否设置css的background-image属性，默认false  

> gradient-value，string，底色渐变值，即css的background-image属性的值，默认不设置，采用默认渐变色   

> background-color，string，底色，在gradient为false的时候才生效，默认不设置，即透明  

> safety，boolean，安全区，页面内容显示的时候，是否将安全区排除，默认true，即排除安全区  

> full，boolean，是否整屏，默认true，如果组件不是用在页面的最外层，应该设置为false，这时候组件的宽高为父容器的100%  

> base-background-color，string，基础底色，页面底色外层的颜色，当有安全区的时候，安全区的颜色为此颜色。默认不设置，即透明，这时候显示的颜色会是uni-app中设置的颜色  
#### 组件插槽： 
> default，默认插槽，页面body内容  
### 三、HeaderPageLayout组件，带有头部布局的页面外层组件，通常使用在页面的最外层  
#### 组件属性：  
> gradient，boolean，页面body部分底色渐变，即是否设置css的background-image属性，默认false  

> gradient-value，string，页面body部分底色渐变值，即css的background-image属性的值，默认不设置，采用默认渐变色  

> background-color，string，页面body部分底色，在gradient为false的时候才生效，默认不设置，即透明  

> base-background-color，string，基础底色，页面底色外层的颜色，当有安全区的时候，安全区的颜色为此颜色。当不设置头部底色的时候，头部的底色也为此颜色。默认不设置，即透明，这时候显示的颜色会是uni-app中设置的颜色  

> safety，boolean，安全区，页面内容显示的时候，是否将安全区排除，默认true，即排除安全区  

> full，boolean，是否整屏，默认true，如果组件不是用在页面的最外层，应该设置为false，这时候组件的宽高为父容器的100%  

> title，string，头部标题，默认“Title”  

> header-color，string，头部文字颜色，默认“#212121”  

> header-gradient，boolean，头部底色渐变，即是否设置css的background-image属性，默认false  

> header-gradient-value，string，头部底色渐变值，即css的background-image属性的值，默认不设置，采用默认渐变色   

> header-background-color，string，底色，在header-gradient为false的时候才生效，默认不设置，即透明  
#### 组件插槽： 
> default，默认插槽，页面body内容   

> footer，页脚插槽，页面底部内容   

> headerleft，页面标题栏左侧内容  

> headerright，页面标题栏右侧内容  
### 组件事件：
> headerclick，标题栏左右两侧的点击事件
>> 第一个参数，值为“left”，表示左侧的点击，值为“right”，表示右侧的点击  
### 组件方法：  
> getBodyHeight，返回组件body的高度，number类型，单位px。  
>> 提供此方法的原因在于uni-app使用flex布局的时候，app应用在flex标签容器内部flex-grow（flex）的自适应标签中的标签，无法通过%设置高度。  
>> 而本组件使用了flex布局，组件的body部分使用的就是flex-grow自适应  
### 四、DefaultHeaderPageLayout组件，默认的带有标题栏的页面外层组件，通常使用在登录之后页面的最外层  
#### 组件属性：  
> title，string，头部标题，默认“Title”  

> back-custom，boolean，是否开启返回事件自定义，默认false不开启      
#### 组件事件： 
> back，自定义返回事件     

#### 组件插槽： 
> default，默认插槽，页面body内容   

> footer，页脚插槽，页面底部内容   
### 组件方法：  
> getBodyHeight，同HeaderPageLayout组件   

### 五、EasyPicker，简易的单选选择器  
#### 组件属性：  
> visible，boolean，是否显示，默认false   可用语法糖.sync

> list，Array，选择项数据 

> value-field，string，选项value字段，默认“value”   

> label-field，string，选项显示label字段，默认“label”  
#### 组件事件：    
> select，选择事件，返回：  
>> 第一个参数：选中项的value值  
>> 第二个参数，整个选中项  
>> 第三个参数，选中项在选项中的位置  

### 六、ActionUserRow组件，显示操作员（更新2）  

### 七、ScanInputFormItem组件，扫码输入表单（更新2）    
#### 组件属性：  
> type，string，输入框type，可选值：number、idcard、digit、password、text，默认“text”    

> placeholder，string，空置占位文字，默认“请输入...”       

> label，string，标签名称，默认空字符串  

> msg，string，提示信息，默认空字符串  

> msg-type，string，提示信息类型，可选值：error（错误信息，红色）、info（常规信息，蓝色），默认“error”    

> value/v-model，string/number/null    

> has-search，boolean，是否显示搜索按钮，默认false不显示      

> disabled，boolean，是否禁用，默认false不禁用
#### 组件事件： 
> search，搜索按钮点击事件     

> clear，清除icon点击事件       

### 八、EasySelectFormItem组件，单选下拉表单（更新2）  
#### 组件属性：  
> placeholder，string，空置占位文字，默认“请输入...”       

> label，string，标签名称，默认空字符串  

> msg，string，提示信息，默认空字符串  

> msg-type，string，提示信息类型，可选值：error（错误信息，红色）、info（常规信息，蓝色），默认“error”    

> value/v-model，string/number/boolean/null    

> list，Array，选择项数据      

> value-field，string，选项value字段，默认“value”   

> label-field，string，选项显示label字段，默认“label”  

> disabled，boolean，是否禁用，默认false不禁用
#### 组件事件： 
> select，选项点击事件     

> clear，清除icon点击事件      

-----

## 蓝牙打印使用  
### 范例   
~~~
import BluePrint from '@/pages/print/bluePrint.js'

const onPrint = function(){
	let ptintContext = [
		{type:'text',x:0,y:0,text:'',size:2,rotate:0,bold:0,underline:false,reverse:false},
		{type:'text',x:0,y:0,text:'',size:2,rotate:0,bold:0,underline:false,reverse:false},
		{type:'text',x:0,y:0,text:'',size:2,rotate:0,bold:0,underline:false,reverse:false},
		/* 以上空数据，为防止丢包时打印不正常 */
		{type:'text',x:230,y:10,text:'PART:',size:2,rotate:0,bold:0,underline:false,reverse:false},
		{type:'text',x:300,y:10,text:'HFAHFOAJFOAJFO',size:2,rotate:0,bold:0,underline:false,reverse:false},
		{type:'text',x:230,y:95,text:'NAME:',size:2,rotate:0,bold:0,underline:false,reverse:false},
		{type:'text',x:300,y:95,text:'SAHFAKHFKAHFKANFK',size:2,rotate:0,bold:0,underline:false,reverse:false},
		{type:'text',x:230,y:180,text:'LOTS:',size:2,rotate:0,bold:0,underline:false,reverse:false},
		{type:'text',x:300,y:180,text:'LOTSLOTSLOTSLOTSLOTS',size:2,rotate:0,bold:0,underline:false,reverse:false},
		{type:'text',x:13,y:250,text:'SIZE:',size:2,rotate:0,bold:0,underline:false,reverse:false},
		{type:'text',x:83,y:250,text:'SIZESIZESIZESIZESIZESIZESIZESIZE',size:2,rotate:0,bold:0,underline:false,reverse:false},
		{type:'text',x:105,y:315,text:'MAKINO J(CHINA) CO.LTD',size:3,rotate:0,bold:0,underline:false,reverse:false},
		{type:'qr',x:10,y:10,text:'asndkajdkjalda-barcode',width:8,level:1}
	]
	BluePrint.print(this.$store,ptintContext).then(()=>{
		console.log('print成功调用')
	}).catch(()=>{
		console.log('print调用出错')
	})
}
~~~   
### 打印数据字段说明  
> type，string，可选值：(line)直线、(text)文字、(qr)二维码、(bar)条形码  

> x，number，打印文字、二维码、条形码时候，起始点横坐标   

> y，number，打印文字、二维码、条形码时候，起始点纵坐标   

> x1，number，打印直线时候，起始点横坐标   

> y1，number，打印直线时候，起始点纵坐标   

> x2，number，打印直线时候，结束点横坐标   

> y2，number，打印直线时候，结束点纵坐标   

> text，string，打印文字、二维码、条形码时候，打印内容  

> size，number，打印文字时的字体大小，可选值 20/1/2/3/4/5/6/7 ，其他的时候默认为2    

> width，number   
>> 打印直线，表示线宽  
>> 打印二维码，表示二维码刻度值，不是生成的二维码的宽度，取值（2到15）  
>> 打印条形码，表示条形码刻度值，不是生成的条形码的宽度，取值（2到6）  

> level，number，打印二维码时候，纠错等级，取值（0到20）  

> height，number，打印条形码时候，高度，取值（1到255）  

> barType，number，打印条形码时候，条形码类型，可选值：
>> 0：CODE39；  
>> 1：CODE128；   
>> 2：CODE93；   
>> 3：CODEBAR；    
>> 4：EAN8；  
>> 5：EAN13；  
>> 6：UPCA;   
>> 7：UPC-E;   
>> 8：ITF 

> rotate，number，打印文字时候，选择角度  

> bold，number，打印文字时候，是否加粗(0否1是) 

> underline，boolean，打印文字时候，下划线设置    

> reverse，boolean，打印文字时候，反转设置    

-------
## 页面说明  
### /pages/start/index  
> 启动页，根据项目需求调整  

### /pages/login/index  
> 登录页，一般不会大改动了，根据项目需求做微调  

### /pages/home/index
> 登录后的首页，根据项目需求，很可能做局部调整  

### /pages/baseTask/out
> 入库页面，根据项目需求调整

### /pages/baseTask/out  
> 出库页面，根据项目需求调整  

### /pages/print/bluetoothConnection    
> 物蚂蚁蓝牙打印机，蓝牙连接页面，几乎无需改动  

### /pages/setting/host
> 后端接口调用域名设置页面，几乎无需改动     

-----
## json-server  前端数据moke  
### 启动配置，如图在package.json中，然后命令行运行    
~~~
npm run api  
~~~  
> --port 3012  端口号设置为3012  
> --host 0.0.0.0  开启ip地址访问    
<img src="./mdImgs/1665970038354.jpg" style="width:100%" />    

###  RESET Api    
> GET /demo  获取全部列表   
> GET /demo/数据id   获取详情    
> POST  /demo   增加    
> PUT  /demo/数据id  整条数据替换    
> PATCH  /demo/数据id   修改某个字段    
> DELETE /demo/数据id  删除    
###  条件查询和分页查询  
#### 条件查询，字段名称后增加   
> _gte 大于等于  
> _lte 小于等于  
> _ne  不等于  
> _like  包含  

#### 分页查询  
> _page  页码，从1开始  
> _limit 每页多少条数据  
> _start 数据索引开始，从0开始  
> _end 数据索引结束，不包含end索引    

案例：GET模糊查找字段name  
~~~
/demo?name_like=value   
~~~  

-------
## 新建页面步骤  
#### 1、配置pages.json  
#### 2、配置config/index.js中的path  
#### 3、首页配置页面入口（根据需要）  