﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 单据主表状态枚举
    /// </summary>
    public enum OrderStatusEnum
    {
        /// <summary>
        /// 未下发
        /// </summary>
        [Description("未下发")]
        WEIXIAFA = 1,

        /// <summary>
        /// 已下发
        /// </summary>
        [Description("已下发")]
        YIXIAFA = 2,

        /// <summary>
        /// 完成
        /// </summary>
        [Description("完成")]
        WANCHENG = 3,

        /// <summary>
        /// 撤回
        /// </summary>
        [Description("撤回")]
        CHEHUI = 4,

        /// <summary>
        /// 取消
        /// </summary>
        [Description("取消")]
        QUXIAO = 5
    }

    /// <summary>
    /// 采购单主表订单状态枚举
    /// </summary>
    public enum PurchaseOrderStatusEnum
    {
        /// <summary>
        /// 未开始
        /// </summary>
        [Description("未开始")]
        WEIKAISHI = 1,

        /// <summary>
        /// 部分发货
        /// </summary>
        [Description("部分发货")]
        BUFENFAHUO = 2,

        /// <summary>
        /// 全部发货
        /// </summary>
        [Description("全部发货")]
        CHEHUI = 3,

        /// <summary>
        /// 完成
        /// </summary>
        [Description("完成")]
        WANCHENG = 4
    }

    /// <summary>
    /// 采购收货主表订单状态枚举
    /// </summary>
    public enum PurchaseReceiveOrderEnum
    {
        /// <summary>
        /// 待收货
        /// </summary>
        [Description("待收货")]
        DAISHOUHUO = 1,

        /// <summary>
        /// 部分收货
        /// </summary>
        [Description("部分收货")]
        BUFENSHOUHUO = 2,

        /// <summary>
        /// 收货完成
        /// </summary>
        [Description("收货完成")]
        SHWANCHENG = 3,
    }
}
