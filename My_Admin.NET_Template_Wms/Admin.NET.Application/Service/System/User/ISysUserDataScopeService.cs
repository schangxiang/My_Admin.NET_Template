﻿namespace Admin.NET.Application
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISysUserDataScopeService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="orgIdList"></param>
        /// <returns></returns>
        Task DeleteUserDataScopeListByOrgIdList(List<long> orgIdList);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task DeleteUserDataScopeListByUserId(long userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<List<long>> GetUserDataScopeIdList(long userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task GrantData(UpdateUserRoleDataInput input);
    }
}