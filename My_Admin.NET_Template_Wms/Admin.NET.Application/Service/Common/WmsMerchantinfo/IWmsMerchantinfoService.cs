﻿using Admin.NET.Core;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Admin.NET.Application
{
    public interface IWmsMerchantinfoService
    {
        Task Add(AddWmsMerchantinfoInput input);
        Task Delete(DeleteWmsMerchantinfoInput input);
        Task<WmsMerchantinfoOutput> Get([FromQuery] QueryeWmsMerchantinfoInput input);
        Task<List<WmsMerchantinfoOutput>> List([FromQuery] WmsMerchantinfoInput input);
        Task<PageResult<WmsMerchantinfoOutput>> Page([FromQuery] WmsMerchantinfoSearch input);
        Task Update(UpdateWmsMerchantinfoInput input);

        Task<List<WmsMerchantinfoOutput>> ListNonPageAsync([FromQuery] WmsMerchantinfoSearchNonPage input);

        Task<int> FromExcelAsync(IFormFile file, [FromQuery] ImportExcelType importExcelType);

        Task<IActionResult> DownloadExcelTemplate(string version);
    }
}