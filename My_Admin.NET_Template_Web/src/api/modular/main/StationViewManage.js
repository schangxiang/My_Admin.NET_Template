import { axios } from '@/utils/request'

/**
 * 产线下拉
 *
 * @author yhh
 */
export function GetProductionLines () {
  return axios({
    url: '/stationview/SelectProductionLine',
    method: 'get'
  })
}

/**
 * 根据产线获取工位
 *
 * @author yhh
 */
export function GetStationList (id) {
  return axios({
    url: '/stationview/GetStationList',
    method: 'get',
    params:{id}
  })
}

/**
 * 获取工位详情
 *
 * @author yhh
 */
export function GetStationDetail (params) {
  return axios({
    url: '/stationview/GetMaterialDetail',
    method: 'get',
    params:params
  })
}

/**
 * 更新工位使用状态
 *
 * @author yhh
 */
export function UpdateLock (parameter) {
  return axios({
    url: '/stationview/UpdateStationIslock',
    method: 'get',
    params: parameter
  })
}

/**
 * 更新工位空闲状态
 *
 * @author yhh
 */
export function UpdateEmpty (parameter) {
  return axios({
    url: '/stationview/UpdateStationKongXian',
    method: 'get',
    params: parameter
  })
}