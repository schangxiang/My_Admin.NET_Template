﻿#nullable enable
using System.ComponentModel;

namespace Admin.NET.Application
{
    /// <summary>
    /// Excel模板输出参数
    /// </summary>
    [Description("Excel模板")]
    public class SysExcelTemplateOutput
    {
        /// <summary>
        /// 模板名称
        /// </summary>
        [Description("模板名称")]
        public string Name { get; set; } = null!;

        /// <summary>
        /// 版本
        /// </summary>
        [Description("版本")]
        public string Version { get; set; } = null!;

        /// <summary>
        /// 所属应用
        /// </summary>
        [Description("所属应用")]
        public string AppName { get; set; } = null!;

        /// <summary>
        /// 类名
        /// </summary>
        [Description("类名")]
        public string ClassName { get; set; } = null!;

        /// <summary>
        /// 模板文件名称
        /// </summary>
        [Description("模板文件名称")]
        public string TemplateFileName { get; set; } = null!;

        /// <summary>
        /// 唯一字段集
        /// </summary>
        [Description("唯一字段集")]
        public string UnionUniqueFields { get; set; } = null!;

        /// <summary>
        /// 表头开始行
        /// </summary>
        [Description("表头开始行")]
        public int HeadStartLine { get; set; }

        /// <summary>
        /// 数据开始行
        /// </summary>
        [Description("数据开始行")]
        public int DataStartLine { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Description("状态")]
        public Core.CommonStatus Status { get; set; }

        /// <summary>
        /// Id主键
        /// </summary>
        [Description("Id主键")]
        public long Id { get; set; }

    }
}
