﻿using Admin.NET.Core;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;

namespace Admin.NET.Application
{
    public interface IWmsCommonService
    {
        /// <summary>
        /// 获取通用编号
        /// </summary>
        /// <returns></returns>
        Task<string> GetSerial([FromQuery] EnumSerialType SerialType);


        /// <summary>
        /// 生成助记码
        /// 注意：只能调这个接口，不能自己写方法调，否则可能会导致文件读取冲突造成错误！
        /// </summary>
        /// <returns></returns>
        Task<string> GetMnemonicCode([FromQuery] string name);
    }
}