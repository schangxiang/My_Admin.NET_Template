﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application
{
    /// <summary>
    /// 根据批次号获取任务参数
    /// </summary>
    public class AutomaticWarehouseInput
    {
        /// <summary>
        /// 批次号
        /// </summary>
        [Required(ErrorMessage = "批次号不能为空")]
        public virtual string Batch { get; set; }
    }

    /// <summary>
    /// 根据id获取入库口下拉框
    /// </summary>
    public class PdaGetWarehouseEntranceInput
    {
        /// <summary>
        /// id
        /// </summary>
        public long Id { get; set; }
    }
    /// <summary>
    /// 根据单据编号获取任务参数
    /// </summary>
    public class PdaAutomaticWarehouseInput
    {
        /// <summary>
        /// 批次号
        /// </summary>
        [Required(ErrorMessage = "批次号不能为空")]
        public virtual string Batch { get; set; }

        /// <summary>
        /// 流水号
        /// </summary>
        [Required(ErrorMessage = "流水号不能为空")]
        public virtual int SerialNo { get; set; }
        /// <summary>
        /// 入库口
        /// </summary>
        [Required(ErrorMessage = "入库口不能为空")]
        public virtual string EntranceCode { get; set; }
    }
    /// <summary>
    /// 反馈任务状态参数
    /// </summary>
    public class CallBackInput
    {
        /// <summary>
        /// 任务号
        /// </summary>
        [Required(ErrorMessage = "任务号不能为空")]
        public virtual string TaskNo { get; set; }
        /// <summary>
        /// 设备类型
        /// 输送线 = 1,
        /// 堆垛机 = 2,
        /// </summary>
        [Required(ErrorMessage = "设备类型")]
        public virtual string DeviceType { get; set; }

        ///<summary>
        ///设备执行状态
        ///输送线执行中 = 1,
        ///输送线完成 = 2,
        ///堆垛机执行中 = 3,
        ///堆垛机完成 = 4,
        /// </summary>
        [Required(ErrorMessage = "设备类型")]
        public virtual string TaskState { get; set; }
    } 
}
