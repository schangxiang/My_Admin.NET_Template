﻿using System.ComponentModel.DataAnnotations;
using Admin.NET.Core;
using Admin.NET.Core.Service;

namespace Admin.NET.Application
{
    /// <summary>
    /// 库口表查询参数
    /// </summary>
    public class WareWarehouseEntranceSearch : PageInputBase
    {
        /// <summary>
        /// 名称
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// 类型-1.入库口_2.出库口
        /// </summary>
        public virtual WarehouseEntranceEnum? Type { get; set; }

        /// <summary>
        /// 所属车间
        /// </summary>
        public virtual LesWorkShopType? LesWorkShopType { get; set; }

        /// <summary>
        /// 所属巷道
        /// </summary>
        public virtual int? AffiliatedRoadway { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public virtual string Remark { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public virtual CommonStatus? Status { get; set; }
    }

    /// <summary>
    /// 库口表不分页查询参数
    /// </summary>
    public class WareWarehouseEntranceSearchNonPage : PageInputNonPageBase
    {
        /// <summary>
        /// 名称
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// 类型-1.入库口_2.出库口
        /// </summary>
        public virtual WarehouseEntranceEnum? Type { get; set; }

        /// <summary>
        /// 所属车间
        /// </summary>
        public virtual LesWorkShopType? LesWorkShopType { get; set; }

        /// <summary>
        /// 所属巷道
        /// </summary>
        public virtual int? AffiliatedRoadway { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public virtual string? Remark { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public virtual CommonStatus? Status { get; set; }
    }

    /// <summary>
    /// 库口表输入参数
    /// </summary>
    public class WmsWarehouseEntranceInput
    {
        /// <summary>
        /// 名称
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        public virtual string Code { get; set; }

        /// <summary>
        /// 类型-1.入库口_2.出库口
        /// </summary>
        public virtual WarehouseEntranceEnum Type { get; set; }

        /// <summary>
        /// 所属车间
        /// </summary>
        public virtual LesWorkShopType LesWorkShopType { get; set; }

        /// <summary>
        /// 所属巷道
        /// </summary>
        public virtual int AffiliatedRoadway { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public virtual string Remark { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public virtual CommonStatus Status { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class AddWareWarehouseEntranceInput : WmsWarehouseEntranceInput
    {
    }

    /// <summary>
    /// 
    /// </summary>
    public class DeleteWareWarehouseEntranceInput : BaseId
    {
    }

    /// <summary>
    /// 
    /// </summary>
    public class UpdateWareWarehouseEntranceInput : WmsWarehouseEntranceInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class QueryeWareWarehouseEntranceInput : BaseId
    {

    }
}
