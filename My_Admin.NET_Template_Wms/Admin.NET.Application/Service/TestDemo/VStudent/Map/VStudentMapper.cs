﻿using Mapster;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    public class VStudentMapper : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<AddVStudentInput, VStudent>()
            ;
            config.ForType<UpdateVStudentInput, VStudent>()
            ;
            config.ForType<VStudent, VStudentOutput>()
            ;
        }
    }
}
