﻿using System.ComponentModel;

namespace Admin.NET.Application
{
    /// <summary>
    /// 组合类型  
    /// </summary>
    [Description("组合类型")]
    public enum EnumSerialSourceType
    {
        /// <summary>
        /// 字符串描述
        /// </summary>
        [Description("字符串描述")]
        UD,
        /// <summary>
        /// 4位年份
        /// </summary>
        [Description("4位年份")]
        Y4,
        /// <summary>
        /// 2位年份
        /// </summary>
        [Description("2位年份")]
        Y2,
        /// <summary>
        /// 2位月份
        /// </summary>
        [Description("2位月份")]
        M2,
        /// <summary>
        /// 2位天
        /// </summary>
        [Description("2位天")]
        D2,
        /// <summary>
        /// 星期几(W1)
        /// </summary>
        [Description("星期几")]
        W2,
        /// <summary>
        /// 流水号
        /// </summary>
        [Description("流水号")]
        SN,

    }
}
