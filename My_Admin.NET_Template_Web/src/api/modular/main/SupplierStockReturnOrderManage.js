﻿import { axios } from '@/utils/request'

/**
 * 查询供应商退库
 *
 * @author yc
 */
export function SupplierStockReturnOrderPage (parameter) {
  return axios({
    url: '/SupplierStockReturnOrder/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 供应商退库列表
 *
 * @author yc
 */
export function SupplierStockReturnOrderList (parameter) {
  return axios({
    url: '/SupplierStockReturnOrder/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加供应商退库
 *
 * @author yc
 */
export function SupplierStockReturnOrderAdd (parameter) {
  return axios({
    url: '/SupplierStockReturnOrder/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑供应商退库
 *
 * @author yc
 */
export function SupplierStockReturnOrderEdit (parameter) {
  return axios({
    url: '/SupplierStockReturnOrder/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除供应商退库
 *
 * @author yc
 */
export function SupplierStockReturnOrderDelete (parameter) {
  return axios({
    url: '/SupplierStockReturnOrder/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出供应商退库的Excel文件
 *
 * @author yc
 */
export function SupplierStockReturnOrderToExcel (parameter) {
  return axios({
    url: '/SupplierStockReturnOrder/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入供应商退库的Excel文件
 *
 * @author yc
 */
export function SupplierStockReturnOrderFromExcel (data, parameter) {
  return axios({
    url: '/SupplierStockReturnOrder/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载供应商退库的Excel导入模板
 *
 * @author yc
 */
export function SupplierStockReturnOrderDownloadExcelTemplate(parameter) {
  return axios({
    url: '/SupplierStockReturnOrder/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 单据大类下拉
 *
 * @author yhh
 */
export function SupplierStockReturnOrderLargeCategories() {
  return axios({
    url: '/SupplierStockReturnOrder/OrderTypeLargeCategory',
    method: 'get'
  })
}

/**
 * 单据小类下拉
 *
 * @author yhh
 */
export function SupplierStockReturnOrderSubclass(id) {
  return axios({
    url: '/SupplierStockReturnOrder/OrderTypeSubclass',
    method: 'get',
    params:{id}
  })
}


/**
 * 供应商退库单详情
 *
 * @author yhh
 */
export function SupplierStockReturnOrderDetailPage (parameter) {
  return axios({
    url: '/SupplierStockReturnOrder/DetailsPage',
    method: 'get',
    params: parameter
  })
}


