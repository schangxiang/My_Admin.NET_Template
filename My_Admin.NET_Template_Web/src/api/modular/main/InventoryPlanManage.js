﻿import { axios } from '@/utils/request'

/**
 * 查询盘点计划
 *
 * @author li
 */
export function InventoryPlanPage (parameter) {
  return axios({
    url: '/InventoryPlan/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 报告页面查询盘点计划
 *
 * @author yhh
 */
export function InventoryPlanRecordPage (parameter) {
  return axios({
    url: '/InventoryPlan/InventoryComplete',
    method: 'get',
    params: parameter
  })
}

/**
 * 盘点计划列表
 *
 * @author li
 */
export function InventoryPlanList (parameter) {
  return axios({
    url: '/InventoryPlan/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加盘点计划
 *
 * @author yhh
 */
export function InventoryPlanAdd (parameter) {
  return axios({
    url: '/InventoryPlan/AddInventoryPlan',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑盘点计划
 *
 * @author yhh
 */
export function InventoryPlanEdit (parameter) {
  return axios({
    url: '/InventoryPlan/UpdateInventoryPlan',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除盘点计划
 *
 * @author li
 */
export function InventoryPlanDelete (parameter) {
  return axios({
    url: '/InventoryPlan/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出盘点计划的Excel文件
 *
 * @author li
 */
export function InventoryPlanToExcel (parameter) {
  return axios({
    url: '/InventoryPlan/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入盘点计划的Excel文件
 *
 * @author li
 */
export function InventoryPlanFromExcel (data, parameter) {
  return axios({
    url: '/InventoryPlan/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载盘点计划的Excel导入模板
 *
 * @author li
 */
export function InventoryPlanDownloadExcelTemplate(parameter) {
  return axios({
    url: '/InventoryPlan/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}




/**
* 获取InventoryRule列表
* @author li
*/
export function InventoryPlanFkInventoryRuleList() {
  return axios({
    url: '/InventoryPlan/fkInventoryRule',
    method: 'get'
  })
}

/**
* 获取仓库下拉列表
* @author yhh
*/
export function InventoryPlanGetAreas() {
  return axios({
    url: '/InventoryPlan/InventoryArea',
    method: 'get'
  })
}

/**
* 获取盘点规则下拉列表
* @author yhh
*/
export function InventoryPlanGetCheckRules() {
  return axios({
    url: '/InventoryPlan/InventoryRule',
    method: 'get'
  })
}

/**
 * 获取计划号
 *
 * @author yhh
 */
export function GetBillNumber () {
  return axios({
    url: '/InventoryPlan/CreatePlanNo',
    method: 'get'
  })
}

/**
 * 获取可加入计划的明细列表
 *
 * @author yhh
 */
export function GetCouldOuts(params) {
  return axios({
    url: '/InventoryPlan/SelectStock',
    method: 'get',
    params: params
  })
}

/**
 * 盘点计划详情
 *
 * @author yhh
 */
export function InventoryPlanDetailPage (parameter) {
  return axios({
    url: '/InventoryPlan/DetailPage',
    method: 'get',
    params: parameter
  })
}

/**
 * 盘点审核
 *
 * @author yhh
 */
export function InventoryReview (parameter) {
  return axios({
    url: '/InventoryPlan/UpdateExamine',
    method: 'post',
    data: parameter
  })
}

/**
 * 改变库存数
 *
 * @author yhh
 */
export function InventoryChangeStockNumber (parameter) {
  return axios({
    url: '/InventoryPlan/UpdateInventoryStock',
    method: 'post',
    data: parameter
  })
}

