﻿using Furion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Core
{
    public class TemplateConst
    {     
        /// <summary>
        /// 生成导入模板的Excel文件路径(公共)
        /// </summary>
        public static string EXCEL_TEMPLATEFILE_导入模版路径 = App.WebHostEnvironment.WebRootPath + @"\ExcelTemplateFile";
        public static string EXCEL_TEMPLATEFILE_导入模版名称后缀 = "Import";
    }
}
