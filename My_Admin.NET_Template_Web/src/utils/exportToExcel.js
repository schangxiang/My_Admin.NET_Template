//@ts-nocheck
import * as XLSX from 'xlsx';
export function exportExcel(dataSource, entozh, type = "xlsx", name = "全部信息") {
    const nowdata = dataSource;
    if (nowdata != undefined) {
        let i = 1;
        const json = nowdata.map((item) => {
            return Object.keys(item).reduce((newData, key) => {
                console.log(key,'key')
                console.log(entozh,'entozh')
                const newKey = entozh[key] //|| key
                if (newKey !== undefined) {
                    if (key !== 'key') {
                        //不需要key
                        if (key === 'id') {
                            newData[newKey] = i //.toString()
                            i++;
                        } else {
                            newData[newKey] = item[key]
                        }
                    }
                }
                return newData
            }, {})
        });
        // debugger;
        const sheet = XLSX.utils.json_to_sheet(json);
        openDownloadDialog(sheet2blob(sheet, undefined, type), `${name}${getCurrentTime()}.${type}`);
    } else {
        this.$message.warning("无数据")
    }

}

export function openDownloadDialog(url, saveName) {
    if (typeof url == 'object' && url instanceof Blob) {
        url = URL.createObjectURL(url); // 创建blob地址
    }
    var aLink = document.createElement('a');
    aLink.href = url;
    aLink.download = saveName || ''; // HTML5新增的属性，指定保存文件名，可以不要后缀，注意，file:///模式下不会生效
    var event;
    if (window.MouseEvent) event = new MouseEvent('click');
    else {
        event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
    }
    aLink.dispatchEvent(event);
}

export function sheet2blob(sheet, sheetName, type = "xlsx") {
    sheetName = sheetName || 'sheet1';
    var workbook = {
        SheetNames: [sheetName],
        Sheets: {}
    };
    workbook.Sheets[sheetName] = sheet; // 生成excel的配置项

    var wopts = {
        bookType: `${type}`, // 要生成的文件类型
        bookSST: false, // 是否生成Shared String Table，官方解释是，如果开启生成速度会下降，但在低版本IOS设备上有更好的兼容性
        type: 'binary'
    };

    var wbout = XLSX.write(workbook, wopts);
    var blob = new Blob([s2ab(wbout)], {
        type: "application/octet-stream"
    }); // 字符串转ArrayBuffer
    function s2ab(s) {
        var buf = new ArrayBuffer(s.length);
        var view = new Uint8Array(buf);
        for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
        return buf;
    }

    return blob;
}

// 获取当前时间
export function getCurrentTime() {
    let date = new Date();
    //获取年份
    let year = date.getFullYear();
    //获取月份
    let month = date.getMonth() + 1;
    month = month >= 10 ? month : "0" + month;
    //获取天数
    let day = date.getDate();
    day = day >= 10 ? day : "0" + day;
    //获取小时
    let hour = date.getHours();
    hour = hour >= 10 ? hour : "0" + hour;
    //获取分钟
    let min = date.getMinutes();
    min = min >= 10 ? min : "0" + min;
    //获取秒
    let second = date.getSeconds();
    second = second >= 10 ? second : "0" + second;
    //拼接时间字符串
    return `${year}-${month}-${day} ${hour}:${min}:${second}`;
}
