﻿using Admin.NET.Core;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Admin.NET.Application
{
    public interface IWmsOrderService
    {
        Task Add(AddWmsOrderInput input);
        Task Delete(DeleteWmsOrderInput input);
        Task<WmsOrderOutput> Get([FromQuery] QueryeWmsOrderInput input);
        Task<List<WmsOrderOutput>> List([FromQuery] WmsOrderInput input);
        Task<PageResult<WmsOrderOutput>> Page([FromQuery] WmsOrderSearch input);
        Task Update(UpdateWmsOrderInput input);

        Task<List<WmsOrderOutput>> ListNonPageAsync([FromQuery] WmsOrderSearchNonPage input);

        Task<int> FromExcelAsync(IFormFile file, [FromQuery] ImportExcelType importExcelType);

        Task<IActionResult> DownloadExcelTemplate(string version);
    }
}