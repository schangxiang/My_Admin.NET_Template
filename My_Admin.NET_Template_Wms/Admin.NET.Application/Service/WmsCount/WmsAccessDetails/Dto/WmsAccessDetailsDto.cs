﻿using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 出入库记录输出参数
    /// </summary>
    public class AccessDetailsDto
    {
        /// <summary>
        /// 任务号
        /// </summary>
        public string TaskNo { get; set; }
        
        /// <summary>
        /// 任务方式
        /// </summary>
        public TaskModel TaskModel { get; set; }
        
        /// <summary>
        /// 任务类型
        /// </summary>
        public TaskType TaskType { get; set; }
        
        /// <summary>
        /// 任务等级
        /// </summary>
        public int TaskLevel { get; set; }
        
        /// <summary>
        /// 任务状态
        /// </summary>
        public TaskStatusEnum TaskStatus { get; set; }
      
        /// <summary>
        /// 来源
        /// </summary>
        public string SourcePlace { get; set; }
        
        /// <summary>
        /// 目标
        /// </summary>
        public string ToPlace { get; set; }

        /// <summary>
        /// 托盘Id
        /// </summary>
        public long ContainerId { get; set; }
        
        /// <summary>
        /// 托盘编码
        /// </summary>
        public string ContainerCode { get; set; }
        
        /// <summary>
        /// 物料名称
        /// </summary>
        public string MaterialName { get; set; }
        
        /// <summary>
        /// 物料编码
        /// </summary>
        public string MaterialNo { get; set; }
        
        /// <summary>
        /// 物料批次
        /// </summary>
        public string MaterialBatch { get; set; }
        
        /// <summary>
        /// 物料规格
        /// </summary>
        public string MaterialSpec { get; set; }

        /// <summary>
        /// 物料Id
        /// </summary>
        public long MaterialId { get; set; }
        
        /// <summary>
        /// 操作数量
        /// </summary>
        public decimal BindQuantity { get; set; }
        
        /// <summary>
        /// 状态
        /// </summary>
        public CommonStatus BindStatus { get; set; }
    }
}
