﻿import { axios } from '@/utils/request'

/**
 * 查询库区信息
 *
 * @author XJF
 */
export function WmsAreaPage (parameter) {
  return axios({
    url: '/WmsArea/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 库区信息列表
 *
 * @author XJF
 */
export function WmsAreaList (parameter) {
  return axios({
    url: '/WmsArea/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加库区信息
 *
 * @author XJF
 */
export function WmsAreaAdd (parameter) {
  return axios({
    url: '/WmsArea/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑库区信息
 *
 * @author XJF
 */
export function WmsAreaEdit (parameter) {
  return axios({
    url: '/WmsArea/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除库区信息
 *
 * @author XJF
 */
export function WmsAreaDelete (parameter) {
  return axios({
    url: '/WmsArea/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出库区信息的Excel文件
 *
 * @author XJF
 */
export function WmsAreaToExcel (parameter) {
  return axios({
    url: '/WmsArea/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入库区信息的Excel文件
 *
 * @author XJF
 */
export function WmsAreaFromExcel (data, parameter) {
  return axios({
    url: '/WmsArea/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}

/**
 * 下载库区信息的Excel导入模板
 *
 * @author XJF
 */
export function WmsAreaDownloadExcelTemplate(parameter) {
  return axios({
    url: '/WmsArea/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 获取组装车间库区
 *
 * @author yhh
 */
export function GetZuzhuangAreas () {
  return axios({
    url: '/WmsArea/TaskWmsArea',
    method: 'get'
  })
}

