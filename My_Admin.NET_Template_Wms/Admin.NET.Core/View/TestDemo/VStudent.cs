﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

/*
 * @author : shaocx
 * @date : 2024/4/15下午4:44:30
 * @desc : 学生视图
 */
namespace Admin.NET.Core
{
    /// <summary>
    /// 学生视图
    /// </summary>
    [Comment("学生视图")]
    public class VStudent : EntityNotKey
    {
        /// <summary>
        /// 配置视图名
        /// </summary>
        public VStudent() : base("v_student") { }
        /// <summary>
        /// Id主键
        /// 表test_students
        /// </summary>

        public long Id { get; set; }

        /// <summary>
        /// 学生姓名
        /// 表test_students
        /// </summary>
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// 学生年龄
        /// 表test_students
        /// </summary>

        public int Age { get; set; }

        /// <summary>
        /// 是否在校
        /// 表test_students
        /// </summary>

        public bool StartName { get; set; }

        /// <summary>
        /// 性别
        /// 表test_students
        /// </summary>

        public int Gender { get; set; }

        /// <summary>
        /// 出生日期
        /// 表test_students
        /// </summary>

        public DateTime BrithDate { get; set; }

        /// <summary>
        /// 创建时间
        /// 表test_students
        /// </summary>

        public DateTimeOffset? CreatedTime { get; set; }

        /// <summary>
        /// 更新时间
        /// 表test_students
        /// </summary>

        public DateTimeOffset? UpdatedTime { get; set; }

        /// <summary>
        /// 创建者Id
        /// 表test_students
        /// </summary>

        public long? CreatedUserId { get; set; }

        /// <summary>
        /// 创建者名称
        /// 表test_students
        /// </summary>
        [MaxLength(255)]
        public string CreatedUserName { get; set; }

        /// <summary>
        /// 修改者Id
        /// 表test_students
        /// </summary>

        public long? UpdatedUserId { get; set; }

        /// <summary>
        /// 修改者名称
        /// 表test_students
        /// </summary>
        [MaxLength(50)]
        public string UpdatedUserName { get; set; }

        /// <summary>
        /// 软删除标记
        /// 表test_students
        /// </summary>

        public bool IsDeleted { get; set; }

        /// <summary>
        /// 关联老师
        /// 表test_students
        /// </summary>

        public long? TeacherId { get; set; }

        /// <summary>
        /// 名称
        /// 表test_teacher
        /// </summary>
        [MaxLength(255)]
        public string teacherName { get; set; }

    }
}