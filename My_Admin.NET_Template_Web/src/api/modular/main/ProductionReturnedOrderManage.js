﻿import { axios } from '@/utils/request'

/**
 * 查询产线退库管理
 *
 * @author yc
 */
export function ProductionReturnedOrderPage (parameter) {
  return axios({
    url: '/ProductionReturnedOrder/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 产线退库管理列表
 *
 * @author yc
 */
export function ProductionReturnedOrderList (parameter) {
  return axios({
    url: '/ProductionReturnedOrder/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加产线退库管理
 *
 * @author yc
 */
export function ProductionReturnedOrderAdd (parameter) {
  return axios({
    url: '/ProductionReturnedOrder/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑产线退库管理
 *
 * @author yc
 */
export function ProductionReturnedOrderEdit (parameter) {
  return axios({
    url: '/ProductionReturnedOrder/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除产线退库管理
 *
 * @author yc
 */
export function ProductionReturnedOrderDelete (parameter) {
  return axios({
    url: '/ProductionReturnedOrder/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出产线退库管理的Excel文件
 *
 * @author yc
 */
export function ProductionReturnedOrderToExcel (parameter) {
  return axios({
    url: '/ProductionReturnedOrder/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入产线退库管理的Excel文件
 *
 * @author yc
 */
export function ProductionReturnedOrderFromExcel (data, parameter) {
  return axios({
    url: '/ProductionReturnedOrder/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载产线退库管理的Excel导入模板
 *
 * @author yc
 */
export function ProductionReturnedOrderDownloadExcelTemplate(parameter) {
  return axios({
    url: '/ProductionReturnedOrder/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 单据大类下拉
 *
 * @author yhh
 */
export function ProductionReturnedOrderLargeCategories() {
  return axios({
    url: '/ProductionReturnedOrder/OrderTypeLargeCategory',
    method: 'get'
  })
}

/**
 * 单据小类下拉
 *
 * @author yhh
 */
export function ProductionReturnedOrderSubclass(id) {
  return axios({
    url: '/ProductionReturnedOrder/OrderTypeSubclass',
    method: 'get',
    params:{id}
  })
}


/**
 * 产线退库单详情
 *
 * @author yhh
 */
export function ProductionReturnedOrderDetailPage (parameter) {
  return axios({
    url: '/ProductionReturnedOrder/DetailPage',
    method: 'get',
    params: parameter
  })
}



