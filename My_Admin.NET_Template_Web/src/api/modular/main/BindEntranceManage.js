﻿import { axios } from '@/utils/request'

/**
 * 查询组盘入库
 *
 * @author liduanping
 */
export function BindEntrancePage (parameter) {
  return axios({
    url: '/BindEntrance/page',
    method: 'get',
    params: parameter
  })
}
 
/**
 * 组盘入库列表
 *
 * @author liduanping
 */
export function BindEntranceList (parameter) {
  return axios({
    url: '/BindEntrance/list',
    method: 'get',
    params: parameter
  })
}
 
/**
 * 手动入库
 *
 * @author yhh
 */
export function BindEntranceManualWare (parameter) {
  return axios({
    url: '/BindEntrance/ManualWare',
    method: 'post',
    data: parameter
  })
}
 
/**
 * 自动入库
 *
 * @author yhh
 */
export function BindEntranceAutoWare (parameter) {
  return axios({
    url: '/BindEntrance/AutoWare',
    method: 'post',
    data: parameter
  })
}
 
// 组盘 202311
export function AutoWare (parameter) {
  return axios({
    url: '/bindentrance/AutoWare',
    method: 'post',
    data: parameter
  })
}

// 自动入库 202311
export function automaticWarehousing (parameter) {
  return axios({
    url: '/bindentrance/automaticWarehousing',
    method: 'post',
    data: parameter
  })
}


// 人工入库 202311
export function manualWarehousing (parameter) {
  return axios({
    url: '/bindentrance/manualWarehousing',
    method: 'post',
    data: parameter
  })
}


// 物料查询 202311
export function MaterialRukuPage (parameter) {
  return axios({
    url: '/bindentrance/GetMaterial?MaterialNo='+parameter.MaterialNo,
    method: 'get',
    data: parameter
  })
}


/**
 * 获取托盘信息
 *
 * @author yhh
 */
export function BindEntranceGetContainer (parameter) {
  return axios({
    url: '/BindEntrance/GetContainer',
    method: 'get',
    params: parameter
  })
}
 
/**
 * 获取物料信息
 *
 * @author yhh
 */
export function BindEntranceGetMaterial (parameter) {
  return axios({
    url: '/BindEntrance/GetMaterial',
    method: 'get',
    params: parameter
  })
}

/**
 * 获取托盘信息-获取物料
 *
 * @author dla
 */
export function BindMixtureGetContainer (parameter) {
  return axios({    
    url: '/BindEntrance/getcontainer',
    method: 'get',
    params: parameter
  })
}
 
/**
 * 组盘接口
 *
 * @author yhh
 */
export function BindAction (parameter) {
  return axios({
    url: '/BindEntrance/BindEntrance',
    method: 'post',
    data: parameter
  })
}
 
/**
 * 移库
 *
 * @author yhh
 */
export function BindEntranceTransfer (parameter) {
  return axios({
    url: '/BindEntrance/StockTransfer',
    method: 'post',
    data: parameter
  })
}
 
/**
 * 获取目标仓库下拉列表
 *
 * @author yhh
 */
export function BindEntranceGetSelects () {
  return axios({
    url: '/BindEntrance/GetAreaList',
    method: 'get'
  })
}