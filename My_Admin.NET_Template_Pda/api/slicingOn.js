import request from '@/api/api.js'
import qs from 'qs'


// 获取托盘信息 /api/materialsorting/GetSortInfo
export function GetSortInfo (data) {
  return request({
    url: `/api/materialsorting/GetSortInfo?${qs.stringify(data)}`,
    method: 'get'
  })
}

// 分拣确认 /api/materialsorting/SortSure
export function SortSure (data) {
  return request({
    url: `/api/materialsorting/SortSure`,
    method: 'post',
	data
  })
}

// 出库单查询 /api/materialsorting/SortPdaPage
export function SortPdaPage (data) {
  return request({
    url: `/api/materialsorting/SortPdaPage?${qs.stringify(data)}`,
    method: 'get'
  })
}

// 查询出库单获得分拣单明细 /api/materialsorting/SortPdaDetailPage
export function SortPdaDetailPage(data) {
  return request({
    url: `/api/materialsorting/SortPdaDetailPage?${qs.stringify(data)}`,
    method: 'get'
  })
}