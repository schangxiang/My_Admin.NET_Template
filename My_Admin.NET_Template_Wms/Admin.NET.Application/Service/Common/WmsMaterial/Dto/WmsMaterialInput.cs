﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Admin.NET.Core;
using Admin.NET.Core.Service;

namespace Admin.NET.Application
{
    /// <summary>
    /// 物料信息查询参数
    /// </summary>
    public class WmsMaterialSearch : PageInputBase
    {
        /// <summary>
        /// 物料名称
        /// </summary>
        public virtual string MaterialName { get; set; }
        
        /// <summary>
        /// 物料编号
        /// </summary>
        public virtual string MaterialNo { get; set; }
        
        /// <summary>
        /// 物料批次
        /// </summary>
        public virtual string MaterialBatch { get; set; }
        
        /// <summary>
        /// 描述
        /// </summary>
        public virtual string Description { get; set; }
        
        /// <summary>
        /// 物料类别
        /// </summary>
        public virtual MaterialType? MaterialType { get; set; }
        
        /// <summary>
        /// 物料规格
        /// </summary>
        public virtual string MaterialSpec { get; set; }
        
        /// <summary>
        /// 物料密度
        /// </summary>
        public virtual string MaterialDensity { get; set; }


        /// <summary>
        /// 物料单位
        /// </summary>
        /// 
        public string MaterialUnit { get; set; }

        /// <summary>
        /// 库区参数
        /// </summary>
        public virtual string AreaNameParameter { get; set; }
        
        /// <summary>
        /// 工作区参数
        /// </summary>
        public virtual string StationNameParameter { get; set; }
        
        /// <summary>
        /// 熟化时间
        /// </summary>
        public virtual int? MaturationTime { get; set; }
        
        /// <summary>
        /// 是否需要熟化
        /// </summary>
        public virtual bool? IsMaturation { get; set; }


        //1217

        /// <summary>
        /// 检验方式;数据字典
        /// </summary>

        public MaterialInspection InspectionMethod { get; set; }


        /// <summary>
        /// 安全存量
        /// </summary>

        public decimal Safeqty { get; set; }

        /// <summary>
        /// 最大存量
        /// </summary>

        public decimal MaxImumqty { get; set; }

        /// <summary>
        /// 最小库龄
        /// </summary>

        public decimal MinstorageAge { get; set; }

        /// <summary>
        /// 最大库龄
        /// </summary>

        public decimal MaxstorageAge { get; set; }
      
    }

    /// <summary>
    /// 物料信息输入参数
    /// </summary>
    public class WmsMaterialInput
    {
        /// <summary>
        /// 物料名称
        /// </summary>
        public virtual string MaterialName { get; set; }
        
        /// <summary>
        /// 物料编号
        /// </summary>
        public virtual string MaterialNo { get; set; }
        
        /// <summary>
        /// 物料批次
        /// </summary>
        public virtual string MaterialBatch { get; set; }
        
        /// <summary>
        /// 描述
        /// </summary>
        public virtual string Description { get; set; }
        
        /// <summary>
        /// 物料类别
        /// </summary>
        public virtual MaterialType? MaterialType { get; set; }
        
        /// <summary>
        /// 物料规格
        /// </summary>
        public virtual string MaterialSpec { get; set; }
        
        /// <summary>
        /// 物料密度
        /// </summary>
        public virtual string MaterialDensity { get; set; }


        /// <summary>
        /// 物料单位
        /// </summary>
        /// 
        public string MaterialUnit { get; set; }


        /// <summary>
        /// 库区Ids
        /// </summary>

        public string AreaIds { get; set; }

        /// <summary>
        /// 库区参数
        /// </summary>
        public virtual string AreaNameParameter { get; set; }

        /// <summary>
        /// 工作区Ids
        /// </summary>

        public string StationIds { get; set; }

        /// <summary>
        /// 工作区参数
        /// </summary>
        public virtual string StationNameParameter { get; set; }
        
        /// <summary>
        /// 熟化时间
        /// </summary>
        public virtual int MaturationTime { get; set; }
        
        /// <summary>
        /// 是否需要熟化
        /// </summary>
        public virtual bool IsMaturation { get; set; }

        /// <summary>
        /// 工段
        /// </summary>
        public LesWorkshopSection? LesWorkshopSection { get; set; }

        //1217

        /// <summary>
        /// 检验方式;数据字典
        /// </summary>

        public MaterialInspection InspectionMethod { get; set; }
        /// <summary>
        /// 安全存量
        /// </summary>
        public decimal Safeqty { get; set; }

        /// <summary>
        /// 最大存量
        /// </summary>

        public decimal MaxImumqty { get; set; }

        /// <summary>
        /// 最小库龄
        /// </summary>

        public decimal MinstorageAge { get; set; }

        /// <summary>
        /// 最大库龄
        /// </summary>

        public decimal MaxstorageAge { get; set; }

    }

    /// <summary>
    /// 
    /// </summary>
    public class AddWmsMaterialInput : WmsMaterialInput
    {
    }

    /// <summary>
    /// 
    /// </summary>
    public class DeleteWmsMaterialInput : BaseId
    {
    }

    /// <summary>
    /// 
    /// </summary>
    public class UpdateWmsMaterialInput : WmsMaterialInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class QueryeWmsMaterialInput : BaseId
    {

    }
}
