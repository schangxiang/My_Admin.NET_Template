﻿import { axios } from '@/utils/request'

/**
 * 获取库区列表
 *
 * @author yhh
 */
export function GetArea () {
  return axios({
    url: '/Foaminglocationview/GetArea',
    method: 'get'
  })
}


/**
 * 根据库区获取巷道
 *
 * @author yhh
 */
export function GetAisle (parameter) {
  return axios({
    url: '/Foaminglocationview/GetPalceAisle',
    method: 'get',
    params: parameter
  })
}

/**
 * 根据库区获取排列表
 *
 * @author yhh
 */
export function GetPalceRowno (parameter) {
  return axios({
    url: '/Foaminglocationview/GetPalceRowno',
    method: 'get',
    params: parameter
  })
}

/**
 * 获取一个排中的库位列表
 *
 * @author yhh
 */
export function GetPalceList (parameter) {
  return axios({
    url: '/Foaminglocationview/GetPalceList',
    method: 'get',
    params: parameter
  })
}

/**
 * 获取库区详情
 *
 * @author yhh
 */
export function GetLocationDetail (parameter) {
  return axios({
    url: '/Foaminglocationview/GetMaterialDetail',
    method: 'get',
    params: parameter
  })
}

/**
 * 更新库位锁定状态
 *
 * @author yhh
 */
export function UpdateLock (parameter) {
  return axios({
    url: '/Foaminglocationview/UpdatePalceIslock',
    method: 'get',
    params: parameter
  })
}

/**
 * 更新库位空闲状态
 *
 * @author yhh
 */
export function UpdateEmpty (parameter) {
  return axios({
    url: '/Foaminglocationview/UpdatePalceKongXian',
    method: 'get',
    params: parameter
  })
}
