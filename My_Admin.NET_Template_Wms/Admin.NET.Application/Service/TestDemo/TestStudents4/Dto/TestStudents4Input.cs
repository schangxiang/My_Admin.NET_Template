﻿using Admin.NET.Core;
using Admin.NET.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace Admin.NET.Application
{    
    
    /// <summary>
    /// 学生信息表4查询参数
    /// </summary>
    public class TestStudents4Search : PageInputBase
    {
        /// <summary>
        /// 学生姓名
        /// </summary>
        public virtual string Name { get; set; }
        
        /// <summary>
        /// 学生年龄
        /// </summary>
        public virtual int? Age { get; set; }
        
        /// <summary>
        /// 零花钱
        /// </summary>
        public virtual decimal? HasMoney { get; set; }
        
        /// <summary>
        /// 是否在校
        /// </summary>
        public virtual bool? StartName { get; set; }
        
        /// <summary>
        /// 性别
        /// </summary>
        public virtual int? Gender { get; set; }
        
        /// <summary>
        /// 出生日期
        /// </summary>
        public virtual List<string> BrithDate { get; set; }
        
        /// <summary>
        /// 创建时间
        /// </summary>
        public virtual List<string> CreatedTime { get; set; }
        
        /// <summary>
        /// 更新时间
        /// </summary>
        public virtual List<string> UpdatedTime { get; set; }
        
        /// <summary>
        /// 创建者名称
        /// </summary>
        public virtual string CreatedUserName { get; set; }
        
        /// <summary>
        /// 修改者名称
        /// </summary>
        public virtual string UpdatedUserName { get; set; }
        
    }

        /// <summary>
    /// 学生信息表4不分页查询参数
    /// </summary>
    public class TestStudents4SearchNonPage : PageInputNonPageBase
    {
        /// <summary>
        /// 学生姓名
        /// </summary>
        public virtual string Name { get; set; }
        
        /// <summary>
        /// 学生年龄
        /// </summary>
        public virtual int? Age { get; set; }
        
        /// <summary>
        /// 零花钱
        /// </summary>
        public virtual decimal? HasMoney { get; set; }
        
        /// <summary>
        /// 是否在校
        /// </summary>
        public virtual bool? StartName { get; set; }
        
        /// <summary>
        /// 性别
        /// </summary>
        public virtual int? Gender { get; set; }
        
        /// <summary>
        /// 出生日期
        /// </summary>
         public virtual List<DateTime>? BrithDate { get; set; }
        
        /// <summary>
        /// 创建时间
        /// </summary>
         public virtual List<DateTimeOffset>? CreatedTime { get; set; }
        
        /// <summary>
        /// 更新时间
        /// </summary>
         public virtual List<DateTimeOffset>? UpdatedTime { get; set; }
        
        /// <summary>
        /// 创建者名称
        /// </summary>
        public virtual string CreatedUserName { get; set; }
        
        /// <summary>
        /// 修改者名称
        /// </summary>
        public virtual string UpdatedUserName { get; set; }
        
    }

    /// <summary>
    /// 学生信息表4输入参数
    /// </summary>
    public class TestStudents4Input
    {
        /// <summary>
        /// 学生姓名
        /// </summary>
        public virtual string Name { get; set; }
        
        /// <summary>
        /// 学生年龄
        /// </summary>
        public virtual int? Age { get; set; }
        
        /// <summary>
        /// 零花钱
        /// </summary>
        public virtual decimal? HasMoney { get; set; }
        
        /// <summary>
        /// 是否在校
        /// </summary>
        public virtual bool? StartName { get; set; }
        
        /// <summary>
        /// 性别
        /// </summary>
        public virtual int? Gender { get; set; }
        
        /// <summary>
        /// 出生日期
        /// </summary>
        public virtual DateTime? BrithDate { get; set; }
        
    }

    /// <summary>
    /// 学生信息表4新增参数
    /// </summary>
    public class AddTestStudents4Input : TestStudents4Input
    {
    }

    /// <summary>
    /// 学生信息表4删除参数
    /// </summary>
    public class DeleteTestStudents4Input : BaseId
    {
    }

    /// <summary>
    /// 学生信息表4更新参数
    /// </summary>
    public class UpdateTestStudents4Input : TestStudents4Input
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
        
    }

    /// <summary>
    /// 学生信息表4获取单个参数
    /// </summary>
    public class QueryeTestStudents4Input : BaseId
    {

    }
}
