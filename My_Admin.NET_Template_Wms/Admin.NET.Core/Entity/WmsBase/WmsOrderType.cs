﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    /// <summary>
    /// 单据类型维护表
    /// </summary>
    [Table("wms_order_type")]
    [Comment("单据类型维护表")]
    public class WmsOrderType : DEntityBase
    {
        /// <summary>
        /// 父Id
        /// </summary>
        [Comment("父Id")]
        public long Pid { get; set; }

        /// <summary>
        /// 父Ids
        /// </summary>
        [Comment("父Ids")]
        public string Pids { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Comment("名称")]
        [Required, MaxLength(100)]
        public string Name { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        [Comment("编码")]
        [Required, MaxLength(50)]
        public string Code { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        [Comment("排序")]
        public int Sort { get; set; }

        /// <summary>
        /// 所属车间
        /// </summary>
        [Comment("所属车间")]
        public LesWorkShopType LesWorkShopType { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Comment("备注")]
        [MaxLength(100)]
        public string Remark { get; set; }

        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        [Comment("状态")]
        public CommonStatus Status { get; set; } = CommonStatus.ENABLE;
    }
}
