﻿using Admin.NET.Core;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Admin.NET.Application
{
    public interface ITestTeacherService
    {
        Task Add(AddTestTeacherInput input);
        Task Delete(DeleteTestTeacherInput input);
        Task<TestTeacherOutput> Get([FromQuery] QueryeTestTeacherInput input);
        Task<List<TestTeacherOutput>> List([FromQuery] TestTeacherInput input);
        Task<PageResult<TestTeacherOutput>> Page([FromQuery] TestTeacherSearch input);
        Task Update(UpdateTestTeacherInput input);

        Task<List<TestTeacherOutput>> ListNonPageAsync([FromQuery] TestTeacherSearchNonPage input);

        Task<int> FromExcelAsync(IFormFile file, [FromQuery] ImportExcelType importExcelType);

        Task<IActionResult> DownloadExcelTemplate(string version);

        Task<IActionResult> ToExcelAsync([FromQuery] TestTeacherSearchNonPage input);
    }
}