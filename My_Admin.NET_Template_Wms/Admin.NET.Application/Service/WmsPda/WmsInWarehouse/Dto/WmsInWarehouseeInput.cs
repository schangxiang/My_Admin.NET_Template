﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 获取托盘入参
    /// </summary>
    public class GetContainerInput
    {
        /// <summary>
        /// 托盘号
        /// </summary>
        [Required(ErrorMessage = "托盘号不能为空")]
        public string ContainerCode { get; set; }
    }

    /// <summary>
    ///组盘入参
    /// </summary>
    public class GroupDiskInput
    {
        /// <summary>
        /// 托盘
        /// </summary>
        [Required(ErrorMessage = "托盘不能为空")]
        public String ContainerCode { get; set; }

        /// <summary>
        /// 物料信息
        /// </summary>
        public List<ReceiptMaterialContainerInput> WmsMaterials { get; set; }
    }

    /// <summary>
    /// 组盘物料信息
    /// </summary>
    public class ReceiptMaterialContainerInput
    {
        /// <summary>
        /// 明细id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string Materialname { get; set; }

        /// <summary>
        /// 物料编码
        /// </summary>
        public string Materialcode { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Qty { get; set; }
    }

    /// <summary>
    ///提交入库入参
    /// </summary>
    public class PdaInWarehouseInput
    {
        /// <summary>
        /// 托盘
        /// </summary>
        [Required(ErrorMessage = "托盘不能为空")]
        public String ContainerCode { get; set; }

        /// <summary>
        /// 物料信息
        /// </summary>
        public List<ReceiptMaterialContainerInput> WmsMaterials { get; set; }
    }

    /// <summary>
    ///获取物料入参
    /// </summary>
    public class PadGetMaterialInput : PageInputBase
    {
        /// <summary>
        /// 单据编号
        /// </summary>
        [Required(ErrorMessage = "单据编号不能为空")]
        public String NO { get; set; }
    }
}
