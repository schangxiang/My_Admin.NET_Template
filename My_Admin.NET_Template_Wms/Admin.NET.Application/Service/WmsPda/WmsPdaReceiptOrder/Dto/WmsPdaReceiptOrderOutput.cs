﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 单据号返回参数
    /// </summary>
    public class GetOrderDetailOutput
    {
        /// <summary>
        /// Id  
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 单据Id  
        /// </summary>
        public long OrderId { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        public string Materialname { get; set; }
        /// <summary>
        /// 物料编号
        /// </summary>
        public string Materialcode { get; set; }
        /// <summary>
        /// 基本单位    
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public decimal Qty { get; set; }

        /// <summary>
        /// 单根长度 
        /// </summary>
        public decimal SingleLength { get; set; }

        /// <summary>
        /// 理论重量    
        /// </summary>
        public decimal TheoreticalWeight { get; set; }

        /// <summary>
        /// 不含税单价    
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// 成本金额    
        /// </summary>
        public decimal SumPrice { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        public string ProjectCode { get; set; }

        /// <summary>
        /// 合同编号
        /// </summary>
        public string ContractCode { get; set; }

        /// <summary>
        /// TA号
        /// </summary>
        public string TACode { get; set; }

        /// <summary>
        /// 报关品名
        /// </summary>
        public string TradeName { get; set; }

        /// <summary>
        /// 报关项号
        /// </summary>
        public string ItemNo { get; set; }

        /// <summary>
        /// 报关单位
        /// </summary>
        public string CustomsUnitName { get; set; }

        /// <summary>
        /// 报关数量   
        /// </summary>
        public decimal CustomsNum { get; set; }

        /// <summary>
        /// 报关金额 
        /// </summary>
        public decimal CustomsPrices { get; set; }

        /// <summary>
        /// 特殊要求
        /// </summary>
        public string SpecialNeeds { get; set; }

        /// <summary>
        /// 单据状态
        /// </summary>
        public OrderDetailsStatusEnum OrderStatus { get; set; }

        /// <summary>
        /// 送货数量
        /// </summary>
        public decimal DeliveryQty { get; set; }

        /// <summary>
        /// 收货数量
        /// </summary>
        public decimal ReceivedQty { get; set; }
    }
}
