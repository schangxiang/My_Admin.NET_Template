﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    /// <summary>
    /// 设备报警
    /// </summary>
    [Table("les_devicewaring")]
    [Comment("设备报警")]
    public class LesDeviceWaring : DEntityBase
    {
        /// <summary>
        /// WcsId
        /// </summary>
        [Comment("WcsId")]
        public int WcsId { get; set; }

        /// <summary>
        /// 设备名称
        /// </summary>
        [Comment("设备名称")]
        [MaxLength(50)]
        public string DeviceName { get; set; }

        /// <summary>
        /// 故障名称
        /// </summary>
        [Comment("故障名称")]
        [MaxLength(50)]
        public string FaultName { get; set; }

        /// <summary>
        /// 发生时间
        /// </summary>
        [Comment("发生时间")]
        public DateTimeOffset? StartTime  { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        [Comment("结束时间")]
        public DateTimeOffset? EndTime { get; set; }

        /// <summary>
        /// 持续时间
        /// </summary>
        [Comment("持续时间")]
        public int RunningTime { get; set; }
    }
}
