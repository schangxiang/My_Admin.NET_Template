﻿using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 查询所有WCS可以读取的任务输出参数
    /// </summary>
    public class GetAssembleTaskOutput
    {
        /// <summary>
        /// 任务号
        /// </summary>
        public string TaskNo { get; set; }
        /// <summary>
        /// 任务类型
        /// </summary>
        public TaskType TaskType { get; set; }
        /// <summary>
        /// 任务优先级别
        /// </summary>
        public int TaskLevel { get; set; }
        /// <summary>
        /// 巷道/提升机
        /// </summary>
        public int Aisle { get; set; }
        /// <summary>
        /// 托盘号
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 任务状态;数据字典
        /// </summary>
        public TaskStatusEnum TaskStatus { get; set; }
    }
    /// <summary>
    /// 查询所有WCS可以读取的agv报警信息
    /// </summary>
    public class GetAgvWaring
    {
        /// <summary>
        /// 巷道/提升机
        /// </summary>
        public int Aisle { get; set; }
    }
    
}
