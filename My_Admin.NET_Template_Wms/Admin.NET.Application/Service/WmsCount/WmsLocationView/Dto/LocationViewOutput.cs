﻿using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 
    /// </summary>
    public class PalceInfoOtput
    {
        /// <summary>
        /// 总库位数
        /// </summary>
        public int countNum { get; set; }

        /// <summary>
        /// 总存货数
        /// </summary>
        public int cunhuoNum { get; set; }

        /// <summary>
        /// 空库位数
        /// </summary>
        public int emptyNum { get; set; }

        /// <summary>
        /// 空托盘数
        /// </summary>
        public int emptyContainerNum { get; set; }

        /// <summary>
        /// 物料数量
        /// </summary>
        public int materialNum { get; set; }

        /// <summary>
        /// 锁定库位数
        /// </summary>
        public int lockNum { get; set; }

        /// <summary>
        /// 待入库位数
        /// </summary>
        public int DaiRuNum { get; set; }

        /// <summary>
        /// 待出库位数
        /// </summary>
        public int DaichuNum { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<WareLocationAisleData> PalceDetails { get; set; }
    }
    /// <summary>
    /// 巷
    /// </summary>
    public class WareLocationAisleData
    {
        /// <summary>
        /// 巷
        /// </summary>
        public int Aisle { get; set; }

        /// <summary>
        /// 巷属性数据
        /// </summary>
        public List<WareLocationRownoData> wareLocationRownoData { get; set; }

    }

    /// <summary>
    /// 排
    /// </summary>
    public class WareLocationRownoData
    {
        /// <summary>
        /// 排
        /// </summary>
        public int Rowno { get; set; }

        /// <summary>
        /// 排属性数据
        /// </summary>
        public List<WareLocationLayerData> wareLocationLayerData { get; set; }
    }
    /// <summary>
    /// 层
    /// </summary>
    public class WareLocationLayerData
    {
        /// <summary>
        /// 层
        /// </summary>
        public int Layer { get; set; }

        /// <summary>
        /// 层属性数据
        /// </summary>
        public List<PalceDetail> wareLocationColumnNoData { get; set; }
    }

    ///// <summary>
    ///// 列
    ///// </summary>
    //public class WareLocationColumnNoData
    //{
    //    /// <summary>
    //    /// 列
    //    /// </summary>
    //    public int ColumnNo { get; set; }

    //    /// <summary>
    //    /// 列属性数据
    //    /// </summary>
    //    public List<PalceDetail> PalceDetail { get; set; }
    //}

    /// <summary>
    /// 
    /// </summary>
    public class MaterialDetailsOutput
    {
        /// <summary>
        /// 托盘编码
        /// </summary>
        public string Containercode { get; set; }

        /// <summary>
        /// 库位对象
        /// </summary>
        public PalceDetail PalceDetails { get; set; }

        /// <summary>
        /// 物料信息
        /// </summary>
        public List<WmsMaterialStock> WmsMaterialStocks { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class PalceDetail
    {
        /// <summary>
        /// 库位编号
        /// </summary>
        public string PlaceCode { get; set; }

        /// <summary>
        /// 容器编号
        /// </summary>
        public string ContainerCode { get; set; }

        /// <summary>
        /// 库位状态
        /// </summary>
        public PlaceStatus PlaceStatus { get; set; }

        /// <summary>
        /// 是否锁定
        /// </summary>
        public YesOrNot Islock { get; set; } = YesOrNot.N;

        /// <summary>
        /// 是否空托盘
        /// </summary>
        public YesOrNot EmptyContainer { get; set; } = YesOrNot.N;


        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 排
        /// </summary>
        public int RowNo { get; set; }

        /// <summary>
        /// 列
        /// </summary>
        public int ColumnNo { get; set; }

        /// <summary>
        /// 层
        /// </summary>
        public int LayerNo { get; set; }
    }
}
