import { axios } from '@/utils/request'

/**
 * 查询存货库位
 *
 * @author yhh
 */
export function GetCunHouPlace (parameter) {
  return axios({
    url: '/Relocation/GetCunHouPlace',
    method: 'get',
    params: parameter
  })
}

/**
 * 查询空闲库位
 *
 * @author yhh
 */
export function GetKongXianPlace (parameter) {
  return axios({
    url: '/Relocation/GetKongXianPlace',
    method: 'get',
    params: parameter
  })
}

/**
 * 自动倒库
 *
 * @author yhh
 */
export function AutoSubmit (data) {
  return axios({
    url: '/Relocation/ZdSubmitPlace',
    method: 'post',
    data: data
  })
}

/**
 * 手动倒库
 *
 * @author yhh
 */
export function ManualSubmit (data) {
  return axios({
    url: '/Relocation/SdSubmitPlace',
    method: 'post',
    data: data
  })
}

/**
 * 查询库位详情
 *
 * @author yhh
 */
export function GetLocationDetail (parameter) {
  return axios({
    url: '/Relocation/GetPlaceDetail',
    method: 'get',
    params: parameter
  })
}