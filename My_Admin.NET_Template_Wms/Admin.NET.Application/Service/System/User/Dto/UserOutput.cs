﻿using Microsoft.EntityFrameworkCore;
using NetTopologySuite.Index.Strtree;
using System.ComponentModel.DataAnnotations;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 用户参数
    /// </summary>
    public class UserOutput
    {
        /// <summary>
        /// Id
        /// </summary>
        public virtual string Id { get; set; }

        /// <summary>
        /// 账号
        /// </summary>
        public virtual string Account { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        public virtual string NickName { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public virtual string Avatar { get; set; }

        /// <summary>
        /// 生日
        /// </summary>
        public virtual DateTimeOffset? Birthday { get; set; }

        /// <summary>
        /// 性别-男_1、女_2
        /// </summary>
        public virtual int Sex { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        public virtual string Email { get; set; }

        /// <summary>
        /// 手机
        /// </summary>
        public virtual string Phone { get; set; }

        /// <summary>
        /// 电话
        /// </summary>
        public virtual string Tel { get; set; }

        /// <summary>
        /// 状态-正常_0、停用_1、删除_2
        /// </summary>
        public virtual int Status { get; set; }

        /// <summary>
        /// 员工信息
        /// </summary>
        public EmpOutput SysEmpInfo { get; set; }

        /// <summary>
        /// 所属车间
        /// </summary>
        public LesWorkShopType WorkShopType { get; set; }

        /// <summary>
        /// 所属车间产线
        /// </summary>
        public long ProductionlineId { get; set; }
    }

    /// <summary>
    /// 产线参数
    /// </summary>
    public class LesWorkShopOutput
    {
        /// <summary>
        /// 产线Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 所属车间
        /// </summary>
        public LesWorkShopType LesWorkShopType { get; set; }

        /// <summary>
        /// 产线名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 产线编码
        /// </summary>
        public string Code { get; set; }
    }
}