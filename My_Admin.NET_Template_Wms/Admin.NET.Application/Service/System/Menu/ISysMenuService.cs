﻿using Microsoft.AspNetCore.Mvc;

namespace Admin.NET.Application
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISysMenuService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task AddMenu(AddMenuInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<List<AntDesignTreeNode>> ChangeAppMenu(ChangeAppMenuInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task DeleteMenu(DeleteMenuInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="appCode"></param>
        /// <returns></returns>
        Task<List<AntDesignTreeNode>> GetLoginMenusAntDesign(long userId, string appCode);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<List<string>> GetLoginPermissionList(long userId);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<List<string>> GetAllPermissionList();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<dynamic> GetMenu(QueryMenuInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<dynamic> GetMenuList([FromQuery] GetMenuListInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<dynamic> GetMenuTree([FromQuery] GetMenuTreeInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<List<string>> GetUserMenuAppCodeList(long userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appCode"></param>
        /// <returns></returns>
        Task<bool> HasMenu(string appCode);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<dynamic> TreeForGrant([FromQuery] TreeForGrantInput input);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task UpdateMenu(UpdateMenuInput input);
    }
}