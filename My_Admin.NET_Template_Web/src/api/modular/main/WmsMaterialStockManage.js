﻿import { axios } from '@/utils/request'

/**
 * 查询库存表
 *
 * @author XJF
 */
export function WmsMaterialStockPage (parameter) {
  return axios({
    url: '/WmsMaterialStock/page',
    method: 'get',
    params: parameter
  })
}


/**
* 获取所在库区列表
* @author XJF
*/
export function WmsMaterialStockFkWmsAreaList() {
  return axios({
    url: '/WmsMaterialStock/WmsArea',
    method: 'get'
  })
}
