﻿using Admin.NET.Core;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Admin.NET.Application
{
    /// <summary>
    /// 
    /// </summary>
    public interface IWmsContainerService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task Add(AddWmsContainerInput input);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task Delete(DeleteWmsContainerInput input);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<WmsContainerOutput> Get([FromQuery] QueryeWmsContainerInput input);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<List<WmsContainerOutput>> List([FromQuery] WmsContainerInput input);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PageResult<WmsContainerOutput>> Page([FromQuery] WmsContainerSearch input);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task Update(UpdateWmsContainerInput input);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>

        Task<List<WmsContainerOutput>> ListNonPageAsync([FromQuery] WmsContainerSearchNonPage input);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="importExcelType"></param>
        /// <returns></returns>

        Task<int> FromExcelAsync(IFormFile file, [FromQuery] ImportExcelType importExcelType);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>

        Task<IActionResult> DownloadExcelTemplate(string version);
    }
}