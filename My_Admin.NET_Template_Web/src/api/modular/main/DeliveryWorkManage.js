﻿import { axios } from '@/utils/request'

/**
 * 查询收货作业
 *
 * @author yc
 */
export function DeliveryWorkPage (parameter) {
  return axios({
    url: '/DeliveryWork/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 收货作业列表
 *
 * @author yc
 */
export function DeliveryWorkList (parameter) {
  return axios({
    url: '/DeliveryWork/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加收货作业
 *
 * @author yc
 */
export function DeliveryWorkAdd (parameter) {
  return axios({
    url: '/DeliveryWork/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑收货作业
 *
 * @author yc
 */
export function DeliveryWorkEdit (parameter) {
  return axios({
    url: '/DeliveryWork/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除收货作业
 *
 * @author yc
 */
export function DeliveryWorkDelete (parameter) {
  return axios({
    url: '/DeliveryWork/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出收货作业的Excel文件
 *
 * @author yc
 */
export function DeliveryWorkToExcel (parameter) {
  return axios({
    url: '/DeliveryWork/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入收货作业的Excel文件
 *
 * @author yc
 */
export function DeliveryWorkFromExcel (data, parameter) {
  return axios({
    url: '/DeliveryWork/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载收货作业的Excel导入模板
 *
 * @author yc
 */
export function DeliveryWorkDownloadExcelTemplate(parameter) {
  return axios({
    url: '/DeliveryWork/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}


/**
 * 单据详情
 *
 * @author yhh
 */
export function DeliveryWorkDetailPage (parameter) {
  return axios({
    url: '/DeliveryWork/DetailsPage',
    method: 'get',
    params: parameter
  })
}


