﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

/*
 * @author : 您的名字
 * @date : 2024/4/15下午4:23:55
 * @desc : 测试老师表
 */
namespace Admin.NET.Core
{
    /// <summary>
    /// 测试老师表
    /// </summary>
    [Table("test_teacher")]
    [Comment("测试老师表")]
    public class TestTeacher : DEntityBase
    {


        /// <summary>
        /// 名称
        /// </summary>
        [Comment("名称")]
        [Required]
        [MaxLength(255)]
        public string Name { get; set; }









    }
}