﻿using Admin.NET.Core.Util.LowCode.Dto;
using System;

namespace Admin.NET.Application
{
    /// <summary>
    /// PDA菜单输出参数
    /// </summary>
    public class WmsPdaPowerDto
    {
        /// <summary>
        /// 图标
        /// </summary>
        public string Icon { get; set; }
        
        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }
        
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// 颜色
        /// </summary>
        public string Clolor { get; set; }
        
        /// <summary>
        /// 所属车间
        /// </summary>
        public Admin.NET.Core.LesWorkShopType WorkShopType { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public Admin.NET.Core.IssueState IssueState { get; set; }
        
    }
}
