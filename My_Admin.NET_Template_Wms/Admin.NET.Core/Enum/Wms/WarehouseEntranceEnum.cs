﻿using System.ComponentModel;

namespace Admin.NET.Core
{
    /// <summary>
    /// 库口类型枚举
    /// </summary>
    public enum WarehouseEntranceEnum
    {
        /// <summary>
        /// 入库口
        /// </summary>
        [Description("入库口")]
        RUKUKOU = 1,

        /// <summary>
        /// 出库口
        /// </summary>
        [Description("出库口")]
        CHUKUKOU = 2,
    }
}
