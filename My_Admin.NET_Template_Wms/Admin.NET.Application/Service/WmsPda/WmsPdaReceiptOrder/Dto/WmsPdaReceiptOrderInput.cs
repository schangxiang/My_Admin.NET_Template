﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    /// <summary>
    /// 单据号参数
    /// </summary>
    public class GetOrderDetailInput
    {
        /// <summary>
        /// 单据编号  
        /// </summary>
        public string NO { get; set; }
    }
    /// <summary>
    /// 确认收货参数
    /// </summary>
    public class ConfirmReceiptInput
    {

        /// <summary>
        /// 单据编号  
        /// </summary>
        public string NO { get; set; }

        /// <summary>
        /// 详情参数  
        /// </summary>
        public List<GetOrderDetailOutput> GetOrderDetailOutputList { get; set; }
    }
    
}
