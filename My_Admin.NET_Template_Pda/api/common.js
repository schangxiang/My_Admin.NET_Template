
import request from '@/api/api.js'
import qs from 'qs'


// 获取枚举信息 /api/sysEnumData/list
export function getSysEnumData (data) {
  return request({
    url: `/api/sysEnumData/list?${qs.stringify(data)}`,
    method: 'get'
  })
}