﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    /// <summary>
    /// 库口表
    /// </summary>
    [Table("les_entrance")]
    [Comment("库口表")]
    public class LesEntrance : DEntityBase
    {
        /// <summary>
        /// 库区Id
        /// </summary>
        [Comment("库区Id")]
        public long AreaId { get; set; }

        /// <summary>
        /// 库口名称
        /// </summary>
        [Comment("库口名称")]
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// 库口编码
        /// </summary>
        [Comment("库口编码")]
        [MaxLength(50)]
        public string Code { get; set; }

        /// <summary>
        /// 库口类型
        /// </summary>
        [Comment("库口类型")]
        public LesEntranceType EntranceType { get; set; }

        /// <summary>
        /// 库口状态
        /// </summary>
        [Comment("库口状态")]
        public CommonStatus CommenStatus { get; set; }

        /// <summary>
        /// 库区表
        /// </summary>
        public WmsArea WmsArea { get; set; }
    }
}
