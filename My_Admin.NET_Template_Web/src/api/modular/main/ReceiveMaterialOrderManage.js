﻿import { axios } from '@/utils/request'

/**
 * 查询领料单
 *
 * @author yc
 */
export function ReceiveMaterialOrderPage (parameter) {
  return axios({
    url: '/ReceiveMaterialOrder/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 领料单列表
 *
 * @author yc
 */
export function ReceiveMaterialOrderList (parameter) {
  return axios({
    url: '/ReceiveMaterialOrder/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加领料单
 *
 * @author yc
 */
export function ReceiveMaterialOrderAdd (parameter) {
  return axios({
    url: '/ReceiveMaterialOrder/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑领料单
 *
 * @author yc
 */
export function ReceiveMaterialOrderEdit (parameter) {
  return axios({
    url: '/ReceiveMaterialOrder/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除领料单
 *
 * @author yc
 */
export function ReceiveMaterialOrderDelete (parameter) {
  return axios({
    url: '/ReceiveMaterialOrder/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出领料单的Excel文件
 *
 * @author yc
 */
export function ReceiveMaterialOrderToExcel (parameter) {
  return axios({
    url: '/ReceiveMaterialOrder/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入领料单的Excel文件
 *
 * @author yc
 */
export function ReceiveMaterialOrderFromExcel (data, parameter) {
  return axios({
    url: '/ReceiveMaterialOrder/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载领料单的Excel导入模板
 *
 * @author yc
 */
export function ReceiveMaterialOrderDownloadExcelTemplate(parameter) {
  return axios({
    url: '/ReceiveMaterialOrder/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 领料单详情
 *
 * @author yhh
 */
export function ReceiveMaterialOrderDetailPage (parameter) {
  return axios({
    url: '/ReceiveMaterialOrder/DetailsPage',
    method: 'get',
    params: parameter
  })
}




