﻿using Mapster;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    public class TestStudent5Mapper : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<AddTestStudent5Input, TestStudent5>()
            ;
            config.ForType<UpdateTestStudent5Input, TestStudent5>()
            ;
            config.ForType<TestStudent5, TestStudent5Output>()
            ;
        }
    }
}
