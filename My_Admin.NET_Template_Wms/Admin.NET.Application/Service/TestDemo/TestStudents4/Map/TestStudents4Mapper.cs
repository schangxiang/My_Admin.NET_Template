﻿using Mapster;
using Admin.NET.Core;

namespace Admin.NET.Application
{
    public class TestStudents4Mapper : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<AddTestStudents4Input, TestStudents4>()
            ;
            config.ForType<UpdateTestStudents4Input, TestStudents4>()
            ;
            config.ForType<TestStudents4, TestStudents4Output>()
            ;
        }
    }
}
