﻿import { axios } from '@/utils/request'

/**
 * 查询测试学生表
 *
 * @author shaocx
 */
export function TestStudent5Page (parameter) {
  return axios({
    url: '/TestStudent5/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 测试学生表列表
 *
 * @author shaocx
 */
export function TestStudent5List (parameter) {
  return axios({
    url: '/TestStudent5/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加测试学生表
 *
 * @author shaocx
 */
export function TestStudent5Add (parameter) {
  return axios({
    url: '/TestStudent5/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑测试学生表
 *
 * @author shaocx
 */
export function TestStudent5Edit (parameter) {
  return axios({
    url: '/TestStudent5/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除测试学生表
 *
 * @author shaocx
 */
export function TestStudent5Delete (parameter) {
  return axios({
    url: '/TestStudent5/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出测试学生表的Excel文件
 *
 * @author shaocx
 */
export function TestStudent5ToExcel (parameter) {
  return axios({
    url: '/TestStudent5/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入测试学生表的Excel文件
 *
 * @author shaocx
 */
export function TestStudent5FromExcel (data, parameter) {
  return axios({
    url: '/TestStudent5/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载测试学生表的Excel导入模板
 *
 * @author shaocx
 */
export function TestStudent5DownloadExcelTemplate(parameter) {
  return axios({
    url: '/TestStudent5/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}



