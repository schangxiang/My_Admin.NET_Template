﻿using Admin.NET.Core.Util.LowCode.Dto;
using System;

namespace Admin.NET.Application
{
    /// <summary>
    /// 分拣单输出参数
    /// </summary>
    public class WmsSortOrderDto
    {
        /// <summary>
        /// 来源单号
        /// </summary>
        public string OrderNo { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public long OrderDetailID { get; set; }
        
        /// <summary>
        /// 分拣组盘单号
        /// </summary>
        public string ContainerOrderNo { get; set; }
        
        /// <summary>
        /// 项目编号
        /// </summary>
        public string ProjectCode { get; set; }
        
        /// <summary>
        /// TA号
        /// </summary>
        public string TACode { get; set; }
        
        /// <summary>
        /// 分段号
        /// </summary>
        public string PartCode { get; set; }
        
        /// <summary>
        /// 库位编码
        /// </summary>
        public string PlaceCode { get; set; }
        
        /// <summary>
        /// 容器编号
        /// </summary>
        public string ContainerCode { get; set; }
        
        /// <summary>
        /// 分拣数
        /// </summary>
        public decimal SortQuantity { get; set; }
        
        /// <summary>
        /// 实际分拣数
        /// </summary>
        public decimal ActualQuantity { get; set; }
        
        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks { get; set; }
        
        /// <summary>
        /// 状态-未分拣_1、分拣完成_2
        /// </summary>
        public Admin.NET.Core.SortStatusEnum SortStatus { get; set; }
        
        /// <summary>
        /// Id主键
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// 签核状态
        /// </summary>
        public Admin.NET.Core.IssueState IssueState { get; set; }
        
    }
}
