﻿using Admin.NET.Core;
using Admin.NET.Core.Service;
using System.ComponentModel.DataAnnotations;

namespace Admin.NET.Application
{    
    
    /// <summary>
    /// 学生信息表1-邵长祥查询参数
    /// </summary>
    public class TestStudentSearch : PageInputBase
    {
        /// <summary>
        /// 学生姓名
        /// </summary>
        public virtual string Name { get; set; }
        
        /// <summary>
        /// 学生年龄
        /// </summary>
        public virtual int? Age { get; set; }
        
        /// <summary>
        /// 是否在校
        /// </summary>
        public virtual bool? StartName { get; set; }
        
        /// <summary>
        /// 性别
        /// </summary>
        public virtual Admin.NET.Core.GenderChinese? Gender { get; set; }
        
        /// <summary>
        /// 出生日期
        /// </summary>
        public virtual List<string> BrithDate { get; set; }
        
        /// <summary>
        /// 关联老师
        /// </summary>
        public virtual long? TeacherId { get; set; }
        
        /// <summary>
        /// 创建时间
        /// </summary>
        public virtual List<string> CreatedTime { get; set; }
        
        /// <summary>
        /// 更新时间
        /// </summary>
        public virtual List<string> UpdatedTime { get; set; }
        
        /// <summary>
        /// 创建者名称
        /// </summary>
        public virtual string CreatedUserName { get; set; }
        
        /// <summary>
        /// 修改者名称
        /// </summary>
        public virtual string UpdatedUserName { get; set; }
        
    }

        /// <summary>
    /// 学生信息表1-邵长祥不分页查询参数
    /// </summary>
    public class TestStudentSearchNonPage : PageInputNonPageBase
    {
        /// <summary>
        /// 学生姓名
        /// </summary>
        public virtual string Name { get; set; }
        
        /// <summary>
        /// 学生年龄
        /// </summary>
        public virtual int? Age { get; set; }
        
        /// <summary>
        /// 是否在校
        /// </summary>
        public virtual bool? StartName { get; set; }
        
        /// <summary>
        /// 性别
        /// </summary>
        public virtual Admin.NET.Core.GenderChinese? Gender { get; set; }
        
        /// <summary>
        /// 出生日期
        /// </summary>
        public virtual List<string> BrithDate { get; set; }
        
        /// <summary>
        /// 关联老师
        /// </summary>
        public virtual long? TeacherId { get; set; }
        
        /// <summary>
        /// 创建时间
        /// </summary>
        public virtual List<string> CreatedTime { get; set; }
        
        /// <summary>
        /// 更新时间
        /// </summary>
        public virtual List<string> UpdatedTime { get; set; }
        
        /// <summary>
        /// 创建者名称
        /// </summary>
        public virtual string CreatedUserName { get; set; }
        
        /// <summary>
        /// 修改者名称
        /// </summary>
        public virtual string UpdatedUserName { get; set; }
        
    }

    /// <summary>
    /// 学生信息表1-邵长祥输入参数
    /// </summary>
    public class TestStudentInput
    {
        /// <summary>
        /// 学生姓名
        /// </summary>
        [Required(ErrorMessage = "学生姓名不能为空")]
        public virtual string Name { get; set; }
        
        /// <summary>
        /// 学生年龄
        /// </summary>
        [Required(ErrorMessage = "学生年龄不能为空")]
        public virtual int Age { get; set; }
        
        /// <summary>
        /// 是否在校
        /// </summary>
        [Required(ErrorMessage = "是否在校不能为空")]
        public virtual bool StartName { get; set; }
        
        /// <summary>
        /// 性别
        /// </summary>
        [Required(ErrorMessage = "性别不能为空")]
        public virtual Admin.NET.Core.GenderChinese Gender { get; set; }
        
        /// <summary>
        /// 出生日期
        /// </summary>
        [Required(ErrorMessage = "出生日期不能为空")]
        public virtual DateTime BrithDate { get; set; }
        
        /// <summary>
        /// 关联老师
        /// </summary>
        public virtual long? TeacherId { get; set; }
        
    }

    /// <summary>
    /// 学生信息表1-邵长祥新增参数
    /// </summary>
    public class AddTestStudentInput : TestStudentInput
    {
    }

    /// <summary>
    /// 学生信息表1-邵长祥删除参数
    /// </summary>
    public class DeleteTestStudentInput : BaseId
    {
    }

    /// <summary>
    /// 学生信息表1-邵长祥更新参数
    /// </summary>
    public class UpdateTestStudentInput : TestStudentInput
    {
        /// <summary>
        /// Id主键
        /// </summary>
        [Required(ErrorMessage = "Id主键不能为空")]
        public long Id { get; set; }
        
    }

    /// <summary>
    /// 学生信息表1-邵长祥获取单个参数
    /// </summary>
    public class QueryeTestStudentInput : BaseId
    {

    }
}
