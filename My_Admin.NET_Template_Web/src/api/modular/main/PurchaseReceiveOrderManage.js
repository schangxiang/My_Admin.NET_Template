﻿import { axios } from '@/utils/request'

/**
 * 查询采购收货
 *
 * @author yc
 */
export function PurchaseReceiveOrderPage (parameter) {
  return axios({
    url: '/PurchaseReceiveOrder/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 采购收货列表
 *
 * @author yc
 */
export function PurchaseReceiveOrderList (parameter) {
  return axios({
    url: '/PurchaseReceiveOrder/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加采购收货
 *
 * @author yc
 */
export function PurchaseReceiveOrderAdd (parameter) {
  return axios({
    url: '/PurchaseReceiveOrder/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑采购收货
 *
 * @author yc
 */
export function PurchaseReceiveOrderEdit (parameter) {
  return axios({
    url: '/PurchaseReceiveOrder/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除采购收货
 *
 * @author yc
 */
export function PurchaseReceiveOrderDelete (parameter) {
  return axios({
    url: '/PurchaseReceiveOrder/delete',
    method: 'post',
    data: parameter
  })
}


/**
 * 导出采购收货的Excel文件
 *
 * @author yc
 */
export function PurchaseReceiveOrderToExcel (parameter) {
  return axios({
    url: '/PurchaseReceiveOrder/toExcel',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 导入采购收货的Excel文件
 *
 * @author yc
 */
export function PurchaseReceiveOrderFromExcel (data, parameter) {
  return axios({
    url: '/PurchaseReceiveOrder/fromExcel',
    method: 'post',
    data: data,
    params: parameter

  })
}


/**
 * 下载采购收货的Excel导入模板
 *
 * @author yc
 */
export function PurchaseReceiveOrderDownloadExcelTemplate(parameter) {
  return axios({
    url: '/PurchaseReceiveOrder/downloadExcelTemplate',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}

/**
 * 单据大类下拉
 *
 * @author yhh
 */
export function PurchaseReceiveOrderLargeCategories() {
  return axios({
    url: '/PurchaseReceiveOrder/OrderTypeLargeCategory',
    method: 'get'
  })
}

/**
 * 单据小类下拉
 *
 * @author yhh
 */
export function PurchaseReceiveOrderSubclass(id) {
  return axios({
    url: '/PurchaseReceiveOrder/OrderTypeSubclass',
    method: 'get',
    params:{id}
  })
}


/**
 * 销售退库单详情
 *
 * @author yhh
 */
export function PurchaseReceiveOrderDetailPage (parameter) {
  return axios({
    url: '/PurchaseReceiveOrder/DetailsPage',
    method: 'get',
    params: parameter
  })
}

/**
 * 获取采购收购单号
 *
 * @author yhh
 */
export function GetBillNumber () {
  return axios({
    url: '/PurchaseReceiveOrder/GetPurchaseReceiveOrderNo',
    method: 'get'
  })
}

/**
 * 获取未绑定收货单的采购单列表
 *
 * @author yhh
 */
export function GetPurchaseOrders() {
  return axios({
    url: '/PurchaseReceiveOrder/GetPurchaseOrderNo',
    method: 'get'
  })
}

/**
 * 获取可收获的物料列表
 *
 * @author yhh
 */
export function GetCouldReceiveMaterials(params) {
  return axios({
    url: '/PurchaseReceiveOrder/GetDetailByPurchaseOrderNo',
    method: 'get',
    params: params
  })
}

/**
 * 编辑采购收货详情收货数
 *
 * @author yhh
 */
export function PurchaseReceiveOrderEditDetailNumber (parameter) {
  return axios({
    url: '/PurchaseReceiveOrder/EditDetail',
    method: 'post',
    data: parameter
  })
}





