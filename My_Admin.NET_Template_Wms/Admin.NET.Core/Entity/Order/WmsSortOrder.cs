﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    /// <summary>
    /// 分拣表
    /// </summary>
    [Comment("分拣表")]
    [Table("wms_sortorder")]
    public class WmsSortOrder : DEntityBase
    {
        /// <summary>
        /// 来源单号    
        /// </summary>
        [Comment("来源单号")]
        [MaxLength(50)]
        public string OrderNo { get; set; }

        /// <summary>
        /// 出库单据明细Id
        /// </summary>
        public long OrderDetailID { get; set; }

        /// <summary>
        /// 物料编号
        /// </summary>
        [Comment("物料编号")]
        [MaxLength(255)]
        public string Materialcode { get; set; }

        /// <summary>
        /// 物料名称
        /// </summary>
        [Comment("物料名称")]
        [MaxLength(255)]
        public string Materialname { get; set; }

        /// <summary>
        /// 分拣组盘单号    
        /// </summary>
        [Comment("分拣组盘单号")]
        [MaxLength(50)]
        public string ContainerOrderNo { get; set; }

        /// <summary>
        /// 项目编号    
        /// </summary>
        [Comment("项目编号")]
        [MaxLength(255)]
        public string ProjectCode { get; set; }

        /// <summary>
        /// TA号
        /// </summary>
        [Comment("TA号")]
        [MaxLength(255)]
        public string TACode { get; set; }

        /// <summary>
        /// 分段号
        /// </summary>
        [Comment("分段号")]
        [MaxLength(255)]
        public string PartCode { get; set; }

        /// <summary>
        /// 库位编码
        /// </summary>
        [Comment("库位编码")]
        [MaxLength(20)]
        public string PlaceCode { get; set; }

        /// <summary>
        /// 容器编号    
        /// </summary>
        [Comment("容器编号")]
        [MaxLength(50)]
        public string ContainerCode { get; set; }

        /// <summary>
        /// 分拣数    
        /// </summary>
        [Comment("分拣数")]
        public decimal SortQuantity { get; set; }

        /// <summary>
        /// 实际分拣数    
        /// </summary>
        [Comment("实际分拣数")]
        public decimal ActualQuantity { get; set; }

        /// <summary>
        /// 备注    
        /// </summary>
        [Comment("备注")]
        [MaxLength(500)]
        public string Remarks { get; set; }

        /// <summary>
        /// 状态-未分拣_1、分拣完成_2
        /// </summary>
        [Comment("状态-未分拣_1、分拣完成_2")]
        public SortStatusEnum SortStatus { get; set; }
    }
}
