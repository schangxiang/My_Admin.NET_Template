﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Admin.NET.Core
{
    /// <summary>
    /// 托盘库位关系表
    /// </summary>
    [Table("wms_containerplace")]
    [Comment("托盘库位关系表")]
    public class WmsContainerPlace : DEntityBase
    {
        /// <summary>
        /// 库位Id
        /// </summary>
        [Comment("库位Id")]
        [Required]
        public long PlaceId { get; set; }

        /// <summary>
        /// 库位编码
        /// </summary>
        [Comment("库位编码")]
        [Required]
        [MaxLength(50)]
        public string PlaceCode { get; set; }

        /// <summary>
        /// 托盘Id
        /// </summary>
        [Comment("托盘Id")]
        [Required]
        public long ContainerId { get; set; }

        /// <summary>
        /// 库位编码
        /// </summary>
        [Comment("库位编码")]
        [Required]
        [MaxLength(50)]
        public string ContainerCode { get; set; }

        /// <summary>
        /// 状态;数据字典
        /// </summary>
        [Comment("状态")]
        [Required]
        public CommonStatus ContainerPlaceStatus { get; set; }
    }
}
