﻿namespace Admin.NET.Application
{
    /// <summary>
    /// 数据库表列
    /// </summary>
    public class TableColumnOuput
    {
        /// <summary>
        /// 字段名
        /// </summary>
        public string ColumnName { get; set; }

        /// <summary>
        /// 是否可为NULL 【Editby shaocx,2024-04-20】
        /// </summary>
        public bool IsNullable { get; set; }

        /// <summary>
        /// 数据库中类型
        /// </summary>
        public string DataType { get; set; }

        /// <summary>
        /// .NET字段类型
        /// </summary>
        public string NetType { get; set; }

        /// <summary>
        /// 字段描述
        /// </summary>
        public string ColumnComment { get; set; }

        /// <summary>
        /// 主外键
        /// </summary>
        public string ColumnKey { get; set; }


        /// <summary>
        /// 页面列显示最小宽度
        /// </summary>
        public string ShowTitleMinWidth { get; set; }
    }
}